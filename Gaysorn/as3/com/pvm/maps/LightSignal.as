﻿package com.pvm.maps {
	
	import flash.display.Shape;
	import com.greensock.TweenMax;
	import flash.events.Event;
	
	public class LightSignal extends Shape {

		private var iamDoneEvent:Event = new Event("iamDone");
		
		public function LightSignal() {
			// constructor code
			this.graphics.beginFill(0xffffff, 1);
			this.graphics.drawCircle(0, 0, 3);
			//this.graphics.drawRect(-2, -2, 4, 4);
			this.graphics.endFill();
			
		}
		
		public function slowlyFadeOut():void
		{
			TweenMax.to(this, 2, {alpha:0, onComplete:destroyMe});
		}
		
		public function destroyMe():void
		{
			dispatchEvent(iamDoneEvent);
		}

	}
	
}
