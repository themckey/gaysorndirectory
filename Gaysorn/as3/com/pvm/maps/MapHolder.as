﻿package com.pvm.maps {
	
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.events.Event;
	
	import com.greensock.TweenLite;
	
	import com.pvm.maps.pins.Pin;
	import com.pvm.maps.pins.LinkLine;
	
	public class MapHolder extends Sprite {

		private var _map:Map;
		
		private var _viewport:Viewport;
		
		public var start_node:Node;
		public var end_node:Node;
		

		public var youarehereMC:Pin = new Pin();
		public var endOfLineMC:Pin = new Pin();
		
		public var pinMap:Sprite = new Sprite();
		public var pinArray:Array = new Array();
		
		//-------- Temp use -------------
		private var point:Point = new Point();
		
		private var on3D:Boolean = true;
		private var showLandmark:Boolean = true;
		
		public var isOpen:Boolean = true;
		
		//Use for linking floor
		public var linkLine:LinkLine = new LinkLine();
		public var linkNode:Node;
		public var orglinkPoint:Point = new Point();
		public var linkPoint:Point = new Point();
		
		public var floorkey:String;
		
		private var _mapRotation:int = 0;
		
		
		public function MapHolder(f:String) {
		
			this.floorkey = f;
		
		/*
			var circle1:Sprite = new Sprite();
			circle1.x = 0;
			circle1.y = 0;
			circle1.graphics.beginFill(0x000000, 1);
			circle1.graphics.drawCircle(-7.5,-7.5,15);
			circle1.graphics.endFill();
			youarehereMC.addChild(circle1);
			
			
			//set EndOfLine mc
			var circle2:Sprite = new Sprite();
			circle2.x = 0;
			circle2.y = 0;
			circle2.graphics.beginFill(0x000000, 1);
			circle2.graphics.drawCircle(-7.5,-7.5,15);
			circle2.graphics.endFill();
			endOfLineMC.addChild(circle2);
			*/
			
			
		}
		
		var _board:Sprite = new Sprite();
		
		public function set map(theMap:Map):void
		{
			this._map = theMap;
			addChild(_board);
			
			_board.addChild(_map);
			
			theMap.x = 0;
			theMap.y = 0;
			
			
			//addChild(linkLine);
			//pinArray.push(linkLine);
	
			pinMap.x = 0;
			pinMap.y = 0;
			addChild(pinMap);
			
		}
		
		public function get map():Map
		{
			return this._map;
		}
		
		
		public function set mapRotation(theRotation:int):void {
			
			this._map.rotationX = theRotation;
		}
		
		
		
		public function addPin(mc:Sprite, posX:Number = 0, posY:Number = 0, SHOW_OPTION:int = 0):void
		{
			//trace("MapHolder addPin mc="+mc + " x,y = " + posX + ", " + posY);
			var pin:Pin = new Pin(posX, posY);
			pin.setMC(mc);
			pinMap.addChild(pin);
			pinArray.push(pin);
			
			reposition(pin);
			
		}
		
		public function addBitmapPin(url:String, posX:Number = 0, posY:Number = 0, SHOW_OPTION:int = 0, _h:int = 48):void
		{
			//trace("addBitmapPin url = " + url + " SHOW_OPTION = " + SHOW_OPTION);
			var pin:Pin = new Pin(posX, posY);
			
			//trace("addBitmapPIN _h = " + _h);
			if (SHOW_OPTION == 1) { 
				if (_h == 48)
				{
					pin.setBG(48,48);
					pin.setBitmap(url, 48, 48);
				} else {
					
					pin.setBG(_h,_h);
					pin.setBitmap(url, _h, _h);
				}
				
			} else if (SHOW_OPTION == 2) { 
				pin.setBitmap(url, 178, 108);
			} else {
				pin.setBitmap(url);
			}
			//pinMap.addChild(pin);
			pinMap.addChild(pin);
			pinArray.push(pin);
			
			//trace(floorkey + " addBitmapPin pinArray.length = " + pinArray.length);
			reposition(pin);
			
		}
		
		public function addMovieClipPin(mc:DisplayObject, posX:Number = 0, posY:Number = 0, SHOW_OPTION:int = 0):void
		{
			//trace("addMovieClipPin mc = " + mc + " SHOW_OPTION = " + SHOW_OPTION);
			var pin:Pin = new Pin(posX, posY);
			pin.addChild(mc);
			
			//pinMap.addChild(pin);
			pinMap.addChild(pin);
			pinArray.push(pin);
			
			//trace(floorkey + " addBitmapPin pinArray.length = " + pinArray.length);
			reposition(pin);
			
		}
		
		public function showFloor():void
		{
			
			
		}
		
		public function unShowFloor():void
		{
			
		}
		
		
		
		
		public function setEndOfLineMC(node:Node):void
		{
			
			if (node != null)
			{
				//endOfLineMC.posX = node.x;
				//endOfLineMC.posY = node.y;
				endOfLineMC.orgX = node.x;
				endOfLineMC.orgY = node.y;
				//endOfLineMC.showMe();
				
				reposition(endOfLineMC);
		
			}
		}
		
		/*
		public function setYouAreHerePin(node_id:int):void
		{
			//trace("map = " + map);
			//trace("node_id = " + node_id);
		
			var startNode:Node = map.getNode(node_id);
			trace("StartNode = " + startNode);
			
			if (startNode != null)
			{
				map.startNode = startNode;
			
				point.x = startNode.x;
				point.y = startNode.y;
				youarehereMC.set_pos(startNode.x, startNode.y);
		
				point = localToLocal(map,this,point);
				addChild(youareherePin);
		
				youarehereMC.showMe();
				youarehereMC.x = point.x;
				youarehereMC.y = point.y;
			
			//trace("youarehere x,y = " + youareherePin.x + ", " + youareherePin.y);
			}
			
		}*/
		
		public function repositionItems():void
		{
			
			for each (var pin in pinArray)
			{
				reposition(pin);
			}
			
			linkPoint = getPos(orglinkPoint);
	
			//trace("Link orgX,Y = X,Y = " + linkPoint.x + ", " + linkPoint.y);
	
		}
		
		public function getPos(point:Point):Point
		{
			point = localToLocal(_map,this,point);
			return point;
		}
		
		
		public function setLinkPoint(linkNode:Node):void
		{
			//trace("setLinkPoint = " + linkNode.x + ", " + linkNode.y);
			if (linkNode != null)
			{
				orglinkPoint.x = linkNode.x;
				orglinkPoint.y = linkNode.y;
				
				/*
				var circle:Sprite = new Sprite();
				circle.x = linkNode.x;
				circle.y = linkNode.y;
				circle.graphics.beginFill(0x00ffff, 1);
				circle.graphics.drawEllipse(-10, -5, 20, 10);
				circle.graphics.endFill();
				addChild(circle);
				*/
			}
			//trace("setLinkPoint link_node_id = " + link_node_id);
			//trace("linkNode x,y = " + linkNode.x + ", " + linkNode.y);
			
		}
		
		/*
		public function setLinkLine(node_id:String):void
		{
			var linkNode = _map.getNode(node_id);
			
			if (linkNode != null)
			{
				//trace("linkNode = " + linkNode);
				point.x = linkNode.x;
				point.y = linkNode.y;
				point = localToLocal(_map,this,point);
				linkLine.set_pos(linkNode.x, linkNode.y);
				linkLine.showMe();
				linkLine.x = point.x;
				linkLine.y = point.y;
				
				
				linkLine.visible = true;
				trace("SetLinkLine : floor_id = " + floorkey + " : node_id = " + node_id + " : x,y = " + linkLine.x + ", " + linkLine.y);
			}
			
		}*/
		
		public function setLinkLine(linkNode:Node, linkLine:LinkLine):void
		{
			if (linkNode != null)
			{
				//trace("linkNode = " + linkNode);
				point.x = linkNode.x;
				point.y = linkNode.y;
				point = localToLocal(_map,this,point);
				linkLine.set_pos(linkNode.x, linkNode.y);
				pinMap.addChild(linkLine);
				linkLine.showMe();
				linkLine.x = point.x;
				linkLine.y = point.y;
				
				
				linkLine.visible = true;
				//trace("SetLinkLine : floor_id = " + floorkey + " : node_id = " + linkNode.id + " : x,y = " + linkLine.x + ", " + linkLine.y);
			}
			
		
		}
		
		public static function localToLocal(containerFrom:DisplayObject, containerTo:DisplayObject, origin:Point=null):Point
		{
			var point:Point = origin ? origin : new Point();
			//trace("containerFrom = " + containerFrom);
			//trace("containerTo = " + containerTo);
			point = containerFrom.localToGlobal(point);
			point = containerTo.globalToLocal(point);
			return point;
		}
	
		public function reposition(mc:Pin):void
		{
			point.x = mc.orgX;
			point.y = mc.orgY;
			point = localToLocal(_map,this,point);
			
			mc.x = point.x;
			mc.y = point.y;
			//trace("after reposition : point = " + point);
		}
	
		public function resetValue():void
		{
			//map.dehilightShop();
			_map.clearLine();
			//endOfLineMC.hideMe();
	
	
		}
		
		
		
		public function showMe(evt:Event = null):void
		{
			this.visible = true;
			TweenLite.to(this, 0.5, {alpha:1});
			isOpen = true;
		}
		
		public function hideMe(evt:Event = null):void
		{
			//trace("MapMC.hideMe()");
			TweenLite.to(this, 0.3, {alpha:0, onComplete:devisibleMe});
			
			isOpen = false;
		}
		
		private function devisibleMe()
		{
			this.visible = false;
			
		}

	}
	
}
