﻿package com.pvm.maps {
	
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.display.DisplayObject;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	
	import flash.utils.getQualifiedClassName;
	import com.pvm.ui.BitmapHolder;
	import com.pvm.ui.UITooltip;
	import flash.utils.Dictionary;
	
	public class Marker extends Sprite {
		
		private var _position:Point = new Point(0,0);
		private var _animationIn:String = "drop";
		private var _animationOut:String = "none";
		private var _permanent:Boolean = false;
		private var _alwaysShow:Boolean = true;
		
		private var _titleDict:Dictionary;
		
		private var _icon:Sprite = new Sprite();
		private var _shadowFilter:DropShadowFilter = new DropShadowFilter(3, 90, 0x000000, 0.5, 2, 2);
		private var _title:String = '';
		private var _groupKey:String =  '';
		
		private var _scale:Number;
		
		private var _state:String = "normal";
		
		private var _infoWindow:InfoWindow;
		private var _tooltip:UITooltip;
		private var _showTooltip:String = "highlight";
		
		private var _animateOptions:Object;
		
		var _hasIcon:Boolean = false;
			
		private var _lang:String = "en";
		
		public function Marker(options:Object = null) {
			
			
			
			if (options) {
				
				if (options.hasOwnProperty('title')) {
					_title = options['title'];
					
					_tooltip = createTooltip({title : _title});
				}
				
				if (options.hasOwnProperty('showTooltip')) {
					_showTooltip = options['showTooltip'];
				}
				
				if (options.hasOwnProperty('alwaysShow')) {
					this._alwaysShow = options['alwaysShow'];
				}
				
				if (options.hasOwnProperty('icon')) {
					switch (getQualifiedClassName(options['icon'])) {
						
						case 'String' : 
							
							var iconHolder:BitmapHolder = new BitmapHolder();
							iconHolder.setup(40, 40);
							iconHolder.loadImage(options['icon']);
							iconHolder.x = -20;
							iconHolder.y = -20;
							_icon.addChild(iconHolder);
							
							_hasIcon = true;
							break;
							
					}
					
				}
				
				if (options.hasOwnProperty('graphics')) {
					if (options.graphics.hasOwnProperty("stage"))
					{
						this.addChild(options.graphics);
						_hasIcon = true;
					}
				}
				
				if (options.hasOwnProperty('rotation')) {
					
					_icon.rotation = options['rotation'];
				}
				
				_animationIn = (options.hasOwnProperty('animationIn')) ? options['animationIn'] : 'none';
				_animationOut = (options.hasOwnProperty('animationOut')) ? options['animationOut'] : 'none' ;
				
				_permanent = (options.hasOwnProperty('permanent')) ? options['permanent'] : false;
				
				
				configureState();
			}
			
			
			if (!_hasIcon) {
				_icon.graphics.beginFill(0x9e0b0b, 1);
				_icon.graphics.drawCircle(0, 0, 10);
				
				_icon.graphics.beginFill(0x000000, 1);
				_icon.graphics.drawCircle(0, 0, 5);
				
				_icon.graphics.endFill();
				
			}
			
			addChild(_icon);
			_icon.filters = [_shadowFilter];
			
			
			
		}
		
		public function createTooltip(options:Object = null):UITooltip
		{
			
			_tooltip = new UITooltip(options);
			_tooltip.element = this;
			_tooltip.x = 0;
			_tooltip.y = -10;
			
			return _tooltip;
		}
		
		public function set position(thePosition:Point):void
		{
			this._position = thePosition;
		}
		
		public function get position():Point
		{
			return this._position;
		}
		
		public function set icon (theIcon:DisplayObject):void {
			
			_icon.graphics.clear();
			_icon = Sprite(theIcon);
		}
		
		public function set title(theTitle:String):void
		{
			this._title = theTitle;
			if (!_titleDict) _titleDict = new Dictionary();
			_titleDict[this.lang] = theTitle;
		}
		
		public function set scale(theScale:Number):void
		{
			this._scale = theScale;
			this.scaleX = this.scaleY = _scale;
		}
		
		public function get permanent():Boolean 
		{
			return this._permanent;
		}
		
		public function set infoWindow(theInfoWindow:InfoWindow):void
		{
			this._infoWindow = theInfoWindow;
			
			theInfoWindow.y = -10;
			this.addChild(theInfoWindow);
		}
		
		public function set tooltip(theTooltip:UITooltip):void
		{
			this._tooltip = theTooltip;
			theTooltip.element = this;
			theTooltip.x = 0;
			theTooltip.y = -10;
			
			configureState();
		}
		
		public function set state(theState:String):void
		{
			if (this._state != theState) {
				this._state = theState
				configureState();
			}
		}
		
		private function configureState():void
		{
			switch (this._state) {
				case 'normal' : 
					if (!_alwaysShow) this.hide();
					if (_tooltip && _showTooltip != 'always') _tooltip.hide();
					break;
					
				case 'highlight' : 
				case 'active' : 
					this.show();
					if (_tooltip) _tooltip.show();
					break;
			}
		}
		
		
		public function animateIn(options:Object = null):void
		{
			
			//Hide Myself & InfoWindow
			if (_infoWindow) _infoWindow.hide();
			if (_tooltip) _tooltip.hide();
			
			_animateOptions = {};
			var _animationType:String = _animationIn;
			
			if (options) {
				if (options.hasOwnProperty('delay')) _animateOptions.delay = options['delay'];
				if (options.hasOwnProperty('animate') && options['animate'] == false) _animationType = 'none';
			}
			
			switch (_animationType) {
				case 'none' : 
					this.x = _position.x;
					this.y = _position.y;
					onAnimateInCompleteHandler();
					break;
				case 'drop' : 
					this.visible = true;
					this.x = _position.x;
					this.alpha = 0;
					this.y = 100;
					
					_animateOptions.alpha = 1;
					_animateOptions.y = this._position.y;
					_animateOptions.ease = Expo.easeOut;
					_animateOptions.onComplete = onAnimateInCompleteHandler;
					
					TweenMax.to(this, 0.9, _animateOptions);
					break;
			}
		}
		
		public function animateOut():void
		{
			
			//Hide Myself & InfoWindow
			if (_infoWindow) _infoWindow.hide();
			
			switch (_animationOut) {
				case 'none' :
					this.visible = false;
					break;
			}
			
		}
		
		public function show():void
		{
			this.alpha = 3;
			this.visible = true;
			
		}
		
		public function hide():void
		{
			this.alpha = 1;
			this.visible = false;
		}
		
		
		
		private function onAnimateInCompleteHandler():void
		{
			//if (_infoWindow) _infoWindow.show();
			if (_tooltip && _showTooltip == 'always') _tooltip.show();
		}
		
		private function randomBetween(min:Number, max:Number):Number
		{
			return min + Math.random() * (max - min);
		}
		
		public function set lang(value:String):void
		{
			this._lang = value;
			if (_titleDict && _titleDict[this.lang] ) this.title = _titleDict[this.lang];
			if (_tooltip) _tooltip.lang = _lang;
			
		}
		
		public function get lang():String
		{
			return this._lang;
		}
		public function translate():String
		{
			_title.text = this.lang;
		}


	}
	
}
