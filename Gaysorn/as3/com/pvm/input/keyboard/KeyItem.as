﻿package com.pvm.input.keyboard {
	
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	
	import flash.utils.getQualifiedClassName;
	import flash.display.MovieClip;

	
	public class KeyItem extends MovieClip {
		
		private var _default_lang:String = "en";
		
		private var _shift:Boolean = false;
		private var _inputLang:String = 'en';
		
		public var keyID:String;
		private var _keyValue:String = "";
		
		private var _keyDict:Dictionary = new Dictionary();
		
		private var _valueDict:Dictionary = new Dictionary();
		private var _textFormatDict:Dictionary;
		
		private var primaryTF:TextField = new TextField();
		private var shiftTF:TextField = new TextField();
		private var _labelTF:TextField;
				
		
		private var _valueObj:Object;
		
		private var _primaryValue:String;
		private var _shiftValue:String;
		
		private var _default_bg:Shape;
		private var _touchdown_bg:Shape;
		private var _active_bg:Shape;
		
		private var _strokeColor:Number = 0x000000;
		private var _strokeAlpha:Number = 0;
		private var _fillColor:Number = 0xCCCCCC;
		private var _fillAlpha:Number = 1;
		
		private var _strokeWidth:Number = 1;
		
		private var _textColor:uint = 0x000000;
		private var _textAlpha:Number = 1;
		
		private var _keyWidth:int = 70;
		private var _keyHeight:int = 70;
		private var _keyRedius:int = 12;
		private var _keyOffsetX:int = 0;
		
		
		private var _shiftTF_scale:Number = 0.4;
		
		
		

		public function KeyItem(valueObj:Object, options:Object = null) {
			
			this.mouseChildren = false;
			this.buttonMode = true;
			
			
			
			
			_valueObj = valueObj;
			if (_valueObj.width) this._keyWidth = _valueObj.width;
			if (_valueObj.height) this._keyHeight = _valueObj.height;
			if (_valueObj.offsetX) this._keyOffsetX = _valueObj.offsetX;
			
			if (options) {
				
				if (options.hasOwnProperty('keyColor')) this._fillColor = options.keyColor;
				if (options.hasOwnProperty('fillAlpha')) this._fillAlpha = options.fillAlpha;
				
				if (!options.hasOwnProperty('template')) {
					_default_bg = new Shape();
					this.drawBackground();
				}
				
				
				if (options.hasOwnProperty('textFormatDict')) {
					this._textFormatDict = options['textFormatDict'];
				} else {
					initTextFormat();
				}
			}
			
			if( !_default_bg) this.drawBackground();
			
			primaryTF.width = this._keyWidth;
			primaryTF.height = this._keyHeight;
			primaryTF.embedFonts = false;
			primaryTF.y = Math.round((primaryTF.height - primaryTF.textHeight) / 2);
					
			shiftTF.x = (0.8-_shiftTF_scale) * this._keyWidth;
			shiftTF.embedFonts = false;
			shiftTF.scaleX = shiftTF.scaleY = _shiftTF_scale;
					
			
			if (_valueObj.key) {
				if (_valueObj.label) createKeyLabel(_valueObj.key, _valueObj.label);
				else createKeyLabel(_valueObj.key);
				
			} else {
				//Alphabet Key
				for (var lang:String in valueObj)
				{
					if (lang.length == 2) {
						
						var key:String = lang.toString();
						
						this._keyDict[key] = new Dictionary();
						this._keyDict[key]['value'] = new Array();
						
						if (getQualifiedClassName(valueObj[key]) == "Array") {
							this._keyDict[key]['value'][0] = (valueObj[key][0]) ? valueObj[key][0] : '';
							this._keyDict[key]['value'][1] = (valueObj[key][1]) ? valueObj[key][1] : '';
							this._valueDict[key] = new Array();
						
							this._valueDict[key][0] = (valueObj[key][0]) ? valueObj[key][0] : '';
							this._valueDict[key][1] = (valueObj[key][1]) ? valueObj[key][1] : '';
						
						} else {
							this._keyDict[key]['value'] = valueObj[key];
							this._valueDict[key] = valueObj[key];
						}
						
						
						//trace(lang + " : " + _keyDict[lang]);
						
					}
				
				}
				/*
				for (var k:Object in _keyDict) {
				  var value:*=_keyDict[k];
				  var key:String=k;
				 // do stuff	
				 //trace("check " + k + " : " + _keyDict[k]);
				}
				*/
					
				this.createLang(this._default_lang);
				
				
				//this.keyValue = valueObj[_default_lang][0];
			}
			
			
			
			this.addEventListener("mouseDown", onTouchDownHandler);
		}
		
		
		
		
		private function initTextFormat():void
		{
			var _formatT:TextFormat = new TextFormat( );
			_formatT.bold = false; 
			_formatT.color = _textColor; 
			_formatT.align = "center";
			_formatT.font = "Arial";    
			_formatT.size = 26;
			
			
			
			var _keyLabelFormatT:TextFormat = new TextFormat( );
			_keyLabelFormatT.bold = false; 
			_keyLabelFormatT.color = _textColor; 
			_keyLabelFormatT.align = "right";
			_keyLabelFormatT.font = "Arial";    
			_keyLabelFormatT.size = 12;
			
			this._textFormatDict['en'] = _formatT;
			this._textFormatDict['th'] = _formatT;
			//this._textFormatDict['label'] = _keyLabelFormatT;
			
		}
		
		
		public function drawBackground(theme:String = 'default') {
			
			switch (theme) {
				case 'primary' : 
					_default_bg.graphics.clear();
					_default_bg.graphics.beginFill(_fillColor, _fillAlpha);
      				_default_bg.graphics.lineStyle(_strokeWidth, _strokeColor, _strokeAlpha, true);
      				_default_bg.graphics.drawRoundRect(0,0,_keyWidth,_keyHeight,_keyRedius);
					_default_bg.graphics.endFill();
					addChildAt(_default_bg, 0);
					break;
					
				default :
					_default_bg.graphics.clear();
					_default_bg.graphics.beginFill(_fillColor, _fillAlpha);
      				_default_bg.graphics.lineStyle(_strokeWidth, _strokeColor, _strokeAlpha, true);
      				_default_bg.graphics.drawRoundRect(0,0,_keyWidth,_keyHeight,_keyRedius);
					_default_bg.graphics.endFill();
				//	addChildAt(_default_bg, 0);
					break;
			}
		}
		
		public function createKeyLabel(key:String, _label:String = ""):void
		{
			var keyLabel:String  = '';
			this.keyID = key;
			
			if (_label) keyLabel = _label
			else keyLabel = key;
			
			switch (key) {
				case 'space' : this._keyValue = ' '; keyLabel = ' '; break;
				case 'switch_lang' : keyLabel = 'Switch Lang'; break;
				
			}
			
			_labelTF = new TextField();
			_labelTF.width = this._keyWidth;
			_labelTF.x = 0;
			_labelTF.text = keyLabel.toUpperCase();
			_labelTF.embedFonts = false;
			_labelTF.setTextFormat(_textFormatDict['label']);
			addChild(_labelTF);
			_labelTF.y = this._keyHeight - _labelTF.textHeight - 4; 
			
			
		}
		
		public function createLang(lang:String):void
		{
			
			if (getQualifiedClassName(_keyDict[lang]['value']) == "Array") {
				
				if (_keyDict[lang]['value'][0]) {
					primaryTF.text = _keyDict[lang]['value'][0];
					primaryTF.setTextFormat(_textFormatDict[lang]);
					
					//primaryTF.width = this._keyWidth;
					//primaryTF.height = this._keyHeight;
					//primaryTF.y = Math.round((primaryTF.height - primaryTF.textHeight) / 2);
					addChild(primaryTF);
					
				}
				
				if (_keyDict[lang]['value'][1]) {
					
					//shiftTF.x = (0.8-_shiftTF_scale) * this._keyWidth;
					//shiftTF.scaleX = shiftTF.scaleY = _shiftTF_scale;
					shiftTF.text = _keyDict[lang]['value'][1];
					shiftTF.setTextFormat(_textFormatDict[lang]);
					addChild(shiftTF);
					
				}
				
				this._keyValue = primaryTF.text;
			
			} else {
				//String
				primaryTF.width = this._keyWidth;
				primaryTF.height = this._keyHeight;
				primaryTF.text = _keyDict[lang]['value'];
				primaryTF.setTextFormat(_textFormatDict[lang]);
					
				primaryTF.y = Math.round((primaryTF.height - primaryTF.textHeight) / 2);
				addChild(primaryTF);
				
				this._keyValue = primaryTF.text;
					
			}
		}
		
		protected function onTouchDownHandler(e:Event):void
		{
			this.alpha = 0.5;
			this.stage.addEventListener("mouseUp", onTouchUpHandler, false, 0, true);
		}
		
		protected function onTouchUpHandler(e:Event):void
		{
			this.alpha = 1.0;
			this.stage.removeEventListener("mouseUp", onTouchUpHandler);
			
			
		}
		
		
		/* 
		 *Getter  Setter
 		 *************************/
		public function get primaryTextField():TextField
		{
			return this.primaryTF;
		}
		
		public function get shiftTextField():TextField
		{
			return this.shiftTF;
		}
		
		public function get labelTextField():TextField
		{
			return this._labelTF;
		}
		 
		public function get keyWidth():Number
		{
			return this._keyWidth;
		}
		
		public function get keyHeight():Number
		{
			return this._keyHeight;
		}
		
		public function get offsetX():Number
		{
			return this._keyOffsetX;
		}
		
		public function set backgroundColor(theColor:uint):void
		{
			this._fillColor = theColor;
			drawBackground();
		}
		
		public function set backgroundAlpha(theAlpha:Number):void
		{
			this._fillAlpha = theAlpha;
			drawBackground();
		}
		
		public function set strokeColor(theColor:uint):void
		{
			this._strokeColor = theColor;
			drawBackground();
		}
		
		public function set strokeWidth(value:Number):void
		{
			this._strokeWidth = value;
		}
		
		public function set textColor(theColor:uint):void
		{
			
			this._textColor = theColor;
			_textFormatDict['en'].color = theColor;
			_textFormatDict['th'].color = theColor;
			_textFormatDict['label'].color = theColor;
			
			
			primaryTF.setTextFormat(_textFormatDict['en']);
			shiftTF.setTextFormat(_textFormatDict['en']);
			if (_labelTF) _labelTF.setTextFormat(_textFormatDict['label']);
			
		}
		
		public function get textColor():uint
		{
			return this._textColor;
		}
		
		public function set textFormat(tf:TextFormat):void
		{
			
			_textFormatDict['en'] = tf;
			_textFormatDict['th'] = tf;
			_textFormatDict['label'] = tf;
			
			primaryTF.setTextFormat(_textFormatDict['en']);
			shiftTF.setTextFormat(_textFormatDict['en']);
			if (_labelTF) _labelTF.setTextFormat(_textFormatDict['label']);
		}
		
		public function set textAlpha(theAlpha:Number):void
		{
			this._textAlpha = theAlpha;
			primaryTF.alpha = theAlpha;
			shiftTF.alpha = theAlpha;
		}
		
		
		public function set shift(theValue:Boolean):void
		{
			if (this._shift != theValue) {
				this._shift = theValue;
				configureShift();
			}
			
		}
		
		public function get shift():Boolean {
			return this._shift;
		}
		
		public function set inputLang(lang:String):void
		{
			if (this._inputLang != lang) {
				this._inputLang = lang;
				configureInputLang();
			}
		}
		
		
		public function get keyValue():String
		{
			return this._keyValue;
		}
		
		public function set label(theLabel:String):void
		{
			this._labelTF.text = theLabel;
			_labelTF.setTextFormat(_textFormatDict['label']);
			
		}
		
		
		
		private function configureInputLang():void
		{
			
			if (_keyDict[this._inputLang]) {
				
				if (_keyDict[this._inputLang]['value']) {
					///trace(_keyDict[this._inputLang]['value']);
			
					primaryTF.text = _keyDict[this._inputLang]['value'][0];
					primaryTF.setTextFormat(_textFormatDict[this._inputLang]);
				
					shiftTF.text = _keyDict[this._inputLang]['value'][1];
					shiftTF.setTextFormat(_textFormatDict[this._inputLang]);
					
					this._keyValue = primaryTF.text;
				}
			
			}
		}
		
		
		private function configureShift():void
		{
			this.gotoAndStop(2);
			if (_keyDict[this._inputLang]) {
				if (_keyDict[this._inputLang]['value']) {
					
					if (getQualifiedClassName(_keyDict[this._inputLang]['value']) == "Array") {
						this.state = "normal";
						primaryTF.text = (this._shift) ? _keyDict[this._inputLang]['value'][1] : _keyDict[this._inputLang]['value'][0];
						primaryTF.setTextFormat(_textFormatDict[this._inputLang]);
						
						shiftTF.text = (this._shift) ? _keyDict[this._inputLang]['value'][0] : _keyDict[this._inputLang]['value'][1];
						shiftTF.setTextFormat(_textFormatDict[this._inputLang]);
					
						this._keyValue = primaryTF.text;
					} else {
						this.state = "disable";
						
					}
				}
			}
		}
		
		public function set state(theValue:String):void
		{
			switch (theValue) {
				case 'normal' :
					this.alpha = 1;
					break;
				case 'disable' : 
					this.alpha = 0.2;
					break;
			}
		}
		
		
		
		

	}
	
}
