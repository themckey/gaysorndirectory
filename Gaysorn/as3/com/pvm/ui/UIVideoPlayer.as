﻿/* 
 * UIVideoPlayer version 0.5
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.pvm.ui {
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import flash.events.AsyncErrorEvent;
	import flash.events.NetStatusEvent;
	
	import com.pvm.ui.UIView;


	public class UIVideoPlayer extends UIView {

		private var _frame:Rectangle;
		
		private var _videoControlMC:Sprite;
		/* Options */
		private var _controls:Array = ["rew", "play", "stop", "fwd"];
		private var _repeat:int  = 1;
		
		
		
		/* Video object */
		private var _videoSrc:String;
		private var _video:Video;
		private var _nc:NetConnection;
		private var _ns:NetStream;
		
		
		
		private var _newMeta:Object;
		
		public function UIVideoPlayer(frame:Rectangle, options:Object = null) {
			
			this._frame = frame;
			
			super(frame);
			init();
		}
		
		public function init():void
		{
			_video = new Video(_frame.width, _frame.height);  
			_video.x = 0;  
			_video.y = 0;  
			this.addChild(_video);  
			_video.smoothing = true;
			
			_nc = new NetConnection();  
			_nc.connect(null);  
			
			_ns = new NetStream(_nc);  
			_ns.addEventListener(NetStatusEvent.NET_STATUS, myStatusHandler, false, 0, true);  
			_ns.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler, false, 0, true);  
			
			_video.attachNetStream(_ns);  
			_newMeta = new Object();  
			_newMeta.onMetaData = onMetaData;  
			_ns.client = _newMeta;  
			_ns.bufferTime = 5; 
			
		}
		
		public function unload():void
		{
			_ns.close();
			
			_nc.close();
			
			_video.clear();
		}
		
		
		public function get ns():NetStream
		{
			return this._ns;
		}
		
		public function play():void
		{
			_ns.play(_videoSrc);
		}
		
		public function pause():void
		{
			_ns.pause();
		}
		
		public function resume():void
		{
			_ns.resume();
		}
		
		public function stop():void
		{
			_ns.pause();
			_ns.seek(4);
		}
		
		
		public function set repeat(value:int):void
		{
			this._repeat = value;
		}
		
		
		public function set src(theSrc:String):void
		{
			this._videoSrc = theSrc;
		}
		
		public function set controls(arr:Array):void
		{
			this._controls = arr;
			configureControls();
		}
		
		private function configureControls():void
		{
			if (!_videoControlMC) _videoControlMC = new Sprite();
			
			while(_videoControlMC.numChildren) {
				_videoControlMC.removeChildAt(0);
			}
			
			for (var i:int=0;i<this._controls.length;i++) {
				switch (this._controls[i]) {
					
					case 'play' :
						
						break;
				}
			}
			
		}
		
		private function asyncErrorHandler(Event:AsyncErrorEvent):void  
		{  
			//trace(event.text);  
		}  
		
		private function myStatusHandler(event:NetStatusEvent):void  
		{  
			
			switch(event.info.code)  
			{  
				case "NetStream.Buffer.Full":  
					_ns.bufferTime = 10;  
					//Tweener.addTween(videoPreloader, {alpha:0, time:.3});  
				break;  
				case "NetStream.Buffer.Empty":  
					_ns.bufferTime = 10;  
					//Tweener.addTween(videoPreloader, {alpha:1, time:.3});  
				break;  
				case "NetStream.Play.Start":  
					_ns.bufferTime = 10;  
					//Tweener.addTween(videoPreloader, {alpha:1, time:.3});  
				break;  
				case "NetStream.Seek.Notify":  
					_ns.bufferTime = 10;  
					//Tweener.addTween(videoPreloader, {alpha:1, time:.3});  
				break;  
				case "NetStream.Seek.InvalidTime":  
					_ns.bufferTime = 10;  
					//Tweener.addTween(videoPreloader, {alpha:1, time:.3});  
				break;  
				case "NetStream.Play.Stop":  
					//Tweener.addTween(videoPreloader, {alpha:0, time:.3});  
					
					if (_repeat == -1) {
						_ns.play(_videoSrc);
					} else {
						_ns.pause();  
						//_ns.seek(1);  						
					}
				break;  
			}  
			
		}  
		
		private function onMetaData(newMeta:Object):void  
		{  
			//trace("Metadata: duration=" + newMeta.duration + " width=" + newMeta.width + " height=" + newMeta.height + " framerate=" + newMeta.framerate);  
			duration = newMeta.duration;  
		}  
		
		override protected function onConfiguredFrame(theFrame:Rectangle):void
		{
			resizeVideo();
		}
		
		public function resizeVideo():void
		{
			if (_video) {
				_video.width = this.frame.width;
				_video.height = this.frame.height;
			}
		}
	}
	
}
