﻿package com.pvm.ui {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class InteractiveItem extends MovieClip {
		
		public var isActive:Boolean;
		public var holdActive:Boolean = false;
		
		public var id:int;
		public var stat_item_name:String;
		
		public function InteractiveItem() {
			// constructor code
			isActive = false;
			mouseChildren = true;
			
			this.addEventListener("mouseDown", onMouseDownHandler);
			
		}
		
		private function onMouseDownHandler(evt:Event):void
		{
			this.setDownState();
			this.stage.addEventListener("mouseUp", onMouseUpHandler, false, 0, true);
			this.addEventListener("mouseOut", onMouseOutHandler, false, 0, true);
		}
		
		private function onMouseUpHandler(evt:Event):void
		{
			this.setDefaultState();
			this.stage.removeEventListener("mouseUp", onMouseUpHandler);
		}
		
		private function onMouseOutHandler(evt:Event):void
		{
			this.setDefaultState();
			this.removeEventListener("mouseOut", onMouseOutHandler);
		}
		
		
		public function setDefaultState():void
		{
			alpha = 1;
			
		}
		
		public function setDownState():void
		{
			alpha = 0.7;
			
		}
		
		public function setActiveState():void
		{
			alpha = 1;
		}
		
		public function setDisableState():void
		{
			alpha = 0.1;
			
		}

	}
	
}
