﻿/* 
 * UITooltip version 1.0
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.pvm.ui {
	
	import flash.display.Sprite;
	import flash.display.Shape;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	
	
	public class UITooltip extends Sprite {

		private var _shape:Shape;
		private var _arrowHead:Shape;
		
		private var _direction:String = "top";
		
		private var tf:TextFormat = new TextFormat('Miso', 16, 0xffffff);
		private var _title:TextField;
		private var _titleText:String; 
		
		
		var _margin:Number = 15;
		var _padding:Number = 20;
		
		private var _arrowWidth:Number = 20;
		private var _arrowHeight:Number = 10;

		private var _element:DisplayObject;
		
		private var _lang:String = "en";
		private var _titleDict:Dictionary;
		private var _textFormatDict:Dictionary;
		
		public function UITooltip(options:Object = null) {
			
			_title = new TextField();
			_title.embedFonts = true;
			_title.selectable = false;
			tf.align = 'center';
			
			_shape = new Shape();
			_arrowHead = new Shape();
					
			
			addChild(_shape);
			addChild(_arrowHead);
			addChild(_title);
			
			if (options) {
				
				//if (options.hasOwnProperty('textFormat')) _tf = options.textFormat;
				if (options.hasOwnProperty('direction')) _direction = options['direction'];

				if (options.hasOwnProperty('title')) {
					this.title = options.title;
					
				}
			}
			
		}
		
		
		protected function createBackground():void {
			
			//_title.setTextFormat(tf);
					
			this._title.x = -0.5 * _title.textWidth;
			this._title.width = _title.textWidth + 0.4 * _padding;
				
			switch (_direction) {

					case 'top' : 
						_shape.graphics.clear();
						_shape.graphics.beginFill(0x000000, 1);
						_shape.graphics.drawRect(this._title.x  - _padding, -50, this._title.textWidth + 2 * _padding, 40);
						_shape.graphics.endFill();
						
						_arrowHead.graphics.beginFill(0x000000);
						_arrowHead.graphics.moveTo(0, 0);
						_arrowHead.graphics.lineTo(-0.5*_arrowWidth, -_arrowHeight);
						_arrowHead.graphics.lineTo(0.5*_arrowWidth, -_arrowHeight);
						_arrowHead.graphics.moveTo(0, 0);
						
						
						//_title.x = -95;
						_title.y = -40;
						
						break;
					
					case 'bottom' : 
						_shape.graphics.clear();
						_shape.graphics.beginFill(0x000000, 1);
						_shape.graphics.drawRect(this._title.x  - _padding, 10 + _margin, this._title.textWidth + 2 * _padding, 40);
						_shape.graphics.endFill();
						
						_arrowHead.graphics.clear();
						_arrowHead.graphics.beginFill(0x000000);
						_arrowHead.graphics.moveTo(0, _margin);
						_arrowHead.graphics.lineTo(-0.5*_arrowWidth, _margin + _arrowHeight);
						_arrowHead.graphics.lineTo(0.5*_arrowWidth, _margin + _arrowHeight);
						_arrowHead.graphics.moveTo(0, _margin);
					
						_title.y = 20 + _margin;
					
						break;
					
					default: 
						break;
			}
			
		}
		
		public function set element(el:DisplayObject):void
		{
			this._element = el;
			this.x = 0.5 * el.width;
			this.y = 3;
			
			if (el is UIView) 
				(el as UIView).attach(this);
			else 
				el.addChild(this);
		}
		
		public function set title(value:String):void
		{
			_titleText = value;
			this._title.text = value;
			
			if (_textFormatDict && _textFormatDict[this.lang]) {
				_title.defaultTextFormat = _textFormatDict[this.lang];
				_title.setTextFormat(_textFormatDict[this.lang]);
			}else { 
				_title.setTextFormat(tf);
			}
			
			createBackground();
			
		}
		
		public function get titleDict():Dictionary
		{
			return _titleDict;
		}
		
		public function get textFormatDict():Dictionary
		{
			return _textFormatDict;
		}
		
		public function setTitleForLang(lang:String, _title:String, textFormat:TextFormat = null):void
		{
			if (textFormat) {
				if (!_textFormatDict) _textFormatDict = new Dictionary();
				_textFormatDict[lang] = textFormat;
			}
			
			setTextFormatForLang(lang, textFormat);
			
			
		}
		
		public function setTextFormatForLang(lang:String, textFormat:TextFormat):void
		{
			if (!_textFormatDict) _textFormatDict = new Dictionary();
			_textFormatDict[lang] = textFormat;
			
		}
		
		public function get titleLabel():TextField {
			return this._title;
		}
		
		public function get shape():Shape
		{
			return this._shape;
		}
		
		public function hide():void
		{
			//this.visible = false;
			TweenMax.to(this, 0.4, {autoAlpha:0, ease:Back.easeInOut});
		}
		
		public function show(txt:String = ""):void
		{
			
			if (txt != "") {
				this.title = txt;
			} else {
				this.title = _titleText;
			}
			
			
			
			this.visible = true;
			//this.scaleX = this.scaleY = this.alpha = 0;
			TweenMax.to(this, 0.4, {alpha:1, ease:Back.easeOut});
		}
		
		public function get lang():String { return this._lang; }
		public function set lang(value:String):void
		{
			this._lang = value;
			if (_titleDict) 
				this.title = _titleDict[this.lang];
			
			if (this._textFormatDict && _textFormatDict[this.lang]) {
				this._title.defaultTextFormat = this._textFormatDict[this.lang];
			}
			
		}
		public function translate():void
		{
			
			
		}

	}
	
}
