﻿package com.pvm.ui {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class UIButtonGroup extends EventDispatcher{

		public static const RADIO		:String = "radio";
		public static const CHECKBOX	:String = "checkbox";
		
		private var _type:String = "radio";
		private var _lang:String = "en";
		
		private var _btnArray:Array = new Array();
		
		private var stateChangedEvent:Event = new Event("stateChanged");
		
		public function UIButtonGroup() {
			// constructor code
		}
		
		public function set type(theType:String):void
		{
			if (this._type != theType) {
				this._type = theType;
				
				if (_type == "checkbox") {
					for (var i:int=0; i<_btnArray.length; i++)
					{
						_btnArray[i].toggle = true;
					}
				}
			}
		}
		
		public function addBtn(btn:UIButton):void
		{
			_btnArray.push(btn);
			
			btn.addEventListener("tap", onTapHandler);
		}
		
		public function setActive(btn:UIButton):void
		{
			if (_type == "radio") {
				for (var i:int=0; i<_btnArray.length; i++)
				{
					(btn == _btnArray[i])? _btnArray[i].state = "active" : _btnArray[i].state = "normal";
					_btnArray[i].configureState();
				}
				
				dispatchEvent(stateChangedEvent);
				
			} 
		}
		
		public function get btnArray():Array
		{
			return this._btnArray;
		}
		
		private function onTapHandler(evt:UIControlEvent):void
		{
			if (_type == "radio") setActive(evt.from as UIButton);
		}
		
		
		public function set lang(value:String):void
		{
			this._lang = value;
			for (var i:int=0; i<this._btnArray.length; i++) {
				_btnArray[i].lang = _lang;
			}
		}

	}
	
}
