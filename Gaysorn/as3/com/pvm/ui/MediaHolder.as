﻿package com.pvm.ui
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.Bitmap;
	
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.TimerEvent;
    import flash.utils.Timer;
	
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	import flash.net.URLRequest;
	import flash.display.Loader;
	
	
	
	public dynamic class MediaHolder extends Sprite
	{
		
		
		public static const 		FULL_FRAME : int = 0;
		public static const 		FULL_PICTURE : int = 1;
		
		private var 				crop_mode : int = FULL_PICTURE;
		
		public var holder:Sprite;
		
		public var holderW:Number = 100;
		public var holderH:Number = 80;
		
		public var contentW:Number = 100;
		public var contentH:Number = 80;
		
		private var orgX:Number = 0;
		private var orgY:Number = 0;
		
		private var scale:Number;
		private var media_ratio:Number;
		private var holder_ratio:Number;
		
		private var mediaURLReq:URLRequest = new URLRequest();
		private var mediaLdr:Loader = new Loader();
		
		public	var mediaArray:Array = new Array();
		public	var refArray:Array = new Array();
		
		var child:Sprite = new Sprite();
		var 	no:int = 0;
		var		p:int = 0;
		
		var video:Video = new Video();
		var nc:NetConnection = new NetConnection();
			 
		var ns:NetStream;
		var firstMedia:String
		var type:String;
			 
		public var isVideo:Boolean = false;
		
		public var currentMedia:int = 0;
		public var nextMedia:int = 1;
		public var timer:Timer = new Timer(1000, 5);  	//5 Seconds
		var fadeTime:Number = 1; 				//1 Seconds
		
		var thumbContent:DisplayObject;
		var bit:Bitmap;
		
		
		var path_images:String;
		
		
		public function MediaHolder()
		{
			holder = new Sprite();
			addChild(holder);
			
			mediaLdr.contentLoaderInfo.addEventListener(Event.COMPLETE, doneLoaded);
			
			
			
		}
		
		public function setSize(_w:Number, _h:Number):void
		{
			holderW = _w;
			holderH = _h;
			
		}
		
		private function onContentInit(evt:Event):void
		{
			
		}

//============================PRIVATE FUNCTIONS=================================================
		
		private function doneLoaded(evt:Event):void
		{
			/*
			while (child.numChildren > 0)
			{
				child.removeChildAt(0);
			}*/
			//child = new MovieClip();
			
			//this gets around an argument error within the loader class when using
			//a single global loader instance. There is a bug with the way the Loader class
			//checks for the existence of its content when running under Flash Player 9. This is
			//fixed in Flash Player 10.
			
			bit = evt.target.content;
             //correctly formed if statement
            if(bit != null){
            	bit.smoothing = true;
            }
			
			media_ratio = bit.width / bit.height;
			holder_ratio = holderW / holderH;
			
			
			if (media_ratio < holder_ratio)
			{
				//Black space on left-right
				scale = holderH / bit.height;
				contentW = scale * bit.width;
				bit.x = orgX + (holderW - contentW) / 2;
				bit.y = orgY;
				
			} else if (media_ratio > holder_ratio) {
				//Black space on top-bottom
				scale = holderW / bit.width;
				contentH = scale * bit.height;
				
				bit.y = orgY + (holderH - contentH) / 2;
				bit.x = orgX;
				
			} else {
				
				scale = holderH / bit.height;
				bit.x = orgX;
				bit.y = orgY;
				
			}
				
			
			bit.scaleX =  scale;
			bit.scaleY =  scale;
			
			
			mediaLdr.unload();
			
			//add thumb content
			//child.addChild(thumbContent);
			
			
			refArray[p] = bit;
			//refArray[p].alpha = 0;
			
			//add the content to the thumbs track
			//holder.addChild(child);
			holder.addChild(bit);
			
			if (p == 0) 
			{
				startSlideShow();
				refArray[p].visible = true;
				refArray[p].alpha = 1;
			} else {
				refArray[p].visible = false;
				refArray[p].alpha = 0;
			}
			
			if (p < no) {
				p++;
				startLoading();//create loop
			} else if (p == no) {
				
			}
					
		}
		
		private function handleLoadError(event:IOErrorEvent):void {
			//trace("ioErrorHandler: " + event);
			
			if (p < no) {
				p++;
				startLoading();//create loop
			} else if (p == no) {
				
			}
		}
		
		private function startSlideShow():void
		{
			trace("Start SlideShow");
			if (no > 0)
			{
				//refArray[0].alpha = 1;
				timer.reset();
				timer.start();
				timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			}
		}
		
		
		
		private function fadeIn(mc:DisplayObject):void
		{
			mc.visible = true;
			TweenLite.to(mc, 1, {alpha:1});
		}
		
		private function fadeOut(mc:DisplayObject):void
		{
			//trace("fadeout : " + no + " item = " + refArray[no]);
			
			//TweenMax.to(refArray[no], 1, {alpha:0, onComplete:toggleMedia});
			TweenLite.to(mc, 1, {alpha:0, onComplete:devisibleMe, onCompleteParams:[mc]});
		}
		
		private function devisibleMe(mc:DisplayObject):void
		{
			mc.visible = false;
		}
		
		private function toggleMedia():void
		{
			//trace("currentMedia = " + currentMedia + " : nextMedia = " + nextMedia);
			refArray[currentMedia].visible = false;
			
			refArray[nextMedia].visible = true;
		}
//============================EVENTS============================================================	

		
		private function onTimerComplete(event:TimerEvent):void
        {
			
			fadeOut(refArray[currentMedia]);
			//trace("currentMedia = " + currentMedia + " : no = " + no);
			if (currentMedia < no)
			{
				nextMedia = currentMedia + 1;
				
			} else {
				nextMedia = 0;
			}
			
			fadeIn(refArray[nextMedia]);
			
			currentMedia = nextMedia;
			
			
			timer.reset();
			timer.start();
			
            
        }
		
//============================PUBLIC FUNCTIONS==================================================
		public function loadMedia(ma:Array):void
		{
			clearMedia();
			
			this.mediaArray = ma;
			no = ma.length - 1;
			
			
			firstMedia =  mediaArray[0];
			type = firstMedia.substr(-3,3);
			//trace("get last 3 char : " + firstMedia.substr(-3,3));
			if (type.toLowerCase() == "flv") 
			{
				loadVideo(firstMedia);
			} else {
				startLoading();
			}
			trace("MediaArray = " + this.mediaArray);
		}
		
		var url:String;
		
		private function startLoading():void
		{
			isVideo = false;
			
			url = path_images + "room_picture/" + mediaArray[p];
			
			if (url.charAt(0) == "/")
			{
				url = url.substring(1, url.length);
			}
			
			trace("url = " + url);
			mediaURLReq.url = String(url);
			mediaLdr.load(mediaURLReq);//access the thumbnails
			
		}
		
		public function clearMedia():void
		{
			
			for (var i=0;i<holder.numChildren;i++)
			{
				holder.removeChildAt(i);
				
			}
			refArray = [];
			mediaArray = [];
			p = 0;
			currentMedia = 0;
			nextMedia = 1;
			
			
			timer.reset();
			timer.stop();
			
			timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);

		}
		
		public function loadVideo(url:String):void
		{
			isVideo = true;
			holder.addChild(video);
			 
			nc.connect(null);
			 
			ns = new NetStream(nc);
			//ns.client = {onMetaData:ns_onMetaData, onCuePoint:ns_onCuePoint};
			ns.client = {onMetaData:ns_onMetaData, NetStatusEvent:ns_onPlayStatus};
			
			video.attachNetStream(ns);
			ns.play(url);
			 
			ns.addEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
			
		}
		function ns_onMetaData(item:Object):void {
			// trace("metaData");
			 // Resize video instance.
			 var scale:Number = holderW / video.width;
			 video.width = holderW;
			 video.height = video.height * scale;
			 
			 video.x = 0;
			 video.y = (holderH - video.height) / 2;
			 // Center video instance on Stage.
			 //video.x = (stage.stageWidth - video.width) / 2;
			 //video.y = (stage.stageHeight - video.height) / 2;
		}
			 
		function ns_onCuePoint(item:Object):void {
			// trace("cuePoint");
			 //trace(item.name + "\t" + item.time);
		}	
		
		function ns_onPlayStatus(event:NetStatusEvent):void {
			if(event.info.code == "NetStream.Play.Stop"){
				ns.seek(0);
			}
		}
		
		public function stopMedia(evt:Event = null):void
		{
			if (isVideo) {
				ns.pause();
			}
			
			clearMedia();

		}
//============================SETTERS \ GETTERS==================================================
	}
}