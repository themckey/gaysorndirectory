﻿package com.pvm.ui {
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.sampler.Sample;
	import flash.display.Shape;
	
	public class UIPopover extends Sprite {
		
		private var _size:CGSize;
		private var _hidden:Boolean = true;
		
		private var _bg:Sprite;
		
		private var _primaryBG:Shape;


		public function UIPopover(size:CGSize = null) {
			// constructor code
			if (size) {
				this._size = size;
				
				
				_bg = new Sprite();
				_bg.graphics.beginFill(0x8C8C8C, 1);
				_bg.graphics.drawRect(0, 0, size.width, size.height);
				_bg.graphics.endFill();
				addChild(_bg);
				
				_primaryBG = new Shape();
				_primaryBG.graphics.beginFill(0xffffff, 0.9);
				_primaryBG.graphics.drawRoundRect(20,50,380,(size.height-50),20,20)
				_primaryBG.graphics.endFill();
				//addChild(_primaryBG);
			}
			
		}
		
		public function set hidden(value:Boolean):void
		{
			this._hidden = value;
		}
		
		public function get hidden():Boolean 
		{
			return this._hidden;
		}
		
		public function presentPopoverFromRect(theFrame:Rectangle, inView:Sprite):void
		{
			inView.addChild(this);
			
			this.x = theFrame.x + 0.5 * (theFrame.width) - .5 * this._size.width;
			this.y = theFrame.y
			
			this.visible = true;
			this.hidden = false;
			
		}
		
		public function dismissPopover() {
			this.visible = false;
			this.hidden = true;
		}		

	}
	
}
