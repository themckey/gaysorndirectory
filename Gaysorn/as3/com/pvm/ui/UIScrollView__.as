﻿package com.pvm.ui {
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.events.Event;
	
	public class UIScrollView extends UIView {
		
		private var _content:Sprite = new Sprite();
		private var _contentFrame:Rectangle;
		private var _contentSize:CGSize;
		
		
		/* Options */
		private var _scrollEnabled:Boolean = false;
		private var _pagingEnabled:Boolean = false;
		private var _allowBouncing:Boolean = true;
		
		private var _lockScrollHorizontal:Boolean = false;
		private var _lockScrollVertical:Boolean = false;
		
		
		/* Bound */
		private var _topBound:Number = 0;
		private var _bottomBound:Number = 0;
		private var _leftBound:Number = 0;
		private var _rightBound:Number = 0;
		
		private var _totalPageX:int = 0;
		private var _totalPageY:int = 0;
		private var _currentPageX:int = 0;
		private var _currentPageY:int = 0;
		private var _targetPageX:int = 0;
		private var _targetPageY:int = 0;
		
		
		private var _mouseDown:Boolean = false;
		private var _pointDown:Point = new Point();
		private var _pointMove:Point = new Point();
		private var _pointUp:Point = new Point();
		private var _pointTarget:Point = new Point();
		private var _pointOffset:Point = new Point();
		private var _pointHistory:Array = new Array();
		private var _forceMovePointTarget:Point = new Point();
		private var _forceMove:Boolean = false;
		
		private var _contentPoint:Point = new Point();
		
		
		private var _easingPower:Number = 3;
		private var _easingHolder:Number = 3;
		
		private var scrollViewDidScrollEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewDidScroll");
		private var scrollViewWillBeginDraggingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewWillBeginDragging");
		private var scrollViewWillEndDraggingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewWillEndDragging");

		public function UIScrollView(frame:Rectangle = null, options:Object = null) {
			// constructor code
			
			_content = this;
			
			if (frame) this.frame = frame;
			
			
			for (var i:int = 0;i<4;i++) {
				_pointHistory[i] = new Point();
			}
			
			if (options) {
				if (options.hasOwnProperty('allowBouncing')) this._allowBouncing = options.allowBouncing;
			}
			
		}
		
		
		public function moveTo (point:Point, animated:Boolean = true):void
		{
			if (animated) {
				_forceMovePointTarget = point;
				_forceMove = true;
				this.addEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
			} else {
				_content.x = point.x;
				_content.y = point.y;
				
				
			}
		}
		
		public function set contentFrame(theFrame:Rectangle):void
		{
			this._contentFrame = theFrame;
			this._contentSize = new CGSize(theFrame.width, theFrame.height);
			
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill(0xff0000, 0);
			bg.graphics.drawRect(0, 0, _contentFrame.width, _contentFrame.height);
			bg.graphics.endFill();
			addChild(bg);
			
			setScroll();
		}
		
		public function set contentSize(theSize:CGSize):void
		{
			this._contentFrame = new Rectangle(0, 0, theSize.width, theSize.height);
			this._contentSize = theSize;
			
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill(0xff0000, 0);
			bg.graphics.drawRect(0, 0, _contentFrame.width, _contentFrame.height);
			bg.graphics.endFill();
			addChild(bg);

			
			setScroll();
		}
		
		public function get contentSize():CGSize
		{
			return this._contentSize;
		}
		
		public function get contentFrame():Rectangle
		{
			return this._contentFrame;
		}
		
		public function set pagingEnabled(theValue:Boolean):void
		{
			this._pagingEnabled = theValue;
		}
		
		public function set scrollEnabled (theValue:Boolean):void
		{
			this._scrollEnabled = theValue;
			

		}
		
		public function set allowBouncing(theValue:Boolean):void
		{
			this._allowBouncing = theValue;
		}
		
		private function setScroll():void
		{
			_topBound = 0;
			_bottomBound = frame.height - contentFrame.height; 
			_leftBound = 0;
			_rightBound = frame.width - contentFrame.width;
			
			_totalPageX = Math.ceil(contentFrame.width / frame.width);
			_totalPageY = Math.ceil(contentFrame.height / frame.height);
			
			if (true) {
				if (frame.width >= contentFrame.width)  _lockScrollHorizontal = true;
				if (frame.height >= contentFrame.height) _lockScrollVertical = true;
			}
			
			if (frame.width < contentFrame.width || frame.height < contentFrame.height)
			{
				this._scrollEnabled = true;
				//Maybe I should move this addListener to when page is loaded instead, save resources.
				this.addEventListener(MouseEvent.MOUSE_DOWN, _onMouseDownHandler);
				
			} else {
				this._scrollEnabled = false;
				this.removeEventListener(MouseEvent.MOUSE_DOWN, _onMouseDownHandler);
				
			}
			
			
		}
		
		
		
		
		private function _onMouseDownHandler(evt:MouseEvent):void
		{
			
			if (this._scrollEnabled) {
				this._mouseDown = true;
				
				_pointDown.x = evt.stageX;
				_pointDown.y = evt.stageY;
				
				
				
				_pointTarget = _pointDown.clone();
				_pointUp = _pointDown.clone();
				
				
				_pointOffset.x = _content.x - stage.mouseX;
				_pointOffset.y = _content.y - stage.mouseY;
				
				_pointHistory[3] = _pointHistory[2] = _pointHistory[1] = _pointHistory[0] = _pointDown;
				
				
				
				
				this.addEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
				
				dispatchEvent(scrollViewWillBeginDraggingEvent);
			}
			
			this.stage.addEventListener(MouseEvent.MOUSE_UP, _onMouseUpHandler);
				
			
		}
		
		
		
		private function _onMouseUpHandler(evt:MouseEvent):void
		{
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, _onMouseUpHandler);
				
			if (this._scrollEnabled) {
				dispatchEvent(scrollViewWillEndDraggingEvent);
				
				
				this._mouseDown = false;
				
				_pointUp.x = evt.stageX;
				_pointUp.y = evt.stageY;
				
				
				if (!_pagingEnabled) {
					var veloX:Number = _pointUp.x - _pointHistory[3].x;
					var veloY:Number = _pointUp.y - _pointHistory[3].y;
					_pointTarget.x = _content.x + veloX * _easingPower;
					_pointTarget.y = _content.y + veloY * _easingPower;
					
					
				} else {
					
					if (frame.width < contentFrame.width) {
						
						if (_pointUp.x - _pointDown.x < -100) _targetPageX = _currentPageX+1; 
						else if (_pointUp.x - _pointDown.x > 100) _targetPageX = _currentPageX-1;
						else _targetPageX = _currentPageX;
					} 
					
					if (frame.height < contentFrame.height){
						if (_pointUp.y - _pointDown.y < -100) _targetPageY = _currentPageY+1;
						else if (_pointUp.y - _pointDown.y > 100) _targetPageY = _currentPageY-1;
						else _targetPageY = _currentPageY;
					}
					
					if (_targetPageX <= 0) _targetPageX = 0;
					if (_targetPageX >= _totalPageX) _targetPageX = _totalPageX - 1;
					
					if (_targetPageY <= 0) _targetPageY = 0;
					if (_targetPageY >= _totalPageY) _targetPageY = _totalPageY - 1;
					
					
					_pointTarget.x = _targetPageX * -frame.width;
					_pointTarget.y = _targetPageY * -frame.height;
					
					_currentPageX = _targetPageX;
					_currentPageY = _targetPageY;
					
				}
			
			
			
			}
			
			
		}
		
		private function _onEnterFrameHandler(evt:Event):void
		{
			
			
			if (this._mouseDown) {
				_pointTarget.x = stage.mouseX + _pointOffset.x;
				_pointTarget.y = stage.mouseY + _pointOffset.y;
				
				if (!_allowBouncing) {
					if (_pointTarget.x > _leftBound) _pointTarget.x = _leftBound;
					if (_pointTarget.x < _rightBound) _pointTarget.x = _rightBound;
					if (_pointTarget.y > _topBound) _pointTarget.y = _topBound;
					if (_pointTarget.y < _bottomBound) _pointTarget.y = _bottomBound;
				}
				
				_pointHistory[3] = _pointHistory[2];
				_pointHistory[2] = _pointHistory[1];
				_pointHistory[1] = _pointHistory[0];
				_pointHistory[0] = new Point(stage.mouseX, stage.mouseY);
			
			} else {
				
				if (!_pagingEnabled) {
					//Check Bound
					if (_pointTarget.x > _leftBound) _pointTarget.x = _leftBound;
					if (_pointTarget.x < _rightBound) _pointTarget.x = _rightBound;
					if (_pointTarget.y > _topBound) _pointTarget.y = _topBound;
					if (_pointTarget.y < _bottomBound) _pointTarget.y = _bottomBound;
					
				} else {
					
				}
				
			}
			
			if (_forceMove) {
				_pointTarget = _forceMovePointTarget;
			}
			
			//trace("_poinTarget " + _pointTarget);
			//trace("content " + _content.x + ", " + _content.y);
			if (!_lockScrollHorizontal) _content.x += (_pointTarget.x - _content.x) / _easingHolder;
			if (!_lockScrollVertical) _content.y += (_pointTarget.y - _content.y) / _easingHolder;
			
			_contentPoint.x = _content.x;
			_contentPoint.y = _content.y;
			
			_currentPageX = Math.round(-_content.x / frame.width);
			_currentPageY = Math.round(-_content.y / frame.height);
			
			if (!this._mouseDown && Point.distance(_contentPoint, _pointTarget) < 1) {
				
				_forceMove = false;
				
				this.removeEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
				dispatchEvent(scrollViewDidScrollEvent);
			}
			
		}
		
		private function set page(pageIndex:int):void
		{
			
		}

	}
	
}
