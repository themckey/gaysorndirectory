﻿package  com.pvm.ui {
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.display.MovieClip;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Bitmap;
	import flash.net.URLRequest;
	import flash.system.System;
	
	import com.greensock.TweenMax;
	
	import com.pvm.core.ContentMode;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.display.MovieClip;
	import flash.net.URLLoader;
	
	public class UIImageView extends UIView{

		private	var _content:DisplayObject;
		
		
		private	var _mediaLdr:URLLoader = new URLLoader();
		private	var _mediaURLReq:URLRequest = new URLRequest();
		
		private var _contentMode:String = ContentMode.SCALE_ASPECT_FIT;
		private var _imageOriginalSize:CGSize;
		private var _imageSize:CGSize;
		
		private var _transitionIn:String = "fade";
		
		private var _localStorage:Dictionary;
		private var _url:String;
		
		private var _scale:Number = 1;
		
		private var doneLoadedEvent:Event = new Event("doneLoaded");
		
		public function UIImageView(theFrame:Rectangle = null, url:String = "") {
			
			if (theFrame) super(theFrame);
			
			mouseChildren = false;
			
			if (url != "") this.loadImage(url);
			
		}
		
		public function set contentMode (theMode:String):void
		{
			this._contentMode = theMode;
			
			configureImage();
		}
		
		public function set transitionIn(value:String):void
		{
			this._transitionIn = value;
		}
		
		
		public function loadImage(url:String):void
		{
			clear();
			
			if (url != "")
			{
				this.visible = true;
				
				if (url.charAt(0) == "/") url = url.substring(1, url.length);
				
				this._url = url;
				
				if (!_localStorage) _localStorage = new Dictionary(true);
				
				if (_localStorage[url] != null) {
					_content = _localStorage[url];
					this.viewContainer.addChild(_localStorage[url]);
					this.doTransitionIn();
				
				} else {
					_mediaLdr = new URLLoader();
					_mediaLdr.contentLoaderInfo.addEventListener(Event.COMPLETE, doneLoaded, false, 0, true);
					
					_mediaURLReq.url = String(url);
					_mediaLdr.load(_mediaURLReq);//access the thumbnails
				}
			} else {
				this.visible = false;
			}
		}
		
		public function clearMedia():void
		{
			this.unload();
			
		}
		
		public function clear():void
		{
			unload();
		}
		
		public function unload():void
		{
			if(_content && _content is Bitmap) {
				Bitmap(_content).bitmapData.dispose();
			}
			
			if (_content) {
				if (_content is MovieClip) {
					var mc:MovieClip = _content as MovieClip;
					
					
					if (mc.pano) {
						for (var i:int = 0; i< mc.pano.numChildren; i++ ) {
							//trace(mc.pano.getChildAt(i));
							if (mc.pano.getChildAt(i) is MovieClip) {
								for (var j:int = 0; j< mc.pano.getChildAt(i).numChildren; j++ ) {
									//trace("  ::  " + (mc.pano.getChildAt(i) as MovieClip).getChildAt(j));
								}
							}
						}
						
						//mc.pano.cleanup();
					}
					//mc.cleanup();
				}
				
				
				
				this.viewContainer.removeChild(_content);
				_content = null;
			}
			
			if (this.viewContainer) {
				while (this.viewContainer.numChildren)
				{
					this.viewContainer.removeChildAt(0);
				}
			}
			
			if (_mediaLdr) {
				//trace("mediaLdr unloadAndStop");
				_mediaLdr.unloadAndStop();
				_mediaLdr = null;
				
			}
			
			if (_localStorage) _localStorage = null;
			
		}
		
		public function get content():Sprite
		{
			return this._content as Sprite;
		}
		
		
		
		private function doneLoaded(evt:Event):void
		{
		
			_content = DisplayObject(evt.target.content);
			//_localStorage[_url] = _content;
			
			
			if((_content != null) &&(_content is Bitmap)) Bitmap(_content).smoothing = true;
			
			_imageOriginalSize = new CGSize(_content.width, _content.height);
			_imageSize = new CGSize(_content.width, _content.height);
			
			//Recreate Frame, if not previously defined.
			if (!this.frame) {
				this.frame = new Rectangle(this.x, this.y, _imageOriginalSize.width, _imageOriginalSize.height);
			}
			
			
			if (_content.width && _content.height) configureImage();
			
			
			this.viewContainer.addChild(_content);
			//this.viewContainer.addChild(_mediaLdr);
			
			doTransitionIn();
			
			_mediaLdr.contentLoaderInfo.removeEventListener(Event.COMPLETE, doneLoaded);
			
			dispatchEvent(doneLoadedEvent);
		}
		
		private function doTransitionIn():void
		{
			if (_transitionIn == "fade") {
				_content.alpha = 0;
				TweenMax.to(_content, 0.6, {alpha:1});
			}
		}
		
		override protected function onConfiguredFrame(theFrame:Rectangle):void
		{
			configureImage();
			
		}
		
		public function configureImage():void
		{
			if (_content) {
				
				switch (this._contentMode) 
				{
					
					case ContentMode.TOP : 
						
						break;
						
					case ContentMode.LEFT : 
					
						break;
						
					case ContentMode.CENTER : 
						_content.x = (frame.width - _imageSize.width) / 2;
						_content.y = (frame.height - _imageSize.height) / 2;
						break;
					
					case ContentMode.SCALE_ASPECT_FILL : 
						
						if (!this._imageOriginalSize || !this.frameSize) return;
						
						
						if (this._imageOriginalSize.ratio >= this.frameSize.ratio) {
							_scale = frame.height / _imageOriginalSize.height;
							_imageSize.width = _scale * _imageOriginalSize.width;
							_imageSize.height = frame.height;
							_content.x = 0 + (frame.width - _imageSize.width) / 2;
							_content.y = 0;
							
						} else if (this._imageOriginalSize.ratio < this.frameSize.ratio) {
							_scale = frame.width / _imageOriginalSize.width;
							_imageSize.height = _scale * _imageOriginalSize.height;
							_imageSize.width = frame.width;
							_content.x = 0;
							_content.y = 0 + (frame.height - _imageSize.height) / 2;
							
						}
						_content.scaleX = _content.scaleY = _scale;
						break;
					
					case ContentMode.SCALE_ASPECT_FIT : 
						
						if (!this._imageOriginalSize || !this.frameSize) return;
						
						
						if (this._imageOriginalSize.ratio <= this.frameSize.ratio) {
							_scale = frame.height / _imageOriginalSize.height;
							_imageSize.width = _scale * _imageOriginalSize.width;
							_imageSize.height = frame.height;
							_content.x = 0 + (frame.width - _imageSize.width) / 2;
							_content.y = 0;
							
						} else if (this._imageOriginalSize.ratio > this.frameSize.ratio) {
							
							_scale = frame.width / _imageOriginalSize.width;
							_imageSize.height = _scale * _imageOriginalSize.height;
							_imageSize.width = frame.width;
							
							_content.x = 0;
							_content.y = 0  + (frame.height - _imageSize.height) / 2;
							
						}
						
						_content.scaleX = _content.scaleY = _scale;
						
						break;
						
					case ContentMode.SCALE_TO_FILL : 
						_content.x = 0;
						_content.y = 0;
						_content.scaleX = frame.width / _imageOriginalSize.width;
						_content.scaleY = frame.height / _imageOriginalSize.height;
						break;
						
				}
			}
			
		}

	}
	
}
