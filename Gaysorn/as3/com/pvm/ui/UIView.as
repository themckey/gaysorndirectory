﻿package com.pvm.ui {
	import flash.geom.Rectangle;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.Shape;
	
	public class UIView extends Sprite{

		private var _frame:Rectangle;
		private var _frameSize:CGSize = new CGSize();
		
		private var _backgroundColor:uint;
		private var _bg:Shape;
		private var _mask:Sprite;
		
		private var _container:Sprite = new Sprite();
		private var _unmask:Boolean = false;
		
		private var _lang:String = "en";
		
		public function UIView(theFrame:Rectangle = null) {
			// constructor code
			
			if (theFrame) this.frame = theFrame;			
			addChild(_container);
			
			
			
		}
		
		protected function get viewContainer():Sprite
		{
			return this._container;
		}
		
		private function onAddedToStage(evt:Event):void
		{
			configureFrame();
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public function set frame(theFrame:Rectangle):void
		{
			this._frame = theFrame;
			
			if (this.parent) configureFrame();
			else this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
		}
		
		public function get frame():Rectangle
		{
			return this._frame;
		}
		
		private function configureFrame():void
		{
			this.x = this._frame.x;
			this.y = this._frame.y;
			
			
			
			
			if (_bg) {
				_bg.graphics.clear();
				_bg.graphics.beginFill(_backgroundColor, 1);
				_bg.graphics.drawRect(0, 0, _frame.width, _frame.height);
				_bg.graphics.endFill();
				_container.addChildAt(_bg, 0);
			}
			
			if (!_frameSize) {
				_frameSize = new CGSize(this._frame.width, this._frame.height);
			}else {
				_frameSize.width = this._frame.width;
				_frameSize.height = this._frame.height;
			}
			
			configureMask();
			
			onConfiguredFrame(_frame);
		}
		
		override public function addChild (child : DisplayObject) : DisplayObject
        {
			/*
            if (child != _container && (!_mask || child != _mask)  && (!_bg || child != _bg)) _container.addChild( child ); 
			else super.addChild(child);
			*/
			
			if (_mask && child == _mask) super.addChild(child);
			else if (_bg && child == _bg) super.addChild(child);
			else if (child == _container) super.addChild(child);
			else { 
				if (!_container) {
					_container = new Sprite();
					addChild(_container);
				}
				_container.addChild( child );
			}
            return child;
        }
		
		public function attach(child : DisplayObject) : DisplayObject 
		{
			return super.addChild(child);
		}
		
		public function attachAt(child : DisplayObject, index : int ) : DisplayObject 
		{
			return super.addChildAt(child, index);
		}
		
		override public function addChildAt (child : DisplayObject, index : int) : DisplayObject
        {
			if (_mask && child == _mask) super.addChildAt(child, index);
			else if (_bg && child == _bg) super.addChildAt(child, index);
			else if (child == _container) super.addChildAt(child, index);
			else {
				if (!_container) {
					_container = new Sprite();
					addChild(_container);
				}
				_container.addChildAt( child , index);
			
			}
			
            return child;
        }
		
		
		
		
		override public function removeChild ( child : DisplayObject) : DisplayObject 
		{
			if (child == _container)  return super.removeChild(child); 
			if (_mask && child == _mask)  return super.removeChild(child); 
			if (_bg && child == _bg)  return super.removeChild(child); 
			
			return _container.removeChild(child);
		}
		
		override public function removeChildAt ( index : int ) : DisplayObject 
		{
			if (this.getChildAt(index) == _container)  return super.removeChildAt(index); 
			if (_mask && this.getChildAt(index) == _mask)  return super.removeChildAt(index); 
			if (_bg && this.getChildAt(index) == _bg)  return super.removeChildAt(index); 
			
			return _container.removeChildAt(index);
		}
		
		public function set unmask(value:Boolean):void
		{
			this._unmask = value;
			if (this.parent) configureMask();
			else this.addEventListener(Event.ADDED_TO_STAGE, configureMask);
		
		}
		
		private function configureMask(evt:Event = null) {
			
			if (_unmask) {
				
				_container.mask = null;
				if (_mask) { 
					this.removeChild(_mask);
					_mask = null;
				}
				
			} else {
				
				if (!_mask) { 
					_mask = new Sprite();
					this.addChild(_mask);
					//this.parent.addChild(_mask);
				}
				
				_mask.x = 0;
				_mask.y = 0;
				_mask.graphics.clear();
				_mask.graphics.beginFill(0xff0000, 0.2);
				_mask.graphics.drawRect(0, 0, this._frame.width, this._frame.height);
				_mask.graphics.endFill();
				
				if (!_container) { _container = new Sprite(); addChild(_container); }
				_container.mask = _mask;
			}
		}
		
		protected function get viewMask():Sprite 
		{
			return this._mask;
		}
		
		
		protected function onConfiguredFrame(theFrame:Rectangle):void
		{
			
		}
		
		
		public function get frameSize():CGSize
		{
			return _frameSize;
		}
		
		public function set frameSize(frameSize:CGSize):void
		{
			this._frameSize = frameSize;
		}
		
		public function set backgroundColor(theColor:uint):void
		{
			this._backgroundColor = theColor;
			
			if (!_bg) {
				_bg = new Shape();
				_bg.graphics.beginFill(theColor, 1);
				_bg.graphics.drawRect(0, 0, _frame.width, _frame.height);
				_bg.graphics.endFill();
				addChildAt(_bg, 0);
			}
		}
		
		
		public function set lang(value:String):void
		{
			this._lang = value;
			translate();
		}
		
		public function translate():void
		{
			
		}
		
		public function get lang():String
		{
			return this._lang;
		}
		
		


	}
	
}
