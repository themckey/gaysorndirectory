﻿/* 
 * UIControlEvent version 1.0
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.pvm.ui {
	
	import flash.events.Event;
	
	public class UIControlEvent extends Event{

		public static const FOCUS:String						= "focus";
		public static const FOCUS_OUT:String 					= "focusOut";
			
		public static const TOUCH_DOWN:String					= "touchDown";
		public static const TOUCH_DOWN_REPEAT:String			= "touchDownRepeat";
		public static const TOUCH_DRAG_INSIDE:String			= "touchDragInside";
		public static const TOUCH_DRAG_OUTSIDE:String			= "touchDragOutside";
		public static const TOUCH_UP_INSIDE:String				= "touchUpInside";
		public static const TOUCH_UP_OUTSIDE:String				= "touchUpOutside";
		public static const LONG_TOUCH_DOWN:String 				= "longTouchDown";
		public static const TAP:String							= "tap";
		
		
		public static const VALUE_CHANGED:String 				= "valueChanged";
		public static const EDITING_DID_BEGIN:String			= "editingDidBegin";
		public static const EDITING_CHANGED:String				= "editingChanged";
		public static const EDITING_DID_END:String				= "editingDidEnd";
		public static const EDITING_DID_END_ON_EXIT:String		= "editingDidEndOnExit";
		
		public static const SUBMIT:String 						= "submit";
		
		public static const CHANGE_LANGUAGE:String				= "changeLanguage";
		
		
		public var from:*;
		public var lang:String = "en";
		
		public function UIControlEvent(type:String, from:* = null) {
			// constructor code
			this.from = from;
			super(type);

		}

		public override function clone():Event 
		{
			var newEvt : UIControlEvent = new UIControlEvent ( type, from);
			newEvt.from = from;
			newEvt.lang = lang;
			return newEvt;
			
		}

	}
	
}



