﻿package  com.pvm.ui{
	
	import com.greensock.TweenMax;
	import flash.utils.Timer;
	import flash.events.TimerEvent;

	public class UIButtonPlus extends UIButton {
		
		public var timer:Timer = new Timer(1000, 10);

		public function UIButtonPlus() {
			// constructor code
			
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, stopWink);
			timer.start();
						
		}
		
		public function winkBTN():void
		{
			TweenMax.to (this,0.7, {scaleX:0.7, scaleY:0.7, onComplete:setZoomin});
			
		}
		
		public function setZoomin ():void
		{
			
			TweenMax.to (this,0.7, {scaleX:1, scaleY:1, onComplete:winkBTN});
		}
		
		public function stopWink (evt:TimerEvent = null):void
		{
			trace ("stop wink");
			TweenMax.to (this,0.7, {scaleX:1, scaleY:1});
			
		}
		
		

	}
	
}
