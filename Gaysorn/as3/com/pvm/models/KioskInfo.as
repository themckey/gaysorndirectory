﻿package com.pvm.models {
	
	public class KioskInfo {
		
		public var location_key:String;
		public var location_name:String;

		public function KioskInfo(node:Object) {
			// constructor code
			location_key = node.location.@key;
			location_name = node.location.text();
			
			
		}

	}
	
}
