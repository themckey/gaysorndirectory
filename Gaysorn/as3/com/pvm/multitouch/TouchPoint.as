﻿package com.pvm.multitouch {
	
	import flash.display.Shape;
	
	public class TouchPoint extends DisplayPoint {
		
		public function TouchPoint() {
			// constructor code
			var myShape:Shape = new Shape();
			
			myShape.graphics.beginFill(0x00CCFF, 0.6);
			myShape.graphics.drawCircle(-5,-5,10);
			myShape.graphics.endFill();
			addChild(myShape);
			
			mouseChildren = false;
			mouseEnabled = false;
			
		}

	}
	
}
