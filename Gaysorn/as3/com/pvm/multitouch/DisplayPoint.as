﻿package com.pvm.multitouch {
	import flash.display.Sprite;
	
	public class DisplayPoint extends Sprite {

		public var touchID:int;
		public var targetX:Number = 0;
		public var targetY:Number = 0;
		
		public var lastX:Number = 0;
		public var lastY:Number = 0;
		
		public var veloX:Number = 0;
		public var veloY:Number = 0;
		
		public function DisplayPoint() {
			// constructor code
		}

	}
	
}
