﻿//phpConnector.as

package com.pvm.directory.core
{

	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.system.Security;
	
	public dynamic class PHPConnector extends Object
	{
		var targetURL:String;
		var contact_url:String;
		var contactRequest:URLRequest = new URLRequest();
		var contactLoader:URLLoader = new URLLoader();
		var contactVariables:URLVariables = new URLVariables();
		
		var showResult:int = 0;
		
		var str_en:String;
		var str_th:String;
		
		var result_data:String;
		
		public var SERVER_LOCATION:String;
		
		
		public function PHPConnector(targetURL:String = "get_stat.php", phpVar:URLVariables = null, showResult:int = 0)
		{
			//SERVER_LOCATION = DataCenter.getInstance().getServerLocation();
			this.targetURL = targetURL;
			//showResult = 0 ; Not show
			//showResult = 1 ; From Comment Panel
			//showResult = 2 ; From Subscribe Panel
			Security.allowDomain("*");
			
			
			this.showResult = showResult;
			// send data via post
			contactRequest.method = URLRequestMethod.POST;
			
			contactLoader.addEventListener(Event.COMPLETE, onLoaded);
			contactLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
		}
		
		

//============================PRIVATE FUNCTIONS=================================================
		
		
		function onLoaded(evt:Event):void
		{
			
			result_data = String(contactLoader.data);
			//trace("     ************** Send Data to PHP ******************       ");
			//trace("Result : "+result_data);
			
			/*
			if (showResult != 0)
			{
				searchPanel.showThankyou(showResult);
			}*/
			
			
			if (result_data == "ok")
			{
				
				//If showResult != 0 (Send Comment = 1, Subscribe = 2) then showThankyou
				if (showResult != 0)
				{
					//searchPanel.showThankyou(showResult);
				}
				
				//trace("Send Data ok");
				
			}
			else if (result_data == "error")
			{
				//trace("Send Data error");
			}
			//trace("     **************************************************       ");
			
		}

		function ioErrorHandler(event:IOErrorEvent):void
		{
			//trace("ioErrorHandler: " + event);
		}

		
		
//============================EVENTS============================================================	

		
		
		
//============================PUBLIC FUNCTIONS==================================================
		
		public function set_server(_server:String):void
		{
			//targetURL = _server + targetURL;
			targetURL = "http://192.168.1.198/cms2/" + targetURL;
		}
		
		public function _send(phpVar:URLVariables = null, showResult:int = 0):void
		{
			
			this.showResult = showResult;
			
			contactRequest.url = targetURL;
			contactRequest.data = phpVar;
			//trace(phpVar);
			try {
				contactLoader.load(contactRequest);
			} catch (e:Error) {
	
			}
		}
	}
}