﻿//DataCenter Event
package com.pvm.directory.core.events
{
	import flash.events.Event;
	
	
	public class DataManagerEvent extends Event
	{
		
		
		public static const		LOAD_CONFIG_COMPLETE	:String		=	"configLoaded";
		public static const		LOAD_XML_COMPLETE		:String		=	"xmlLoaded";
		public static const		GENERATE_DATA_COMPLETE	:String		= 	"dataGenerated";
		public static const		CREATE_APP_COMPLETE		:String		= 	"appCreated";
		public static const		RESTART_APP				:String		=	"restartApp";
		

		public function DataManagerEvent( type:String ) 
		{
			super( type );
		
		}

		public override function clone():Event 
		{
			return new DataManagerEvent( type );
		}
		
	}
	
}