﻿//DataCenter Event
package com.pvm.directory.core.events
{
	import flash.events.Event;
	
	
	public class TouchScreenEvent extends Event
	{
		
		
		public static const		TAP	:String		=	"tap";
		
		public var from:Object;

		public function TouchScreenEvent( type:String, from:Object = null) 
		{
			this.from = from;
			super( type, from );
		
		}

		public override function clone():Event 
		{
			var newEvt : TouchScreenEvent = new TouchScreenEvent ( type, from);
			
			newEvt.from = from;
			
			return newEvt;
		}
		
	}
	
}