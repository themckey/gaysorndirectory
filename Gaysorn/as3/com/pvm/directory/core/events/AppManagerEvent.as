﻿package com.pvm.directory.core.events {

	import flash.events.Event;
	import com.pvm.directory.core.App;
	
	public class AppManagerEvent extends Event {


		public static const		OPEN_APP			:String		=	"openApp";
		public static const		OPEN_APP_NO_INTRO	:String		= 	"openApp_noIntro";
		public static const 	CLOSE_APP			:String		= 	"closeApp";
		public static const 	HIDE_INTRO			:String		= 	"hideIntro";
		public static const 	SHOW_INTRO			:String		= 	"showIntro";
		
		
		public var appStr : String;

		
		public function AppManagerEvent( type:String, appStr = "" ) {
			
			this.appStr = appStr;
			super( type );
		}
		
		public override function clone():Event 
		{
			var newEvt : AppManagerEvent = new AppManagerEvent ( type, appStr);
			
			newEvt.appStr = appStr;
			
			return newEvt;
		}
		

	}
	
}
