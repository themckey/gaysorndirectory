﻿package com.pvm.directory.manager 
{
	import flash.display.Shape;
	import flash.geom.Transform;
	import flash.geom.ColorTransform;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.EventDispatcher;
	import flash.display.DisplayObject;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.system.System;
	import flash.system.fscommand;
	
	
	import com.pvm.ui.InteractiveItem;
	

	//import apps.App;
	
	public dynamic class EventManager extends EventDispatcher
	{
		protected static var		instance				: EventManager;
		protected static var		allowInstantiation		: Boolean			= false;
		
		protected var				dispatchersArray		: Array;
		protected var				permDispatchers			: Array;
		protected var				listenerList			: Array				= [];							//array of EventListenerObjects
		protected var				permListeners			: Array				= [];
	
		protected var				_listenerID				: uint				= 0 ;
		protected var				_dispatcherID			: uint				= 0 ;
		
		protected static const		MAX_LISTENERS			: uint				= 1000;
		protected static const		MAX_DISPATCHERS			: uint				= 1000;
		
		/* Event */
			
			
			// MainEvent
			//public var changeitemEvent:MainEvent = new MainEvent("changeitem");
			public var touchDownEvent:TouchScreenEvent = new TouchScreenEvent("touchDown");
			public var touchUpEvent:TouchScreenEvent = new TouchScreenEvent("touchUp");
			public var tapEvent:TouchScreenEvent = new TouchScreenEvent("tap");
			
			



		/* Checker */
		
		public var dispatchObj:Object;
		public var mouseDownTarget:DisplayObject;
		public var param:String;
		public var str:String = "";
		public var keyid:String;

		/* MouseUp & Down */
		public var downX:Number;
		public var downY:Number;
		public var upX:Number;
		public var upY:Number;
		public var moveX:Number;
		public var moveY:Number;
		public var isDragging:Boolean = false;
		public var selectionTreshold:Number = 30;
		public var moveTreshold:Number = 50;
		
		private var idleTimer:Timer = new Timer(1000, 30);
		private var checkMemoryTimer:Timer = new Timer(1000, 4);
		private var restartTimer:Timer = new Timer(1000, 90);
		
		
		public function EventManager() 
		{
			if (!allowInstantiation )			
			{
				throw new ReferenceError ( "Error: Instantiation failed. Use EventManager.getInstance() instead of new." ) ;				
			}
			else
			{
				init ( ) ;
			}
		}
		protected function init() : void
		{
			trace("   -----------------  LOADING EVENTMANAGER.AS ---------------------");
			this.name = "EventManager";
			
			dispatchersArray 		= new Array ( ) ;
			listenerList			= new Array ( ) ;	
			permDispatchers 		= new Array ( ) ;	
			permListeners	 		= new Array ( ) ;	
			
			//idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _onIdleTimerComplete);
			
			checkMemoryTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _checkMemory);
			checkMemoryTimer.start();
			
			
			
			//openAppEvent.appStr = "Home";
			//dispatchEvent(openAppEvent);
			//idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _onIdleTimerComplete);
			//restartTimer.addEventListener(TimerEvent.TIMER_COMPLETE, restartApp);
			idleTimer.reset();
			idleTimer.start();
			
			
			
			trace("   --------------- DONE LOAD EVENTMANAGER.AS ----------------------");
		}
		
		
		
		
		
		protected function receiveEvent ( e : Event ) : void
		{
			//trace(">> receiveEvent:  type = " + e.type +" event from " + e.target);
			//trace(">> --------------------------------------------");
			
			dispatchObj = e.target;
			param = "";
			
			
			
			if (e.type == "mouseDown") {
				downX = MouseEvent(e).stageX;
				downY = MouseEvent(e).stageY;
				
				mouseDownTarget = DisplayObject(e.target);
				
				//trace("mouseDown on : " + e.target.parent.name); 
				idleTimer.reset();
				idleTimer.start();
				
				restartTimer.reset();
				restartTimer.start();
				
				if (dispatchObj is InteractiveItem)
				{
					InteractiveItem(dispatchObj).setDownState();
				}
				
				dispatchObj.stage.addEventListener("mouseMove", receiveEvent, false,0, true);
				
				touchDownEvent.from = mouseDownTarget;
				dispatchEvent(touchDownEvent);
				
				//trace(dispatchObj.name + " : " + dispatchObj.name.substr(0,2));
				
			} else if (e.type == "mouseMove") {
				
				moveX = MouseEvent(e).stageX;
				moveY = MouseEvent(e).stageY;
				
				if ((Math.abs(moveY - downY) >= moveTreshold) || ((Math.abs(moveX - downX) >= moveTreshold) ))
				{
					isDragging = true;
					
					//longPressEvent.localX = moveX;
					//longPressEvent.localY = moveY;
					//longPressTimer.stop();
				}
				
			}else if (e.type == "mouseOut") {
				if (e.target == mouseDownTarget) {
					//do something
					if (dispatchObj is InteractiveItem)
					{
						if ((!InteractiveItem(dispatchObj).isActive)&&(dispatchObj.holdActive == false))
						{
							InteractiveItem(dispatchObj).setDefaultState();
						}
					} 
					
					
				}
			} else if (e.type == "mouseUp") {
				
				
				
				upX = MouseEvent(e).stageX;
				upY = MouseEvent(e).stageY;
				
				touchUpEvent.from = e.target;
				dispatchEvent(touchUpEvent);
				
				

				
				

				dispatchObj.stage.removeEventListener("mouseMove", receiveEvent);
				if ((Math.abs(upY - downY) <= selectionTreshold) &&
					(Math.abs(upX - downX) <= selectionTreshold) &&
					!isDragging)
					
					//trace("isDragging >>> "+isDragging);
				{
				//======================= SELECTING ITEMS =============================
				
					trace("select item on : " + e.target.name + " : parent = " + e.target.parent.parent + " : " + e.target.parent.parent.name); 
				
					//trace(">> receiveEvent: " + e.type +" event from " + e.target.name);
					//trace(">> --------------------------------------------");
					
					if (dispatchObj is InteractiveItem)
					{
						if ((!InteractiveItem(dispatchObj).isActive)&&(dispatchObj.holdActive == false))
						{
							InteractiveItem(dispatchObj).setDefaultState();
						}
					}
					
					tapEvent.from = dispatchObj;
					dispatchEvent(tapEvent);
					
					
				}
				
				isDragging = false;
				
				
			}
			dispatchEvent( e );
		}
		
		public static function getInstance ( ) : EventManager
		{
			if ( instance == null )
			{
				allowInstantiation 		= true;
				instance 				= new EventManager ( ) ;			
				allowInstantiation 		= false;
			}
			return instance;
		}
		
		public function _checkMemory(evt:TimerEvent):void {
			//trace("System.totalMemory = " + Math.floor(System.totalMemory/10)/100 + " KB");
			checkMemoryTimer.reset();
			checkMemoryTimer.start();
			
		}
		
		
		
		public function registerDispatcher ( 	dispatchingObj 	: Object, 
												makePermanent 	: Boolean 	= false 	) : void
		{			
			
			/**
			 * Get The events
			 * getEvents ( ) is a custom method that must be in any class/obj registering as a dispatcher	
			 */
			
			var eventArray : Array  = dispatchingObj.getEvents ( ) ; 			
			/**
			 * Add the listeners
			 */			
			var len		: uint 		= eventArray.length					
			for ( var i : uint = 0 ; i < len ; i++ )
			{
				var dispatcherEventName 	: String 	= eventArray [ i ] ;		
				dispatchingObj.addEventListener( dispatcherEventName, receiveEvent )				
			}
			
			/**
			 * Save a reference in an array and check how many we have.
			 */	
			
			if ( makePermanent )
			{
				permDispatchers.push ( dispatchingObj ) ;
			}
			else
			{
				dispatchersArray.push ( dispatchingObj ) ;
				//trace ( "EventCenter : Added dispatcher", dispatchersArray ) ;
			}
			
			_dispatcherID++;			
			
			if ( _dispatcherID >= MAX_DISPATCHERS )
			{
				throw new RangeError ( this.toString() + "WARNING: there are over " + MAX_DISPATCHERS + " dispatchers registered." ) ;
			}
		}
		
		public function registerEventDispatcher ( 	dispatchingObj 	: Object, 
												 	eventType : String,
												makePermanent 	: Boolean 	= false 	) : void
		{			
			
			dispatchingObj.addEventListener( eventType, receiveEvent )				
			
			/**
			 * Save a reference in an array and check how many we have.
			 */	
			
			if ( makePermanent )
			{
				permDispatchers.push ( dispatchingObj ) ;
			}
			else
			{
				dispatchersArray.push ( dispatchingObj ) ;
				//trace ( "EventCenter : Added dispatcher", dispatchersArray ) ;
			}
			
			_dispatcherID++;			
			
			if ( _dispatcherID >= MAX_DISPATCHERS )
			{
				throw new RangeError ( this.toString() + "WARNING: there are over " + MAX_DISPATCHERS + " dispatchers registered." ) ;
			}
		}
		
		public function _onIdleTimerComplete (evt:Event):void
		{
			
			//restartTimer.reset();
			//restartTimer.start();
		}
		
		public function restartApp(evt:Event):void
		{
		//	fscommand("exec", "start_app.bat");
			//fscommand("quit", "");
		}
	}
}