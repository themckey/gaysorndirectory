﻿/*
DataManager version 1.00

Update 24 April 2012
By Pitipat Srichairat

*/

package com.pvm.manager  {
	
	/* Global Class */
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	
	/*  Flash Class */
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	/* Project Class */
	import com.pvm.directory.core.events.DataManagerEvent;
	import com.pvm.directory.core.interfaces.IRegisteredDispatcher;
	
	
	public dynamic class DataManager extends EventDispatcher  implements IRegisteredDispatcher{

		protected static var		instance				: DataManager;
		protected static var		allowInstantiation		: Boolean		= false;		
		
		
		public var _eventManager:EventManager;
		public var _eventsManager:EventsManager;
		
		/* Config */
		public var configLoader:BulkLoader = new BulkLoader("config");
		public var configXML:XML; 
		
		public var myLoader:BulkLoader = new BulkLoader("content");
		
		public var xmlDict:Dictionary = new Dictionary();
		
		public function DataManager() {
			// constructor code
			//EventManager.getInstance().registerDispatcher ( this ) ;
			
		}
		
		public function set eventManager(em:EventManager):void
		{
			this._eventManager = em;
			_eventManager.registerEventDispatcher(this, "xmlLoaded");

		}
		
		public function get eventManager():EventManager
		{
			return this._eventManager;
		}
		
		
		
		
		public function set eventsManager(em:EventsManager):void
		{
			this._eventsManager = em;
			_eventsManager.registerEventDispatcher(this, "xmlLoaded");
		}
		
		public function get eventsManager():EventsManager
		{
			return this._eventsManager;
		}

		
		//---------------------------------------------------------------------------
		//						Public function
		//---------------------------------------------------------------------------
		public function registerXML(name:String, url:String):void
		{
			xmlDict[name] = new Dictionary();
			xmlDict[name]["url"] = url;
		}
		
		public function getXML(key:String):XML
		{
			if (!xmlDict[key]) return null
			else return xmlDict[key]["content"];
		}
		
		
		//--------------- Config -----------------
		
		public function loadConfig():void
		{
			configLoader.add("config.xml");
			configLoader.addEventListener(BulkProgressEvent.COMPLETE, _onLoadConfig);
			configLoader.start();
			
		}

		private function _onLoadConfig(evt:Event):void
		{
			configXML = configLoader.getXML("config.xml");
			//config = new Config(configXML);

			loadXML();
		}

//--------------------------------------------------------------------------
//                              Public function    
//--------------------------------------------------------------------------
		
		public static function getInstance() : DataManager
		{
			if ( instance == null )
			{
				allowInstantiation 		= true;
				instance 				= new DataManager ( ) ;			
				allowInstantiation 		= false;
			}
			return instance;
		}
		
		public function loadXML() {
			
			for (var key:String in xmlDict)
			{
				myLoader.add(xmlDict[key]["url"]);
			}
			
			myLoader.addEventListener(BulkProgressEvent.COMPLETE, _onLoadContentComplete);
			myLoader.start();
		}
		
		
		
		public function getEvents ( ) : Array
		{
			var myEventsArray : Array = new Array();
			
			myEventsArray [ 0 ] = DataManagerEvent.LOAD_CONFIG_COMPLETE;
			myEventsArray [ 1 ] = DataManagerEvent.LOAD_XML_COMPLETE;
			myEventsArray [ 2 ] = DataManagerEvent.GENERATE_DATA_COMPLETE;
			
			return myEventsArray;		
		}		
		
//--------------------------------------------------------------------------
//                              Event    
//--------------------------------------------------------------------------



		private function _onLoadContentComplete(evt:Event):void
		{
			XML.ignoreWhitespace=true;
			
			/* System XML */
			for (var key:String in xmlDict)
			{
				xmlDict[key]["content"] = myLoader.getXML(xmlDict[key]["url"]);
				xmlDict[key]["num"] = xmlDict[key]["content"].*.length();
				
				
			}
			trace("dataManager >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
			
			initData();
			
			dispatchEvent(new Event("xmlLoaded"));
			
			
		}

//--------------------------------------------------------------------------
//                            Private Function    
//--------------------------------------------------------------------------
		
		public function initData():void
		{
			
		}
		
		
		
	}
	
}
