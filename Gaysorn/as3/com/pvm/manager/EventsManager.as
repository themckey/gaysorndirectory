﻿package com.pvm.manager 
{
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.system.System;
	import flash.system.fscommand;
	
	
	public dynamic class EventsManager extends EventDispatcher
	{
		
		public var					dispatchersArray		: Array;
		protected var				permDispatchers			: Array;
		protected var				listenerList			: Array				= [];							//array of EventListenerObjects
		protected var				permListeners			: Array				= [];
	
		protected var				_listenerID				: uint				= 0 ;
		protected var				_dispatcherID			: uint				= 0 ;
		
		protected static const		MAX_LISTENERS			: uint				= 1000;
		protected static const		MAX_DISPATCHERS			: uint				= 1000;
		
		/* Event */
			
			
			



		/* Checker */
		
		public var dispatchObj:Object;
		public var mouseDownTarget:DisplayObject;
		public var param:String;
		
		private var idleTimer:Timer = new Timer(30 * 1000);
		private var checkMemoryTimer:Timer = new Timer(4000);
		private var restartTimer:Timer = new Timer(90 *1000);
		
		
		public function EventsManager() 
		{
			//init();
		}
		protected function init() : void
		{
			trace("   -----------------  LOADING EVENTSMANAGER.AS ---------------------");
			//this.name = "EventManager";
			
			dispatchersArray 		= new Array ( ) ;
			listenerList			= new Array ( ) ;	
			permDispatchers 		= new Array ( ) ;	
			permListeners	 		= new Array ( ) ;	
			
			//idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _onIdleTimerComplete);
			
			checkMemoryTimer.addEventListener(TimerEvent.TIMER, _checkMemory);
			checkMemoryTimer.start();
			
			
			
			//openAppEvent.appStr = "Home";
			//dispatchEvent(openAppEvent);
			//idleTimer.addEventListener(TimerEvent.TIMER, _onIdleTimerComplete);
			//restartTimer.addEventListener(TimerEvent.TIMER, restartApp);
			//idleTimer.reset();
			//idleTimer.start();
			
			
			
			trace("   --------------- DONE LOAD EVENTMANAGER.AS ----------------------");
		}
		
		
		public function set lang(value:String):void
		{
			this._lang = value;
		}
		
		public function get lang():String
		{
			return this._lang;
		}
		
		
		protected function receiveEvent ( e : Event ) : void
		{
			//trace(">> receiveEvent:  type = " + e.type +" event from " + e.target);
			//trace(">> --------------------------------------------");
			
			dispatchObj = e.target;
			param = "";
			
			dispatchEvent( e );
		}
		
		public static function getInstance ( ) : EventManager
		{
			if ( instance == null )
			{
				allowInstantiation 		= true;
				instance 				= new EventManager ( ) ;			
				allowInstantiation 		= false;
			}
			return instance;
		}
		
		public function _checkMemory(evt:TimerEvent):void {
			//trace("System.totalMemory = " + Math.floor(System.totalMemory/10)/100 + " KB");
			checkMemoryTimer.reset();
			checkMemoryTimer.start();
			
		}
		
		
		
		public function registerDispatcher ( 	dispatchingObj 	: Object, 
												makePermanent 	: Boolean 	= false 	) : void
		{			
			
			/**
			 * Get The events
			 * getEvents ( ) is a custom method that must be in any class/obj registering as a dispatcher	
			 */
			
			var eventArray : Array  = dispatchingObj.getEvents ( ) ; 			
			/**
			 * Add the listeners
			 */			
			var len		: uint 		= eventArray.length					
			for ( var i : uint = 0 ; i < len ; i++ )
			{
				var dispatcherEventName 	: String 	= eventArray [ i ] ;		
				dispatchingObj.addEventListener( dispatcherEventName, receiveEvent )				
			}
			
			/**
			 * Save a reference in an array and check how many we have.
			 */	
			
			if ( makePermanent )
			{
				permDispatchers.push ( dispatchingObj ) ;
			}
			else
			{
				dispatchersArray.push ( dispatchingObj ) ;
				//trace ( "EventCenter : Added dispatcher", dispatchersArray ) ;
			}
			
			_dispatcherID++;			
			
			if ( _dispatcherID >= MAX_DISPATCHERS )
			{
				throw new RangeError ( this.toString() + "WARNING: there are over " + MAX_DISPATCHERS + " dispatchers registered." ) ;
			}
		}
		
		public function registerEventDispatcher ( 	dispatchingObj 	: Object, 
												 	eventType : String,
												makePermanent 	: Boolean 	= false 	) : void
		{			
			
			dispatchingObj.addEventListener( eventType, receiveEvent )				
			
			/**
			 * Save a reference in an array and check how many we have.
			 */	
			
			if ( makePermanent )
			{
				permDispatchers.push ( dispatchingObj ) ;
			}
			else
			{
				dispatchersArray.push ( dispatchingObj ) ;
				//trace ( "EventCenter : Added dispatcher", dispatchersArray ) ;
			}
			
			_dispatcherID++;			
			
			if ( _dispatcherID >= MAX_DISPATCHERS )
			{
				throw new RangeError ( this.toString() + "WARNING: there are over " + MAX_DISPATCHERS + " dispatchers registered." ) ;
			}
		}
		
		public function _onIdleTimerComplete (evt:Event):void
		{
			
			//restartTimer.reset();
			//restartTimer.start();
		}
		
		public function restartApp(evt:Event):void
		{
			//fscommand("exec", "start_app.bat");
			//fscommand("quit", "");
		}
	}
}