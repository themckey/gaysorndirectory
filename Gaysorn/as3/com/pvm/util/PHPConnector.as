﻿package com.pvm.util {
	
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.net.URLRequestMethod;
	
	import flash.events.HTTPStatusEvent;
	
	public class PHPConnector extends EventDispatcher {

		private var _url:String = '';
		
		private var _request:URLRequest = new URLRequest();
		private var _loader:URLLoader = new URLLoader();
		private var _vars:URLVariables = new URLVariables();
		
		private var _offline_url:String = '';
		private var _offline_request:URLRequest = new URLRequest();
		private var _offline_loader:URLLoader;
		private var _offline_vars:URLVariables;
		
		private var onResponseEvent:PHPConnectorEvent = new PHPConnectorEvent("response");
		private var onOfflineResponseEvent:PHPConnectorEvent = new PHPConnectorEvent("responseOffline");
		
		public function PHPConnector() {
			
			_request.data = _vars;
			
			// send data via post
			_request.method = URLRequestMethod.POST;
			_loader.addEventListener(Event.COMPLETE, onLoaded);
			_loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			_loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			
		}
		
		public function set url(theURL:String):void
		{
			this._url = theURL;
			_request.url = this._url;
		}
		
		public function set offline_url(theURL:String):void
		{
			this._offline_url = theURL;
			_offline_request = new URLRequest();
			_offline_request.url = this._offline_url;
			_offline_request.method = URLRequestMethod.POST;
			
			_offline_loader = new URLLoader();
			_offline_loader.addEventListener(Event.COMPLETE, onOfflineLoaded);
			_offline_loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			
		}
		
		public function set vars(theVariables:URLVariables):void
		{
			this._vars = theVariables;
			_request.data = _vars;
			
		}
		
		public function addVariable(theKey:String, theValue:String):void
		{
			this._vars[theKey] = theValue;
		}
		
		public function submit():void
		{
			try {
				_loader.load(_request);
			} catch (e:Error) {
				trace(e.toString());
			}
			
			submit_offline();
		}
		
		private function submit_offline() {
			_offline_request.data = this._vars;
			try {
				_offline_loader.load(_offline_request);
			} catch (e:Error) {
	
			}
		}
		
		private function onLoaded(evt:Event):void
		{
			
			var result_data:String = String(_loader.data);
			//trace("     ************** Send Data to PHP ******************       ");
			//trace("Result : "+result_data);
			onResponseEvent.data = result_data;
			dispatchEvent(onResponseEvent);
			
		}
		
		private function onOfflineLoaded(evt:Event):void
		{
			
			var result_data:String = String(_offline_loader.data);
			//trace("     ************** Send Data to PHP ******************       ");
			//trace("Offline Result : "+result_data);
			onOfflineResponseEvent.data = result_data;
			dispatchEvent(onOfflineResponseEvent);
			
		}

		private function ioErrorHandler(event:IOErrorEvent):void
		{
			//trace("ioErrorHandler: " + event);
		}
		
		 private function httpStatusHandler(event:HTTPStatusEvent):void {
            //trace("httpStatusHandler: " + event);
            //trace("status: " + event.status);
			/*
			if (event.status != 200) {
				if (this._offline_url != '') submit_offline(); 
			}
			*/
			
        }
		
		

	}
	
}
