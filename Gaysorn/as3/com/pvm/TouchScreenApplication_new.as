﻿package com.pvm {
	
	import flash.display.MovieClip;
	
	import flash.system.Capabilities;
	import flash.system.fscommand;
	
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.MouseEvent;
	
	import com.pvm.input.keyboard.TouchScreenKeyboard;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	
	import com.pvm.models.KioskInfo;
	
	import com.pvm.ui.UIView;
	import com.pvm.util.PHPConnector;
	import com.pvm.util.PHPConnectorEvent;
	import flash.net.URLVariables;
	
	import com.pvm.ViewController;
	
	public class TouchScreenApplication extends MovieClip {

		protected static var		instance				: TouchScreenApplication;
		protected static var		allowInstantiation		: Boolean			= false;
		
		
		private var _lang:String = "en";
		
		private var _kioskInfo:KioskInfo;
		
		private var _keyboard:TouchScreenKeyboard;
		
		private var _fullscreen:Boolean = true;
		private var _timeRestart:Number = 180;// Second;
		private var _timeIdle:Number = 60;
		
		public var _restartTimer:Timer;
		public var _idleTimer:Timer;
		private var _closeauto:Timer;
		
		private var _settings:Dictionary;
		private var _settingDict:Dictionary;
		private var _settingXML:XML;
		
		private var _click_on_top_right:Boolean = false;
		private var _click_on_top_left:Boolean = false;
		private var _checkCloseTimer:Timer = new Timer(5000);
		
		private var _touchscreenKeyboard:TouchScreenKeyboard = new TouchScreenKeyboard();
		
		
		
		
		
		
		/* Event */
		public function TouchScreenApplication() {
			
			instance = this;
			init();
			
			
		}
		
		public function init():void
		{
			
			setupFullscreen();
			
			this.touchScreenKeyboard = this._touchscreenKeyboard;
			touchScreenKeyboard.hideMe();
			
			this.stage.addEventListener(MouseEvent.MOUSE_DOWN, clickOnStage); 
			_checkCloseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onCheckCloseTimerComplete);
			
			
			
			onInitted();
		}
		
		
		
		public function onInitted():void
		{
			
		}
		
		public static function getInstance():TouchScreenApplication
		{
			return instance;

		}
		
		public function get kioskInfo():KioskInfo
		{
			return this._kioskInfo;
		}
		
		
		public function loadSetting(path:String):void
		{
			var urlRequest:URLRequest = new URLRequest(path);
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.load(urlRequest);
			urlLoader.addEventListener(Event.COMPLETE, onLoadSettingComplete);
			
			urlRequest = null;
		}
		
		public function onLoadSettingComplete(e:Event):void
		{
			_settingXML = XML(e.target.data);
			
			_settings = new Dictionary();
			
			/* Kiosk Information */
			_kioskInfo = new KioskInfo(_settingXML.kiosk_info);
			
			
			if (settingXML.hasOwnProperty("language")) {
				
				_settings["defaultLang"] = (settingXML.language.hasOwnProperty("@default")) ? settingXML.language.@default : "en";
				_settings["language"] = new Array();
				
				for (var l:int=0;l<settingXML.language.lang.length();l++) {
					_settings["language"].push(settingXML.language.lang[l].@key);
				}
			}
import flash.utils.Timer;
import flash.events.Event;

			if (settingXML.hasOwnProperty("project_xml")) {
				_settings["project_xml"] = settingXML.project_xml.text();
			}
			
			if (settingXML.hasOwnProperty("screensaver")) {
				_settings["screensaver_seconds_to_start"] = (settingXML.screensaver.hasOwnProperty("@seconds_to_start")) ? settingXML.screensaver.@seconds_to_start : "600";
				_settings["screensaver"] = settingXML.screensaver.text();
			}
			
			if (_settings["defaultLang"] ) this.lang = _settings["defaultLang"];
			
			
			_settingDict = new Dictionary();
			
			
			
			for each (var prop:XML in _settingXML.children()) 
			{ 
				var _name:String = prop.name();
				_settingDict[_name] = prop.text();
				
			}
		

			dispatchEvent(new Event("settingLoaded"));
		}
		
		public function get settingXML():XML
		{
			return this._settingXML;
		}
		
		public function get settings():Dictionary
		{
			return this._settings;
		}
		
		public function get settingDict():Dictionary
		{
			return this._settingDict;
		}
		public function getSetting(key:String):Object
		{
			return this._settings[key];
		}
		
		public function set lang(value:String):void
		{
			this._lang = value;
			
			for (var i:int=0;i<this.numChildren; i++ )
			{
				if (this.getChildAt(i) is ViewControllerManager) (this.getChildAt(i) as ViewControllerManager).lang = _lang;
				if (this.getChildAt(i) is UIView) (this.getChildAt(i) as UIView).lang = _lang;
				
			}
		}
		
		public function restart(evt:Event = null):void
		{
			
			// Go to restart
			
			trace("Go to restart >>>>>>>>>>>>>>> ");
			
			fscommand("exec", "start_app.exe");
			fscommand("quit", "");
			
			/*
			switch (Capabilities.playerType) {
				case 'Desktop':
					//air runtime
					break;
				case 'External' : 
					break;
				case 'PlugIn':
				case 'ActiveX':
					//Flash Player
					break;
			}*/
			
		}
		
		var timeToCount:int = 3;
		public function showCountDown(evt:Event = null):void
		{
			var secondTimer:Timer = new Timer(1000, timeToCount);
			secondTimer.addEventListener(TimerEvent.TIMER, doCountDown);
		}
		
		public function doCountDown(evt:TimerEvent):void
		{
			trace(timeToCount);
			timeToCount--;
		}
		
		public function exitApp():void
		{
			trace("exitApp");
			fscommand("quit");
			
			/*
			switch (Capabilities.playerType) {
				case 'Desktop':
					//air runtime
					break;
				case 'External' : 
					break;
				case 'PlugIn':
				case 'ActiveX':
					//Flash Player
					fscommand("quit");
					break;
			}
			*/
		}
		
		public function get idleTimer():Timer
		{
			return this._idleTimer;
		}
		

		public function set idleTime(theTime:Number):void
		{
			if (_idleTimer) _idleTimer = null;
			
			_idleTimer = new Timer(theTime);
			_idleTimer.addEventListener(TimerEvent.TIMER, onIdle);
			
			//resetIdleTimer(null);
			
			this.stage.addEventListener("mouseDown", resetIdleTimer, true);
			this.stage.addEventListener("touchBegin", resetIdleTimer, true);
			
		}
		public function set closeautoTime(theTime:Number):void{
			if (_closeauto) _closeauto = null;
			_closeauto = new Timer(theTime);
			_closeauto.addEventListener(TimerEvent.TIMER, restart);
			
			_closeauto.start();
			
			this.stage.addEventListener("mouseDown", resetCloseTimer, true);
			this.stage.addEventListener("touchBegin", resetCloseTimer, true);
		}
		
			public function onIdle(evt:TimerEvent):void
			{
				//Do something
				
			}
			
			public function resetIdleTimer(evt:Event):void
			{
				_idleTimer.reset();
				_idleTimer.start();
				
				_restartTimer.reset();
				_restartTimer.stop();
				
				
			}
			public function resetCloseTimer(evt:Event):void{
				trace("goto resetCloseTimer >>>>>>>>>>>>> ");
				_closeauto.reset();
				_closeauto.stop();
			}
		public function get getRestartTimer():Timer{
			return this._restartTimer;
		}
		public function set timeToRestart(theTime:Number):void
		{
			//_restartTimer = new Timer(theTime);
			//_restartTimer.addEventListener(TimerEvent.TIMER, showCountDown);
			
			//trace("set timertorestart >>>> ",theTime);
			if (_restartTimer) _restartTimer = null;
			
			_restartTimer = new Timer(theTime);
			_restartTimer.addEventListener(TimerEvent.TIMER, restart);
		}
		
		public function set fullscreen(theValue:Boolean):void
		{
			this._fullscreen = theValue;
			setupFullscreen();
		}
		
		public function set touchScreenKeyboard(theKeyboard:TouchScreenKeyboard):void
		{
			
			if (this._keyboard) {
				this.removeChild(_keyboard);
				_keyboard = null;
			}
			
			this._keyboard = theKeyboard;
			addChild(_keyboard);
			_keyboard.hideMe();
		}
		
		public function get touchScreenKeyboard():TouchScreenKeyboard
		{
			return _keyboard;
		}
		
		private function setupFullscreen():void
		{
			if (this._fullscreen) fscommand('fullscreen', 'true');
		}
		
		
		
		
		private function clickOnStage(evt:MouseEvent):void
		{
			if ((evt.stageX >= this.stage.stageWidth - 100) && (evt.stageY <= 100)) _click_on_top_right = true;
			else if ((evt.stageX <= 100) && (evt.stageY <= 100)) _click_on_top_left = true;
			onCloseApplication();

		}
		
		private function onCheckCloseTimerComplete(evt:TimerEvent):void
		{
			_click_on_top_left = false;
			_click_on_top_right = false;
		}
		
		
		private function onCloseApplication():void
		{
			
			if (_click_on_top_right && _click_on_top_left)
			{
				_click_on_top_left = false;
				_click_on_top_right = false;
				exitApp();
			}
			
			_checkCloseTimer.reset();
			_checkCloseTimer.start();
		}
		
	}
	
}
