﻿package com.pvm.analytics {
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.getQualifiedClassName;
	
	import com.pvm.util.PHPConnector;
	import com.pvm.util.PHPConnectorEvent;
	import com.pvm.ViewControllerManager;
	import com.pvm.ViewControllerEvent;
	
	public class AnalyticsManager {
		
		protected static var		instance				: AnalyticsManager;
		protected static var		allowInstantiation		: Boolean			= false;
		
		private var _useGoogle:Boolean = true;
		
		public static const HitTypeScreenTracking	:String = 'appview';
		public static const HitTypeEvent			:String = 'event';
		

		private var TIME_BETWEEN_VISITOR:Number;
		var loader:URLLoader;
		
		private var _applicationName:String = '';
		private var _applicationVersion:String = '';
		private var _kioskLocation:String = '';
		private var _trackingID:String = '';
		private var _lang:String = 'en';
		
		private var _guid:GUID = new GUID();
		public var _kioskKey:String;
		public var _server:String;
		public var _variables:URLVariables = new URLVariables();

		public var _phpConnector:PHPConnector;
		public var _googleAnalyticsConnector:PHPConnector;
		
		
		var sendStatusServerURL:String;
		var sendStatusServerOfflineURL:String;
		
		
		private var _statusReportTimer:Timer;
		private var _statusReportPHPConnector:PHPConnector;
		
		
		var sendPageServerURL:String;
		var sendPageServerOfflineURL:String;
		
		private var vmArray:Array;
		private var _vmReportPHPConnector:PHPConnector;
		private var vmVariables:URLVariables = new URLVariables();
		
		
		public function AnalyticsManager() {
			
			if (!allowInstantiation )			
			{
				throw new ReferenceError ( "Error: Instantiation failed. Use AnalyticsManager.getInstance() instead of new." ) ;				
			}
			else
			{
				init ( ) ;
			}
		}
		
		public function init() {
			
		}
		
		public function set xml (thePath:String):void {
			loader = new URLLoader(new URLRequest(thePath));
			loader.addEventListener(Event.COMPLETE, onLoadXMLComplete);
		}
		
		public function set lang(theLang:String):void
		{
			this._lang = theLang;
		}
		
		public static function getInstance ( ) : AnalyticsManager
		{
			if ( instance == null )
			{
				allowInstantiation 		= true;
				instance 				= new AnalyticsManager ( ) ;			
				allowInstantiation 		= false;
			}
			return instance;
		}
		
		
		public function onLoadXMLComplete(evt:Event):void
		{
			var xml:XML = new XML(evt.target.data);
			
			//Send Status
			_applicationVersion			= xml.applicationVersion.text();
			_kioskLocation				= xml.kioskLocation.text();
			//_server						= xml.server.text()+'/analytics/get';
			_trackingID 				= xml.TrackingID.text();
			TIME_BETWEEN_VISITOR		= Number(xml.timeBetweenVisitor.text()) * 1000;
			
		}
		
		

		public function onResponse(evt:PHPConnectorEvent):void
		{
			trace("response = ", evt.data);
		}
		
		/*
		 * Send Action
		 ***************************************/
		private var _currentGUID:String;
		private var _lastActionTimestamp:Number = 0;
		
		private var _timestamp:Number;
		
		public function set applicationName(value:String):void
		{
			_applicationName = value;
		}
		
		private function isFirstAction() {
			
			var date:Date = new Date();
			_timestamp = date.time;
			
			date = null;
			
			return (_timestamp - _lastActionTimestamp > TIME_BETWEEN_VISITOR) ? true : false;
		}
		 
		
		public function dispatchScreenTracking(screenName:String):void
		{
			dispatch(AnalyticsManager.HitTypeScreenTracking, screenName);
		}
		
		public function dispatchAppEvent(_category:String, _action:String, _label:String = '', _value:Number = 0):void
		{
			dispatch(AnalyticsManager.HitTypeEvent, _category, _action, _label, _value);
		}
		
		public function dispatch(hitType:String, categoryOrScreenName:String, _action:String = '', _label:String = '', _value:Number = 0):void
		{
			if (!_useGoogle && !_phpConnector) {
				_phpConnector = new PHPConnector();
				_phpConnector.url = _server;
				_phpConnector.addEventListener('response', onResponse);
			}
			
			if (_trackingID != '' && !_googleAnalyticsConnector) {
				_googleAnalyticsConnector = new PHPConnector();
				_googleAnalyticsConnector.url = 'http://www.google-analytics.com/collect';
				_googleAnalyticsConnector.addEventListener('response', onResponse);
			}
			
			/* Required Parameter */
			_variables.v = 1;
			_variables.an = _applicationName;
			_variables.av = _kioskLocation;
			_variables.cd1 = _kioskLocation;
			_variables.tid = _trackingID;
			_variables.ul = this._lang;
			
			
			switch (hitType) {
				case AnalyticsManager.HitTypeScreenTracking : 
					_variables.t = 'appview'; 
					_variables.cd = categoryOrScreenName; //Screen Name
					break;
				
				case AnalyticsManager.HitTypeEvent : 
					_variables.t = 'event';
					_variables.ec = categoryOrScreenName; //Category, Required
					_variables.ea = _action; //Action, Required
					_variables.el = _label; //Label, Required
					_variables.ev = _value; //Value, Required
					break;
			}
			
			if (isFirstAction()) {
				_variables.cid = GUID.create();
				_currentGUID = _variables.cid;
				
			} else {
				_variables.cid = _currentGUID;
				
			}
		
			if (!_useGoogle) {
				_phpConnector.vars = _variables;
				_phpConnector.submit();
			}
			
			_googleAnalyticsConnector.vars = _variables;
			_googleAnalyticsConnector.submit();

			
			_lastActionTimestamp = _timestamp;
		}
		
		

	}
	
}
