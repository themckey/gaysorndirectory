﻿package com.forviz.ui {
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.events.Event;
	
	import flash.utils.getQualifiedClassName;
	import flash.utils.getDefinitionByName;
	import flash.display.DisplayObject;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.net.URLRequest;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	import com.forviz.ui.UIButton;
	import com.forviz.ui.CGSize;
	import com.forviz.core.ContentMode;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.display.Shape;
	
	
	
	public class UICarousel extends UIView {

		//private var _frame:Rectangle;
		
		private var _contents:Sprite = new Sprite();
		//private var _contentMask:Sprite = new Sprite();
		private var _uiPageController:UIPageController;

		private var _btnPrev:UIButton;
		private var _btnNext:UIButton;
		private var _btnPages:Array;
		
		private var _disabled:Boolean = false;
		
		private var _pages:Array = new Array();
		
		private var _currentIndex:int = 0;
		
		/* Options */
		private var _start:int = 0;
		private var _horizontal:Boolean = true;
		private var _autoTimer:Timer; 
		private var _tweenTime:Number = 3;
		private var _contentMode:String = ContentMode.SCALE_ASPECT_FIT;
		
		private var _circular:Boolean = false;
		private var _animation:String = "slide";
		
		private var _treshold:Number = 100;
		
		/* Interaction */
		public var pointDown:Point = new Point();
		public var pointUp:Point = new Point();
		
		private var _diff:Number;
		private var _target:Number = 0;
		private var _easingHolder:Number = 2;
		
		private var loadQueue:Array = new Array();
		private var loadIndex:int = 0;
		
		
		/* Event */
		private var loadedAllCompleteEvent:UICarouselEvent = new UICarouselEvent("loadedAllComplete", this);
		private var draggingBeginEvent:UICarouselEvent = new UICarouselEvent("carouselDraggingBegin", this);
		private var draggingFinishEvent:UICarouselEvent = new UICarouselEvent("carouselDraggingFinish", this);
		private var carouselWillMoveEvent:UICarouselEvent = new UICarouselEvent("carouselWillMove", this);
		private var carouselMovingEvent:UICarouselEvent = new UICarouselEvent("carouselIsMoving", this);
		private var carouselDidMoveEvent:UICarouselEvent = new UICarouselEvent("carouselDidMove", this);
		private var carouselIsDraggingEvent:UICarouselEvent = new UICarouselEvent("carouselIsDragging", this);
		
		
		public function UICarousel(frame:Rectangle, theOptions:Object = null) {
			
			this.frame = frame;
			this.x = frame.x;
			this.y = frame.y;
		
			/* Get Options */
			if (theOptions) {
				if (theOptions.hasOwnProperty('auto')) this.auto = theOptions.auto;
				if (theOptions.hasOwnProperty('speed')) this.speed = theOptions.speed;
				if (theOptions.hasOwnProperty('start')) this._start = theOptions.start;
			}
			
			
			addChild(_contents);
			
			this.disabled = this._disabled; //Add Event if not disabled
			
			go(_start);
			
		}
		
		
		
		
		override protected function onConfiguredFrame(theFrame:Rectangle):void
		{
			//this.mask.visible = false;
			//this.mask = null;
			
			for (var i:int = 0; i< pages.length ;i++ )
			{
				if (_pages[i] is UIView) {
					if (_horizontal) {
						_pages[i].frame = new Rectangle(i * theFrame.width, 0, theFrame.width, theFrame.height);
					} else {
						_pages[i].frame = new Rectangle(0, i * theFrame.height, theFrame.width, theFrame.height);
					}
					
				}
			}
			
			go(this._currentIndex);
		}
		
		var page:*;
			
		public function addPage(thePage:*):void
		{
			var _frame:Rectangle;
			
			if (thePage.hasOwnProperty("stage")) {
				//Given Sprite, MovieClip, Bitmap, or other DisplayObject
				page = thePage;
				
				if (_horizontal) {
					page.x = _pages.length * frame.width;
					page.y = 0;
				} else {
					page.x = 0;
					page.y = _pages.length * frame.height;
				}
				
			}else if(getQualifiedClassName(thePage) == "String") {
				
				if (_horizontal) {
					_frame = new Rectangle(_pages.length * frame.width, 0, frame.width, frame.height);
				} else {
					_frame = new Rectangle(0, _pages.length * frame.height, frame.width, frame.height);
				}
				
				//Add Images to Queue
				
				
				page = createPageWrapper(_frame);
				
				loadQueue.push({src:thePage, target:page});
				
				//page = new UIImageView(_frame, thePage);
				//page.contentMode = this._contentMode;
				//page.addEventListener("doneLoaded", onDoneLoaded);
			}
			
			
			//Temporary for Gaysorn Project
			if (this._animation == "fade") {
				page.x = 0;
				page.y = 0;
				page.alpha = 0;
				if (_pages.length == 0) page.alpha = 1;
			}
			
			var bg:Shape = new Shape();
			bg.graphics.beginFill(0xff0000, 0);
			bg.graphics.drawRect(0, 0, frame.width, frame.height);
			bg.graphics.endFill();
			
			page.addChildAt(bg, 0);
			
			_pages.push(page);
			_contents.addChild(page);
			
			onPageAdded(page, _pages.length - 1);
			
			
			if (loadQueue.length) {
				loadImageByQueue();
			}
		}
		
		public function unload():void
		{
			var len:int = _pages.length;
			for (var i:int = 0 ; i < len ; i++ ) {
				if (_pages[i] is UIImageView) _pages[i].unload();
			}
			
			loadQueue = [];
			_pages = [];
			
			currentTarget = 0;
			
			while(_contents.numChildren) {
				_contents.removeChildAt(0);
			}
			loadIndex = 0;
		}
		
		private function createPageWrapper(_frame:Rectangle):UIImageView
		{
			var loadingPage:UIImageView = new UIImageView(_frame);
			loadingPage.contentMode = this._contentMode;
			return loadingPage;
		}
		
		private function loadImageByQueue():void
		{
			var uiImageView:UIImageView = loadQueue[loadIndex].target;
			uiImageView.transitionIn = "fade";
			uiImageView.loadImage(loadQueue[loadIndex].src);
			uiImageView.addEventListener("doneLoaded", onDoneLoaded, false, 0, true);
		}
		
		private function onDoneLoaded(evt:Event):void
		{
			loadIndex++;
			if (loadIndex < loadQueue.length) {
				loadImageByQueue();
			} else {
				onLoadedAllComplete();
				dispatchEvent(loadedAllCompleteEvent);
			}
		}
		
		public function onLoadedAllComplete():void
		{
			
		}
		
		public function onPageAdded(page:*, index:int):void
		{
			
		}
		
		public function set circular(value:Boolean):void
		{
			this._circular = value;
		}
		
		public function set disabled(value:Boolean):void
		{
			this._disabled = value;
			if (_disabled) {
				this.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
				
				if (_btnNext) _btnNext.visible = false;
				if (_btnPrev) _btnPrev.visible = false;
				
			} else {
				this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
				
				if (_btnNext) _btnNext.visible = true;
				if (_btnPrev) _btnPrev.visible = true;
			}
		}
		
		
		private function onMouseDownHandler(evt:MouseEvent):void
		{
			_diff = (_horizontal) ? _contents.x - evt.stageX :  _contents.y - evt.stageY ;
			
			pointDown.x = evt.stageX;
			pointDown.y = evt.stageY;
			
			dispatchEvent(draggingBeginEvent);
			
			updateTarget(evt);
			
			this.addEventListener(Event.ENTER_FRAME, src);
			this.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			this.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
			
			
			
		}
		
		public function get diff():Number
		{
			return _diff;
		}
		
		
		private function onMouseMoveHandler(evt:MouseEvent):void
		{
			trace (this._contents.x);
		 	updateTarget(evt);
			dispatchEvent(carouselIsDraggingEvent);
		}
		
		private function updateTarget(evt:MouseEvent):void
		{
			(_horizontal) ? _target = evt.stageX + _diff :  _target = evt.stageY + _diff;
			
		}
		
		private function src(evt:Event):void
		{
			(_horizontal) ? _contents.x += (_target - _contents.x) / _easingHolder : _contents.y += (_target - _contents.y) / _easingHolder;
			
		}
		
		private function onMouseUpHandler(evt:MouseEvent):void
		{
			if (_horizontal) {
				if (evt.stageX - pointDown.x > _treshold) {
					goPrev();
				
				}else if (evt.stageX - pointDown.x < -_treshold) {
					goNext();
				
				}else {
					go(this.currentIndex);
				}
			} else {
				if (evt.stageY - pointDown.y > _treshold) {
					goPrev();
				}else if (evt.stageY - pointDown.y < -_treshold) {
					goNext();
				} else {
					go(this.currentIndex);
				}
			}
			
			this.removeEventListener(Event.ENTER_FRAME, src);
			this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
			dispatchEvent(draggingFinishEvent);
		}
		
		public function forceMouseUp():void
		{
			
			this.removeEventListener(Event.ENTER_FRAME, src);
			this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
		}
		
		/*
		 *	Set BtnPrev / BtnNext / BtnPages
		 ******************************************************************/
		
		public function set btnPrev(thePrev:*):void
		{
			_btnPrev = setButton(thePrev);
			
			if (_btnPrev) {
				if (!_btnPrev.parent) addChild(_btnPrev);
				_btnPrev.addEventListener("tap", goPrev);
				
				if (_start == 0) _btnPrev.state = "disabled";
			}else {
				throw new ReferenceError ( "Error: UIButton btnPrev must be UIButton, DisplayObject or String") ;
			}
		}
		
		public function get btnPrev():UIButton { return this._btnPrev; }
		
		public function set btnNext(theNext:*):void
		{
			_btnNext = setButton(theNext);
			
			if (_btnNext) {
				if (!_btnNext.parent) addChild(_btnNext);
				_btnNext.addEventListener("tap", goNext);
			}else {
				throw new ReferenceError ( "Error: UIButton btnNext must be UIButton, DisplayObject or String") ;
			}
		}
		
		public function get btnNext():UIButton { return this._btnNext; }

		public function set uiPageController(thePageController:UIPageController):void
		{
			this._uiPageController = thePageController;
			this._uiPageController.carousel = this;
			
		}
		
		
		private function setButton(obj:*):UIButton
		{
			var theButton:UIButton;
			
			if (obj is UIButton) {
				theButton = obj as UIButton;
				trace("1");
			} else if (getQualifiedClassName(obj) == "UIButton" || getQualifiedClassName(obj) == "com.forviz.ui::UIButton") {
				theButton = obj as UIButton;
				trace("2");
				
			} else if (obj.hasOwnProperty("stage")) {
				//Given Sprite, MovieClip, Bitmap, or other DisplayObject
				theButton = new UIButton(new Rectangle(0, 0, obj.width, obj.height));
				theButton.x = obj.x; 
				theButton.y = obj.y;
				obj.x = 0; 
				obj.y = 0;
								trace("3");

				theButton.setBackgroundDisplayObject(DisplayObject(obj), "normal");
				
			}else if(getQualifiedClassName(obj) == "String") {
				//Given URL to image
				
				theButton = new UIButton();
				theButton.setBackgroundDisplayObject(DisplayObject(obj), "normal");
				//theButton.icon = obj as String;
				
			}
			
			return theButton;
			
		}
		
		
		/*
		 *	End of Set BtnPrev / BtnNext / BtnPages
		 ******************************************************************/
		
		/*
		 *	Set Options
		 ******************************************************************/
		public function set animation(value:String):void
		{
			this._animation = value;
			
			switch (_animation) {
				case 'fade' : 
					for (var i:int = 0; i < _pages.length; i++) {
						_pages[i].x = 0;
						_pages[i].y = 0;
					}
					break;
					
			}
		}
		
		public function set contentMode(mode:String):void
		{
			this._contentMode = mode;
		}
		
		public function set auto(autoValue:Number):void
		{
			if (autoValue) {
				
				_circular = true;
				_autoTimer = new Timer(autoValue * 1000);  // +  this._tweenTime
				
				_autoTimer.addEventListener(TimerEvent.TIMER, goNext);
			} else {
				this.stopSlide();
				_autoTimer.removeEventListener(TimerEvent.TIMER, goNext);
				_autoTimer = null;
			}
		}
		
		public function set tweenTime(theTweenTime:Number):void
		{
			this._tweenTime = theTweenTime;
		}
		
		public function set start(theStart:Number):void
		{
			this._start = theStart;
			go(this._start);
		}
		
		public function get pages():Array
		{
			return this._pages;
		}
		
		public function get currentIndex():int 
		{
			return this._currentIndex;
		}
		
		public function get contentOffsetX():Number
		{
			return _contents.x;
		}
		
		public function get content():Sprite
		{
			return this._contents;
		}
		/*
		 *	End of Set Options
		 ******************************************************************/
		
		
		
		public function playSlide():void
		{
			
			_autoTimer.reset();
			_autoTimer.start();
		}
		
		public function stopSlide():void
		{
			if (_autoTimer) {
				_autoTimer.reset();
				_autoTimer.stop();
			}
		}
		
		/*
		 *	Function 'Go'
		 ******************************************************************/
		public function goPrev(evt:Event = null):void
		{
			go(_currentIndex - 1);
		}
		
		public function goNext(evt:Event = null):void
		{
			go(_currentIndex + 1);
		}
		
		public function go(targetIndex:int = 0):void
		{
			//Validate
			if (targetIndex <= 0) targetIndex = 0;
			else if (targetIndex > _pages.length - 1) {
				targetIndex = (_circular) ? 0 : _pages.length - 1;
			}
			
			//trace("go ", targetIndex);
			
			
			
			carouselWillMoveEvent.fromIndex = _currentIndex;
			carouselWillMoveEvent.toIndex = targetIndex;
			
			carouselMovingEvent.fromIndex = _currentIndex;
			carouselMovingEvent.toIndex = targetIndex;
			
			carouselDidMoveEvent.fromIndex = _currentIndex;
			carouselDidMoveEvent.toIndex = targetIndex;
			
			
			draggingBeginEvent.fromIndex = _currentIndex;
			draggingFinishEvent.fromIndex = _currentIndex;
			
			dispatchEvent(carouselWillMoveEvent);
			
			
			//Animate
			this.doAnimation(_currentIndex, targetIndex);
			if (this._uiPageController) this._uiPageController.highlightPage(targetIndex);
			
			if (this._btnPrev) this._btnPrev.state = (targetIndex == 0 ) ? "disabled" : "normal";
			if (this._btnNext) this._btnNext.state = (targetIndex == _pages.length - 1) ? "disabled" : "normal";
			
			
			this._currentIndex = targetIndex;
			
			if (_autoTimer) this.playSlide();
			
			
		}
		
		public function doAnimation(currentTarget:int, targetIndex:int):void
		{
			
			trace("doAnimation", currentTarget, targetIndex);
			//Animate
			switch (this._animation) {
				case 'fade' : 
					if (this._pages[currentTarget]) 
						TweenMax.to(this._pages[_currentIndex], 2, {alpha:0});
					if (this._pages[targetIndex])
						TweenMax.to(this._pages[targetIndex] , this._tweenTime, {alpha : 1, overwrite:true, 
								//ease:Expo.easeOut,
								onComplete : function(){
									
									dispatchEvent(carouselDidMoveEvent);
					
								},
								onUpdate : function() {
									dispatchEvent(carouselMovingEvent);
								}
						});
					break;
					
					
					
				case 'slide' : 
				default : 
					TweenMax.to(this.content, this._tweenTime, {x : targetIndex * -frame.width, overwrite:true, 
							ease:Expo.easeOut,
							onComplete : function(){
								
								dispatchEvent(carouselDidMoveEvent);
				
							},
							onUpdate : function() {
								dispatchEvent(carouselMovingEvent);
							}
					}); 
					break;
			}
			
		}

	}
	
}
