﻿package com.forviz.ui {
	
	import flash.display.Sprite;
	
	public class UIPageController extends Sprite {

	
		private var _pageControls:Array = new Array();
		private var _carousel:UICarousel;
		
		private var _currentPageButton:UIButton;
		
		private var _start:int = 0;
		
		private var _btnWidth:Number = 0;
		private var _margin:int = 0;
		
		public function UIPageController() {
			// constructor code
		}
		
		public function set margin(value:int):void
		{
			this._margin = value;
		}
		
		public function get span():Number 
		{
			return _btnWidth + _margin;
		}
		
		public function addBtn(btnPageControl:*, btnActiveControl:* = null):void
		{
			var btn:UIButton = new UIButton();
			addChild(btn);
			
			btn.background = btnPageControl;
			if (btnActiveControl) btn.backgroundActive = btnActiveControl;
			
			addUIButton(btn);
			/*
			btn.index = _pageControls.length;
			btn.x = 30 * btn.index;
			btn.y = 0;
			btn.alpha = 0.5;
			_pageControls.push(btn);
			btn.addEventListener("tap", onTapHandler);
			*/
		}
		
		public function addUIBtn(btn:UIButton, autoPosition:String = 'left'):void
		{
			
			
			btn.index = _pageControls.length;
			_btnWidth = btn.frame.width;
			
			if (autoPosition == 'left') {
				btn.x = (btn.frame.width + _margin) * btn.index;
				btn.y = 0;
			}
			
			
			if (!btn.backgroundActive) btn.alpha = 0.3;
			
			if (btn.index == _start) {
				_currentPageButton = btn;
				setHighlightState(btn);
			}
			
			_pageControls.push(btn);
			btn.addEventListener("tap", onTapHandler);
		}
		
		public function get controls():Array
		{
			return _pageControls;
		}
		
		public function highlightPage(index:Number):void
		{
			if (_currentPageButton) setNormalState(_currentPageButton);
			
			if (index >= 0 &&  index < _pageControls.length) {
				_currentPageButton = _pageControls[index];
				setHighlightState(_currentPageButton);
			}
		}
		
		public function setNormalState(btn:UIButton):void
		{
			btn.state = "normal";
			if (!btn.backgroundActive) btn.alpha = 0.3;
			
		}
		
		public function setHighlightState(btn:UIButton):void
		{
			btn.state = "highlight";
			if (!btn.backgroundActive) btn.alpha = 1;
			
		}
		
		public function set carousel(theCarousel:UICarousel):void
		{
			this._carousel = theCarousel;
		}
		
		private function onTapHandler(evt:UIControlEvent):void
		{
			var targetIndex:int = (evt.from as UIButton).index;
			if (this._carousel) this._carousel.go(targetIndex);
		}


	}
	
}
