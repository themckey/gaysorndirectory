﻿package models {
	import com.forviz.data.models.Model;
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	
	public class ShopModel extends Model{
		
		private var _id:String;
		
		private var _nameEn:String;
		private var _nameCn:String;
		private var _nameJp:String;
		
		private var _desEn:String;
		private var _desCn:String;
		private var _desJp:String;
		
		//private var _catId:String;
		private var _catIDs:Array;
		private var _catString:String;
		
		private var _node:String;
		private var _unitNo:String;
		private var _unitKey:String;
		private var _floor:String;
		
		private var _telNo:String;
				
		private var _logoPath:String;
		private var _mediaPath:String;
		
		private var _pathMediaArr:Array = new Array();
		
		
		private var _shopMC:MovieClip;
		
		private var _shopBTN:UIButton;
		
		public function ShopModel(obj:Object) {
			// constructor code
			this.setData(obj);
			
		}
		public function setData(shopXML:Object):void
		{
			_id = shopXML.attribute('id');
			_nameEn = shopXML.name_en.text();
			_nameCn = shopXML.name_cn.text();
			_nameJp = shopXML.name_jp.text() ;
			_desEn = shopXML.description_en.text() ;
			_desCn = shopXML.description_cn.text() ;
			_desJp = shopXML.description_jp.text() ;
			_catString = shopXML.categories_id.text();
			_node = shopXML.node.text() ;
			_unitNo = shopXML.unit_no.text() ;
			_unitKey = shopXML.unit_key.text() ;
			_floor = shopXML.floor.text() ;
			_telNo = shopXML.tel.text() ;
			_logoPath = shopXML.logoURL.text() ;
			_mediaPath = shopXML.mediaURL.text() ;
			
			_pathMediaArr = _mediaPath.split(',');
			
			
			//_catIDs = getCategoryIDs(_catString);
			
		}
		
		
		
		
		public function get id ():String
		{
			return _id;
		}
		public function get nameEn ():String
		{
			return _nameEn;
		}
		public function get nameCn ():String
		{
			return _nameCn;
		}
		public function get nameJp():String
		{
			return _nameJp;
		}
		public function get desEn ():String
		{
			return _desEn;
		}
		public function get desCn ():String
		{
			return _desCn;
		}
		public function get desJp ():String
		{
			return _desJp;
		}
		public function get catId ():String
		{
			return _catId;
		}
		public function get node ():String
		{
			return _node;
		}
		public function get unitNo():String
		{
			return _unitNo;
		}
		public function get unitKey ():String
		{
			return _unitKey;
		}
		public function get floor ():String
		{
			return _floor;
		}
		public function get telNo():String
		{
			return _telNo;
		}
		public function get logoPath ():String
		{
			return _logoPath;
		}
		public function get mediaPath ():String
		{
			return _mediaPath;
		}
		public function get pathMediaArr ():Array
		{
			return _pathMediaArr;
		}
		
		public function get catString():String
		{
			return this._catString;
		}
		
		public function set catIDs(arr:Array):void
		{
			this._catIDs = arr;
		}
		
		public function get catIDs():Array
		{
			return this._catIDs;
		}
		
		public function isCategory(targetCatID:int):Boolean
		{
			if (_catIDs == null) return false;
			
			
			if (_catIDs.indexOf(targetCatID) >= 0)
				return true;
			else
				return false;
				
		}
		
		public function set shopMC(_shopMC:MovieClip):void
		{
			this._shopMC = _shopMC;
		}
		
		public function get shopMC():MovieClip
		{
			return this._shopMC;
		}
		public function set shopBTN(_shopBTN:UIButton):void
		{
			this._shopBTN = _shopBTN;
		}
		
		public function get shopBTN():UIButton
		{
			return this._shopBTN;
		}
	}
	
}
