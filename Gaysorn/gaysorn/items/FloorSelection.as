﻿package items {
	
	import flash.display.MovieClip;
	import com.greensock.TweenMax;
	
	
	public class FloorSelection extends MovieClip {
		
		public function FloorSelection() {
			// constructor code
			this._desShop.y = this._here.y;
			
			(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
			(this.getChildByName('_2_') as MovieClip).gotoAndStop(0);
			(this.getChildByName('_3_') as MovieClip).gotoAndStop(0);
			(this.getChildByName('_4_') as MovieClip).gotoAndStop(0);
			(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
		}
		
		public function showFloorDetail(youareHere:String, floorSTR:String = ''):void
		{
			if(youareHere == '2'){
				_here.y = 470.25
				if(floorSTR == 'G'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					_desShop.y = _here.y;
					
				}
				if(floorSTR == 'L'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 1.5, {y:603.95});
					
				}
				if(floorSTR == '1'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 1.5, {y:349.5});
					
					
				}
				if(floorSTR == '2'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 3, {y:228});
					
				}
				if(floorSTR == '3'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(20);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(40);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = true;
					
					TweenMax.to(_desShop, 4.5, {y:99});
					
				}
			}
			else
			{
				_here.y = 227.30;
				if(floorSTR == 'G'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 3, {y:470.25});
					
					
				}
				if(floorSTR == 'L'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 4.5, {y:603.95});
					
				}
				if(floorSTR == '1'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 1.5, {y:349.5});
				}
				if(floorSTR == '2'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(40);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(0);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = false;
					
					TweenMax.to(_desShop, 1, {y:228});
					
				}
				if(floorSTR == '3'){
					(this.getChildByName('_1') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4') as MovieClip).gotoAndStop(20);
					
					(this.getChildByName('_1_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_2_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_3_') as MovieClip).gotoAndStop(0);
					(this.getChildByName('_4_') as MovieClip).gotoAndStop(20);
					(this.getChildByName('_5_') as MovieClip).gotoAndStop(40);
					
					(this.getChildByName('__1') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__2') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__3') as MovieClip).mouseEnabled = false;
					(this.getChildByName('__4') as MovieClip).mouseEnabled = true;
					(this.getChildByName('__5') as MovieClip).mouseEnabled = true;
					
					TweenMax.to(_desShop, 1.5, {y:99});
					
				}
			}
			
			
			
			
			
		}
	}
	
}
