﻿package items {
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class SelectionExtention extends MovieClip{

		public function SelectionExtention() {
			// constructor code
			this.alpha = 0;
			this.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		public function onDown(e:MouseEvent):void
		{
			this.alpha = 0.5;
		}
		public function onUp(e:MouseEvent):void
		{
			this.alpha = 0;
		}

	}
	
}
