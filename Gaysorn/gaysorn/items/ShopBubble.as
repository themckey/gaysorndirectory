﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UIImageView;
	import com.greensock.TweenMax;
	import com.forviz.ui.UICarousel;
	import flash.text.TextField;
	
	
	public class ShopBubble extends MovieClip {
		var uiImage:UIImageView;
		var picArr:Array = new Array();
		var str:String;
		var fadeImgArr:Array = new Array();
		
		var uiCarousel:UICarousel;
		
		public function ShopBubble() {
			// constructor code
			
			uiCarousel = new UICarousel(new Rectangle(-148.50,17,300,150));
			uiCarousel.animation = "fade";
			_pane.addChild(uiCarousel);
			
			onTranslate(langs);
			
		}
		var aZarr:Array = new Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '(', ')'
									,'-', '_', '+');
		var checkString:Array = "abcdefghijklmnopqrstuvwxyz()-/_+".split();
		var txt:String;
		public function set model(theModel:ShopModel):void
		{
			hide();
			_pane._names.text = theModel.nameEn;
			
			_pane._des_en.text = theModel.desEn;
			_pane._des_cn.text = theModel.desCn;
			
			_pane._des_jp.text = theModel.desJp.substr(0,45) + ' ...';
			if(_pane._des_jp.text == ' ...')_pane._des_jp.text = 'Coming Soon';
			if(theModel.desJp == '')_pane._des_jp.text = '';
			while( _pane._des_en.numLines > 3 )
			{
				txt = _pane._des_en.getLineText(_pane._des_en.numLines-1);
				_pane._des_en.replaceText(_pane._des_en.text.length - txt.length,_pane._des_en.text.length, " ");
				var numLines:int = _pane._des_en.numLines;
				
				var str:String = (_pane._des_en.text as String).slice( 0, -1 );
				var trunc:String = str.slice(0, str.lastIndexOf(" "));
				var trunc2:String = trunc.slice(0, trunc.lastIndexOf(" "));
				if(_pane._des_en.text != 'Coming soon')
				{
					_pane._des_en.text = trunc2 + ' ...';
				}
					
				
			}
			if(_pane._des_cn.numLines > 3)
			{
				while( _pane._des_cn.numLines > 3 )
				{
					txt = _pane._des_cn.getLineText(_pane._des_cn.numLines-1);
					_pane._des_cn.replaceText(_pane._des_cn.text.length - txt.length,_pane._des_cn.text.length, " ");
					
					var strcn:String = (_pane._des_cn.text as String).slice( 0, -1);
					var trunc3:String = strcn.slice(0, -1);
					var trunc4:String = trunc3.slice(0, trunc3.lastIndexOf(" "));
					
					for(var i:int = 0; i<aZarr.length; i++)
					{
						if(aZarr[i].toString().indexOf(getLastCharInString(strcn).toLowerCase()) == 0)
						{
							strcn = trunc4;
						}
						else
						{
							strcn = trunc3;
						}
					}
					
				}
				_pane._des_cn.text = strcn+' ...';	
			}
			else
			{
				_pane._des_cn.text = theModel.desCn;
			}
			
			if(_pane._des_en.text == 'Coming soon')
			{
				_pane._des_cn.text = 'Coming Soon';
			}
			
			
			
			picArr = theModel.pathMediaArr;
			_loadImage();
			
			
		}
		
		function getLastCharInString(s:String):String
		{
			return s.substr(s.length-1,s.length);
		}
		
		
		function _loadImage() : void {
			
			uiCarousel.unload();
 
	   		for(var i:int = 0; i < picArr.length; i++)
			{
				
				uiCarousel.addPage(picArr[i].toString());
			}
			
			
			
			
		}
		
		public function show():void
		{
			this.visible = true;
			uiCarousel.auto = 4;
			uiCarousel.playSlide();
		}
		
		public function hide():void
		{
			this.visible = false;
			uiCarousel.stopSlide();
		}
		var langs:String = 'en';
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			if(langs == 'en')
			{
				_pane._des_en.visible = true;
				_pane._des_cn.visible = false;
				_pane._des_jp.visible = false;
				
				_pane._moreInfoBTN._en.visible = true;
				_pane._moreInfoBTN._cn.visible = false;
				_pane._moreInfoBTN._jp.visible = false;
				
			}
			if(langs == 'cn')
			{
				_pane._des_en.visible = false;
				_pane._des_cn.visible = true;
				_pane._des_jp.visible = false;
				
				_pane._moreInfoBTN._en.visible = false;
				_pane._moreInfoBTN._cn.visible = true;
				_pane._moreInfoBTN._jp.visible = false;
				

			}
			if(langs == 'jp')
			{
				_pane._des_en.visible = false;
				_pane._des_cn.visible = false;
				_pane._des_jp.visible = true;
				
				_pane._moreInfoBTN._en.visible = false;
				_pane._moreInfoBTN._cn.visible = false;
				_pane._moreInfoBTN._jp.visible = true;
				

			}
		}
		
		
		
		
	}
	
}
