﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIControl;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	
	public class BackBTN extends UIButton {
		var langs:String = 'en';
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		public function BackBTN() {
			// constructor code
			super(theFrame);
			
			
		}
		
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			if(langs == 'en')
			{
				this._en.visible = true;
				this._cn.visible = false;
				this._jp.visible = false;
			}
			if(langs == 'cn')
			{
				this._en.visible = false;
				this._cn.visible = true;
				this._jp.visible = false;
			}
			if(langs == 'jp')
			{
				this._en.visible = false;
				this._cn.visible = false;
				this._jp.visible = true;
			}
		}
	}
	
}
