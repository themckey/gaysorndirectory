﻿package items.floorplan
{

	import flash.display.MovieClip;
	import com.forviz.ui.UIImageView;
	import flash.geom.Rectangle;
	import manager.EventManagers;
	import manager.LGNDataManager;
	import events.TouchScreenEvent;
	import flash.events.Event;
	import com.greensock.TweenMax;


	public class ToolTip extends MovieClip
	{

		public var uiimage:UIImageView;
		public var xmllist:XMLList = new XMLList();
		var shopname_:String;
		var _langs:String = 'en';
		var difWidth:Number;

		public function ToolTip()
		{
			// constructor code

			_thenameth_short.visible = false;
			_thenameth_long.visible = false;
			_thenameen.visible = true;
			LGNDataManager.getInstance().addEventListener('dataInitted', loadData);
			EventManagers.getInstance().addEventListener('tap', onTap);

		}
		public function loadData(e:Event = null):void
		{

		}
		public function addLogo(strname:String, _floor:String = ''):void
		{
			//ส่งมาชื่อชั้น จะมี F มาด้วย  >>>>>>>_floor

			xmllist = LGNDataManager.getInstance().shopXMLList.(floor == _floor);

			for (var i:int = 0; i < xmllist.length(); i++)
			{
				if (xmllist[i].instance_name == strname)
				{

					var shopname_:String = xmllist[i].name.text.(@lang == 'en');
					var shopname_th:String = xmllist[i].name.text.(@lang == 'th');

				}


			}

			_thenameen.htmlText = shopname_;
			_thenameth_short.htmlText = shopname_th;
			_thenameth_long.htmlText = shopname_th;

			if (_thenameth_long.textWidth > 88.5)
			{
				_thenameth_short.visible = false;
				_thenameth_long.visible = true;
				moveTextName();

			}
			else
			{
				_thenameth_short.visible = true;
				_thenameth_long.visible = false;

			}

			moveTF();


		}
		public function addLogoById(_name:String = ''):void
		{

			xmllist = LGNDataManager.getInstance().shopXMLList.(attribute('id') == _name);;

			for (var i:int = 0; i < xmllist.length(); i++)
			{

				shopname_ = xmllist[i].name.text.(@lang == 'en').toString();
				var shopname_th:String = xmllist[i].name.text.(@lang == 'th');


			}


			_thenameen.htmlText = shopname_;
			_thenameth_short.htmlText = shopname_th;
			_thenameth_long.htmlText = shopname_th;
			if (_thenameth_long.textWidth > 88.5)
			{
				_thenameth_short.visible = false;
				_thenameth_long.visible = true;
				moveTextName();

			}
			else
			{
				_thenameth_short.visible = true;
				_thenameth_long.visible = false;

			}

			moveTF();


		}

		private function moveTextName():void
		{


			TweenMax.to(_thenameth_long, 2.5, {x:(-50.65)+(-(_thenameth_long.textWidth - 50)), delay:1.5, onComplete:moveBack});


		}

		private function moveBack():void
		{
			moveTextName();
			TweenMax.to(_thenameth_long, 0.1, {x:-50.65, delay:1.5, onComplete:moveTextName});
		}
		private function moveTF():void
		{
			if (_thenameen.length <= 12)
			{
				_thenameen.y = -120;
			}
			if (_thenameen.length >= 13)
			{
				_thenameen.y = -133;
			}
			if (_thenameen.length >= 21)
			{
				_thenameen.y = -140;
			}


		}
		public function onTap(e:TouchScreenEvent):void
		{

		}

		public function translates(thelang:String):void
		{
			if (thelang == 'en')
			{
				_thenameth_short.visible = false;
				_thenameth_long.visible = false;
				_thenameen.visible = true;

			}
			else
			{
				if (_thenameth_long.textWidth > 88.5)
				{
					_thenameth_short.visible = false;
					_thenameth_long.visible = true;
					_thenameen.visible = false;
					moveTextName();

				}
				else
				{
					_thenameth_short.visible = false;
					_thenameth_long.visible = true;
					_thenameen.visible = false;

				}
				
			}



		}
	}

}