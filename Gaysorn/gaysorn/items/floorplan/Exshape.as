﻿package  items.floorplan{
	import flash.display.MovieClip;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.net.URLLoader;
	
	import flash.events.MouseEvent;
	import flash.display.Loader;
	import flash.filters.DropShadowFilter;
	import flash.filters.*;
	import flash.display.Sprite;
	import itemMC.Floormcitem;
	import itemMC.Youareheremc;
	import flash.utils.Dictionary;
	
	public class Exshape extends MovieClip{
		
		var IMAGE_WIDTH:uint = 1480;
		var IMAGE_HEIGHT:uint = 585;
		
		var imgurl:URLRequest = new URLRequest()
		var loadedimgs:uint = 0;
		var countload:int = 0;
		var images_num = 0;
		var imageHolders:Array = new Array();
		 
		//Set the focal length
		var focalLength:Number = 4000; //500
		 
		//Set the vanishing point
		
		var vanishingPointX:Number = 1080 / 2;
		var vanishingPointY:Number = 1920 / 4;
		
		//The 3D floor for the images
		var floor:Number = 40; //40
		 
		//We calculate the angleSpeed in the ENTER_FRAME listener
		var angleSpeed:Number = 0;
		 
		//Radius of the circle
		var radius:Number = 700; // related with scale
		var radiusX:Number = 700; // 120 pin add more
		
		var xmlLoader:URLLoader = new URLLoader();
		var xmlData:XML = new XML();
		
		var xmlLoaderShop:URLLoader = new URLLoader();
		var xmlDataShop:XML = new XML();
		
//============================================== pin add more
		var hitSp:Sprite = new Sprite();
		var nowi:int = 0;
		var hitMC:MovieClip = new MovieClip();
		var nowcurrentAngle:int = 0;
		var nowangle0:int = 0;
		var nowangle1:int = 0;
		var nowangle2:int = 0;
		var nowangle3:int = 0;
		var nowangle4:int = 0;
		var nowangle5:int = 0;
		var nowangle6:int = 0;
		public var nowitem:int = 0;
		public var beforemoveitem:int = 0;
		
		var oriangle:int = 0;
		var foundright:Boolean = false;
		
		var dictShopwithflag:Dictionary = new Dictionary();
//============================================== pin add more

		var facilityDict:Dictionary = new Dictionary();
		var facilityArray:Array = new Array();

		public function Exshape() {
			// constructor code
			
			xmlLoaderShop.load(new URLRequest("content/xml/Shoplistbeflag.xml"));
			xmlLoaderShop.addEventListener(Event.COMPLETE, LoadXMLShop);
			
// ================ pin add more ==========================================
			hitSp.graphics.beginFill(0xff0000, 0);
			hitSp.graphics.drawRect(0,0, 1080, 555);
			hitSp.graphics.endFill();
			hitMC.addChild(hitSp);
			hitMC.y =  555/2;
			addChild(hitMC);
			
			
			hitMC.addEventListener(MouseEvent.MOUSE_OVER, showRotate);
			hitMC.addEventListener(MouseEvent.MOUSE_OUT, hideRotate);
			
// ================ pin add more ==========================================			
			
		}
		public function showRotate(evt:MouseEvent = null):void{
			
			addEventListener(Event.ENTER_FRAME, rotateCarousel);
			
		}
		public function hideRotate(evt:MouseEvent = null):void{
			removeEventListener(Event.ENTER_FRAME, rotateCarousel);
		}
		public function LoadXMLShop(evt:Event):void{
			xmlDataShop = new XML(evt.target.data);
			
			var shopxmlflag:XMLList = xmlDataShop.flaglist;
			
			for (var i :int = 0; i < shopxmlflag.length(); i++){
				
				var shoparray:Array = new Array();
				for (var j:int = 0; j < shopxmlflag[i].shopname.split(",").length; j++){
					shoparray.push(shopxmlflag[i].shopname.split(",")[j].toString());
					
				}
				dictShopwithflag[i.toString()] = shoparray;
			}
			
			xmlLoader.load(new URLRequest("content/xml/carousel.xml"));
			xmlLoader.addEventListener(Event.COMPLETE, LoadXML);
		}
		public function LoadXML(e:Event):void {
			xmlData = new XML(e.target.data);
			trace(xmlData);
			Parseimage(xmlData);
		}
		function Parseimage(imageinput:XML):void
		{
	
			var imageurl:XMLList = imageinput.image.iurl;
		
			images_num = imageurl.length();
			for (var i:int = 0; i < images_num; i++) {
				var urlElement:XML = imageurl[i];
				
				var imageHolder:MovieClip = new MovieClip();
				var imageinside:Floormcitem = new Floormcitem();
				var imageLoader = new Loader();
				imageinside.addChild(imageLoader);
				imageinside.mouseChildren = false;
				imageLoader.x = - (IMAGE_WIDTH / 2) - 135;
				imageLoader.y = - (IMAGE_HEIGHT / 2);
				imageHolders.push(imageHolder);
				imageLoader.name = i.toString(); 
				imageLoader.load(new URLRequest(imageurl[i]));
				imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
				imageinside.name = i.toString();
				imageHolder.name = i.toString();
				imageHolder.addChild(imageinside);
				
				var myShadow:DropShadowFilter = new DropShadowFilter();
				myShadow.distance = 35;
				myShadow.color = 0x545044; //0x000000 0x1b1b1b 0x111111
				myShadow.blurX = 100;
				myShadow.blurY = 100;
				myShadow.quality = 8;
				imageinside.filters = [myShadow];
		
				
				
			}
		}
		
		private function imageLoaded(e:Event):void {
		 
			loadedimgs++;
			
			var mc:MovieClip = e.target.content;
			
			for(var i:int = 0; i < mc.numChildren; i++){
				
				if(mc.getChildAt(i).name == "shops_mc"){
					
					for (var p:int = 0; p < (mc.getChildAt(i) as MovieClip).numChildren; p++){
						
						if((dictShopwithflag[(0).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else if((dictShopwithflag[(1).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else if((dictShopwithflag[(2).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else if((dictShopwithflag[(3).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else if((dictShopwithflag[(4).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else if((dictShopwithflag[(5).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else if((dictShopwithflag[(6).toString()] as Array).indexOf((mc.getChildAt(i) as MovieClip).getChildAt(p).name.toString()) >= 0){
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = true;
						}else{
							(mc.getChildAt(i) as MovieClip).getChildAt(p).visible = false;
						}
					}
					
				}else if(mc.getChildAt(i).name == "pathMap"){
					mc.getChildAt(i).visible = false;
				}else if(mc.getChildAt(i).name == "nodeMap"){
					mc.getChildAt(i).visible = false;
				}else if (mc.getChildAt(i).name == "_facility"){
					facilityDict[loadedimgs] = mc.getChildAt(i);
					facilityArray.push(mc.getChildAt(i));
				}
			}
			
			countload += 1;
			
			
			if (loadedimgs == images_num) {
		 
				initializeCarousel();
			}
		}
		public function set showFacility(str:String):void{
			
			for (var i:int = 0; i < facilityArray.length; i++){
				for (var j:int = 0; j < (facilityArray[i] as MovieClip).numChildren; j++){
					if (facilityArray[i].getChildAt(j).name == "_"+str){
						facilityArray[i].getChildAt(j).visible = true;
						facilityArray[i].getChildAt(j).scaleX = facilityArray[i].getChildAt(j).scaleY = 0.8;
					}else{
						facilityArray[i].getChildAt(j).visible = false;
						facilityArray[i].getChildAt(j).scaleX = facilityArray[i].getChildAt(j).scaleY = 0.5;
					}
				}
			}
		}
		public function setyouarehere(floorkey:String, intx:String, inty:String, radar:String):void{
			
			if (floorkey == "G"){
				this.Setitem = 0;
			}else{
				this.Setitem = int(floorkey);
			}
			
			
			for (var i:uint = 0; i < imageHolders.length; i++) {
				if (imageHolders[i].name == floorkey){
					var youarehere:Youareheremc = new Youareheremc();
					youarehere.x = int(intx);
					youarehere.y = int(inty);
					youarehere.scaleX = youarehere.scaleY = 1; //0.8
					youarehere._radarmc._radar.rotation = int(radar);
					imageHolders[i].getChildAt(0).getChildAt(0).getChildAt(0).addChild(youarehere);
				}
				
			}
			
			
		}
		
		function initializeCarousel():void {
		 
			//Calculate the angle difference between the images (in radians)
			var angleDifference:Number = Math.PI * (360 / images_num) / 180;
			
			//Loop through the images
			for (var i:uint = 0; i < imageHolders.length; i++) {
		 
				//Assign the imageHolder to a local variable
				var imageHolder:MovieClip = (MovieClip)(imageHolders[i]);
		 
				//Get the angle for the image (we space the images evenly)
				var startingAngle:Number = angleDifference * i;
		 
				//Position the imageHolder
				imageHolder.xpos3D = radiusX * Math.cos(startingAngle);
				imageHolder.zpos3D = radius * Math.sin(startingAngle);
				imageHolder.ypos3D = floor;
				
				
				//Set a "currentAngle" attribute for the imageHolder
				imageHolder.currentAngle = startingAngle;
				if (i == 0){
					nowangle0 = imageHolder.currentAngle;
				}else if (i == 1){
					nowangle1 = imageHolder.currentAngle;
				}else if (i == 2){
					nowangle2 = imageHolder.currentAngle;
				}else if (i == 3){
					nowangle3 = imageHolder.currentAngle;
				}else if (i == 4){
					nowangle4 = imageHolder.currentAngle;
				}else if (i == 5){
					nowangle5 = imageHolder.currentAngle;
				}else if (i == 6){
					nowangle6 = imageHolder.currentAngle;
				}
		 
				//Calculate the scale ratio for the imageHolder (the further the image -> the smaller the scale)
				var scaleRatio = 0.75;//focalLength/(focalLength + imageHolder.zpos3D); //0.5
				
				//Scale the imageHolder according to the scale ratio
				imageHolder.scaleX = imageHolder.scaleY = scaleRatio;
		 
				//Set the alpha for the imageHolder
				imageHolder.alpha = 1; //0.8
		 
				//We want to know when the mouse is over and out of the imageHolder
				imageHolder.addEventListener(MouseEvent.MOUSE_OVER, mouseOverImage);
				imageHolder.addEventListener(MouseEvent.MOUSE_OUT, mouseOutImage);
		 
				//We also want to listen for the clicks
				imageHolder.addEventListener(MouseEvent.CLICK, imageClicked);
		 
				//Position the imageHolder to the stage (from 3D to 2D coordinates)
				imageHolder.x = vanishingPointX + imageHolder.xpos3D * scaleRatio;
				imageHolder.y = vanishingPointY + imageHolder.ypos3D * scaleRatio;
		 
				//Add the imageHolder to the stage
				addChild(imageHolder);
			}
		 
			//Add an ENTER_FRAME for the rotation
			addEventListener(Event.ENTER_FRAME, rotateCarousel);
			
			dispatchEvent(new Event("completeload"));
		}
		public function set Setitem(ints:int):void{
			foundright = false;
			
			while(goRightrotateCarousel(ints) != true){
				goRightrotateCarousel(ints);
			}
			
		}
		public function goRightrotateCarousel(targetitem:int):Boolean{
			angleSpeed = (950 - vanishingPointX) / 4096;
			
			for (var i:uint = 0; i < imageHolders.length; i++) {
		 
				//Assign the imageHolder to a local variable
				var imageHolder:MovieClip = (MovieClip)(imageHolders[i]);
		 
				//Update the imageHolder's current angle
				imageHolder.currentAngle += angleSpeed;
				
		 
				//Set a new 3D position for the imageHolder
				imageHolder.xpos3D=radiusX*Math.cos(imageHolder.currentAngle);
				imageHolder.zpos3D=radius*Math.sin(imageHolder.currentAngle);
		 		imageHolder.ypos3D= 0.2 * radius*Math.sin(imageHolder.currentAngle);
//		 		imageHolder.alpha = ( 0.8 * (1 + Math.sin(Math.PI + imageHolder.currentAngle)) ) - 0.6;
		 		imageHolder.alpha = Math.sin(Math.PI + imageHolder.currentAngle);
				
				//Calculate a scale ratio
				var scaleRatio = 0.75;//focalLength/(focalLength + imageHolder.zpos3D); //0.5
				
		 
				//Scale the imageHolder according to the scale ratio
		 		imageHolder.scaleX=imageHolder.scaleY= (0.3 * Math.sin(Math.PI + imageHolder.currentAngle)) + 0.5;
		 
				//Update the imageHolder's coordinates
				imageHolder.x=vanishingPointX+imageHolder.xpos3D*scaleRatio;
				imageHolder.y=vanishingPointY+imageHolder.ypos3D*scaleRatio;
			}
		 
			//Call the function that sorts the images so they overlap each others correctly
			sortZ();
			
			if ((imageHolders[6].name == targetitem.toString()) && ((Math.floor(imageHolders[6].zpos3D)) == -700)){
				foundright = true;
				hideRotate();
			}
			
			return foundright;
			
			
		}
		function rotateCarousel(e:Event):void {
		 
			//Calculate the angleSpeed according to mouse position
			angleSpeed = (mouseX - vanishingPointX) / 2048; //4096 2048 5120 1024
			//trace("-------------------- >>>>>>>>>>>>>>>>>>>>>>>>>>>> ------------------------------------------------------");
			//trace("mouseX >>>>>>>>>>>>>>>>>>>>>> ",mouseX.toString());
		 
			//Loop through the images
			for (var i:uint = 0; i < imageHolders.length; i++) {
		 
				//Assign the imageHolder to a local variable
				var imageHolder:MovieClip = (MovieClip)(imageHolders[i]);
		 
				//Update the imageHolder's current angle
				imageHolder.currentAngle += angleSpeed;
				if (i == 0){
					nowangle0 = imageHolder.currentAngle;
				}else if (i == 1){
					nowangle1 = imageHolder.currentAngle;
				}else if (i == 2){
					nowangle2 = imageHolder.currentAngle;
				}else if (i == 3){
					nowangle3 = imageHolder.currentAngle;
				}else if (i == 4){
					nowangle4 = imageHolder.currentAngle;
				}else if (i == 5){
					nowangle5 = imageHolder.currentAngle;
				}else if (i == 6){
					nowangle6 = imageHolder.currentAngle;
				}
		 
				//Set a new 3D position for the imageHolder
				imageHolder.xpos3D=radiusX*Math.cos(imageHolder.currentAngle);
				imageHolder.zpos3D=radius*Math.sin(imageHolder.currentAngle);
		 		imageHolder.ypos3D= 0.2 * radius*Math.sin(imageHolder.currentAngle);
//		 		imageHolder.alpha = ( 0.8 * (1 + Math.sin(Math.PI + imageHolder.currentAngle)) ) - 0.6;
		 		imageHolder.alpha = Math.sin(Math.PI + imageHolder.currentAngle);
				
				//Calculate a scale ratio
				var scaleRatio = 0.75;//focalLength/(focalLength + imageHolder.zpos3D); //0.5
				
		 
				
		 		imageHolder.scaleX=imageHolder.scaleY= (0.3 * Math.sin(Math.PI + imageHolder.currentAngle)) + 0.5;
		 
				//Update the imageHolder's coordinates
				imageHolder.x=vanishingPointX+imageHolder.xpos3D*scaleRatio;
				imageHolder.y=vanishingPointY+imageHolder.ypos3D*scaleRatio;
				
				
			}
		 
			//Call the function that sorts the images so they overlap each others correctly
			sortZ();
			
			//trace("-------------------- >>>>>>>>>>>>>>>>>>>>>>>>>>>> ------------------------------------------------------");
			
		}
		 
		//This function sorts the images so they overlap each others correctly
		function sortZ():void {
		 
			
			imageHolders.sortOn("zpos3D", Array.NUMERIC | Array.DESCENDING);
		 
			//Set new child indexes for the images
			for (var i:uint = 0; i < imageHolders.length; i++) {
				setChildIndex(imageHolders[i], i);
				
				if (i == 6){
						nowitem = imageHolders[i].name;
						if (beforemoveitem != imageHolders[i].name){
							dispatchEvent(new Event("sendnowitem"));
						}
						beforemoveitem = nowitem;
				}
				
				
			}
		}
		 
		//This function is called when the mouse is over an imageHolder
		function mouseOverImage(e:Event):void {
		 
			
			e.target.alpha=1;
			nowi = int(e.target.name);
			
			
		}
		 
		//This function is called when the mouse is out of an imageHolder
		function mouseOutImage(e:Event):void {
		 
			
		}
		
		//This function is called when an imageHolder is clicked
		function imageClicked(e:Event):void {
		 
			
		}

	}
	
}
