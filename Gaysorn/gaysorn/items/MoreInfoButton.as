﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	
	public class MoreInfoButton extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width,this.height);
		public function MoreInfoButton() {
			// constructor code
			super(theFrame);
		}
	}
	
}
