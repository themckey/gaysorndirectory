﻿package viewcontroller {
	import com.forviz.ViewController;
	
	public class RatchaprasongViewcontroller extends ViewController{
		
		var langs:String = 'en';
		public function RatchaprasongViewcontroller() {
			// constructor code
			
			onTranslate(langs);
			
			
		}
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			if(langs == 'en')
			{
				this._en.visible = true;
				this._cn.visible = false;
				this._jp.visible = false;
				
				this._enDes.visible = true;
				this._cnDes.visible = false;
				this._jpDes.visible = false;
			}
			if(langs == 'cn')
			{
				this._en.visible = false;
				this._cn.visible = true;
				this._jp.visible = false;
				
				this._enDes.visible = false;
				this._cnDes.visible = true;
				this._jpDes.visible = false;
			}
			if(langs == 'jp')
			{
				this._en.visible = false;
				this._cn.visible = false;
				this._jp.visible = true;
				
				this._enDes.visible = false;
				this._cnDes.visible = false;
				this._jpDes.visible = true;
				
			}
		}

	}
	
}
