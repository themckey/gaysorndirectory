﻿package viewcontroller
{
	import com.forviz.ViewController;
	import items.floorplan.FloorplanCoverflow;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import maps.CustomMapsMC;
	import items.NextBTN;
	import items.PrevBTN;
	import com.forviz.ui.UIControlEvent;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import com.greensock.TweenMax;
	import items.ShowShopnameMC;
	import flash.events.MouseEvent;
	import com.greensock.easing.Expo;
	import items.SkipButton;
	import com.forviz.ui.UIControl;
	import com.jmx2.delayedFunctionCall;
	import models.ShopModel;
	import items.BackBTN;
	import events.FacilityEvent;

	public class DirectoryViewController extends ViewController
	{
		public var floorOptionCoverFlow:FloorplanCoverflow = new FloorplanCoverflow();
		
		var mapMC:CustomMapsMC = new CustomMapsMC();
		var animationFloorplanMC:MovieClip = new MovieClip();
		var animationFloorplanMC4:MovieClip = new MovieClip();
		var animationLoader:Loader = new Loader();
		var animationLoader2:Loader = new Loader();
		var extSwf:MovieClip;
		var extSwf2:MovieClip;
		
		var nextBTN:NextBTN = new NextBTN();
		var prevBTN:PrevBTN = new PrevBTN();
		var skipButton:SkipButton = new SkipButton();
		
		var showShopName:ShowShopnameMC = new ShowShopnameMC();
		
		
		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();

		var visitfloor:String;
		var visitnode:String;
		var youarehereFloor:int = 0;
		var youarehereNode:String;
		
		var visitfloorNumBer:String;
		
		var shopDetail:ShopDeatailViewController = new ShopDeatailViewController();
		
		var liftbtn:MovieClip = new MovieClip();
		var backbtn:BackBTN = new BackBTN();
		
		public function DirectoryViewController()
		{
			animationFloorplanMC.y = 0;
			addChild(animationFloorplanMC);
			animationFloorplanMC.addChild(animationLoader);
			
			
			this.addEventListener(MouseEvent.CLICK, onClickMainDirectView);
			
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
		}
		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereNode = GaysornDataManager.getInstance().configsXML.start_node.text();
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor.text();
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			this.floorProperty.addEventListener('chosefacility', facilityChoose);
			
			if(youarehereFloor == '4')
			{
				animationLoader.load(new URLRequest('content/GoUp4.swf'));
				
			}
			else
			{
				animationLoader.load(new URLRequest('content/GoUp.swf'));
			
			}
			animationLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, animationLoaded);


		}
		public function setDefaultDirectoryViewController():void
		{
			addChild(floorOptionCoverFlow);
			floorOptionCoverFlow.addEventListener('ClickMoreInfo', getShopInfo);
			floorOptionCoverFlow.addEventListener('setFirstFloorDefault', checkIndex);
			this.name = 'floorOptionCoverFlow';
			
			
			nextBTN.x = 1770;
			nextBTN.y = 550;
			addChild(nextBTN);
			nextBTN.addEventListener('tap', onTapNextBTN);
			
			prevBTN.x = 30;
			prevBTN.y = 550;
			addChild(prevBTN);
			prevBTN.addEventListener('tap', onTapPrevBTN);
			
			mapMC.x = 585;
			mapMC.y = 150;
			addChild(mapMC);
			mapMC.alpha = 0;;
			mapMC.addEventListener('endRouting', onRoutingEnd);
			mapMC.addEventListener('beforeRouting', beforeRouting);
			
			
			addChild(shopDetail);
			TweenMax.to(shopDetail,0.5,{autoAlpha:0});	
			TweenMax.to(shopDetail.floorselection,0.1,{x:1950,autoAlpha:0});
			
			backbtn = shopDetail.getChildByName('_back');
			shopDetail.addChild(backbtn);
			backbtn.addEventListener('tap', onTapBackBTNHandler);
			backbtn.visible = false;
			
			
		}
		
		
		public function onClickMainDirectView (e:Event):void
		{
			if(e.target.name == 'floorOptionCoverFlow')floorOptionCoverFlow.hideAllShop();
			if(e.target.name.substr(0,4) == 'inst')floorOptionCoverFlow.hideAllShop();
			
		}
		public function scaleFloorOptionCoverFlow(_scale:String):void
		{
			if(_scale == 'in')
			{
				showfloorOptionCoverFlowProperty();
				TweenMax.to(prevBTN, 1, {y:550, ease:Expo.easeOut});
				TweenMax.to(nextBTN, 1, {y:550, ease:Expo.easeOut});
				TweenMax.to(floorOptionCoverFlow, 1, {autoAlpha:1, x:0, y:0, scaleX:1,scaleY:1, ease:Expo.easeOut});
			}
			if(_scale == 'out')
			{
				floorProperty.visible = false;
				gaysornLogo.visible = false;
				TweenMax.to(prevBTN, 1, {y:400, ease:Expo.easeOut});
				TweenMax.to(nextBTN, 1, {y:400, ease:Expo.easeOut});
				TweenMax.to(floorOptionCoverFlow, 1, {autoAlpha:1, x:140 ,y:0, scaleX:0.857, scaleY:0.857, ease:Expo.easeOut});
				
			}
			
		}
		public function animationLoaded (e:Event):void
		{
			extSwf = animationLoader.content as MovieClip;
			addChild(extSwf);
			extSwf.y = -20;
			extSwf.gotoAndStop(0);
			TweenMax.to(extSwf, 0.5, {autoAlpha:0});
			addChild(showShopName);
			showShopName.visible = false;
			
			
		}
		
		public function clearStage():void
		{
			mapMC.alpha = 0;
			mapMC.clearPath();
			mapMC.unHilightAll();
			mapMC.mouseChildren = false;
			mapMC.mouseEnabled = false;
			
			
			floorOptionCoverFlow.hideAllShop();
			trace ('clearStage');
			if (extSwf) extSwf.visible = false;
			showShopName.visible = false;
			
			shopDetail.visible = false;
			shopDetail.floorselection.visible = false;
			
			
			showfloorOptionCoverFlowProperty();
			
			
		}
		public var isDone:Boolean = true;
		
		private function onTapSkipHandler(e:UIControlEvent):void
		{
			isDone = true;
			_onShowShopDetail();
				//mapMC.clearPath();
				if(youarehereFloor == '2')
				{
					if(visitfloorNumBer == '1')
					{
						//mapMC.showFloorByKey('0', '');
						mapMC.showRoute('0','19','0',visitnode);
					}
					if(visitfloorNumBer == '3')
					{
						//mapMC.showFloorByKey('2', '');
						mapMC.showRoute('2','27','2',visitnode);
					}
					if(visitfloorNumBer == '4')
					{
						//mapMC.showFloorByKey('3', '');
						mapMC.showRoute('3','14','3',visitnode);
					}
					if(visitfloorNumBer == '5')
					{
						//mapMC.showFloorByKey('4', '');
						mapMC.showRoute('4','19','4',visitnode);
					}
				}
				if(youarehereFloor == '4')
				{
					if(visitfloorNumBer == '1')
					{
						//mapMC.showFloorByKey('0', '');
						mapMC.showRoute('0','19','0',visitnode);
					}
					if(visitfloorNumBer == '2')
					{
						//mapMC.showFloorByKey('3', '');
						mapMC.showRoute('1','27','1',visitnode);
					}
					if(visitfloorNumBer == '3')
					{
						//mapMC.showFloorByKey('2', '');
						mapMC.showRoute('2','27','2',visitnode);
					}
					
					if(visitfloorNumBer == '5')
					{
						//mapMC.showFloorByKey('4', '');
						mapMC.showRoute('4','19','4',visitnode);
					}
				}
			//doneShowRoute();
		}
		private function onTapBackBTNHandler (e:UIControlEvent):void
		{
			//viewWillAppear();
			
			mapMC.alpha = 0;
			mapMC.clearPath();
			mapMC.unHilightAll();
			mapMC.mouseChildren = false;
			mapMC.mouseEnabled = false;
			
			floorOptionCoverFlow.hideAllShop();
			trace ('clearStage');
			if (extSwf) extSwf.visible = false;
			showShopName.visible = false;
			
			shopDetail.visible = false;
			shopDetail.floorselection.visible = false;
			
			showfloorOptionCoverFlowProperty();
			scaleFloorOptionCoverFlow('in');
			
			isDone = false;
			
		}
		
		private function _isDone(_done:String):Boolean
		{
			//if(_done == '')
			return false;	
		}
		public function doneShowRoute():void
		{
			if(isDone == false)
			{
				clearStage();
			}
			else
			{
				_onShowShopDetail();
				//mapMC.clearPath();
				if(youarehereFloor == '2')
				{
					if(visitfloorNumBer == '1')
					{
						//mapMC.showFloorByKey('0', '');
						mapMC.showRoute('0','19','0',visitnode);
					}
					if(visitfloorNumBer == '3')
					{
						//mapMC.showFloorByKey('2', '');
						mapMC.showRoute('2','27','2',visitnode);
					}
					if(visitfloorNumBer == '4')
					{
						//mapMC.showFloorByKey('3', '');
						mapMC.showRoute('3','14','3',visitnode);
					}
					if(visitfloorNumBer == '5')
					{
						//mapMC.showFloorByKey('4', '');
						mapMC.showRoute('4','19','4',visitnode);
					}
				}
				if(youarehereFloor == '4')
				{
					if(visitfloorNumBer == '1')
					{
						//mapMC.showFloorByKey('0', '');
						mapMC.showRoute('0','19','0',visitnode);
					}
					if(visitfloorNumBer == '2')
					{
						//mapMC.showFloorByKey('3', '');
						mapMC.showRoute('1','27','1',visitnode);
					}
					if(visitfloorNumBer == '3')
					{
						//mapMC.showFloorByKey('2', '');
						mapMC.showRoute('2','10','2',visitnode);
					}
					
					if(visitfloorNumBer == '5')
					{
						//mapMC.showFloorByKey('4', '');
						mapMC.showRoute('4','19','4',visitnode);
					}
				}
				
			}
			
			
			//mapMC.showFloorByKey(visitfloorNumBer, '');
			

		}
		private function _onShowShopDetail():void
		{
			// hide extSwf(animetion changeFloor)
			if(isDone == false)
			{
				clearStage();
			}
			else
			{
				showShopBubble();
				
				if (extSwf) TweenMax.to(extSwf, 0.2, {autoAlpha:0});
				mapMC.scaleX = mapMC.scaleY = 0.7;
				mapMC.x = 880;
				mapMC.y = 85;
				//TweenMax.to(mapMC,0.3,{x:930, y:85});
				TweenMax.to(mapMC,1,{alpha:1, delay:0.5});
				
				
				// open shopDetail
				TweenMax.to(shopDetail,0.5,{autoAlpha:1});	
				TweenMax.to(shopDetail.floorselection,0.4,{x:1677.35,autoAlpha:1, delay:0.9});	
				
				showShopName.visible = false;
				
				for(var i:int = 0; i<shopModelArray.length; i++)
				{
					if((shopModelArray[i] as ShopModel).unitKey == this.floorOptionCoverFlow.sendUnit)
					{
						shopDetail.model = shopModelArray[i];
					}
				}
			}
			
		}
		public function onTapPrevBTN (e:UIControlEvent):void
		{
			floorOptionCoverFlow.goPrev();
			checkIndex(floorOptionCoverFlow._currentFloorIndex);
		}
		public function onTapNextBTN (e:UIControlEvent):void
		{
			floorOptionCoverFlow.goNext();
			checkIndex(floorOptionCoverFlow._currentFloorIndex);
		}
		private function checkIndex(currentIndex:int):void
		{
			setButtonName(floorOptionCoverFlow._currentFloorIndex);
		}
		
		public function getShopInfo(e:Event = null):void
		{
			isDone = true;
			mapMC.alpha = 0;;
			getShopInfoFromShopCategory(this.floorOptionCoverFlow.sendUnit);
		}
		public function getShopInfoFromShopCategory(shopName:String):void
		{
			backbtn.visible = false;
			for (var i:int = 0; i<shopXMLList.length(); i++)
			{
				if (this.floorOptionCoverFlow.sendUnit == shopXMLList[i].unit_key.text())
				{
					showShopName._shopname.text = 'Show Route to '+ shopXMLList[i].name_en.text();
					visitfloor = shopXMLList[i].floor.text();
					visitnode = shopXMLList[i].node.text();
				}
				else if(shopName == shopXMLList[i].attribute('id'))
				{
					showShopName._shopname.text = 'Show Route to '+ shopXMLList[i].name_en.text();
					visitfloor = shopXMLList[i].floor.text();
					visitnode = shopXMLList[i].node.text();
					this.floorOptionCoverFlow.sendUnit = shopXMLList[i].unit_key.text()
				
				}
			}
			
			showAnimationChangeFloor(visitfloor);
			
		}
		private function showAnimationChangeFloor(_visitfloor:String):void
		{
			dispatchEvent(new Event('showRouting'));
			isDone = true;
			
			if(_visitfloor == 'L')visitfloorNumBer = '1';
			if(_visitfloor == 'G')visitfloorNumBer = '2';
			if(_visitfloor == '1')visitfloorNumBer = '3';
			if(_visitfloor == '2')visitfloorNumBer = '4';
			if(_visitfloor == '3')visitfloorNumBer = '5';
			
			if(visitfloorNumBer == youarehereFloor) showDirectionSameFloor();
			if(visitfloorNumBer != youarehereFloor) showDirectionDifferenceFloor();
			
			hidefloorOptionCoverFlowProperty();
			showShopName.visible = true;
			
			_moveFloor();
		}
		private function _moveFloor():void
		{
			
			if(youarehereFloor == '4')
			{
				mapMC.showFloorByKey('3', '');
				TweenMax.to(mapMC,0.2,{alpha:0});
			}
			else
			{
				mapMC.showFloorByKey('1', '');
				TweenMax.to(mapMC,0.2,{alpha:0});
			}
		}
		public function showDirectionSameFloor():void{
			floorOptionCoverFlow.visible = false;
			if (extSwf) TweenMax.to(extSwf, 0.2, {autoAlpha:0});
			
			if(youarehereFloor == '2') _delayShowPath('1');
			if(youarehereFloor == '4') _delayShowPath('2');

		}
		private function _delayShowPath(floorKey:String):void{
			
			 mapMC.showRoute(floorKey,youarehereNode,floorKey,visitnode);
			 doneShowRoute();
		}
		
		public function beforeRouting(e:Event):void
		{
			backbtn.visible = false;
		}
		public function onRoutingEnd(e:Event):void
		{
			backbtn.visible = true;
		}
		public function showDirectionDifferenceFloor():void
		{
			//trace('showDirectionDifferenceFloor');
			trace ('visitnode', this.visitnode);
			floorOptionCoverFlow.visible = false;
			if(youarehereFloor == '2')
			{
				
				if (extSwf) {
					TweenMax.to(extSwf, 0.5, {autoAlpha:1});
					if(visitfloorNumBer == '1' && visitfloorNumBer != '2')extSwf.gotoAndPlay(0);
					else if(visitfloorNumBer != '1' && visitfloorNumBer != '2')extSwf.gotoAndPlay(290);
					//extSwf.parent.setChildIndex(extSwf,1);
					
					extSwf.addEventListener(
					Event.ENTER_FRAME,
					
					function(e:Event):void
					{
						if(visitfloorNumBer == '1')
						{
							//mapMC.alpha = 1;
								mapMC.showFloorByKey('0','');
							if(extSwf.currentFrame == 280)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
								
								
							}
						}
						if(visitfloorNumBer == '3')
						{
							//mapMC.alpha = 1;
							mapMC.showFloorByKey('2','');
							if(extSwf.currentFrame == 615)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
							}
						}
						if(visitfloorNumBer == '4')
						{
							//mapMC.alpha = 1;
							mapMC.showFloorByKey('3','');
							if(extSwf.currentFrame == 925)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
								
								
							}
						}
						
						if(visitfloorNumBer == '5')
						{
							//mapMC.alpha = 1;
							mapMC.showFloorByKey('4','');
							if(extSwf.currentFrame == 1085)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
								
							}
						}
						
						
					});
					
				}
			}
			else
			{
				if (extSwf) {
					TweenMax.to(extSwf, 0.5, {autoAlpha:1});
					if(visitfloorNumBer == '5' && visitfloorNumBer != '4')extSwf.gotoAndPlay(0);
					else if(visitfloorNumBer != '5' && visitfloorNumBer != '4')extSwf.gotoAndPlay(284);
					//extSwf.parent.setChildIndex(extSwf,1);
					
					extSwf.addEventListener(
					Event.ENTER_FRAME,
					
					function(e:Event):void
					{
						if(visitfloorNumBer == '1')
						{
							//mapMC.alpha = 1;
								mapMC.showFloorByKey('0','');
							if(extSwf.currentFrame == 1228)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
								
								
							}
						}
						if(visitfloorNumBer == '2')
						{
							//mapMC.alpha = 1;
							mapMC.showFloorByKey('1','');
							if(extSwf.currentFrame == 926)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
								
							}
						}
						if(visitfloorNumBer == '3')
						{
							//mapMC.alpha = 1;
							mapMC.showFloorByKey('2','');
							if(extSwf.currentFrame == 580)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								_onShowShopDetail();
								//new delayedFunctionCall(doneShowRoute, 2500);
								
							}
						}
						
						if(visitfloorNumBer == '5')
						{
							//mapMC.alpha = 1;
							mapMC.showFloorByKey('4','');
							if(extSwf.currentFrame == 267)
							{
								extSwf.stop();
								extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
								
								TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8});
								new delayedFunctionCall(doneShowRoute, 2500);
							}
						}
						
						
					});
					
				}
			}
			
			
			
		
		}// Function 
		
		
		private function showShopBubble():void
		{
			if(visitfloorNumBer == '1') mapMC.addBuble(0, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '2') mapMC.addBuble(1, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '3') mapMC.addBuble(2, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '4') mapMC.addBuble(3, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '5') mapMC.addBuble(4, this.floorOptionCoverFlow.sendUnit);
			
			if(visitfloorNumBer == '1') mapMC.hilightMC(0,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '2') mapMC.hilightMC(1,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '3') mapMC.hilightMC(2,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '4') mapMC.hilightMC(3,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '5') mapMC.hilightMC(4,this.floorOptionCoverFlow.sendUnit);
			
			
		}
		
		
		private function hidefloorOptionCoverFlowProperty():void
		{
			prevBTN.visible = false;
			nextBTN.visible = false;
			floorProperty.visible = false;
			gaysornLogo.visible = false;
			
			
		}
		
		private function showfloorOptionCoverFlowProperty():void
		{
			prevBTN.visible = true;
			nextBTN.visible = true;
			floorProperty.visible = true;
			gaysornLogo.visible = true;
			
			
		}
		
		override public function viewWillAppear():void
		{
			floorOptionCoverFlow.unHilightMCAndCloseBubble();
			setDefaultDirectoryViewController();
			

		}
		
		private function setButtonName(_index:int):void
		{
			
			if(_index == 1){
				//floorKey = 0;
				floorProperty._floorname.text = 'Lobby Floor';
				nextBTN._flNamae.text = 'Groud Floor';
				prevBTN._flNamae.text = '3rd Floor';
			}
			if(_index == 2){
				//floorKey = 1;
				floorProperty._floorname.text = 'Ground Floor';
				nextBTN._flNamae.text = '1st Floor';
				prevBTN._flNamae.text = 'Lobby Floor';
			}
			if(_index == 3){
				//floorKey = 2;
				floorProperty._floorname.text = '1st Floor';
				nextBTN._flNamae.text = '2nd Floor';
				prevBTN._flNamae.text = 'Ground Floor';
			}
			if(_index == 4){
				//floorKey = 3;
				floorProperty._floorname.text = '2nd Floor';
				nextBTN._flNamae.text = '3rd Floor';
				prevBTN._flNamae.text = '1st Floor';
			}
			if(_index == 0){
				//floorKey = 4;
				floorProperty._floorname.text = '3rd Floor';
				nextBTN._flNamae.text = 'Lobby Floor';
				prevBTN._flNamae.text = '2nd Floor';
			}
			dispatchEvent(new Event('setFloorActive'));
		}
		
		public function facilityChoose(e:FacilityEvent):void
		{
			floorOptionCoverFlow.showFacility(e.obj.name);
		}


	}
}