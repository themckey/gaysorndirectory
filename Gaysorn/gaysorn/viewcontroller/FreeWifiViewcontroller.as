﻿package viewcontroller
{
	import com.forviz.ViewController;
	import com.forviz.ui.UITextField;
	import flash.utils.Dictionary;
	import flash.text.TextFormat;
	import com.forviz.ui.UIScrollView;
	import flash.geom.Rectangle;
	import com.forviz.ui.CGSize;
	import com.forviz.ui.UIDropdown;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import com.greensock.TweenMax;
	import main.wifi.CountryItem;
	import com.forviz.ui.UIControlEvent;
	import flash.events.Event;
	import com.forviz.ui.UIControl;
	import com.forviz.ui.UIButton;
	import flash.display.MovieClip;
	import main.wifi.CountryDropdown;
	import main.wifi.CountryDropdownBTN;
	import flash.text.TextField;
	import flash.utils.Timer;
	import flash.events.TimerEvent;


	public class FreeWifiViewcontroller extends ViewController
	{

		/* All Interactive elements */
		var countryDropdown:CountryDropdownBTN;



		public var id_TF:UITextField;
		public var email_TF:UITextField;
		public var country_TF:UITextField;

		var textFormat:Dictionary = new Dictionary();
		var textFormaten:TextFormat = new TextFormat();
		var textFormatcn:TextFormat = new TextFormat();
		var textFormatjp:TextFormat = new TextFormat();
		
		var tfOptions:Object;
		var tfOptionsEmail:Object;
		var tfOptionsCountry:Object;


		var countryList:Array = new Array("Thailand","Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda","Argentina","Armenia","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Central African Republic","Chad","Chile","China","Colombia","Comoros","Congo (Brazzaville)","Congo","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor (Timor Timur)","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland","France","Gabon","Gambia, The","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica","Japan",
										  "Jordan","Kazakhstan","Kenya","Kiribati","Korea, North","Korea, South","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Northern Ireland","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Qatar","Romania","Russia","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Scotland","Senegal","Serbia and Montenegro","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","Spain","Sri Lanka","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan",											"Tanzani", "Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom (UK)","United States (USA)","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Wales","Yemen","Zambia","Zimbabwe");


		var idStr:String;
		var emailStr:String;
		var countrySTR:String;

		var emailValid:Number;



		var correctID:Boolean = false;
		var correctEmail:Boolean = false;
		var correctCountry:Boolean = false;


		public function FreeWifiViewcontroller()
		{
			// constructor code

			/* Get all Interactive elements */
			countryDropdown = this.getChildByName("countryDropDown") as CountryDropdownBTN;

			
			
			tfOptions = {
			'placeholder' : {
			'text' : {
			'en' : 'Insert your ID no. or Passport no.'
			},
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			},
			
			'input' : {
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			}
			
			};

			id_TF = new UITextField(new Rectangle(188.05,387.15,482,60),tfOptions);
			addChild(id_TF);
			id_TF.name = 'idNumber';
			id_TF.addEventListener('valueChanged', onValueChanged);
			id_TF.addEventListener('focus', onFocusId);


			tfOptionsEmail = {
			'placeholder' : {
			'text' : {
			'en' : 'Insert your email'
			},
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			},
			
			'input' : {
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			}
			
			};

			email_TF = new UITextField(new Rectangle(720,387.15,482,60),tfOptionsEmail);
			addChild(email_TF);
			email_TF.name = 'email';
			email_TF.addEventListener('valueChanged', onValueChanged);
			email_TF.addEventListener('focus', onFocusEmail);
			
			


			tfOptionsCountry = {
			'placeholder' : {
			'text' : {
			'en' : 'Select country'
			},
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			},
			
			'input' : {
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			}
			
			};

			country_TF = new UITextField(new Rectangle(1248.3,386.15,482,60),tfOptionsCountry);
			addChild(country_TF);
			//countryDropDown.mouseChildren = false;
			country_TF.name = 'country';
			country_TF.addEventListener('valueChanged', onValueChanged);
			country_TF.addEventListener('focus', onFocusCountry);




			// Setting up Events

			this.addEventListener(MouseEvent.CLICK, onClick);
			
			country_TF.addEventListener("tap", onTapCountryDropdownHandler);
			this._submit.addEventListener("tap", onTapBtnSubmitHandler);

			email_TF.addEventListener("tap", onTapEmailTextFieldHandler);
			id_TF.addEventListener("tap", onTapIDTextFieldHandler);
			
			
			
			textFormaten : new TextFormat('AccentGraphic', 24, 0x464646, false, null, null, null, null, 'left', 50);
			textFormatcn : new TextFormat('微软雅黑', 24, 0x464646, false, null, null, null, null, 'left', 50);
			textFormatjp : new TextFormat('Hiragino Sans GB', 24, 0x464646, false, null, null, null, null, 'left', 50);
			
			
			onTranslate(langs);


		}
		
		private function onFocusId(e:UIControlEvent):void
		{
			id_TF.placeHolder.visible = false;
			//id_TF.value = '';
			
			this.hideDropdown();
			
			if(email_TF.value == '')
			{
				//email_TF.placeHolder.visible = true;
				//country_TF.placeHolder.visible = true;
			}
		}
		private function onFocusEmail(e:UIControlEvent):void
		{
			email_TF.placeHolder.visible = false;
			//email_TF.value = '';
			this.hideDropdown();
			
			if(id_TF.value == '')
			{
				//id_TF.placeHolder.visible = true;
				//country_TF.placeHolder.visible = true;
			}
		}
		
		private function onFocusCountry(e:UIControlEvent):void
		{
			country_TF.placeHolder.visible = false;
			//country_TF.value = '';
			this.showDropdown();
			if(country_TF.value == '')
			{
				//id_TF.placeHolder.visible = true;
				//email_TF.placeHolder.visible = true;
			}
			
			
		}
		
		public function onClick(e:MouseEvent = null):void
		{
			for (var i:int = 0; i<countryList.length; i++)
			{
				if (e.target.name == countryList[i].toString())
				{
					country_TF.visible = true;
					country_TF.value = countryList[i].toString();
					hideDropdown();
				}
			}
			
			if(e.target.name == '_continue')
			{
				hideAlert();
				onDefault();
				
			}
		}

		private function onTapCountryDropdownHandler(e:UIControlEvent):void
		{
			showDropdown();
			//country_TF.visible = false;
			hideAlert();

		}

		private function onTapEmailTextFieldHandler(e:UIControlEvent):void
		{
			hideDropdown();
			hideAlert();
		}


		private function onTapIDTextFieldHandler(e:UIControlEvent):void
		{
			hideDropdown();
			hideAlert();
		}

		private function onTapBtnSubmitHandler(e:UIControlEvent):void
		{
			//hideTexField();
			hideDropdown();
			showAlert();
			
			
		}

		// show or hide dropdown ====================================================//
		private function showDropdown():void
		{
			_dropdown.visible = true;
			
		}
		private function hideDropdown():void
		{
			_dropdown.visible = false;
		}
		//==========================================================================//



		//show or hide all textField =================================================//
		private function hideTexField():void
		{
			id_TF.visible = false;
			email_TF.visible = false;
			country_TF.visible = false;
		}
		private function showTexField():void
		{
			id_TF.visible = true;
			email_TF.visible = true;
			country_TF.visible = true;
		}

		//==========================================================================//


		// show or hide alert id ===================================================//
		private function showAlertId():void
		{
			this._success._alert._alertid.visible = true;
			onTranslate(langs)
		}
		
		private function hideAlertId():void
		{
			this._success._alert._alertid.visible = false;
		}
		//==========================================================================//

		// show or hide alert email ===============================================//
		private function showAlertBlankEmail():void
		{
			this._success._alert._alertemail.visible = true;
			this._success._alert._invalidmail.visible = false;
			onTranslate(langs)
		}
		

		private function hideAlertBlankEmail():void
		{
			this._success._alert._alertemail.visible = false;
		}

		private function showAlertInvalidEmail():void
		{
			this._success._alert._alertemail.visible = false;
			this._success._alert._invalidmail.visible = true;
			onTranslate(langs)
		}

		private function hideAlertInvalidEmail():void
		{
			this._success._alert._invalidmail.visible = false;
		}

		//=========================================================================//

		//show or hide alert country =============================================//
		private function showAlertCountry():void
		{
			this._success._alert._alertcountry.visible = true;
			onTranslate(langs)
		}
		private function hideAlertCountry():void
		{
			this._success._alert._alertcountry.visible = false;
		}
		//========================================================================//

		private function hideAlert():void
		{
			this._success.visible = false;
			
		}
		private function showAlert():void
		{
			trace (correctID,
				   correctEmail,
				   correctCountry);
				   
			
			if(correctID == true && correctEmail == true && correctCountry == true)
			{
				showSuccess();
				
				
			}
			else
			{
				this._success.visible = true;
				this._success._alert.visible = true;
				this._success._successpane.visible = false;
	
				if (idStr == '')
				{
					showAlertId();
				}
				if (idStr != '')
				{
					hideAlertId();
				}
				if (emailStr == '')
				{
					showAlertBlankEmail();
				}
				if (emailStr != '')
				{
					hideAlertBlankEmail();
					emailStr = (email_TF.value).substr(1,(email_TF.value).length);
					checkEmail(emailStr);
				}


			}
			
		}
		private function onValueChanged(evt:UIControlEvent):void
		{
			if (id_TF.value == '')
			{
				idStr = '';
			}
			else
			{
				idStr = id_TF.value;
				correctID = true;
				id_TF.placeHolder.visible = false;
			}

			if (email_TF.value == '')
			{
				emailStr = '';
			}
			else
			{
				emailStr = (email_TF.value).substr(1,(email_TF.value).length);
				checkEmail(emailStr);
			}
			
			
			
			
			trace ('country_TF.value               ',country_TF.value);
			if (country_TF.value == '')
			{
				countrySTR = '';
				showAlertCountry();
			}
			else
			{
				countrySTR = country_TF.value;
				hideAlertCountry();
				correctCountry = true;
			}
			
			if(country_TF.value.length != 0)
			{
				//showDropdown();
				_dropdown.arrangeCountryList(country_TF.value);
				hideAlert();
			}
			



		}


		private function checkEmail(str:String ):void
		{

			var regExpPattern:RegExp = /^[0-9a-zA-Z][-._a-zA-Z0-9]*@([0-9a-zA-Z][-._0-9a-zA-Z]*\.)+[a-zA-Z]{2,4}$/;
			trace("checkEmail " + str + " result = " + str.match(regExpPattern));
			if (str.match(regExpPattern) == null)
			{
				showAlertInvalidEmail();
			}
			else
			{
				hideAlertInvalidEmail();

				correctEmail = true;


			}



		}
	
		var myTimer:Timer = new Timer(40000);
		
		private function showSuccess():void
		{

			this._success.visible = true;
			this._success._successpane.visible = true;
			this._success._alert.visible = false;
			onDefault();
			hideTexField();
			
			myTimer.reset()
			myTimer.start();
			myTimer.addEventListener(TimerEvent.TIMER, hideSucces);


		}
		
		private function hideSucces(e:TimerEvent):void
		{
			trace ('hideSuccess');
			onDefault();
			this._success.visible = false;
		}
		
		
		override public function viewWillAppear():void
		{
			onDefault();
			this._success.visible = false;

		}

		private function onDefault():void
		{
			this._dropdown.visible = false;
			country_TF.value = '';
			email_TF.value = '';
			id_TF.value = '';
			
			id_TF.placeHolder.visible = true;
			email_TF.placeHolder.visible = true;
			country_TF.placeHolder.visible = true;

			correctID = false;
			correctEmail = false;
			correctCountry = false;

			showTexField();
			
		}
		
		
		var langs:String = 'en';
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			if(langs == 'en')
			{
				this._en.visible = true;
				this._cn.visible = false;
				this._jp.visible = false;
				
				_en_info.visible = true;
				_cn_info.visible = false;
				_jp_info.visible = false;
				
				_en_order.visible = true;
				_cn_order.visible = false;
				_jp_order.visible = false;
				
				id_TF.placeHolder.text = 'Insert your ID no. or Passport no.';
				email_TF.placeHolder.text = 'Insert your email';
				country_TF.placeHolder.text = 'Select country';
				
				id_TF.placeHolder.setTextFormat(textFormaten);
				email_TF.placeHolder.setTextFormat(textFormaten);
				country_TF.placeHolder.setTextFormat(textFormaten);
				
				this._success._alert._alertid.id_en.visible = true;
				this._success._alert._alertid.id_cn.visible = false;
				this._success._alert._alertid.id_jp.visible = false;
				
				this._success._alert._alertemail.mail_en.visible = true;
				this._success._alert._alertemail.mail_cn.visible = false;
				this._success._alert._alertemail.mail_jp.visible = false;
				
				this._success._alert._alertcountry.country_en.visible = true;
				this._success._alert._alertcountry.country_cn.visible = false;
				this._success._alert._alertcountry.country_jp.visible = false;
				
				this._success._alert._invalidmail._en.visible = true;
				this._success._alert._invalidmail._cn.visible = false;
				this._success._alert._invalidmail._jp.visible = false;
				
				this._success._successpane._en.visible = true;
				this._success._successpane._cn.visible = false;
				this._success._successpane._jp.visible = false;
				
				
				
				
				
			}
			if(langs == 'cn')
			{
				this._en.visible = false;
				this._cn.visible = true;
				this._jp.visible = false;
				
				_en_info.visible = false;
				_cn_info.visible = true;
				_jp_info.visible = false;
				
				_en_order.visible = false;
				_cn_order.visible = true;
				_jp_order.visible = false;
				
				id_TF.placeHolder.text = '将您的身份证号码。或护照号码。';
				email_TF.placeHolder.text = '将您的电子邮件';
				country_TF.placeHolder.text = '选择国家';
				
				id_TF.placeHolder.setTextFormat(textFormatcn);
				email_TF.placeHolder.setTextFormat(textFormatcn);
				country_TF.placeHolder.setTextFormat(textFormatcn);
				
				this._success._alert._alertid.id_en.visible = false;
				this._success._alert._alertid.id_cn.visible = true;
				this._success._alert._alertid.id_jp.visible = false;
				
				this._success._alert._alertemail.mail_en.visible = false;
				this._success._alert._alertemail.mail_cn.visible = true;
				this._success._alert._alertemail.mail_jp.visible = false;
				
				this._success._alert._alertcountry.country_en.visible = false;
				this._success._alert._alertcountry.country_cn.visible = true;
				this._success._alert._alertcountry.country_jp.visible = false;
				
				this._success._alert._invalidmail._en.visible = false;
				this._success._alert._invalidmail._cn.visible = true;
				this._success._alert._invalidmail._jp.visible = false;
				
				this._success._successpane._en.visible = false;
				this._success._successpane._cn.visible = true;
				this._success._successpane._jp.visible = false;
				
				
				
				
			}
			if(langs == 'jp')
			{
				this._en.visible = false;
				this._cn.visible = false;
				this._jp.visible = true;
				
				_en_info.visible = false;
				_cn_info.visible = false;
				_jp_info.visible = true;
				
				_en_order.visible = false;
				_cn_order.visible = false;
				_jp_order.visible = true;
				
				id_TF.placeHolder.text = 'あなたのIDなしを挿入します。またはパスポートなし。';
				email_TF.placeHolder.text = 'あなたのメールアドレスを挿入します';
				country_TF.placeHolder.text = '国を選択';
				
				id_TF.placeHolder.setTextFormat(textFormatjp);
				email_TF.placeHolder.setTextFormat(textFormatjp);
				country_TF.placeHolder.setTextFormat(textFormatjp);
				
				this._success._alert._alertid.id_en.visible = false;
				this._success._alert._alertid.id_cn.visible = false;
				this._success._alert._alertid.id_jp.visible = true;
				
				this._success._alert._alertemail.mail_en.visible = false;
				this._success._alert._alertemail.mail_cn.visible = false;
				this._success._alert._alertemail.mail_jp.visible = true;
				
				this._success._alert._alertcountry.country_en.visible = false;
				this._success._alert._alertcountry.country_cn.visible = false;
				this._success._alert._alertcountry.country_jp.visible = true;
				
				this._success._alert._invalidmail._en.visible = false;
				this._success._alert._invalidmail._cn.visible = false;
				this._success._alert._invalidmail._jp.visible = true;
				
				this._success._successpane._en.visible = false;
				this._success._successpane._cn.visible = false;
				this._success._successpane._jp.visible = true;
				
				
				
			}
			
			_submit.onTranslate(langs);
			
		}



	}

}