﻿package viewcontroller
{
	import com.forviz.ViewController;
	import items.floorplan.FloorplanCoverflow;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import maps.CustomMapsMC;
	import items.NextBTN;
	import items.PrevBTN;
	import com.forviz.ui.UIControlEvent;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import com.greensock.TweenMax;
	import items.ShowShopnameMC;
	import flash.events.MouseEvent;
	import com.greensock.easing.Expo;
	import items.SkipButton;
	import com.forviz.ui.UIControl;
	import com.jmx2.delayedFunctionCall;
	import models.ShopModel;
	import items.BackBTN;
	import events.FacilityEvent;
	import items.floorSelection;
	import items.FloorSelection;
	import items.WalkerMan;
	import items.NextBTNcn;
	import items.NextBTNjp;
	import items.PrevBTNcn;
	import items.PrevBTNjp;

	public class DirectoryViewController extends ViewController
	{
		private var _dm:GaysornDataManager;

		public var floorOptionCoverFlow:FloorplanCoverflow = new FloorplanCoverflow();
		private var _pathAnimationG:String = 'content/GoUp.swf';
		private var _pathAnimationThird:String = 'content/GoUp4.swf';

		var mapMC:CustomMapsMC = new CustomMapsMC();
		var animationFloorplanMC:MovieClip = new MovieClip();
		var animationLoader:Loader = new Loader();
		var extSwf:MovieClip;

		var nextBTN:NextBTN = new NextBTN();
		var prevBTN:PrevBTN = new PrevBTN();
		
		var nextBTNcn:NextBTNcn = new NextBTNcn();
		var prevBTNcn:PrevBTNcn = new PrevBTNcn();
		
		var nextBTNjp:NextBTNjp = new NextBTNjp();
		var prevBTNjp:PrevBTNjp = new PrevBTNjp();
		
		
		var skipButton:SkipButton = new SkipButton();

		var showShopName:ShowShopnameMC = new ShowShopnameMC();


		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();

		var visitfloor:String;
		var visitnode:String;
		var youarehereFloor:int = 0;
		var youarehereNode:String;

		var visitfloorNumBer:String;

		public var shopDetail:ShopDeatailViewController = new ShopDeatailViewController();
		var floorSelection:FloorSelection = new FloorSelection();

		var liftbtn:MovieClip = new MovieClip();
		var backbtn:BackBTN = new BackBTN();

		var floorplanIsShowingIndex:int;

		private var _mapMCScale:Number;
		private var _mapMCX:Number;
		private var _mapMCY:Number;


		var _isFull:Boolean = false;



		public function DirectoryViewController()
		{
			_setDefaultViewcontroller();
			_dm = GaysornDataManager.getInstance();
			_dm.addEventListener('dataInitted', onInitData);
			
			onTranslate(langs);

			this.addEventListener(MouseEvent.CLICK, _onClickMainDirectView);

		}
		public function _onClickMainDirectView(e:Event):void
		{

			if (e.target.name == 'floorOptionCoverFlow')
			{
				floorOptionCoverFlow.hideAllShop();
			}
			if (e.target.name.substr(0,4) == 'inst')
			{
				floorOptionCoverFlow.hideAllShop();
			}
		}



		private function onInitData(e:Event = null):void
		{
			shopXMLList = _dm.shopXMLList;
			youarehereNode = _dm.configsXML.start_node.text();
			youarehereFloor = _dm.configsXML.kiosk_floor.text();
			shopModelArray = _dm.shopArr;
			
			_chectKioskFloor(youarehereFloor);

		}
		private function _chectKioskFloor(kioskStr:String):void
		{

			(kioskStr == '4') ? _floorplanAnimation(_pathAnimationThird):_floorplanAnimation(_pathAnimationG);
		}

		private function _setDefaultViewcontroller():void
		{

			_createFloorplanCoverflow();
			_createShopDetail();
			
		}

		private function _createFloorplanCoverflow():void
		{

			addChild(floorOptionCoverFlow);
			this.name = 'floorOptionCoverFlow';

			floorOptionCoverFlow.addEventListener('ClickMoreInfo', getShopInfo);
			floorOptionCoverFlow.addEventListener('setFirstFloorDefault', _checkIndex);
			this.floorProperty.addEventListener('chosefacility', facilityChoose);

			nextBTN.x = 1770;
			nextBTN.y = 400;
			floorOptionCoverFlow.addChild(nextBTN);
			nextBTN.addEventListener('tap', onTapNextBTN);

			prevBTN.x = 30;
			prevBTN.y = 400;
			floorOptionCoverFlow.addChild(prevBTN);
			prevBTN.addEventListener('tap', onTapPrevBTN);
			
			
			nextBTNcn.x = 1770;
			nextBTNcn.y = 400;
			floorOptionCoverFlow.addChild(nextBTNcn);
			nextBTNcn.addEventListener('tap', onTapNextBTN);

			prevBTNcn.x = 30;
			prevBTNcn.y = 400;
			floorOptionCoverFlow.addChild(prevBTNcn);
			prevBTNcn.addEventListener('tap', onTapPrevBTN);
			
			
			
			nextBTNjp.x = 1770;
			nextBTNjp.y = 400;
			floorOptionCoverFlow.addChild(nextBTNjp);
			nextBTNjp.addEventListener('tap', onTapNextBTN);

			prevBTNjp.x = 30;
			prevBTNjp.y = 400;
			floorOptionCoverFlow.addChild(prevBTNjp);
			prevBTNjp.addEventListener('tap', onTapPrevBTN);

		}
		
		
		private function _createShopDetail():void
		{

			addChild(shopDetail);
			TweenMax.to(shopDetail,0.5,{autoAlpha:0});

			backbtn = shopDetail.getChildByName('_back');
			shopDetail.addChild(backbtn);
			backbtn.addEventListener('tap', onTapBackBTNHandler);
			backbtn.visible = false;

			_createCustomMapmc();

		}

		private function _createCustomMapmc():void
		{

			mapMC.x = 580;
			mapMC.y = 165;
			addChild(mapMC);
			mapMC.alpha = 0;
			mapMC.addEventListener('endRouting', onRoutingEnd);
			mapMC.addEventListener('beforeRouting', beforeRouting);

			animationFloorplanMC.y = 0;
			addChild(animationFloorplanMC);
			animationFloorplanMC.addChild(animationLoader);
		}
		private function _creatfloorSelection():void
		{

			floorSelection.x = 1950;
			floorSelection.y = 85;
			addChild(floorSelection);
			
			floorSelection.getChildByName('_exit').visible = false;
			floorSelection.getChildByName('_full').addEventListener('tap', _onTapFullScreenButteonHandler);
			floorSelection.getChildByName('_exit').addEventListener('tap', _onTapExitFullScreenButteonHandler);
			floorSelection.getChildByName('_replay').addEventListener('tap', _onTapReplayHandler);
			
			(floorSelection.getChildByName('__1') as MovieClip).addEventListener(MouseEvent.CLICK, onClickLobby);
			(floorSelection.getChildByName('__2') as MovieClip).addEventListener(MouseEvent.CLICK, onClickGround);
			(floorSelection.getChildByName('__3') as MovieClip).addEventListener(MouseEvent.CLICK, onClickFirst);
			(floorSelection.getChildByName('__4') as MovieClip).addEventListener(MouseEvent.CLICK, onClickSecond);
			(floorSelection.getChildByName('__5') as MovieClip).addEventListener(MouseEvent.CLICK, onClickThird);
			

		}
		
		public function onClickLobby(e:MouseEvent):void
		{
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);
			_doWhenReviewFloor('0');
		}
		public function onClickGround(e:MouseEvent):void
		{
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);
			_doWhenReviewFloor('1');
		}
		public function onClickFirst(e:MouseEvent):void
		{
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);
			_doWhenReviewFloor('2');
		}
		public function onClickSecond(e:MouseEvent):void
		{
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);
			_doWhenReviewFloor('3');
		}
		public function onClickThird(e:MouseEvent):void
		{
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);
			extSwf.removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);
			_doWhenReviewFloor('4');
		}
		
		private function _doWhenReviewFloor(floortap:String):void
		{
			if(mapMC.alpha == 1) mapMC.alpha = 0;
			
			mapMC.clearPath();
			mapMC.unHilightAll();
	
			if(youarehereFloor == '2'){
				
				if(floortap == '0')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '1')
					{
						
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
						
						
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(583);
					}
				}
				if(floortap == '1')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '2')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
						
					}
					else if (visitfloorNumBer == '1')
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(185);
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(775);
					}
					
				}
				if(floortap == '2')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '3')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
						
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(1366);
					}
					
				}
				if(floortap == '3')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '4')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(1883);
					}
					
				}
				if(floortap == '4')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '5')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(2190);
					}
					
				}
			}
			if(youarehereFloor == '4'){
				
				if(floortap == '0')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '1')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
						
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(1745);
					}
				}
				if(floortap == '1')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '2')
					{
						
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
						
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(1356);
					}
					
				}
				if(floortap == '2')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '3')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
						
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(805);
					}
					
				}
				if(floortap == '3')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '4')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
					}
					else if(visitfloorNumBer == '4')
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(1958);
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(209);
					}
					
				}
				if(floortap == '4')
				{
					mapMC.stopLateWalker();
					if (visitfloorNumBer == '5')
					{
						extSwf.alpha = 0;
						mapMC.alpha = 1;
						doneShowRoute();
					}
					else
					{
						extSwf.alpha = 1;
						mapMC.alpha = 0;
						extSwf.gotoAndStop(2333);
					}
					
				}
			}
			
		}
		
		private function _onTapReplayHandler(e:UIControlEvent):void
		{
			getShopInfoFromShopCategory(shopnameforreplay);
			mapMC.stopLateWalker();
			mapMC.unHilightAll();
			
			

		}
		private function _onTapFullScreenButteonHandler(e:UIControlEvent):void
		{
			_setScaleFullScreen(true);
			
		}
		private function _onTapExitFullScreenButteonHandler(e:UIControlEvent):void
		{
			_setScaleFullScreen(false);
			
		}
		var _full:Boolean;
		
		private function _setScaleFullScreen(value:Boolean):void
		{	
			_full = value;
			
			if (value == false)
			{

				TweenMax.to(mapMC, 0.5,{x:885, y:85, scaleX:0.7, scaleY:0.7});
				TweenMax.to(shopDetail,0.5,{alpha:1});
				shopDetail.visible = true;
				TweenMax.to(extSwf, 0.5, {x:470, y:70, scaleX:0.7, scaleY:0.7});
				


				floorSelection.getChildByName('_full').visible = true;
				floorSelection.getChildByName('_exit').visible = false;
			}
			else
			{
				TweenMax.to(mapMC, 0.5,{x:588, y:50, scaleX:1, scaleY:1});
				floorProperty.alpha = 0;
				TweenMax.to(shopDetail,0.5,{alpha:0});
				TweenMax.to(extSwf, 0.5, {x:0, y:30, scaleX:1, scaleY:1});

				floorSelection.getChildByName('_full').visible = false;
				floorSelection.getChildByName('_exit').visible = true;
			}
		}

		private function _floorplanAnimation(_pathStr:String = 'content/GoUp.swf'):MovieClip
		{
			animationLoader.load(new URLRequest(_pathStr));
			animationLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, _animationLoaded);

			return animationFloorplanMC;

		}
		private function _animationLoaded(e:Event):void
		{

			extSwf = animationLoader.content as MovieClip;
			addChild(extSwf);
			extSwf.y = 70;
			extSwf.x = 470;
			extSwf.gotoAndStop(0);
			TweenMax.to(extSwf, 0.5, {autoAlpha:0});


			_creatfloorSelection();





		}

		public function scaleFloorOptionCoverFlow(_scale:String):void
		{

			floorProperty.alpha = 1;
			(_scale == 'in') ? TweenMax.to(floorOptionCoverFlow,1,{autoAlpha:1,x:0,y:0,scaleX:1,scaleY:1,ease:Expo.easeOut}):TweenMax.to(floorOptionCoverFlow,1,{autoAlpha:1,x:140,y:0,scaleX:0.857,scaleY:0.857,ease:Expo.easeOut});

		}

		public function onTapMainnavHandler():void
		{
			_full = false;
			
			_setScaleFullScreen(false);
			clearStage();
			scaleFloorOptionCoverFlow('out');
			mapMC.alpha = 0;
			mapMC.clearPath();
			mapMC.stopLateWalker();
			mapMC.unHilightAll();
			mapMC.mouseChildren = false;
			mapMC.mouseEnabled = false;

			floorOptionCoverFlow.hideAllShop();

			if (extSwf)
			{
				extSwf.visible = false;
				extSwf.stop();
				extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
			}


			shopDetail.visible = false;
			_hideFloorSelection();

			_isDone(false);
		}

		private function onTapBackBTNHandler(e:UIControlEvent):void
		{
			_setScaleFullScreen(false);
			mapMC.stopLateWalker();
			mapMC.alpha = 0;
			mapMC.clearPath();
			mapMC.unHilightAll();
			mapMC.mouseChildren = false;
			mapMC.mouseEnabled = false;

			floorOptionCoverFlow.hideAllShop();

			if (extSwf)
			{
				extSwf.visible = false;
			}
			
			shopDetail.visible = false;
			floorSelection.visible = false;
			
			scaleFloorOptionCoverFlow('in');

			_isDone(false);

		}

		var pointStart:WalkerMan = new WalkerMan();

		public function doneShowRoute():void
		{
			
			if (youarehereFloor == '2')
			{
				if (visitfloorNumBer == '1')
				{
					mapMC.showRoute('0','19','0',visitnode);
					mapMC.addPinOnNode(pointStart, '0', '19');
					
				}
				if (visitfloorNumBer == '3')
				{

					mapMC.showRoute('2','27','2',visitnode);
					mapMC.addPinOnNode(pointStart, '2','27');
				}
				if (visitfloorNumBer == '4')
				{

					mapMC.showRoute('3','31','3',visitnode);
					mapMC.addPinOnNode(pointStart, '3','31');
				}
				if (visitfloorNumBer == '5')
				{

					mapMC.showRoute('4','37','4',visitnode);
					mapMC.addPinOnNode(pointStart, '4','37');
				}
			}
			if (youarehereFloor == '4')
			{
				
				if (visitfloorNumBer == '1')
				{

					mapMC.showRoute('0','19','0',visitnode);
					mapMC.addPinOnNode(pointStart, '0', '19');
					
				}
				if (visitfloorNumBer == '2')
				{

					mapMC.showRoute('1','27','1',visitnode);
					mapMC.addPinOnNode(pointStart, '1', '27');
				}
				if (visitfloorNumBer == '3')
				{

					mapMC.showRoute('2','10','2',visitnode);
					mapMC.addPinOnNode(pointStart, '2', '10');
				}
				if (visitfloorNumBer == '5')
				{

					mapMC.showRoute('4','37','4',visitnode);
					mapMC.addPinOnNode(pointStart, '4', '37');
				}
			}

			dispatchEvent(new Event('_doneRouting'))
			
		}
		private function _onShowShopDetail():void
		{
			
			if(_full == true)
			{
				TweenMax.to(shopDetail,0.5,{autoAlpha:0});
				this._showFloorSelection();
				
				for (var i:int = 0; i<shopModelArray.length; i++)
				{
					if ((shopModelArray[i] as ShopModel).unitKey == this.floorOptionCoverFlow.sendUnit)
					{
						shopDetail.model = shopModelArray[i];
					}
				}

			}
			else
			{
				TweenMax.to(shopDetail,0.5,{autoAlpha:1});
				this._showFloorSelection();
				
				for (var i:int = 0; i<shopModelArray.length; i++)
				{
					if ((shopModelArray[i] as ShopModel).unitKey == this.floorOptionCoverFlow.sendUnit)
					{
						shopDetail.model = shopModelArray[i];
					}
				}
				shopDetail._loadImage();
			}
			
			shopDetail.onTranslate(langs);
		}
		private function set visibleMap(value:Boolean):void
		{

			mapMC.x = _mapMCX;
			mapMC.y = _mapMCY;
			mapMC.scaleX = mapMC.scaleY = _mapMCScale;
			mapMC.alpha = 1;

		}

		private function get scale():Number
		{

			return _mapMCScale;
		}
		private function get mapX():Number
		{

			return _mapMCX;
		}
		private function get mapY():Number
		{

			return _mapMCY;
		}

		public function getShopInfo(e:Event = null):void
		{

			_isDone(true);
			mapMC.alpha = 0;;
			getShopInfoFromShopCategory(this.floorOptionCoverFlow.sendUnit);
		}
		var shopnameforreplay:String;
		
		public function getShopInfoFromShopCategory(shopName:String):void
		{
			shopnameforreplay = shopName;
			backbtn.visible = false;
			for (var i:int = 0; i<shopXMLList.length(); i++)
			{
				if (this.floorOptionCoverFlow.sendUnit == shopXMLList[i].unit_key.text())
				{
					if(shopName != 'BF01')
					{
						visitfloor = shopXMLList[i].floor.text();
						visitnode = shopXMLList[i].node.text();	
					}
					else
					{
						visitfloor = 'G';
						visitnode = '16';
						this.floorOptionCoverFlow.sendUnit = 'GF03';
					}
					
				}
				else if (shopName == shopXMLList[i].attribute('id'))
				{
					
					if(shopName != '21')
					{
						visitfloor = shopXMLList[i].floor.text();
						visitnode = shopXMLList[i].node.text();
						this.floorOptionCoverFlow.sendUnit = shopXMLList[i].unit_key.text();
					}
					else
					{
						visitfloor = 'G';
						visitnode = '16';
						this.floorOptionCoverFlow.sendUnit = 'GF03';
					}
					
				}
			}

			showAnimationChangeFloor(visitfloor);
			this.floorSelection.showFloorDetail(youarehereFloor, visitfloor);
			

		}
		private function showAnimationChangeFloor(_visitfloor:String):void
		{

			dispatchEvent(new Event('showRouting'));
			
			if(_full == false)
			{
				_setScaleFullScreen(false);
		
			}
			else
			{
				_setScaleFullScreen(true);
		
			}
			

			if (_visitfloor == 'L')
			{
				visitfloorNumBer = '1';
			}
			if (_visitfloor == 'G')
			{
				visitfloorNumBer = '2';
			}
			if (_visitfloor == '1')
			{
				visitfloorNumBer = '3';
			}
			if (_visitfloor == '2')
			{
				visitfloorNumBer = '4';
			}
			if (_visitfloor == '3')
			{
				visitfloorNumBer = '5';
			}

			if (visitfloorNumBer == youarehereFloor)
			{
				_showDirectionSameFloor();
				mapMC.alpha = 1;
				
			}
			if (visitfloorNumBer != youarehereFloor)
			{
				_showDirectionDifferenceFloor();
				mapMC.alpha = 0;
			}

			_moveFloor();


		}

		private function _moveFloor():void
		{
			if (youarehereFloor == '4')
			{
				mapMC.showFloorByKey('3', '');

			}
			else
			{
				mapMC.showFloorByKey('1', '');

			}
		}

		private function _showDirectionSameFloor():void
		{
			floorOptionCoverFlow.visible = false;
			TweenMax.to(mapMC,0.2,{alpha:1});
			
			if (youarehereFloor == '2')
			{
				_delayShowPath('1');
			}
			if (youarehereFloor == '4')
			{
				_delayShowPath('3');

			}
		}
		private function _delayShowPath(floorKey:String):void
		{
			TweenMax.to(extSwf, 0.3, {alpha:0});
			mapMC.showRoute(floorKey,youarehereNode,floorKey,visitnode);
			mapMC.addPinOnNode(pointStart, floorKey,youarehereNode);
			_onShowShopDetail();
		}

		public function beforeRouting(e:Event):void
		{
			backbtn.visible = false;
		}
		public function onRoutingEnd(e:Event):void
		{
			backbtn.visible = true;
			_showShopBubble();
			
		}

		private function setMapMCState():void
		{

			if (extSwf.scaleX == 0.7)
			{

				this._mapMCScale = 0.7;
				this._mapMCX = 880;
				this._mapMCY = 85;
			}
			else
			{
				this._mapMCScale = 1;
				this._mapMCX = 588;
				this._mapMCY = 50;
			}

			
		}
		private function removeShowFloorFromKioskGroundFloor():void{
			removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);
			removeEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);
		}
		private function _showFloorFromKioskGroundFloor(e:Event):void
		{
			setMapMCState();
			mapMC.clearPath();	
			if (extSwf)
			{
				if (visitfloorNumBer == '1')
				{
					
					mapMC.showFloorByKey('0','');

					if (extSwf.currentFrame == 585)
					{
						
						visibleMap = true;
						extSwf.stop();
						extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
						TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});

					}
				}
				if (visitfloorNumBer == '3')
				{

					mapMC.showFloorByKey('2','');

					if (extSwf.currentFrame == 1120)
					{
						visibleMap = true;
						extSwf.stop();
						extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
						TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});


					}
				}
				if (visitfloorNumBer == '4')
				{
					mapMC.showFloorByKey('3','');

					if (extSwf.currentFrame == 1745)
					{
						visibleMap = true;
						extSwf.stop();
						extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
						TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});

					}
				}
				if (visitfloorNumBer == '5')
				{

					mapMC.showFloorByKey('4','');

					if (extSwf.currentFrame == 2160)
					{
						visibleMap = true;
						extSwf.stop();
						extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
						TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});
					}
				}
			}

		}
		private function _showFloorFromKioskSecondFloor(e:Event):void
		{
			setMapMCState();
			mapMC.clearPath();	
			if (visitfloorNumBer == '1')
			{

				mapMC.showFloorByKey('0','');

				if (extSwf.currentFrame == 1745)
				{
					visibleMap = true;
					extSwf.stop();
					extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
					TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});

				}
			}
			if (visitfloorNumBer == '2')
			{

				mapMC.showFloorByKey('1','');

				if (extSwf.currentFrame == 1175)
				{

					visibleMap = true;
					extSwf.stop();
					extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
					TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});
				}
			}
			if (visitfloorNumBer == '3')
			{

				mapMC.showFloorByKey('2','');

				if (extSwf.currentFrame == 545)
				{
					visibleMap = true;
					extSwf.stop();
					extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
					TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});
				}
			}
			if (visitfloorNumBer == '5')
			{
				mapMC.showFloorByKey('4','');
				if (extSwf.currentFrame == 2333)
				{	
					visibleMap = true;
					extSwf.stop();
					extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
					TweenMax.to(extSwf, 0.3, {alpha:0, onComplete:doneShowRoute});
				}
			}
		}
		private function _whenShowAnimationChangeFloor():void
		{

			floorOptionCoverFlow.visible = false;
			gaysornLogo.visible = false;

			_onShowShopDetail();
		}
		private function _showDirectionDifferenceFloor():void
		{
			
			_whenShowAnimationChangeFloor();

			if (youarehereFloor == '2')
			{
				
				if (extSwf)
				{
					TweenMax.to(extSwf, 0.5, {autoAlpha:1});
					if (visitfloorNumBer == '1' && visitfloorNumBer != '2')
					{
						extSwf.gotoAndPlay(0);
					}
					else if (visitfloorNumBer != '1' && visitfloorNumBer != '2')
					{
						extSwf.gotoAndPlay(600);
						
					}

					extSwf.addEventListener(Event.ENTER_FRAME, _showFloorFromKioskGroundFloor);

				}
			}
			else
			{

				if (extSwf)
				{

					TweenMax.to(extSwf, 0.5, {scaleX:0.7, scaleY:0.7, autoAlpha:1});
					
					if (visitfloorNumBer == '5' && visitfloorNumBer != '4')
					{
						extSwf.gotoAndPlay(1790);
					}
					else if (visitfloorNumBer != '5' && visitfloorNumBer != '4')
					{
						extSwf.gotoAndPlay(0);

					}
					extSwf.addEventListener(Event.ENTER_FRAME, _showFloorFromKioskSecondFloor);

				}
			}

		}

		private function _showShopBubble():String
		{
			//
			if (visitfloorNumBer == '1')
			{
				return _onHilightShopandAddBubbleHandler(0, this.floorOptionCoverFlow.sendUnit);
			}

			if (visitfloorNumBer == '2')
			{
				return _onHilightShopandAddBubbleHandler(1, this.floorOptionCoverFlow.sendUnit);
			}

			if (visitfloorNumBer == '3')
			{
				return _onHilightShopandAddBubbleHandler(2, this.floorOptionCoverFlow.sendUnit);
			}

			if (visitfloorNumBer == '4')
			{
				return _onHilightShopandAddBubbleHandler(3, this.floorOptionCoverFlow.sendUnit);
			}

			if (visitfloorNumBer == '5')
			{
				return _onHilightShopandAddBubbleHandler(4, this.floorOptionCoverFlow.sendUnit);


			}
		}
		var _floorKey:String;
		private function _onHilightShopandAddBubbleHandler(floorKey:String, shopKey:String = ''):void
		{
			_floorKey = floorKey;
			mapMC.addBuble(floorKey, shopKey);
			mapMC.hilightMC(floorKey, shopKey);
			//mapMC.addPinOnNode(pointStart,floorKey,visitnode);
			mapMC.animateLateWalker();
			_showFloorSelection();
		}
		private function _showFloorSelection():void
		{

			floorSelection.visible = true;
			floorSelection.x = 1920-floorSelection.width;
		}
		private function _hideFloorSelection():void
		{
			floorSelection.visible = false;
			floorSelection.x = 1990;
		}


		public function facilityChoose(e:FacilityEvent):void
		{
			floorOptionCoverFlow.showFacility(e.obj.name);
		}

		var allDone:Boolean = false;

		private function _isDone(_done:Boolean):Boolean
		{
			allDone = _done;
			return allDone;
		}
		public function clearStage():void
		{

			mapMC.alpha = 0;
			mapMC.clearPath();
			mapMC.unHilightAll();
			mapMC.mouseChildren = false;
			mapMC.mouseEnabled = false;


			floorOptionCoverFlow.hideAllShop();
			if (extSwf)
			{
				extSwf.visible = false;
			}


			shopDetail.visible = false;
			floorSelection.visible = false;

		}

		//---------------------------------------- this part for check index and set floorplan text--------------------------------------------------------------//
		public function onTapPrevBTN(e:UIControlEvent):void
		{

			floorOptionCoverFlow.goPrev();
			_checkIndex(floorOptionCoverFlow._currentFloorIndex);
		}
		public function onTapNextBTN(e:UIControlEvent):void
		{

			floorOptionCoverFlow.goNext();
			_checkIndex(floorOptionCoverFlow._currentFloorIndex);
		}
		
		private function _checkIndex(currentIndex:int):int
		{
			if (currentIndex != null)
			{
				floorplanIsShowingIndex = floorOptionCoverFlow._currentFloorIndex;
				return _setButtonName(floorplanIsShowingIndex);

			}
			else
			{
				return null;
			}

		}
		var langs:String = 'en';
		private function _setButtonName(_index:int):void
		{
			trace (langs);

			if (_index == 1)
			{
				floorProperty._floorname.text = 'Lobby Floor';
				nextBTN._flName.text = 'Ground Floor';
				prevBTN._flName.text = '3rd Floor';
				
				floorProperty._floorname_cn.text = '大堂楼层';
				nextBTNcn._flName.text = '底层';
				prevBTNcn._flName.text = '第三楼';
				
				floorProperty._floorname_jp.text = 'ロビー階';
				nextBTNjp._flName.text = 'G階';
				prevBTNjp._flName.text = '３階';
			
			}
			if (_index == 2)
			{
				floorProperty._floorname.text = 'Ground Floor';
				nextBTN._flName.text = '1st Floor';
				prevBTN._flName.text = 'Lobby Floor';
				
				floorProperty._floorname_cn.text = '底层';
				nextBTNcn._flName.text = '第一楼';
				prevBTNcn._flName.text = '大堂楼层';
				
				floorProperty._floorname_jp.text = 'G階';
				nextBTNjp._flName.text = '１階';
				prevBTNjp._flName.text = 'ロビー階';
				
			}
			if (_index == 3)
			{
				floorProperty._floorname.text = '1st Floor';
				nextBTN._flName.text = '2nd Floor';
				prevBTN._flName.text = 'Ground Floor';
				
				floorProperty._floorname_cn.text = '第一楼';
				nextBTNcn._flName.text = '第二楼';
				prevBTNcn._flName.text = '底层';
				
				
				floorProperty._floorname_jp.text = '１階';
				nextBTNjp._flName.text = '２階';
				prevBTNjp._flName.text = 'G階';
				
			}
			if (_index == 4)
			{
				floorProperty._floorname.text = '2nd Floor';
				nextBTN._flName.text = '3rd Floor';
				prevBTN._flName.text = '1st Floor';
				
				floorProperty._floorname_cn.text = '第二楼';
				nextBTNcn._flName.text = '第三楼';
				prevBTNcn._flName.text = '第一楼';
				
				
				floorProperty._floorname_jp.text = '２階';
				nextBTNjp._flName.text = '３階';
				prevBTNjp._flName.text = '１階';
				
				
			}
			if (_index == 0)
			{
				floorProperty._floorname.text = '3rd Floor';
				nextBTN._flName.text = 'Lobby Floor';
				prevBTN._flName.text = '2nd Floor';
				
				floorProperty._floorname_cn.text = '第三楼';
				nextBTNcn._flName.text = '大堂楼层';
				prevBTNcn._flName.text = '第二楼';
				
				
				floorProperty._floorname_jp.text = '３階';
				nextBTNjp._flName.text = 'ロビー階';
				prevBTNjp._flName.text = '２階';
				
				
			}

			dispatchEvent(new Event('setFloorActive'));
		}

		override public function viewWillAppear():void
		{
			//_setDefaultViewcontroller();
			
			
		}
		override public function viewWillDisappear():void
		{


		}
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			
			if(langs == 'en')
			{
				floorProperty._floorname.visible = true;
				floorProperty._floorname_cn.visible = false;
				floorProperty._floorname_jp.visible = false;
				
				nextBTN.visible = true;
				nextBTNcn.visible = false;
				nextBTNjp.visible = false;
				
				prevBTN.visible = true;
				prevBTNcn.visible = false;
				prevBTNjp.visible = false;
				
				floorProperty.facility_en.visible = true;
				floorProperty.facility_cn.visible = false;
				floorProperty.facility_jp.visible = false;
				
			
			}
			if(langs == 'cn')
			{
				floorProperty._floorname.visible = false;
				floorProperty._floorname_cn.visible = true;
				floorProperty._floorname_jp.visible = false;
				
				nextBTN.visible = false;
				nextBTNcn.visible = true;
				nextBTNjp.visible = false;
				
				prevBTN.visible = false;
				prevBTNcn.visible = true;
				prevBTNjp.visible = false;
				
				floorProperty.facility_en.visible = false;
				floorProperty.facility_cn.visible = true;
				floorProperty.facility_jp.visible = false;
				
				
			}
			if(langs == 'jp')
			{
				floorProperty._floorname.visible = false;
				floorProperty._floorname_cn.visible = false;
				floorProperty._floorname_jp.visible = true;
				
				nextBTN.visible = false;
				nextBTNcn.visible = false;
				nextBTNjp.visible = true;
				
				prevBTN.visible = false;
				prevBTNcn.visible = false;
				prevBTNjp.visible = true;
				
				floorProperty.facility_en.visible = false;
				floorProperty.facility_cn.visible = false;
				floorProperty.facility_jp.visible = true;
				
				
			}
			
			
			backbtn.onTranslate(langs);
		}
	}
}