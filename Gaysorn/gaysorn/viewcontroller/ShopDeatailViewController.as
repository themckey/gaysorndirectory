﻿package viewcontroller {
	
	import flash.display.MovieClip;
	import com.forviz.ViewController;
	import com.forviz.ui.UIImageView;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UICarousel;
	import com.pvm.ui.UIScrollView;
	import items.TextDetail;
	import com.pvm.ui.CGSize;
	
	import main.directory.ShopScrollBar
	import flash.geom.Point;
	
	public class ShopDeatailViewController extends ViewController {
		
		var logoIMG:UIImageView = new UIImageView(new Rectangle (198.95, 51.05, 216, 216));
		var uiCarousel:UICarousel;
		var picArr:Array = new Array();
		
		var scr:UIScrollView = new UIScrollView(new Rectangle(650, 753.95, 982.25, 238.05));
		var shopDetail:TextDetail = new TextDetail();
		 
	 	var scroll_btn:ShopScrollBar = new ShopScrollBar();
		
		public function ShopDeatailViewController() {
			// constructor code
			
			addChild(logoIMG);
			
			uiCarousel = new UICarousel(new Rectangle (107, 403.95, 400, 200));
			uiCarousel.animation = "fade";
			addChild(uiCarousel);
			
			
			addChild(scr);
			scr.addChild(shopDetail);
			
			scroll_btn.x = 1650;
			scroll_btn.y = 780;
			addChild(scroll_btn);
			
			
			onTranslate(langs);
			
			
		}
		
		public function set model(theModel:ShopModel):void
		{
			uiCarousel.unload();
			
			if(theModel.floor == 'G')
			{
				_unitarea.text = ': Ground Floor';
				_unitarea_cn.text = ': 底层';
				_unitarea_jp.text = ': G階';
			}
			else if(theModel.floor == 'L')
			{
				_unitarea.text = ': Lobby Floor';
				_unitarea_cn.text = ': 大堂楼层';
				_unitarea_jp.text = ': ロビー階';
			}
			else if(theModel.floor == '1')
			{
				_unitarea.text = ': 1st Floor';
				_unitarea_cn.text = ': 第一楼';
				_unitarea_jp.text = ': １階';
			}
			else if(theModel.floor == '2')
			{
				_unitarea.text = ': 2nd Floor';
				_unitarea_cn.text = ': 第二楼';
				_unitarea_jp.text = ': ２階';
			}
			else if(theModel.floor == '3')
			{
				_unitarea.text = ': 3rd Floor';
				_unitarea_cn.text = ': 第三楼';
				_unitarea_jp.text = ': ３階';
			}
			
			_tel.text = ': ' + theModel.telNo;
			
			shopDetail._detail.htmlText = theModel.desEn;
			shopDetail._detail_cn.htmlText = theModel.desCn;
			shopDetail._detail_jp.htmlText = theModel.desJp;
			
			logoIMG.loadImage(theModel.logoPath);
			picArr = theModel.pathMediaArr;
			
			
		}
		public function _loadImage() : void {
			
			
	   		for(var i:int = 0; i < picArr.length; i++)
			{
				uiCarousel.addPage(picArr[i].toString());
			}
			
			show();
			
			
		}
		public function show():void
		{
			this.visible = true;
			uiCarousel.auto = 5;
			uiCarousel.playSlide();
		}
		
		public function hide():void
		{
			
			this.visible = false;
			uiCarousel.stopSlide();
		}
		var langs:String = 'en';
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			scr.moveTo(new Point(0,0));
			
			if(langs == 'en')
			{
				shopDetail._detail.visible = true;
				shopDetail._detail_cn.visible = false;
				shopDetail._detail_jp.visible = false;
				
				_unitarea.visible = true;
				_unitarea_cn.visible = false;
				_unitarea_jp.visible = false;
				
				if(shopDetail._detail.textHeight < 212)
				{
					scroll_btn.visible = false;
					
				}
				else
				{
					scroll_btn.visible = true;
				}
				scr.contentSize = new CGSize(982.25, shopDetail._detail.textHeight + 80);
				trace(shopDetail._detail.textHeight);
			}
			if(langs == 'cn')
			{
				shopDetail._detail.visible = false;
				shopDetail._detail_cn.visible = true;
				shopDetail._detail_jp.visible = false;
				
				_unitarea.visible = false;
				_unitarea_cn.visible = true;
				_unitarea_jp.visible = false;
				
				if(shopDetail._detail_cn.textHeight < 212)
				{
					scroll_btn.visible = false;
					
				}
				else
				{
					scroll_btn.visible = true;
				}
				scr.contentSize = new CGSize(982.25, shopDetail._detail_cn.textHeight + 80);
				
				
				

			}
			if(langs == 'jp')
			{
				shopDetail._detail.visible = false;
				shopDetail._detail_cn.visible = false;
				shopDetail._detail_jp.visible = true;
				
				_unitarea.visible = false;
				_unitarea_cn.visible = false;
				_unitarea_jp.visible = true;
				
				if(shopDetail._detail_jp.textHeight < 212)
				{
					scroll_btn.visible = false;
					
				}
				else
				{
					scroll_btn.visible = true;
				}
				
				scr.contentSize = new CGSize(982.25, shopDetail._detail_jp.textHeight + 80);
				

			}
		}
		
	}
	
}
