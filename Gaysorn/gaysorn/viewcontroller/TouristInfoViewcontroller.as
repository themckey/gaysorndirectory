﻿package viewcontroller {
	import com.forviz.ViewController;
	import com.forviz.ui.UICarousel;
	import flash.geom.Rectangle;
	import main.BlurContentTourist;
	
	public class TouristInfoViewcontroller extends ViewController{
		
		var uiSlide:UICarousel = new UICarousel(new Rectangle(20,50,1770,970));
		var uiSlideCN:UICarousel = new UICarousel(new Rectangle(20,50,1770,970));
		var uiSlideJP:UICarousel = new UICarousel(new Rectangle(20,50,1770,970));
		var blurContent:BlurContentTourist = new BlurContentTourist();
		
		var langs:String = 'en';
		
		public function TouristInfoViewcontroller() {
			// constructor code
			
			this.addChild(uiSlide);
			this.addChild(uiSlideCN);
			this.addChild(uiSlideJP);
			
			uiSlide.addPage('content/tourist01.png');
			uiSlide.addPage('content/tourist02.png');
			
			uiSlideCN.addPage('content/tourist01_cn.png');
			uiSlideCN.addPage('content/tourist02_cn.png');
			
			uiSlideJP.addPage('content/tourist01_jp.png');
			uiSlideJP.addPage('content/tourist02_jp.png');
			
			blurContent.x = 1770;
			blurContent.height = 1080-110;
			addChild(blurContent);
			
			onTranslate(langs);
			
		}
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			if(langs == 'en')
			{
				uiSlide.visible = true;
				uiSlideCN.visible = false;
				uiSlideJP.visible = false;
				
				_en.visible = true;
				_jp.visible = false;
			}
			if(langs == 'cn')
			{
				uiSlide.visible = false;
				uiSlideCN.visible = true;
				uiSlideJP.visible = false;
				
				_en.visible = true;
				_jp.visible = false;
			}
			if(langs == 'jp')
			{
				uiSlide.visible = false;
				uiSlideCN.visible = false;
				uiSlideJP.visible = true;
				
				_en.visible = false;
				_jp.visible = true;
			}
		}

	}
	
}
