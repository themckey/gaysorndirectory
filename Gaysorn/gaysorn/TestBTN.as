﻿package  {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	
	public class TestBTN extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		public function TestBTN() {
			// constructor code
			
			super(theFrame);
			
			this.state = 'active';
		}
	}
	
}
