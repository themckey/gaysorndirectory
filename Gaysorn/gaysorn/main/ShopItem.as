﻿package main {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIControl;
	import manager.GaysornDataManager;
	import flash.events.Event;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UIControlEvent;
	
	
	public class ShopItem extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		var shopData:XMLList = new XMLList();
		var shopName:String;
		
		private var _model:ShopModel;
		var btnActiveName:String;
		
		public function ShopItem() {
			// constructor code
			this.mouseChildren = false;
			super(theFrame);
			
			
		
		}
		
		
		public function setupData(shopSTR:String):void
		{
			
			_shopname.text = shopSTR;
			
		}
		
		public function setNormal():void
		{
			this._ac.visible = false;
		}
		public function setActive():void
		{
			this._ac.visible = true;
		}
		
		
		public function set model(theModel:ShopModel):void
		{
			this._model = theModel;
			_shopname.text = _model.nameEn;
			
		}
		public function get model():ShopModel
		{
			return this._model;
		}
		
		
		
	}
	
}
