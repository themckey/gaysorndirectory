﻿package main {
	
	import flash.text.TextFormat;
	import flash.utils.getQualifiedClassName;
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	import com.forviz.input.keyboard.KeyItem;
	import com.forviz.input.keyboard.TouchScreenKeyboard;
	import flash.utils.Dictionary;
	import flash.geom.Rectangle;
	import com.forviz.ui.UIControlEvent;
	
	public class GaysornKeyboard extends TouchScreenKeyboard {

		var _keyItem:GaysornKeyItem;
		var _textFormatDict:Dictionary;
		//var _panel:Panel = new Panel();
		
		//var _btnClose:BtnClose = new BtnClose(new Rectangle(1780, 20, 74, 74));
		
		public function GaysornKeyboard() {
			
			
			super();
			
			this.keyWidth = 50;
			this.keyHeight = 50;
			
			
		}
		
		override public function defineKeysDefault():Array
		{
			/* Default Layout */
			
			var dr0:Array = new Array(); //Default row 0
			var dr1:Array = new Array(); //Default row 1
			var dr2:Array = new Array(); //Default row 2
			var dr3:Array = new Array(); //Default row 3
			var dr4:Array = new Array(); //Default row 4
			
			
			dr0.push({en:['1', '!']});
			dr0.push({en:['2', '@'], th:['/', '๑']});
			dr0.push({en:['3', '#'], th:['_', '๒']});
			dr0.push({en:['4', '$'], th:['ภ', '๓']});
			dr0.push({en:['5', '%'], th:['ถ', '๔']});
			dr0.push({en:['6', '^'], th:['ุ', 'ู']});
			dr0.push({en:['7', '&'], th:['ึ', '฿']});
			dr0.push({en:['8', '*'], th:['ค', '๕']});
			dr0.push({en:['9', '('], th:['ต', '๖']});
			dr0.push({en:['0', ')'], th:['จ', '๗']});
			dr0.push({en:['-', '_'], th:['ข', '๘']});
			dr0.push({en:['=', '+'], th:['ช', '๙']});
			dr0.push({key:'backspace', width:(_keyWidth) });
			
			dr1.push({en:['q', 'Q'], th:['ๆ', '๐'], offsetX:(.8 * _keyWidth)});
			dr1.push({en:['w', 'W'], th:['ไ', '"']});
			dr1.push({en:['e', 'E'], th:['ำ', 'ฎ']});
			dr1.push({en:['r', 'R'], th:['พ', 'ฑ']});
			dr1.push({en:['t', 'T'], th:['ะ', 'ธ']});
			dr1.push({en:['y', 'Y'], th:['ั', 'ํ']});
			dr1.push({en:['u', 'U'], th:['ี', '๊']});
			dr1.push({en:['i', 'I'], th:['ร', 'ณ']});
			dr1.push({en:['o', 'O'], th:['น', 'ฯ']});
			dr1.push({en:['p', 'P'], th:['ย', 'ญ']});
			dr1.push({en:['[', '{'], th:['บ', 'ฐ']});
			dr1.push({en:[']', '}'], th:['ล', ',']});
			
			
			dr2.push({en:['a', 'A'], th:['ฟ', 'ฤ'], offsetX:(1.6 * _keyWidth)});
			dr2.push({en:['s', 'S'], th:['ห', 'ฆ']});
			dr2.push({en:['d', 'D'], th:['ก', 'ฏ']});
			dr2.push({en:['f', 'F'], th:['ด', 'โ']});
			dr2.push({en:['g', 'G'], th:['เ', 'ฌ']});
			dr2.push({en:['h', 'H'], th:['้', '็']});
			dr2.push({en:['j', 'J'], th:['่', '๋']});
			dr2.push({en:['k', 'K'], th:['า', 'ษ']});
			dr2.push({en:['l', 'L'], th:['ส', 'ศ']});
			dr2.push({en:[';', ':'], th:['ว', 'ซ']});
			dr2.push({en:['\'', '"'], th:['ง', '.']});
			
			
			
			
			dr3.push({en:['z', 'Z'], th:['ผ', '('], offsetX:(0.5 * _keyWidth)});
			dr3.push({en:['x', 'X'], th:['ป', ')']});
			dr3.push({en:['c', 'C'], th:['แ', 'ฉ']});
			dr3.push({en:['v', 'V'], th:['อ', 'ฮ']});
			dr3.push({en:['b', 'B'], th:['ิ', 'ฺ']});
			dr3.push({en:['n', 'N'], th:['ื', '์']});
			dr3.push({en:['m', 'M'], th:['ท', '?']});
			dr3.push({en:[',', '<'], th:['ม', 'ฒ']});
			dr3.push({en:['.', '>'], th:['ใ', 'ฬ']});
			dr3.push({en:['/', '?'], th:['ฝ', 'ฦ']});
			dr3.push({en:['@', '@'], th:['@', '@']});
			dr3.push({key:'shift'});
			//dr3.push({key:'Enter'});
			
			//dr3.push({key:'shift', width:(_keyWidth)});
			
			//dr4.push({key:'switch_lang',  width:(_keyWidth), offsetX:(20)});
			dr4.push({key:'space', width:(_keyWidth), offsetX:(4.8 * _keyWidth)});
			
			
			
			
			return [dr0, dr1, dr2, dr3, dr4];
			
			
		}
		override public function defineKeysEmail():Array
		{
			/* Default Layout */
			
			var dr0:Array = new Array(); //Default row 0
			var dr1:Array = new Array(); //Default row 1
			var dr2:Array = new Array(); //Default row 2
			var dr3:Array = new Array(); //Default row 3
			var dr4:Array = new Array(); //Default row 4
			
			
			dr0.push({en:['1', '!']});
			dr0.push({en:['2', '@'], th:['/', '๑']});
			dr0.push({en:['3', '#'], th:['_', '๒']});
			dr0.push({en:['4', '$'], th:['ภ', '๓']});
			dr0.push({en:['5', '%'], th:['ถ', '๔']});
			dr0.push({en:['6', '^'], th:['ุ', 'ู']});
			dr0.push({en:['7', '&'], th:['ึ', '฿']});
			dr0.push({en:['8', '*'], th:['ค', '๕']});
			dr0.push({en:['9', '('], th:['ต', '๖']});
			dr0.push({en:['0', ')'], th:['จ', '๗']});
			dr0.push({en:['-', '_'], th:['ข', '๘']});
			dr0.push({en:['=', '+'], th:['ช', '๙']});
			dr0.push({key:'backspace', width:(_keyWidth) });
			
			dr1.push({en:['q', 'Q'], th:['ๆ', '๐'], offsetX:(.8 * _keyWidth)});
			dr1.push({en:['w', 'W'], th:['ไ', '"']});
			dr1.push({en:['e', 'E'], th:['ำ', 'ฎ']});
			dr1.push({en:['r', 'R'], th:['พ', 'ฑ']});
			dr1.push({en:['t', 'T'], th:['ะ', 'ธ']});
			dr1.push({en:['y', 'Y'], th:['ั', 'ํ']});
			dr1.push({en:['u', 'U'], th:['ี', '๊']});
			dr1.push({en:['i', 'I'], th:['ร', 'ณ']});
			dr1.push({en:['o', 'O'], th:['น', 'ฯ']});
			dr1.push({en:['p', 'P'], th:['ย', 'ญ']});
			dr1.push({en:['[', '{'], th:['บ', 'ฐ']});
			dr1.push({en:[']', '}'], th:['ล', ',']});
			
			
			dr2.push({en:['a', 'A'], th:['ฟ', 'ฤ'], offsetX:(1.6 * _keyWidth)});
			dr2.push({en:['s', 'S'], th:['ห', 'ฆ']});
			dr2.push({en:['d', 'D'], th:['ก', 'ฏ']});
			dr2.push({en:['f', 'F'], th:['ด', 'โ']});
			dr2.push({en:['g', 'G'], th:['เ', 'ฌ']});
			dr2.push({en:['h', 'H'], th:['้', '็']});
			dr2.push({en:['j', 'J'], th:['่', '๋']});
			dr2.push({en:['k', 'K'], th:['า', 'ษ']});
			dr2.push({en:['l', 'L'], th:['ส', 'ศ']});
			dr2.push({en:[';', ':'], th:['ว', 'ซ']});
			dr2.push({en:['\'', '"'], th:['ง', '.']});
			
			
			
			
			dr3.push({en:['z', 'Z'], th:['ผ', '('], offsetX:(0.5 * _keyWidth)});
			dr3.push({en:['x', 'X'], th:['ป', ')']});
			dr3.push({en:['c', 'C'], th:['แ', 'ฉ']});
			dr3.push({en:['v', 'V'], th:['อ', 'ฮ']});
			dr3.push({en:['b', 'B'], th:['ิ', 'ฺ']});
			dr3.push({en:['n', 'N'], th:['ื', '์']});
			dr3.push({en:['m', 'M'], th:['ท', '?']});
			dr3.push({en:[',', '<'], th:['ม', 'ฒ']});
			dr3.push({en:['.', '>'], th:['ใ', 'ฬ']});
			dr3.push({en:['/', '?'], th:['ฝ', 'ฦ']});
			dr3.push({en:['@', '@'], th:['@', '@']});
			dr3.push({key:'shift'});
			//dr3.push({key:'Enter'});
			
			//dr3.push({key:'shift', width:(_keyWidth)});
			
			//dr4.push({key:'switch_lang',  width:(_keyWidth), offsetX:(20)});
			dr4.push({key:'space', width:(_keyWidth), offsetX:(4.8 * _keyWidth)});
			
			
			
			
			return [dr0, dr1, dr2, dr3, dr4];
		}
		
		private function onTapCloseKeyboard(evt:UIControlEvent):void
		{
			this.hideMe();
		}
		
		override protected function drawBackground():void
		{
			/*
			if (!_boardBG) _boardBG = new Shape();
			
			_boardBG.graphics.clear();
			_boardBG.graphics.beginFill(0x009b7b, 0.6);
      		_boardBG.graphics.drawRoundRect(-10,-10, 20, 20, 6);
			_boardBG.graphics.endFill();
			_board.addChildAt(_boardBG, 0);*/
			
			//_boardWidth = _largestX;
			//_boardHeight = _largestY;
		}
		
		override public function animateShowMe(posX:Number, posY:Number):void
		{
			TweenMax.to(this, 0.5, {x:posX, y:posY, alpha:1, ease:Expo.easeOut, overwrite:3});
			
		}
		
		override public function animateHideMe(posX:Number, posY:Number):void
		{
			TweenMax.to(this, 0.7, {x:posX, y:posY, overwrite:true, ease:Expo.easeOut});
		}
		
		
		override public function createKey(obj:Object, options:Object = null):KeyItem
		{
			if (!_textFormatDict) {
				_textFormatDict = new Dictionary();
				_textFormatDict["en"] = new TextFormat("AccentGraphic", 24, 0x4f3933, null, null, null, null, null, "left", 11.5);
				_textFormatDict["th"] = new TextFormat("AccentGraphic", 24, 0x4f3933, null, null, null, null, null, "left");
				_textFormatDict["label"] = new TextFormat("AccentGraphic",24, 0x4f3933, null, null, null, null, null, "center");
				
				
			}
			
			_keyItem = new GaysornKeyItem(obj, {'textFormatDict' : _textFormatDict});
			
			if (obj.hasOwnProperty("key")) {
				_keyItem.gotoAndStop(obj.key);
			} 
			
			return _keyItem;
		}
		

	}
	
}
