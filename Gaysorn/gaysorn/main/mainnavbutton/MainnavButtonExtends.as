﻿package main.mainnavbutton {
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import com.forviz.ui.UIControlEvent;
	
	import com.forviz.events.DirectoryEvent;
	import flash.events.Event;
	
	public class MainnavButtonExtends extends UIButton{

		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		public var id:int = 0;
		
		public function MainnavButtonExtends() {
			// constructor code
			
			super(theFrame);
			
			
			setStateNormal();
			
		}
		
		public function setStateActive():void
		{
			this._state = 'active';
			changeStateButton();
		}
		public function setStateNormal():void
		{
			this._state = 'normal';
			changeStateButton();
		}
		
		public function changeStateButton():void
		{
			if(this._state == 'active')
			{
				this._ac.visible = true;
				//this._in.visible = false;
				this.y = -5;
			}
			else if(this._state == 'normal')
			{
				this._ac.visible = false;
				//this._in.visible = true;
				this.y = 0;
			}
		}
		
		
		
	}
	
}
