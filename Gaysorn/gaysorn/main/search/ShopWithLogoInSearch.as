﻿package main.search {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UIImageView;
	import models.FacilityModel;
	
	public class ShopWithLogoInSearch extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		public var uiImage:UIImageView = new UIImageView(new Rectangle(20.05,0,108,108));
		
		public function ShopWithLogoInSearch() {
			// constructor code
			super(theFrame);
			addChild(uiImage);
		}
		
		
		public function set model(theModel:ShopModel):void
		{
			
			_shopname.text = theModel.nameEn;
			uiImage.loadImage(theModel.logoPath);
			
		}
		
		public function set modelfacility(theModel:FacilityModel):void
		{
			
			_shopname.text = theModel.nameEn;
			uiImage.loadImage(theModel.pathMedia);
			
		}
		
	}
}
