﻿package main.language {
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	public class MainLanguageExtends extends UIButton{

		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		public function MainLanguageExtends() {
			// constructor code
			
			
			super(theFrame);
		}
		
		public function setStateActive():void
		{
			this._state = 'active';
			changeStateButton();
		}
		public function setStateNormal():void
		{
			this._state = 'normal';
			changeStateButton();
		}
		
		public function changeStateButton():void
		{
			if(this._state == 'active')
			{
				this._ac.visible = false;
				
			}
			else if(this._state == 'normal')
			{
				this._ac.visible = true;
				
			}
		}

	}
	
}
