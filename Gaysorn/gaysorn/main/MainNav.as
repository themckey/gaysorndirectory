﻿package main {
	
	import flash.display.MovieClip;
	import com.jmx2.delayedFunctionCall;
	import com.forviz.ui.UIControlEvent;
	
	import com.greensock.TweenMax;
	import flash.events.MouseEvent;
	import com.forviz.ui.UIControl;
	import main.mainnavbutton.MainnavButtonExtends;
	import com.greensock.TweenMax;
	import com.jmx2.delayedFunctionCall;
	import main.mainnavbutton.FashionCategoryButton;
	import main.mainnavbutton.JewelryCategoryButton;
	import main.mainnavbutton.LifeStyleCategoryButton;
	import main.mainnavbutton.CuisineCategoryButton;
	import main.mainnavbutton.AllCategoryButton;
	
	import manager.GaysornDataManager;
	
	public class MainNav extends MovieClip {
		
		var buttonArr:Array = new Array('all_btn', 'fashion_btn', 'jewelry_btn', 'liftstyle_btn', 'cuisine_btn', 'search_button', 'brand_button', 'tourist_button', 'ratchaprosong_button', 'wifi_button');
		var buttonName:String = '';
		
		var pointerArr:Array = new Array('_pointer1','_pointer2','_pointer3','_pointer4','_pointer5');
		
		
		var _allBtn:AllCategoryButton;
		var _fashionBtn:FashionCategoryButton;
		var _jewelryBtn:JewelryCategoryButton;
		var _lifestyleBtn:LifeStyleCategoryButton;
		var _cuisineBtn:CuisineCategoryButton;
		
		
		public function MainNav() {
			// constructor code
			
			
			
			pointer.mouseEnabled = false;
			hideCategoryPanel();
			setStateButton('hideBTN');
			showLighting();
			
			_allBtn = this.getChildByName("all_btn");
			_fashionBtn = this.getChildByName("fashion_btn");
			_jewelryBtn = this.getChildByName("jewelry_btn");
			_lifestyleBtn = this.getChildByName("liftstyle_btn");
			_cuisineBtn = this.getChildByName("cuisine_btn");
			
			_allBtn.id = GaysornDataManager.ALL;
			_fashionBtn.id = GaysornDataManager.FASHION;
			_jewelryBtn.id = GaysornDataManager.JEWELLERY;
			_lifestyleBtn.id = GaysornDataManager.LIFESTYLE;
			_cuisineBtn.id = GaysornDataManager.CUISINE;

			
			this.addEventListener(MouseEvent.CLICK, onClickMainNav);
			
			
		}
		
		public function setStateButton(btnName:String = ''):void
		{
			trace (btnName);
			onTranslate(langs);
			for(var i:int = 0; i < buttonArr.length; i++)
			{
				(this.getChildByName(buttonArr[i]) as MainnavButtonExtends).setStateNormal();
				
				if(btnName == 'hideBTN')
				{
					
					TweenMax.to(this.pointer, 0.5, {x:-100});
					showLighting();
					//(this.getChildByName(buttonArr[i]) as MainnavButtonExtends).setStateNormal();
					showPointer('');
					
				}
				if(btnName == '')
				{
					TweenMax.to(this.pointer, 0.5, {x:-200});
				}
				else if(btnName == buttonArr[i].toString())
				{
					
					(this.getChildByName(btnName) as MainnavButtonExtends).setStateActive();
					
					if(btnName == 'all_btn')
					{
						TweenMax.to(this.pointer, 0.5, {x:56.6});
						hideThenShowPanel();
						showPointer('_pointer1');
					}
					else if(btnName == 'fashion_btn')
					{
						TweenMax.to(this.pointer, 0.5, {x:191});
						hideThenShowPanel();
						showPointer('_pointer2');
					}
					else if(btnName == 'jewelry_btn')
					{
						TweenMax.to(this.pointer, 0.5, {x:420.1});
						hideThenShowPanel();
						showPointer('_pointer3');
					}
					else if(btnName == 'liftstyle_btn')
					{
						TweenMax.to(this.pointer, 0.5, {x:682.2});
						hideThenShowPanel();
						showPointer('_pointer4');
					}
					else if(btnName == 'cuisine_btn')
					{
						TweenMax.to(this.pointer, 0.5, {x:848});
						hideThenShowPanel();
						showPointer('_pointer5');
					}
					else if(btnName == 'search_button')
					{
						TweenMax.to(this.pointer, 0.5, {x:1059});
						hideCategoryPanel();
					}
					else if(btnName == 'brand_button')
					{
						TweenMax.to(this.pointer, 0.5, {x:1232.9});
						hideCategoryPanel();
					}
					else if(btnName == 'tourist_button')
					{
						TweenMax.to(this.pointer, 0.5, {x:1484.95});
						hideCategoryPanel();
					}
					else if(btnName == 'ratchaprosong_button')
					{
						TweenMax.to(this.pointer, 0.5, {x:1654});
						hideCategoryPanel();
						
					}
					else if(btnName == 'wifi_button')
					{
						TweenMax.to(this.pointer, 0.5, {x:1805.05});
						hideCategoryPanel();
					}
					
				}
			}
		}
		
		public function showPointer (_pointerName:String):void
		{
			
			for(var i:int = 0; i <pointerArr.length; i++)
			{
				var poniterAc:MovieClip = new MovieClip();
				poniterAc = this.getChildByName(pointerArr[i]);
				addChild(poniterAc);
				
				
				if(_pointerName == poniterAc.name)
				{
					TweenMax.to(poniterAc, 0.3, {y:-0.25, delay:0.5});
				}
				else
				{
					TweenMax.to(poniterAc, 0.3, {y:-26.25});
				}
			}
			
		}
		public function hidePointer ():void
		{
			for(var i:int = 0; i <pointerArr.length; i++)
			{
				var poniterAc:MovieClip = new MovieClip();
				poniterAc = this.getChildByName(pointerArr[i]);
				addChild(poniterAc);
				TweenMax.to(poniterAc, 0.3, {y:-26.25});
			}
		}
		
		private function onClickMainNav(e:MouseEvent):void
		{
			if(e.target.parent.name == 'mainnav')
			{
				showCategoryPanel();
				setStateButton(e.target.name);
			}
			if(e.target.name == 'hideBTN')
			{
				hideCategoryPanel();
				setStateButton(e.target.name);
			}
		}
		
		private function showLighting ():void
		{
			this.lighting.gotoAndPlay(5);
		}
		
		private function hideLighting ():void
		{
			this.lighting.gotoAndStop(0);
		}
		
		public function showCategoryPanel():void
		{
			hideLighting();
			TweenMax.to(this.categorypanel, 1, {y:-297});
			//TweenMax.to(this.pointer, 0.5, {x:56.6, delay:0.5});
			
			for(var i:int = 0; i < buttonArr.length; i++)
			{
			
				(this.getChildByName('all_btn') as MainnavButtonExtends).setStateActive();
					
			}
			
		}
		public function hideThenShowPanel():void
		{
			TweenMax.to(this.categorypanel, 0.2, {y:112, onComplete:showPanelAgain});
		}
		public function showPanelAgain():void
		{
			TweenMax.to(this.categorypanel, 0.4, {y:-297});
		}
		public function hideCategoryPanel():void
		{
			TweenMax.to(this.categorypanel, 0.2, {y:112});
		}
		var langs:String = 'en';
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			
			if(langs == 'en')
			{
				this.fashion_btn._ac._en.visible = true;
				this.fashion_btn._ac._cn.visible = false;
				this.fashion_btn._ac._jp.visible = false;
				
				this.fashion_btn._in.visible = true;
				this.fashion_btn._in_cn.visible = false;
				this.fashion_btn._in_jp.visible = false;
				
				this.jewelry_btn._ac._en.visible = true;
				this.jewelry_btn._ac._cn.visible = false;
				this.jewelry_btn._ac._jp.visible = false;
				
				this.jewelry_btn._in.visible = true;
				this.jewelry_btn._in_cn.visible = false;
				this.jewelry_btn._in_jp.visible = false;

				this.liftstyle_btn._ac._en.visible = true;
				this.liftstyle_btn._ac._cn.visible = false;
				this.liftstyle_btn._ac._jp.visible = false;
				
				this.liftstyle_btn._in.visible = true;
				this.liftstyle_btn._in_cn.visible = false;
				this.liftstyle_btn._in_jp.visible = false;


				this.cuisine_btn._ac._en.visible = true;
				this.cuisine_btn._ac._cn.visible = false;
				this.cuisine_btn._ac._jp.visible = false;
				
				this.cuisine_btn._in.visible = true;
				this.cuisine_btn._in_cn.visible = false;
				this.cuisine_btn._in_jp.visible = false;
				
				this.all_btn._ac._en.visible = true;
				this.all_btn._ac._cn.visible = false;
				this.all_btn._ac._jp.visible = false;
				
				this.all_btn._in.visible = true;
				this.all_btn._in_cn.visible = false;
				this.all_btn._in_jp.visible = false;
				
				this.search_button._ac._en.visible = true;
				this.search_button._ac._cn.visible = false;
				this.search_button._ac._jp.visible = false;
				
				this.search_button._in.visible = true;
				this.search_button._in_cn.visible = false;
				this.search_button._in_jp.visible = false;
				
				this.brand_button._ac._en.visible = true;
				this.brand_button._ac._cn.visible = false;
				this.brand_button._ac._jp.visible = false;
				
				this.brand_button._in.visible = true;
				this.brand_button._in_cn.visible = false;
				this.brand_button._in_jp.visible = false;
				
				this.tourist_button._ac._en.visible = true;
				this.tourist_button._ac._cn.visible = false;
				this.tourist_button._ac._jp.visible = false;
				
				this.tourist_button._in.visible = true;
				this.tourist_button._in_cn.visible = false;
				this.tourist_button._in_jp.visible = false;
				
				this.ratchaprosong_button._ac._en.visible = true;
				this.ratchaprosong_button._ac._cn.visible = false;
				this.ratchaprosong_button._ac._jp.visible = false;
				
				this.ratchaprosong_button._in.visible = true;
				this.ratchaprosong_button._in_cn.visible = false;
				this.ratchaprosong_button._in_jp.visible = false;
				
				this.wifi_button._ac._en.visible = true;
				this.wifi_button._ac._cn.visible = false;
				this.wifi_button._ac._jp.visible = false;
				
				this.wifi_button._in.visible = true;
				this.wifi_button._in_cn.visible = false;
				this.wifi_button._in_jp.visible = false;
				
			}
			if(langs == 'cn')
			{
				this.fashion_btn._ac._en.visible = false;
				this.fashion_btn._ac._cn.visible = true;
				this.fashion_btn._ac._jp.visible = false;
				
				this.fashion_btn._in.visible = false;
				this.fashion_btn._in_cn.visible = true;
				this.fashion_btn._in_jp.visible = false;
				
				this.jewelry_btn._ac._en.visible = false;
				this.jewelry_btn._ac._cn.visible = true;
				this.jewelry_btn._ac._jp.visible = false;
				
				this.jewelry_btn._in.visible = false;
				this.jewelry_btn._in_cn.visible = true;
				this.jewelry_btn._in_jp.visible = false;
				
				this.liftstyle_btn._ac._en.visible = false;
				this.liftstyle_btn._ac._cn.visible = true;
				this.liftstyle_btn._ac._jp.visible = false;
				
				this.liftstyle_btn._in.visible = false;
				this.liftstyle_btn._in_cn.visible = true;
				this.liftstyle_btn._in_jp.visible = false;
				
				this.cuisine_btn._ac._en.visible = false;
				this.cuisine_btn._ac._cn.visible = true;
				this.cuisine_btn._ac._jp.visible = false;
				
				this.cuisine_btn._in.visible = false;
				this.cuisine_btn._in_cn.visible = true;
				this.cuisine_btn._in_jp.visible = false;
				
				this.all_btn._ac._en.visible = false;
				this.all_btn._ac._cn.visible = true;
				this.all_btn._ac._jp.visible = false;
				
				this.all_btn._in.visible = false;
				this.all_btn._in_cn.visible = true;
				this.all_btn._in_jp.visible = false;
				
				this.search_button._ac._en.visible = false;
				this.search_button._ac._cn.visible = true;
				this.search_button._ac._jp.visible = false;
				
				this.search_button._in.visible = false;
				this.search_button._in_cn.visible = true;
				this.search_button._in_jp.visible = false;
				
				this.brand_button._ac._en.visible = false;
				this.brand_button._ac._cn.visible = true;
				this.brand_button._ac._jp.visible = false;
				
				this.brand_button._in.visible = false;
				this.brand_button._in_cn.visible = true;
				this.brand_button._in_jp.visible = false;
				
				this.tourist_button._ac._en.visible = false;
				this.tourist_button._ac._cn.visible = true;
				this.tourist_button._ac._jp.visible = false;
				
				this.tourist_button._in.visible = false;
				this.tourist_button._in_cn.visible = true;
				this.tourist_button._in_jp.visible = false;
				
				this.ratchaprosong_button._ac._en.visible = false;
				this.ratchaprosong_button._ac._cn.visible = true;
				this.ratchaprosong_button._ac._jp.visible = false;
				
				this.ratchaprosong_button._in.visible = false;
				this.ratchaprosong_button._in_cn.visible = true;
				this.ratchaprosong_button._in_jp.visible = false;
				
				this.wifi_button._ac._en.visible = false;
				this.wifi_button._ac._cn.visible = true;
				this.wifi_button._ac._jp.visible = false;
				
				this.wifi_button._in.visible = false;
				this.wifi_button._in_cn.visible = true;
				this.wifi_button._in_jp.visible = false;
				
			}
			
			if(langs == 'jp')
			{
				this.fashion_btn._ac._en.visible = false;
				this.fashion_btn._ac._cn.visible = false;
				this.fashion_btn._ac._jp.visible = true;
				
				this.fashion_btn._in.visible = false;
				this.fashion_btn._in_cn.visible = false;
				this.fashion_btn._in_jp.visible = true;
				
				this.jewelry_btn._ac._en.visible = false;
				this.jewelry_btn._ac._cn.visible = false;
				this.jewelry_btn._ac._jp.visible = true;
				
				this.jewelry_btn._in.visible = false;
				this.jewelry_btn._in_cn.visible = false;
				this.jewelry_btn._in_jp.visible = true;
				
				this.liftstyle_btn._ac._en.visible = false;
				this.liftstyle_btn._ac._cn.visible = false;
				this.liftstyle_btn._ac._jp.visible = true;
				
				this.liftstyle_btn._in.visible = false;
				this.liftstyle_btn._in_cn.visible = false;
				this.liftstyle_btn._in_jp.visible = true;
				
				this.cuisine_btn._ac._en.visible = false;
				this.cuisine_btn._ac._cn.visible = false;
				this.cuisine_btn._ac._jp.visible = true;
				
				this.cuisine_btn._in.visible = false;
				this.cuisine_btn._in_cn.visible = false;
				this.cuisine_btn._in_jp.visible = true;
				
				this.all_btn._ac._en.visible = false;
				this.all_btn._ac._cn.visible = false;
				this.all_btn._ac._jp.visible = true;
				
				this.all_btn._in.visible = false;
				this.all_btn._in_cn.visible = false;
				this.all_btn._in_jp.visible = true;
				
				this.search_button._ac._en.visible = false;
				this.search_button._ac._cn.visible = false;
				this.search_button._ac._jp.visible = true;
				
				this.search_button._in.visible = false;
				this.search_button._in_cn.visible = false;
				this.search_button._in_jp.visible = true;
				
				this.brand_button._ac._en.visible = false;
				this.brand_button._ac._cn.visible = false;
				this.brand_button._ac._jp.visible = true;
				
				this.brand_button._in.visible = false;
				this.brand_button._in_cn.visible = false;
				this.brand_button._in_jp.visible = true;
				
				this.tourist_button._ac._en.visible = false;
				this.tourist_button._ac._cn.visible = false;
				this.tourist_button._ac._jp.visible = true;
				
				
				this.tourist_button._in.visible = false;
				this.tourist_button._in_cn.visible = false;
				this.tourist_button._in_jp.visible = true;
				
				this.ratchaprosong_button._ac._en.visible = false;
				this.ratchaprosong_button._ac._cn.visible = false;
				this.ratchaprosong_button._ac._jp.visible = true;
				
				this.ratchaprosong_button._in.visible = false;
				this.ratchaprosong_button._in_cn.visible = false;
				this.ratchaprosong_button._in_jp.visible = true;
				
				this.wifi_button._ac._en.visible = false;
				this.wifi_button._ac._cn.visible = false;
				this.wifi_button._ac._jp.visible = true;
				
				this.wifi_button._in.visible = false;
				this.wifi_button._in_cn.visible = false;
				this.wifi_button._in_jp.visible = true;
				
			}
		}
		
	}
	
}
