﻿package main.wifi
{

	import flash.display.MovieClip;
	import com.forviz.ui.UIScrollView;
	import flash.geom.Rectangle;
	import com.forviz.ui.CGSize;
	import flash.geom.Point;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import items.CountryIndexList;
	import flash.utils.Dictionary;

	public class DropdownCountry extends MovieClip
	{
		var countryList:Array = new Array("Thailand","China","Japan","Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda","Argentina","Armenia","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Central African Republic","Chad","Chile","Colombia","Comoros","Congo (Brazzaville)","Congo","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor (Timor Timur)","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland","France","Gabon","Gambia, The","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica","Jordan",
		  "Kazakhstan","Kenya","Kiribati","Korea, North","Korea, South","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Northern Ireland","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Qatar","Romania","Russia","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Scotland","Senegal","Serbia and Montenegro","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","Spain","Sri Lanka","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzani"  ,
		  "Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom (UK)","United States (USA)","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Wales","Yemen","Zambia","Zimbabwe");

		var countryOptionBTN:CountryItem;

		var itemSCR:UIScrollView;

		var gridContainer:Sprite = new Sprite();

		var _indexList:CountryIndexList;

		var firstSttringCountry:Array = new Array();
		var sp:Sprite = new Sprite();

		public function DropdownCountry()
		{
			// constructor code

			itemSCR = new UIScrollView(new Rectangle(0,0,480,30));
			_content.addChild(itemSCR);
			itemSCR.addChild(gridContainer);
			

			addButton();
			itemSCR.contentSize = new CGSize(480,11600);
			
			this.addEventListener(MouseEvent.CLICK, hideAfterClick);


			for (var i:int = 0; i < countryList.length; i++)
			{
				var str:String = countryList[i].toString().substr(0,1);
				firstSttringCountry.push(str);
			}

			var i:int = 0;
			while (i < firstSttringCountry.length)
			{
				while (i < firstSttringCountry.length+1 && firstSttringCountry[i] == firstSttringCountry[i+1])
				{
					firstSttringCountry.splice(i, 1);
				}
				i++;
			}
			
			

		}
		public var countryListDict:Dictionary = new Dictionary();
		var countryListArr:Array = new Array();
		var count:int = 0;
		private function addButton():void
		{
			count = 0;
			for (var i:int = 0; i < countryList.length; i++)
			{
				countryOptionBTN = new CountryItem();
				countryOptionBTN.x = 5;
				countryOptionBTN.y = 5 + (i * 60);

				countryOptionBTN._names.text = countryList[i].toString();
				countryOptionBTN.name = countryList[i].toString();
				gridContainer.addChild(countryOptionBTN);
				countryListArr.push(countryOptionBTN);
				countryListDict[countryList[i].toString()] = countryOptionBTN;
				count++;
			}

			itemSCR.contentSize = new CGSize(480,5 + (count * 60));
			

		}
		public function defaultDropdown():void
		{
			//addButton();
			arrangeCountryList('');
		}
		public function arrangeCountryList(strsearch:String):void
		{
			count = 0;
			for(var i:int = 0; i < countryListArr.length; i++)
			{
				var countryString:String = countryListArr[i].name.toString().toLowerCase();
				
				if(countryListArr[i].name.toString().toLowerCase().indexOf(strsearch.toLowerCase()) == 0)
				{
					countryListArr[i].visible = true;
					countryListArr[i].y =  5 + (count * 60);
					count++;
					itemSCR.contentSize = new CGSize(480,(count * 60));
				}
				
				else
				{
					countryListArr[i].visible = false;
				}
			}
			
			
		}
		private function hideAfterClick(e:MouseEvent):void
		{
			this.visible = false;
			/*for (var i:int = 0; i < countryList.length; i++)
			{
				if (e.target.name == countryList[i].toString())
				{
					
					//itemSCR.moveTo(new Point(0,0));
					this.visible = false;
				}
			}*/
			/*if (e.target.name == 'countryDropDown')
			{
				itemSCR.moveTo(new Point(0,0));
			}*/
		}






	}

}