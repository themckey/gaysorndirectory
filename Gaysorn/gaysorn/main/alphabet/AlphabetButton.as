﻿package main.alphabet {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	
	public class AlphabetButton extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		
		public function AlphabetButton() {
			// constructor code
			
			super(theFrame);
			
			
		}
		
		public function setActive():void
		{
			this._acLabel.visible = true;
			this._inLabel.visible = false;
		}
		public function setNormal():void
		{
			this._acLabel.visible = false;
			this._inLabel.visible = true;
		}
	}
	
}
