﻿package main.alphabet
{

	import flash.display.MovieClip;
	import com.forviz.ViewController;
	import maon.alphabet.AlphabetButton;
	import com.forviz.ui.UIControlEvent;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import models.ShopModel;

	public class AlphabetButtonHolder extends MovieClip
	{

		public var alphabetArr:Array = new Array();
		public var alphabetBTN:AlphabetButton;
		public var allBTN:AlphabetButton = new AlphabetButton();

		private var buttonArray:Array = new Array();

		private var SPAN:Number = 300;

		var shopModelArray:Array = new Array();
		var shopName:String;

		public function AlphabetButtonHolder()
		{
			// constructor code



			GaysornDataManager.getInstance().addEventListener('dataInitted', onDataInnited);

		}
		private function onDataInnited(e:Event = null):void
		{

			shopModelArray = GaysornDataManager.getInstance().shopArr;

			for (var i:int = 0; i<shopModelArray.length; i++)
			{
				shopName = (shopModelArray[i] as ShopModel).nameEn.toUpperCase().substr(0,1);
				alphabetArr.push(shopName);

			}
			alphabetArr.sort();
			
			var i:int = 0;
			while (i < alphabetArr.length)
			{
				while (i < alphabetArr.length+1 && alphabetArr[i] == alphabetArr[i+1])
				{
					alphabetArr.splice(i, 1);
					
				}
				i++;
				
			}
			addAlphabetButton();


		}

		private function addAlphabetButton():void
		{
			allBTN._inLabel.text = 'All';
			allBTN._acLabel.text = 'All';
			allBTN.x = SPAN;
			//addChild(allBTN);
			allBTN.name = 'All';

			allBTN.addEventListener('tap', onTapAllButtonHandler);
			
			SPAN = SPAN + 100;
			for (var i:int = 0; i<alphabetArr.length; i++)
			{
				alphabetBTN = new AlphabetButton();
				alphabetBTN.y = -15;
				alphabetBTN.x = SPAN + (i * 60);
				this.addChild(alphabetBTN);
				
				alphabetBTN._inLabel.text = alphabetArr[i].toString().toUpperCase();
				alphabetBTN._acLabel.text = alphabetArr[i].toString().toUpperCase();
				alphabetBTN.name = alphabetArr[i].toString().toUpperCase();
				alphabetBTN.setNormal();
				buttonArray.push(alphabetBTN);
				alphabetBTN.addEventListener('tap', onTapAlphabetButtonHandler);
			}
		}

		private function onTapAllButtonHandler(e:UIControlEvent):void
		{
			setDefaultView();
		}
		private function onTapAlphabetButtonHandler(e:UIControlEvent):void
		{
			setStateButton(e.from.name);
		}
		public function setStateButton(btnName:String):void
		{
			for (var i:int = 0; i<buttonArray.length; i++)
			{
				if (btnName == (buttonArray[i] as AlphabetButton).name)
				{
					(buttonArray[i] as AlphabetButton).setActive();
					allBTN.setNormal();
				}
				else
				{
					(buttonArray[i] as AlphabetButton).setNormal();
				}
			}
		}

		public function setDefaultView():void
		{
			allBTN.setActive();
			for (var i:int = 0; i<buttonArray.length; i++)
			{
				(buttonArray[i] as AlphabetButton).setNormal();
			}
		}


	}

}