﻿package main {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import main.flooroption.MainFlootBTNExtends;
	import com.pvm.ui.UIScrollView;
	import flash.geom.Rectangle;
	import com.pvm.ui.CGSize;
	import manager.GaysornDataManager;
	import flash.events.Event;
	import flash.geom.Point;
	import main.flooroption.MainFloorBTNExtends;
	import com.forviz.ui.UIControlEvent;
	import models.ShopModel;
	import flash.display.Sprite;
	import main.directory.CategoryMask;
	import flash.utils.Dictionary;
	import main.directory.ShopScrollBar;
	import com.pvm.ui.UIScrollViewEvent;
	
	import com.greensock.TweenMax;
	import events.ShopEvent;
	import events.FloorButtonEvent;
	import flash.sampler.Sample;
	import items.NoshopTeller;
	
	
	
	public class CategoryPanel extends MovieClip {
		
		
		var floorNameArr:Array = new Array('lobby', 'ground', 'first', 'second', 'third');
		var scrollNameArr:Array = new Array('L', 'G', '1', '2', '3');
		var floorButtonArr:Array = new Array();
		var floorButton:MainFloorBTNExtends;
		
		var uiScr:UIScrollView; 
		
		var shopHolderArr:Array = new Array();
		
		var shopItem:ShopItem;
		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();
		
		var shopDict:Dictionary = new Dictionary();
		var shopid:String;
		
		var countFound:Number;
		var shopCatStr:String;
		
		var activeFloor:String;
		
		var shopScrollBar:ShopScrollBar;
		var shopScrArr:Array = new Array();
		var scrollHolder:Sprite = new Sprite();
		var tapShopEvent:ShopEvent = new ShopEvent('shopchoose');
		var tapFloorEvent:FloorButtonEvent = new FloorButtonEvent('floorchose');
		var youarehereFloor:String;
		
		//var shopEvent:ShopEvent = new ShopEvent();
		
		
		
		
		
		var count:int = 0;
		var scrollHeight:Number;
		
		var Lnoshop:NoshopTeller = new NoshopTeller();
		var Gnoshop:NoshopTeller = new NoshopTeller();
		var noshop1:NoshopTeller = new NoshopTeller();
		var noshop2:NoshopTeller = new NoshopTeller();
		var noshop3:NoshopTeller = new NoshopTeller();
		
		
		public function CategoryPanel() {
			// constructor code
			
			lobby.addChild(Lnoshop);
			Lnoshop.x = -58;
			Lnoshop.y = 76.55;
			
			ground.addChild(Gnoshop);
			Gnoshop.x = -58;
			Gnoshop.y = 76.55;
			
			first.addChild(noshop1);
			noshop1.x = -58;
			noshop1.y = 76.55;
			
			second.addChild(noshop2);
			noshop2.x = -58;
			noshop2.y = 76.55;
			
			third.addChild(noshop3);
			noshop3.x = -58;
			noshop3.y = 76.55;
			
			
			onTranslate(langs);
			
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
			
		}
		
		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor;
			onDefault();
			
			
		}
		
		private function onDefault():void
		{
			setDefaultFloorButton(youarehereFloor);
			addShopToPanel();
			
			this.hideBTN.parent.setChildIndex(hideBTN,this.numChildren - 1);
			
			
			
		}
		private function setDefaultFloorButton(youhereFloor:String):void
		{
			if(youhereFloor == '1') youhereFloor = 'lobby';
			if(youhereFloor == '2') youhereFloor = 'ground';
			if(youhereFloor == '3') youhereFloor = 'first';
			if(youhereFloor == '4') youhereFloor = 'second';
			if(youhereFloor == '5') youhereFloor = 'third';
			
			for(var i:int = 0; i < floorNameArr.length; i++)
			{
				floorButton = this.getChildByName(floorNameArr[i]) as MainFloorBTNExtends;
				floorButtonArr.push(floorButton);
				if(floorNameArr[i] == youhereFloor)
				{
					floorButton.setActive();
				}
				else
				{
					floorButton.setNormal();
				}
			}
			
			assignFloorButton();
		}
		private function addShopToPanel():void
		{
			var theFrameL:Rectangle = new Rectangle(40,120,367,279)
			addShopByFloor('L', theFrameL);
			
			var theFrameG:Rectangle = new Rectangle(408,120,367,279)
			addShopByFloor('G', theFrameG);
			
			var theFrame1:Rectangle = new Rectangle(777,120,366.5,279)
			addShopByFloor('1', theFrame1);
			
			var theFrame2:Rectangle = new Rectangle(1145,120,366.5,279)
			addShopByFloor('2', theFrame2);
			
			var theFrame3:Rectangle = new Rectangle(1513.2,120,366.5,279)
			addShopByFloor('3', theFrame3);
			
			
			
		}
		public function assignFloorButton():void
		{
			for(var i:int = 0; i<floorButtonArr.length; i++)
			{
				(floorButtonArr[i] as MainFloorBTNExtends).addEventListener('tap', onTapFloorBTNHandler);
			}
			
		}
		
		private function onTapFloorBTNHandler (e:UIControlEvent):void
		{
			setFloorBtnActiveByName(e.target.name);
			hideAllShopActive();
			
			tapFloorEvent.obj = e.from;
			dispatchEvent(tapFloorEvent);
			
		}
		
		public function setFloorBtnActiveByName(floorItemName:String):void
		{
			trace("setFloorBtnActiveByName", floorItemName);
			//trace('floorItemName',  floorItemName);
			if(floorItemName == 'L') floorItemName = 'lobby';
			if(floorItemName == 'G') floorItemName = 'ground';
			if(floorItemName == '1') floorItemName = 'first';
			if(floorItemName == '2') floorItemName = 'second';
			if(floorItemName == '3') floorItemName = 'third';
			

			for(var i:int = 0; i < floorButtonArr.length; i++)
			{
				(floorButtonArr[i] as MainFloorBTNExtends).setNormal();
				if(floorButtonArr[i].name == floorItemName)
				{
					(floorButtonArr[i] as MainFloorBTNExtends).setActive();
					activeFloor = floorItemName;
				}
				
			}
			
			showScrollShopCat(floorItemName);
			
			
			
		}
		public function get floorItemName():String
		{
			return activeFloor;
		}
		
		private function showScrollShopCat(catShow:String):void
		{
			if(catShow == 'lobby') catShow = 'L';
			if(catShow == 'ground') catShow = 'G';
			if(catShow == 'first') catShow = '1';
			if(catShow == 'second') catShow = '2';
			if(catShow == 'third') catShow = '3';
			
			for(var i:int = 0; i<shopScrArr.length; i++)
			{
				TweenMax.to(shopScrArr[i], 0.3, {y:40});
				
				if(shopScrArr[i].name == catShow)
				{
					shopScrArr[i].visible = true;
				}
				else
				{
					shopScrArr[i].visible = false;
				}
			}
			
			
		}
		
		var scrArr:Array = new Array();
		var scrDict:Dictionary = new Dictionary();
		var noshopDict:Dictionary = new Dictionary();
		
		private function addShopByFloor(floorName:String, frame:Rectangle):void
		{
			
			count = 0;
			var nameText:String;
			
			uiScr = new UIScrollView(frame);
			uiScr.name = floorName;
			addChild(uiScr);
			scrDict[floorName] = uiScr;
			
			
			
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			for (var i:int = 0; i < shopModelArray.length; i++) {
				floorText = (shopModelArray[i] as ShopModel).floor;
				shopid = (shopModelArray[i] as ShopModel).id;
				if(floorText == floorName)
				{ 
					
					shopItem = new ShopItem();
					shopItem.y = (count * 50);
					uiScr.addChild(shopItem);
					
					shopItem.setNormal();
					shopItem.name = shopid;
					shopItem.model = shopModelArray[i];
					shopItem.addEventListener('tap', onTap);
					shopHolderArr.push(shopItem);
					shopDict[shopid] = shopItem;
					count++;
					
				}
			}
			
			for(var n:int = 0; n<shopHolderArr.length; n++)
			{
				(shopHolderArr[n] as ShopItem).addEventListener('tap', onTapShopItemHamdler);
			}

			
			scrollHeight = (count * 50) + 100;
			uiScr.contentSize = new CGSize(366, scrollHeight);
			
			
			addScrollBar(floorName);
			uiScr.addEventListener('scrollViewDidScroll', onMoveScrollButton);
			uiScr.addEventListener('scrollViewWillBeginDragging', onBeginDragingScroll);
			
			
			
		}
		public function onTap(e:UIControlEvent):void
		{
			//trace ('from cat panel',e.from.name);
			tapShopEvent.obj = e.from;
			//trace ('from cat panel',tapShopEvent.obj);
			dispatchEvent(tapShopEvent);
			
		}
		
		private function addScrollBar(cat:String):void
		{
			shopScrollBar = new ShopScrollBar();
			shopScrollBar.x = (uiScr.width + uiScr.x) - 15;
			shopScrollBar.y = 40;
			addChild(shopScrollBar);
			
			shopScrollBar.name = cat;
			shopScrArr.push(shopScrollBar);
			
			for(var i:int = 0; i<shopScrArr.length; i++)
			{
				if(shopScrArr[i].name == 'G')
				{
					shopScrArr[i].visible = true;
				}
				else
				{
					shopScrArr[i].visible = false;
				}
			}
			
		}
		public var scr:UIScrollView;
		private function onMoveScrollButton(e:UIScrollViewEvent):void
		{
			scr = (e.from as UIScrollView);
			trace("onMoveScrollButton", scr.name);
			showScrollShopCat(scr.name);
			//setFloorBtnActiveByName(scr.name);
			setCategoryScrollBar(scr.contentPoint.y);
			
			
		}
		private function onBeginDragingScroll(e:UIScrollViewEvent):void
		{
			hideAllShopActive();
		}
		
		private function setCategoryScrollBar(_yScr:Number = 0):void
		{
		
			var contentStreak:Number = Math.floor(_yScr/100);
			var scrollStreak:Number = (85) * (-contentStreak);
			
			if(contentStreak < 0)
			{
				if(scrollStreak > 120)
				{
					scrollStreak = 120;
				}
				for(var i:int = 0; i<shopScrArr.length; i++)
				{
					TweenMax.to(shopScrArr[i], 0.3, {y:scrollStreak});
				}
				
				
			}
			else
			{
				if(scrollStreak <= 40)
				{
					scrollStreak = 40;
				}
				for(var i:int = 0; i<shopScrArr.length; i++)
				{
					TweenMax.to(shopScrArr[i], 0.3, {y:scrollStreak});
				}
			}
			
			
		}
		
		
		private function onTapShopItemHamdler (e:UIControlEvent):void
		{
			setShopActiveByName(e.from.name);
			dispatchEvent(new Event('floorActive'));
			dispatchEvent(new Event('shopActiveDone'));
			
			
		}
		public function hideAllShopActive ():void
		{
			for(var i:int = 0; i < shopHolderArr.length; i++)
			{
				(shopHolderArr[i] as ShopItem).setNormal();
			}
		}
		public function setShopActiveByName (shops:String = ''):void
		{
			for(var i:int = 0; i < shopHolderArr.length; i++)
			{
				(shopHolderArr[i] as ShopItem).setNormal();
				
				if(shopHolderArr[i].name == shops)
				{
					(shopHolderArr[i] as ShopItem).setActive();
					
				}
				if(shops == 'dummy')
				{
					(shopHolderArr[i] as ShopItem).setNormal();
				}
				
			}
			
		}
		var ssName:String;
		private function translateShopFloor(shopSTR:String):void
		{
			for (var i:int = 0; i < shopModelArray.length; i++) {
				var idShop:String = (shopModelArray[i] as ShopModel).id;
				if(shopSTR == idShop)
				{
					var floorSTR:String = (shopModelArray[i] as ShopModel).floor;
					
					if(floorSTR == 'L') floorSTR = 'lobby';
					if(floorSTR == 'G') floorSTR = 'ground';
					if(floorSTR == '1') floorSTR = 'first';
					if(floorSTR == '2') floorSTR = 'second';
					if(floorSTR == '3') floorSTR = 'third';
				}
			}
			setFloorBtnActiveByName(floorSTR);
			ssName = floorSTR;
		}
		var _tModel:ShopModel;
		var countShopInCategoryByFloor:Dictionary = new Dictionary();
		
		public function reArrangeShopByCat(category:int):void
		{
			countShopInCategoryByFloor["L"] = 0;
			countShopInCategoryByFloor["G"] = 0;
			countShopInCategoryByFloor["1"] = 0;
			countShopInCategoryByFloor["2"] = 0;
			countShopInCategoryByFloor["3"] = 0;
			
			shopHolderArr.sort();
			//setFloorBtnActiveByName(ssName);
			
			for (var i:int = 0; i < shopHolderArr.length; i++ ) {
				
				var shopItem:ShopItem = shopHolderArr[i] as ShopItem;
				if(category == 0)
				{
					shopItem.visible = true;
					shopItem.y = (countShopInCategoryByFloor[shopItem.model.floor] * 50);
					countShopInCategoryByFloor[shopItem.model.floor] = countShopInCategoryByFloor[shopItem.model.floor] + 1;
					
					
				}
				else if (shopItem.model.isCategory(category)) {
					
					
					shopItem.visible = true;
					shopItem.y = (countShopInCategoryByFloor[shopItem.model.floor] * 50);
					countShopInCategoryByFloor[shopItem.model.floor] = countShopInCategoryByFloor[shopItem.model.floor] + 1;
					
					
				} else {
					shopItem.visible = false;
					
				}
				shopItem = null;
				
			}
			setButtonScroll();
		}
		
		private function setButtonScroll():void
		{
			scrollHeight = ((countShopInCategoryByFloor['L'] * 50)) + 150;
			scrDict['L'].contentSize = new CGSize(366, scrollHeight);
			scrDict['L'].moveTo(new Point(0,0));
		
			scrollHeight = ((countShopInCategoryByFloor['G'] * 50)) + 150;
			scrDict['G'].contentSize = new CGSize(366, scrollHeight);
			scrDict['G'].moveTo(new Point(0,0));
			
			scrollHeight = ((countShopInCategoryByFloor['1'] * 50)) + 150;
			scrDict['1'].contentSize = new CGSize(366, scrollHeight);
			scrDict['1'].moveTo(new Point(0,0));
			
			scrollHeight = ((countShopInCategoryByFloor['2'] * 50)) + 150;
			scrDict['2'].contentSize = new CGSize(366, scrollHeight);
			scrDict['2'].moveTo(new Point(0,0));
			
			scrollHeight = ((countShopInCategoryByFloor['3'] * 50)) + 150;
			scrDict['3'].contentSize = new CGSize(366, scrollHeight);
			scrDict['3'].moveTo(new Point(0,0));
			
			
			if((countShopInCategoryByFloor['L']) == 0)
		  	{
				Lnoshop.visible = true;
			}
			else if((countShopInCategoryByFloor['L']) != 0)
			{
				Lnoshop.visible = false;
			}
			if((countShopInCategoryByFloor['G']) == 0)
		  	{
				Gnoshop.visible = true;
			}
			else if((countShopInCategoryByFloor['G']) != 0)
			{
				Gnoshop.visible = false;
			}
			if((countShopInCategoryByFloor['1']) == 0)
		  	{
				noshop1.visible = true;
			}
			else if((countShopInCategoryByFloor['1']) != 0)
			{
				noshop1.visible = false;
			}
			if((countShopInCategoryByFloor['2']) == 0)
		  	{
				noshop2.visible = true;
			}
			else if((countShopInCategoryByFloor['2']) != 0)
			{
				noshop2.visible = false;
			}
			if((countShopInCategoryByFloor['3']) == 0)
		  	{
				noshop3.visible = true;
			}
			else if((countShopInCategoryByFloor['3']) != 0)
			{
				noshop3.visible = false;
			}
		}
		
		public function viewWillAppear():void
		{
			setDefaultFloorButton(youarehereFloor);
		
			
			
		}
		
		var langs:String = 'en';
		
		public function onTranslate(_langs:String):void
		{
			langs = _langs;
			
			if(langs == 'en')
			{
				ground.in_en.visible = true;
				ground.in_cn.visible = false;
				ground.in_jp.visible = false;
				
				ground._acText.ac_en.visible = true;
				ground._acText.ac_cn.visible = false;
				ground._acText.ac_jp.visible = false;
				
				lobby.in_en.visible = true;
				lobby.in_cn.visible = false;
				lobby.in_jp.visible = false;
				
				lobby._acText.ac_en.visible = true;
				lobby._acText.ac_cn.visible = false;
				lobby._acText.ac_jp.visible = false;
				
				first.in_en.visible = true;
				first.in_cn.visible = false;
				first.in_jp.visible = false;
				
				first._acText.ac_en.visible = true;
				first._acText.ac_cn.visible = false;
				first._acText.ac_jp.visible = false;
				
				second.in_en.visible = true;
				second.in_cn.visible = false;
				second.in_jp.visible = false;
				
				second._acText.ac_en.visible = true;
				second._acText.ac_cn.visible = false;
				second._acText.ac_jp.visible = false;
				
				third.in_en.visible = true;
				third.in_cn.visible = false;
				third.in_jp.visible = false;
				
				third._acText.ac_en.visible = true;
				third._acText.ac_cn.visible = false;
				third._acText.ac_jp.visible = false;
			}
			if(langs == 'cn')
			{
				ground.in_en.visible = false;
				ground.in_cn.visible = true;
				ground.in_jp.visible = false;
				
				ground._acText.ac_en.visible = false;
				ground._acText.ac_cn.visible = true;
				ground._acText.ac_jp.visible = false;
				
				lobby.in_en.visible = false;
				lobby.in_cn.visible = true;
				lobby.in_jp.visible = false;
				
				lobby._acText.ac_en.visible = false;
				lobby._acText.ac_cn.visible = true;
				lobby._acText.ac_jp.visible = false;
				
				first.in_en.visible = false;
				first.in_cn.visible = true;
				first.in_jp.visible = false;
				
				first._acText.ac_en.visible = false;
				first._acText.ac_cn.visible = true;
				first._acText.ac_jp.visible = false;
				
				second.in_en.visible = false;
				second.in_cn.visible = true;
				second.in_jp.visible = false;
				
				second._acText.ac_en.visible = false;
				second._acText.ac_cn.visible = true;
				second._acText.ac_jp.visible = false;
				
				third.in_en.visible = false;
				third.in_cn.visible = true;
				third.in_jp.visible = false;
				
				third._acText.ac_en.visible = false;
				third._acText.ac_cn.visible = true;
				third._acText.ac_jp.visible = false;
			}
			if(langs == 'jp')
			{
				ground.in_en.visible = false;
				ground.in_cn.visible = false;
				ground.in_jp.visible = true;
				
				ground._acText.ac_en.visible = false;
				ground._acText.ac_cn.visible = false;
				ground._acText.ac_jp.visible = true;
				
				lobby.in_en.visible = false;
				lobby.in_cn.visible = false;
				lobby.in_jp.visible = true;
				
				lobby._acText.ac_en.visible = false;
				lobby._acText.ac_cn.visible = false;
				lobby._acText.ac_jp.visible = true;
				
				first.in_en.visible = false;
				first.in_cn.visible = false;
				first.in_jp.visible = true;
				
				first._acText.ac_en.visible = false;
				first._acText.ac_cn.visible = false;
				first._acText.ac_jp.visible = true;
				
				second.in_en.visible = false;
				second.in_cn.visible = false;
				second.in_jp.visible = true;
				
				second._acText.ac_en.visible = false;
				second._acText.ac_cn.visible = false;
				second._acText.ac_jp.visible = true;
				
				third.in_en.visible = false;
				third.in_cn.visible = false;
				third.in_jp.visible = true;
				
				third._acText.ac_en.visible = false;
				third._acText.ac_cn.visible = false;
				third._acText.ac_jp.visible = true;
			}
		}
		
		
		
		
		
	}
	
}
