﻿package events {
	import flash.events.Event;
	
	public class LanguageEvent extends Event{

		public static const		LANGUAGECHANGED		:String		=	"languagechanged";
		
		
		public var obj:Object;
		public var strname:String;
		
		public function LanguageEvent(type:String, strname = "", obj:Object = null) {
			// constructor code
			
			this.obj = obj;
			this.strname = strname;
			super( type );
		}
		
		public override function clone():Event 
		{
			var newEvt : LanguageEvent = new LanguageEvent ( type, strname, obj);
			newEvt.obj = obj;
			newEvt.strname = strname;
			return newEvt;
		}

	}
	
}
