﻿package 
{

	import flash.display.MovieClip;
	import com.forviz.TouchScreenApplication;
	import flash.geom.Rectangle;
	import com.forviz.ViewControllerManager;
	import viewcontroller.DirectoryViewController;
	import viewcontroller.SearchViewcontroller;
	import viewcontroller.AlphabetViewcontroller;
	import viewcontroller.TouristInfoViewcontroller;
	import viewcontroller.RatchaprasongViewcontroller;
	import viewcontroller.FreeWifiViewcontroller;
	import flash.events.MouseEvent;
	import com.forviz.ui.UIControlEvent;
	import main.GaysornKeyboard;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import main.wifi.DropdownCountry;
	
	import com.greensock.TweenMax;
	import main.wifi.SuccessWifi;
	import com.forviz.events.ViewControllerEvent;
	
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.AutoAlphaPlugin;
	import events.ShopEvent;
	import models.ShopModel;
	import events.FloorButtonEvent;
	import viewcontroller.LandingPageViewcontroller;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.TimerEvent;
	import com.pvm.LanguageEvent;
	import events.LanguageEvent;




	public class Gaysorn extends TouchScreenApplication
	{

		var vcManagerFrame:Rectangle = new Rectangle(0,0,1920,1080);
		private var vcManager:ViewControllerManager = new ViewControllerManager(vcManagerFrame);
		
		private var dm:GaysornDataManager;
		
		private var _keyboard:GaysornKeyboard = new GaysornKeyboard();

		//variable viewcontroller

		private var directoryVC:DirectoryViewController = new DirectoryViewController();
		private var searchVC:SearchViewcontroller = new SearchViewcontroller();
		private var alphabetVC:AlphabetViewcontroller = new AlphabetViewcontroller();
		private var touristinfoVC:TouristInfoViewcontroller = new TouristInfoViewcontroller();
		private var ratchaprasongVC:RatchaprasongViewcontroller = new RatchaprasongViewcontroller();
		private var wifiVC:FreeWifiViewcontroller = new FreeWifiViewcontroller();
		private var landingVC:LandingPageViewcontroller = new LandingPageViewcontroller();
		
		var btnName:String;
		
		public var mainnav:MovieClip = new MovieClip();
		public var languageBar:MovieClip = new MovieClip();
		
		
		var vcName:String;
		
		
		var xmlList:XMLList = new XMLList();
		var shopXMLList:XMLList = new XMLList();
		var shopModelArr:Array = new Array();
		
		
		var loader:Loader = new Loader();
		var refreshHome:Number;
		var restartApp:Number;
		var showLang:String;
		
		var langs:String = 'en';
		public function Gaysorn()
		{
			
			this.debug();
			
			// constructor code
			

			
			dm = GaysornDataManager.getInstance();
			dm.registerXML('shop', 'content/shop.xml');
			dm.registerXML('config', 'config.xml');
			dm.registerXML('facility', 'content/facilities.xml');

			dm.loadXML();
			
			dm.addEventListener('dataInitted', onInitData);
			
			
			
			this.touchScreenKeyboard = _keyboard;
			this.touchScreenKeyboard.animateHideMe(1040,2000);
			TweenPlugin.activate([AutoAlphaPlugin]);
			
			
			VCHolder.addChild(vcManager);
			mainnav = VCHolder.getChildByName('mainnav');
			addChild(mainnav);
			
			
			
			languageBar = VCHolder.getChildByName('languageBar');
			VCHolder.addChild(languageBar);
			VCHolder.languageBar.addEventListener('languagechanged', onChangeLanguage);
			
			addChild(landingVC);
			landingVC.addChild(loader);
			loader.load(new URLRequest('LandingPage.swf'));
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, landingPageLoaded);

			vcManager.addViewController(directoryVC, 'directory');
			vcManager.addViewController(searchVC, 'search');
			vcManager.addViewController(alphabetVC, 'alphabet');
			vcManager.addViewController(touristinfoVC, 'touristinfo');
			vcManager.addViewController(ratchaprasongVC, 'ratchaprasong');
			vcManager.addViewController(wifiVC, 'wifi');
			
			
			searchVC.search_TF.keyboard = _keyboard;
			searchVC.search_TF.addEventListener("focus", onFocusSearchTextField);
			searchVC.search_TF.addEventListener("valueChanged", onSearchValueChanged);
			
			wifiVC.id_TF.keyboard = _keyboard;
			wifiVC.email_TF.keyboard = _keyboard;
			wifiVC.country_TF.keyboard = _keyboard;
			
			

			vcManager.initialViewController = directoryVC;
			vcManager.animation = "fade";
			
			
			this.addEventListener(MouseEvent.CLICK, onOpenView);
			
			directoryVC.addEventListener('setFloorActive', setFloortoActive);
			directoryVC.addEventListener('showRouting', onDirectoryViewShowRouting);
			directoryVC.addEventListener('_doneRouting', onDirectoryViewDoneRouting);
			
			searchVC.addEventListener('shopchoose', onTapSearchItem);
			alphabetVC.addEventListener('shopchoose',onTapBrandItem);
			
			mainnav.categorypanel.addEventListener('shopchoose', onTapShopEvent);
			mainnav.categorypanel.addEventListener('floorchose', onTapFloorBTNHandler);
			mainnav.categorypanel.addEventListener('floorActive', onFloorActive);
			
			
			TweenMax.to(VCHolder, 0.9, {visible:false});
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitdataGaysorn);
			
			
			
			

		}
		
		
		private function onInitData(e:Event = null):void
		{
			
			refreshHome = dm.configsXML.resfresh_timer;
			restartApp = dm.configsXML.restart_timer;
			showLang = dm.configsXML.show_lang.text();
			
			setupRefreshAndRestart();
			showLangBar(showLang);
			
		}
		public function setupRefreshAndRestart():void
		{
			trace(refreshHome)
			this.idleTime = Number(refreshHome*1000);
			this.timeToRestart = Number(restartApp*1000);
			
		}
		private function showLangBar(_show:String):void
		{
			trace (_show);
			(_show == 'true')?VCHolder.languageBar.visible = true:VCHolder.languageBar.visible = false;
			
			
		}
		
		override public function onIdle(evt:TimerEvent):void
		{
			TweenMax.to(landingVC, 0.9, {autoAlpha:1});
			landingVC.parent.setChildIndex(landingVC, landingVC.parent.numChildren -1);
			vcManager.closeAllViewController();
			this.touchScreenKeyboard.animateHideMe(1040,2000);
			this._restartTimer.start();
		}
		public function landingPageLoaded(e:Event):void
		{
			//trace (e.target.content);
			var mcLanding:MovieClip = new MovieClip();
			mcLanding = e.target.content;
			
			var directBTN:MovieClip = new MovieClip();
			directBTN = (e.target.content as MovieClip).getChildByName('direct');
			directBTN.addEventListener(MouseEvent.CLICK, clickDirect);
			
			var touristBTN:MovieClip = new MovieClip();
			touristBTN = (e.target.content as MovieClip).getChildByName('tourist');
			touristBTN.addEventListener(MouseEvent.CLICK, clickTourist);
			
			var districtBTN:MovieClip = new MovieClip();
			districtBTN = (e.target.content as MovieClip).getChildByName('ratchaprasong');
			districtBTN.addEventListener(MouseEvent.CLICK, clickRatchaprasong);
			
			var wifiBTN:MovieClip = new MovieClip();
			wifiBTN = (e.target.content as MovieClip).getChildByName('wifi');
			wifiBTN.addEventListener(MouseEvent.CLICK, clickWifi);
			
			var _gaysorn:MovieClip = new MovieClip();
			_gaysorn = (e.target.content as MovieClip).getChildByName('_gaysorn');
			_gaysorn.addEventListener(MouseEvent.CLICK, clickLogo);
			
		}
		
		public function clickDirect (e:MouseEvent):void
		{
			mainnav.setStateButton('');
			TweenMax.to(VCHolder, 0.9, {autoAlpha:1});
			TweenMax.to(landingVC, 0.9, {autoAlpha:0});
			vcManager.openViewController(directoryVC);
		}
		
		public function clickLogo (e:MouseEvent):void
		{
			mainnav.setStateButton('');
			TweenMax.to(VCHolder, 0.9, {autoAlpha:1});
			TweenMax.to(landingVC, 0.9, {autoAlpha:0});
			vcManager.openViewController(directoryVC);
		}
		public function clickTourist (e:MouseEvent):void
		{
			mainnav.setStateButton('tourist_button');
			TweenMax.to(VCHolder, 0.9, {autoAlpha:1});
			TweenMax.to(landingVC, 0.9, {autoAlpha:0});
			vcManager.openViewController(touristinfoVC);
		}
		public function clickRatchaprasong (e:MouseEvent):void
		{
			mainnav.setStateButton('ratchaprosong_button');
			TweenMax.to(VCHolder, 0.9, {autoAlpha:1});
			TweenMax.to(landingVC, 0.9, {autoAlpha:0});
			vcManager.openViewController(ratchaprasongVC);
		}
		public function clickWifi (e:MouseEvent):void
		{
			mainnav.setStateButton('wifi_button');
			TweenMax.to(VCHolder, 0.9, {autoAlpha:1});
			TweenMax.to(landingVC, 0.9, {autoAlpha:0});
			vcManager.openViewController(wifiVC);
			this.touchScreenKeyboard.animateShowMe(960, 840);
		}
		
		public function onDirectoryViewShowRouting (e:Event = null):void
		{
			mainnav.hideCategoryPanel();
			mainnav.setStateButton('');
			mainnav.showPointer('');
			mainnav.categorypanel.setShopActiveByName('dummy');
			this._idleTimer.stop();
			
		}
		public function onDirectoryViewDoneRouting (e:Event = null):void
		{
			trace('_idleTimer start ')
			this._idleTimer.reset();
			this._idleTimer.start();
		}
		public function onInitdataGaysorn(e:Event):void
		{
			xmlList = GaysornDataManager.getInstance().shopXMLList;
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			
		}
		public function onTapShopEvent(e:ShopEvent):void{
			
			
			shopModelArr = GaysornDataManager.getInstance().shopArr;
			
			for(var i:int = 0; i < shopModelArr.length; i++){
				
				var shopId:String = (shopModelArr[i] as ShopModel).id;
				if(e.obj.name == shopId)
				{
					var flShop:String = (shopModelArr[i] as ShopModel).floor;
					var __node:String = (shopModelArr[i] as ShopModel).node;
					var unitkey:String = (shopModelArr[i] as ShopModel).unitKey;
					mainnav.categorypanel.setFloorBtnActiveByName(flShop);
					
					moveFloor(flShop);
					directoryVC.floorOptionCoverFlow.hideAllShop();
					mainnav.hideCategoryPanel();
					if(__node != '-') directoryVC.getShopInfoFromShopCategory(e.obj.name);
					
				}
				
			}
			
			
		}
		public function moveFloor(str:String = ''):void{
			
			if(str == 'L') directoryVC.floorOptionCoverFlow.goToFloor(1);
			if(str == 'G') directoryVC.floorOptionCoverFlow.goToFloor(2);
			if(str == '1') directoryVC.floorOptionCoverFlow.goToFloor(3);
			if(str == '2') directoryVC.floorOptionCoverFlow.goToFloor(4);
			if(str == '3') directoryVC.floorOptionCoverFlow.goToFloor(5);
			
		}
						
		public function onTapSearchItem(e:ShopEvent):void
		{
			trace (e.obj.name);
			vcManager.openViewController(directoryVC);
			directoryVC.getShopInfoFromShopCategory(e.obj.name);
			this.touchScreenKeyboard.animateHideMe(1040,2000);
		}
		
		public function onTapBrandItem(e:ShopEvent):void
		{
			//trace (e.obj.name);
			vcManager.openViewController(directoryVC);
			directoryVC.getShopInfoFromShopCategory(e.obj.name);
			this.touchScreenKeyboard.animateHideMe(1040,2000);
		}
		
		public function onTapFloorBTNHandler(e:FloorButtonEvent):void{
			
			if(e.obj.name == 'lobby') directoryVC.floorOptionCoverFlow.goToFloor(1);
			if(e.obj.name == 'ground') directoryVC.floorOptionCoverFlow.goToFloor(2);
			if(e.obj.name == 'first') directoryVC.floorOptionCoverFlow.goToFloor(3);
			if(e.obj.name == 'second') directoryVC.floorOptionCoverFlow.goToFloor(4);
			if(e.obj.name == 'third') directoryVC.floorOptionCoverFlow.goToFloor(5);
			
		}
		public function setFloortoActive(e:Event = null):void{

			setFloor();
			
		}
		
		function setFloor():void{
			
			if(directoryVC.floorProperty._floorname.text == 'Lobby Floor') mainnav.categorypanel.setFloorBtnActiveByName('lobby');
			if(directoryVC.floorProperty._floorname.text == 'Ground Floor') mainnav.categorypanel.setFloorBtnActiveByName('ground');
			if(directoryVC.floorProperty._floorname.text == '1st Floor') mainnav.categorypanel.setFloorBtnActiveByName('first');
			if(directoryVC.floorProperty._floorname.text == '2nd Floor') mainnav.categorypanel.setFloorBtnActiveByName('second');
			if(directoryVC.floorProperty._floorname.text == '3rd Floor') mainnav.categorypanel.setFloorBtnActiveByName('third');
			
			mainnav.categorypanel.setShopActiveByName('');
		}
		public function onFloorActive(e:Event = null):void
		{
			
			
		}
		public function onFocusSearchTextField(e:Event):void
		{
			this.touchScreenKeyboard.animateShowMe(960, 840);
		}
		
		public function onSearchValueChanged(evt:UIControlEvent):void
		{
			
		}
		
		public function onFloorplanMoved(e:Event = null):void
		{
			this.mouseChildren = true;
		}
		
		public function onOpenView(e:MouseEvent):void
		{
			btnName = e.target.name;
			if(e.target.parent.name == 'mainnav'){
				
				directoryVC.onTapMainnavHandler();
				
				mainnav.categorypanel.setShopActiveByName('dummy');
				mainnav.parent.setChildIndex(mainnav, mainnav.parent.numChildren - 1);
				
				//setFloortoActive();
				
				
			}
		
			if (btnName == 'ratchaprosong_button'){
				vcManager.openViewController(ratchaprasongVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
			}
			else if (btnName == 'search_button'){
				vcManager.openViewController(searchVC);
				this.touchScreenKeyboard.animateShowMe(960, 840);
				searchVC.search_TF.focus = true;
				searchVC.search_TF.clear();
				mainnav.parent.setChildIndex(mainnav, mainnav.parent.numChildren - 1);
				searchVC.onTranslate(langs);
			}
			else if (btnName == 'tourist_button'){
				vcManager.openViewController(touristinfoVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
			}
			else if (btnName == 'brand_button'){
				vcManager.openViewController(alphabetVC);
				alphabetVC._buttonHolder.setDefaultView();
				this.touchScreenKeyboard.animateHideMe(1040,2000);
			}
			else if (btnName == 'wifi_button' || e.target.name == '_continue'){
				vcManager.openViewController(wifiVC);
				this.touchScreenKeyboard.animateShowMe(960, 840);
				TweenMax.to(wifiVC.keyboardBG, 0.5, {y:624.1});
				touchScreenKeyboard.mode = 'email';
				wifiVC.onTranslate(langs);
				
			}
			if(e.target.parent.name == 'idNumber'){
				this.touchScreenKeyboard.animateShowMe(960, 840);
				TweenMax.to(wifiVC.keyboardBG, 0.5, {y:624.1});
				touchScreenKeyboard.mode = 'email';
				
			}
			if(e.target.parent.name == 'email'){
				this.touchScreenKeyboard.animateShowMe(960, 840);
				TweenMax.to(wifiVC.keyboardBG, 0.5, {y:624.1});
				touchScreenKeyboard.mode = 'email';
				
			}
			if(e.target.parent.name == 'country'){
				this.touchScreenKeyboard.animateShowMe(650, 840);
				TweenMax.to(wifiVC.keyboardBG, 0.5, {y:624.1});
				touchScreenKeyboard.mode = 'email';
				
			}
			if(e.target.name == '_submit'){
				this.touchScreenKeyboard.animateHideMe(1040,2000);
				
			}
			if(btnName == 'countryDropDown'){
				this.touchScreenKeyboard.animateHideMe(1040,2000);
			}


			if (btnName == 'all_btn'){
				
				vcManager.openViewController(directoryVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
				directoryVC.floorOptionCoverFlow.getCategoryButtonTapped(GaysornDataManager.ALL);
				mainnav.categorypanel.reArrangeShopByCat(GaysornDataManager.ALL);
			}
			else if (btnName == 'fashion_btn'){
				
				vcManager.openViewController(directoryVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
				directoryVC.floorOptionCoverFlow.getCategoryButtonTapped(GaysornDataManager.FASHION);
				mainnav.categorypanel.reArrangeShopByCat(GaysornDataManager.FASHION);
				
				
			}
			else if (btnName == 'jewelry_btn'){
				vcManager.openViewController(directoryVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
				directoryVC.floorOptionCoverFlow.getCategoryButtonTapped(GaysornDataManager.JEWELLERY);
				mainnav.categorypanel.reArrangeShopByCat(GaysornDataManager.JEWELLERY);
			}
			else if (btnName == 'liftstyle_btn'){
				vcManager.openViewController(directoryVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
				directoryVC.floorOptionCoverFlow.getCategoryButtonTapped(GaysornDataManager.LIFESTYLE);
				mainnav.categorypanel.reArrangeShopByCat(GaysornDataManager.LIFESTYLE);
			}
			else if (btnName == 'cuisine_btn'){
				vcManager.openViewController(directoryVC);
				this.touchScreenKeyboard.animateHideMe(1040,2000);
				directoryVC.floorOptionCoverFlow.getCategoryButtonTapped(GaysornDataManager.CUISINE);
				mainnav.categorypanel.reArrangeShopByCat(GaysornDataManager.CUISINE);
			}
			else if(btnName == 'hideBTN'){
				directoryVC.scaleFloorOptionCoverFlow('in');
			}
			
			

		}
		
		public function onChangeLanguage(e:LanguageEvent):void
		{
			trace('Lang', e.obj);
			langs = e.obj;
			directoryVC.onTranslate(e.obj);
			searchVC.onTranslate(e.obj);
			mainnav.onTranslate(e.obj);
			touristinfoVC.onTranslate(e.obj);
			ratchaprasongVC.onTranslate(e.obj);
			wifiVC.onTranslate(e.obj);
			
			directoryVC.floorOptionCoverFlow.shopBubble.onTranslate(e.obj);
			directoryVC.shopDetail.onTranslate(e.obj);
			mainnav.categorypanel.onTranslate(e.obj);
			
		}





	}

}