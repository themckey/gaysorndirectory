﻿package models
{
	import com.forviz.data.models.Model;

	public class FacilityModel extends Model
	{

		private var _id:String;

		private var _nameEn:String;
		private var _nameCn:String;
		private var _nameJp:String;

		private var _pathMedia:String;

		public function FacilityModel(obj:Object)
		{
			this.setData(obj);
		}
		
		public function setData(facilityXML:Object):void
		{
			_id = facilityXML.id.text();
			_nameEn = facilityXML.name_en.text();
			_nameCn = facilityXML.name_cn.text();
			_nameJp = facilityXML.name_jp.text() ;
			_pathMedia = facilityXML.path_media.text() ;
			
			
		}
		public function get id ():String
		{
			return _id;
		}
		public function get nameEn ():String
		{
			return _nameEn;
		}
		public function get nameCn ():String
		{
			return _nameCn;
		}
		public function get nameJp():String
		{
			return _nameJp;
		}
		public function get pathMedia ():String
		{
			return _pathMedia;
		}

	}

}