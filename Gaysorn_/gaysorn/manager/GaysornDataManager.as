﻿package manager {
	
	import flash.utils.Dictionary;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.StyleSheet;

	import com.pvm.manager.DataManager;
	import models.*;
	import flash.utils.Timer;
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	import flash.events.TimerEvent;
	import flash.display.Loader;
	import com.forviz.data.models.ShopModel;
	import manager.models.ShopModel;
	import com.forviz.data.DataManager;
	
	public class GaysornDataManager extends DataManager {


		protected static var		instance				: GaysornDataManager;
		protected static var		allowInstantiation		: Boolean			= false;
		
		
		private var _promotionXML:XML;
		private var _departmentXML:XML;
		private var _contentFolder:String = '';
		
		public var milestoneXML:XML;
		
		private var postsLoader:URLLoader = new URLLoader();
		public var postsDict:Dictionary = new Dictionary();
		public var postsArray:Array = new Array();
		
	
		
		private var dataInittedEvent:Event = new Event("dataInitted");
		private var postsInittedEvent:Event = new Event("postsInitted");
		private var cssLoadedEvent:Event = new Event("cssLoaded");
		
		
		private var cssLoader:URLLoader = new URLLoader();
		private var cssRequest:URLRequest = new URLRequest("style.css");
		public var styles:StyleSheet = new StyleSheet();
		
		
		
		
		//News
		
		public var datestatus:String = "";
		public var xmldates:XML = new XML();
		var mydateloader:URLLoader = new URLLoader();
		
		var dictLink:Dictionary = new Dictionary();
		var arrayCat:Array = new Array();
		
		
		
		
		public var shopXML:XML = new XML();
		public var shopXMLList:XMLList = new XMLList();
		public var shopXMLAlphabetList: XMLList = new XMLList();
		public var shopDict:Dictionary = new Dictionary();
		public var shopDictByInstanceName:Dictionary = new Dictionary();
		public var shopArr:Array = new Array();
		public var shopAlphabetArr:Array = new Array();
		
		
		public var facilityXML:XML = new XML();
		public var facilityXMLList:XMLList = new XMLList();
		public var facilityDict:Dictionary = new Dictionary()
		public var facilityArr:Array = new Array();
		
		
		
		public var configsXML:XML = new XML();
		public var configsXMLList:XMLList = new XMLList();
		
		
		private var _categoryDict:Dictionary = new Dictionary();
		private var _categories:Array = new Array();
		
		private var _tempArray:Array;
		
		public static const ALL		 	: int = 0;		
		public static const FASHION 	: int = 1;
		public static const JEWELLERY 	: int = 2;
		public static const LIFESTYLE	: int = 3;
		public static const CUISINE 	: int = 4;
		public static const OTHER 		: int = 5;
		
		public function GaysornDataManager() {
			
			
			if (!allowInstantiation )			
			{
				throw new ReferenceError ( "Error: Instantiation failed. Use EventManager.getInstance() instead of new." ) ;				
			}
			else
			{
				init ( ) ;
			}
			
		}
		
		private function init():void
		{
			
			_categoryDict["Apparel for Him"] = FASHION;
			_categoryDict["Apparel for Her"] = FASHION;
			_categoryDict["แฟชั่น"] = FASHION;
			_categoryDict["Shoes & Hand Bags"] = JEWELLERY;
			_categoryDict["Shoes & Handbags"] = JEWELLERY;
			_categoryDict["Jewellery & Accessories"] = JEWELLERY;
			_categoryDict["Beauty & Spa"] = LIFESTYLE;
			_categoryDict["Gadgets & Lifestyle"] = LIFESTYLE;
			_categoryDict["Home & Accessories"] = LIFESTYLE;
			_categoryDict["Food & Wine"] = CUISINE;
			_categoryDict["Other"] = OTHER;
		
		}
		
		private function loadRoomStatusXML():void
		{
		//	trace("datamanager.loadRoomStatusXML");
			roomStatusLdr.add(pathreload);
			
			
			roomStatusLdr.addEventListener(BulkProgressEvent.COMPLETE, _onLoadRoomStatusComplete);
			roomStatusLdr.addEventListener(BulkLoader.ERROR, _onLoadRoomStatusError);
			roomStatusLdr.start();
			
			reloadXMLTimer.addEventListener(TimerEvent.TIMER_COMPLETE, reloadRoomStatusXML);
			
		}
		
		private function reloadRoomStatusXML(evt:Event = null):void
		{
			
		}
		
		private function _onLoadRoomStatusError(evt:Event):void
		{
		//	trace("datamanager _onLoadRoomStatusError");
			
		}
		
		private function _onLoadRoomStatusComplete(evt:Event):void
		{
			//trace("datamanager _onLoadRoomStatusComplete");
			
			
			
			
			
		}
		public function processXML(evt:Event = null):void{
		//	trace("kiosksetxml >>>>>> ");
			xmldates = new XML(evt.target.data);
			
			datestatus = xmldates.roomStatus.dates.text();
			
			var newString:String = roomStatusXML.roomStatus.status.text();
			if (roomStatusString != newString) {
			//	trace("different XML");
				roomStatusString = roomStatusXML.roomStatus.status.text();
				dispatchEvent(new Event("loadstatuscomplete"));
			} else {
				//trace("same XML");
			}
		}
		
		public static function getInstance():GaysornDataManager
		{
			if ( instance == null )
			{
				allowInstantiation 		= true;
				instance 				= new GaysornDataManager ( ) ;			
				allowInstantiation 		= false;
			}
			return instance;


		}
		
		
		function cssLoadComplete(event:Event):void
		{
			styles.parseCSS(cssLoader.data);
			dispatchEvent(cssLoadedEvent);
			
		}
		
		override public function initData():void
		{
			configsXML = xmlDict["config"]["content"];
			
			shopXML = this.getXML("shop");
			shopDict = new Dictionary();
			shopArr = new Array();
			shopXMLList = xmlDict["shop"]["content"].shop;
			var shopxml:XML = XML(shopXML);
			shopXMLAlphabetList = xmlDict["shop"]["content"].shop;
			
			for (var i:int=0;i<shopxml.shop.length();i++) 
			{
				
				var shopmodel:ShopModel = new ShopModel(shopXMLList[i]);
				
				shopArr.push(shopmodel);
				shopAlphabetArr.push(shopXMLList[i].name_en);
				
				//trace(shopmodel.nameEn);
				shopmodel.catIDs = this.getCategoryIDs(shopmodel.catString);
				
				//trace(shopmodel.catIDs);
				var id:String = shopmodel.id;
				
				shopDict[id] = shopmodel;
				shopDictByInstanceName[shopmodel.unitKey] = shopmodel;

			}
			shopAlphabetArr.sort();
			
			facilityXML = this.getXML("facility");
			facilityDict = new Dictionary();
			facilityArr = new Array();
			facilityXMLList = xmlDict["facility"]["content"].facility;
			var facilityxml:XML = XML(facilityXML);
			
			for (var i:int=0;i<facilityXMLList.length();i++) 
			{
				
				var facilitymodel:FacilityModel = new FacilityModel(facilityXMLList[i]);
				
				facilityArr.push(facilitymodel);
				
				var id:String = facilitymodel.id;
				facilityDict[id] = facilitymodel;
		
			}
			
			
			
			
			
			
			
			dispatchEvent(dataInittedEvent);
			
			
			
			//postsLoader.load(new URLRequest("http://lagunaproperty.com/feed"));
			//postsLoader.addEventListener(Event.COMPLETE, loadPostsComplete);
			
		}
		
		
		
		public function getCategoryIDs(_str:String):Array
		{
			
			if (_str == "-" || _str == "") return null;
			
			var _response:Array;
			
			_str = _str.replace('&amp;', '&');
			
			_tempArray = _str.split(";");
			//trace("tempArray len ", _tempArray.length);
			for (var i:int = 0;i < _tempArray.length; i++) {
				
				var catID:int = _getCategoryIDFromName(_tempArray[i]);
				if (_response == null) _response = new Array();	
				_response.push(catID);

				
			}
			
			return _response;
		}
		
		private function _getCategoryIDFromName(str:String):int
		{
			str = trimWhitespace(str);
			
			if (_categoryDict[str] == null) {
				//Doesn't have yet, generate one.
				return 0;
			} else {
				//Already has, return it.
				return _categoryDict[str];
			}
			
		}
		
		function trimWhitespace($string:String):String {
			if ($string == null) {
				return "";
			}
			return $string.replace(/^\s+|\s+$/g, "");
		}
		
		
		private function loadPostsComplete(evt:Event):void
		{
			var postsXML:XML = new XML(postsLoader.data);
			newsxml = new XML(postsLoader.data);
			newsxmllist = newsxml.channel.item;

			
			for (var i:int = 0; i < newsxmllist.length(); i++){
				var categorylenght:Number = newsxmllist[i].category.length();
				for (var j:int = 0; j < categorylenght; j++){
					if (newsxmllist[i].category[j] == "News" || newsxmllist[i].category[j] == "Event" || newsxmllist[i].category[j] == "Uncategorized" || newsxmllist[i].category[j] == "Exhibition"  || newsxmllist[i].category[j] == "Launch"){
						
					}
				}
			}
			
			
			
			dispatchEvent(postsInittedEvent);
		}
		
		public function get contentXML():XML
		{
			return this._contentXML;
		}
		
		public function getTypeKey(roomNo:String):String
		{
			return roomNo.substr(0, roomNo.indexOf("-") - 1).toUpperCase();
		}
		
		
	
	}
	
}
