﻿package coverflow
{

	import flash.display.*;
	import flash.geom.*;
	import flash.events.*;
	import flash.net.*;
	import flash.text.*;
	import flash.utils.*;
	import com.jmx2.delayedFunctionCall;


	public class CoverFlow extends Sprite
	{

		private var _coversContainer:Sprite;
		private var _width:Number;
		private var _height:Number;
		private var _backgroundColor:uint = 0;
		private var _background:Shape;
		private var _covers:Vector.<Cover > ;
		private var _urlLoader:URLLoader;
		private var _xml:XML;
		private var _loadCounter:uint;
		private var _bytesPerImage:int;
		private var _bytesTotal:int;
		private var _selectedIndex:uint;
		private var _centerMargin:Number;
		private var _horizontalSpacing:Number;
		private var _backRowDepth:Number;
		private var _backRowAngle:Number;
		private var _verticalOffset:Number;
		private var _captionField:TextField;
		private var _captionFormat:TextFormat;

		private var _tweenDuration:int = 1200;
		private var _startTime:int;
		private var _elapsed:int;
		private var _coversLength:uint;
		private var _iteration:uint;
		private var _iterationCover:Cover;
		private var _sortedCovers:Vector.<Cover > ;
		private var _unitsFromCenter:uint;
		private var _distanceA:Number;
		private var _distanceB:Number;


		private static const DEFAULT_CENTER_MARGIN:Number = 60;
		private static const DEFAULT_HORIZONTAL_SPACING:Number = 30;
		private static const DEFAULT_BACK_ROW_DEPTH:Number = 150;
		private static const DEFAULT_BACK_ROW_ANGLE:Number = 45;
		private static const DEFAULT_VERTICAL_OFFSET:Number = 0;

		var _indesdes:Object;
		var tm:Timer = new Timer(1000,1);
		public function CoverFlow(w:Number, h:Number)
		{
			trace ('create coverflow');
			_width = w;
			_height = h;
			_covers = new Vector.<Cover>();

			_centerMargin = DEFAULT_CENTER_MARGIN;
			_horizontalSpacing = DEFAULT_HORIZONTAL_SPACING;
			_backRowDepth = DEFAULT_BACK_ROW_DEPTH;
			_backRowAngle = DEFAULT_BACK_ROW_ANGLE;
			_verticalOffset = DEFAULT_VERTICAL_OFFSET;

			_coversContainer = new Sprite();
			addChild(_coversContainer);

			_background = new Shape();
			addChildAt(_background, 0);
			drawBackground();

			scrollRect = new Rectangle(0,0,_width,_height);
			this.transform.perspectiveProjection = new PerspectiveProjection();
			this.transform.perspectiveProjection.projectionCenter = new Point(_width/2, _height-190);

			_urlLoader = new URLLoader();
			_urlLoader.addEventListener(Event.COMPLETE, onXMLLoad);
			_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onXMLLoadError);

			this.addEventListener(Event.RENDER, onRender);

			_captionField = new TextField  ;
			addChild(_captionField);
			_captionField.width = 1500;
			_captionField.height = 50;
			_captionField.x = (_width - _captionField.width) / 2;
			_captionField.y = _height - 50;
			_captionField.multiline = true;
			_captionField.wordWrap = true;
			var fontName:String;
			var fonts:Array = Font.enumerateFonts(true);
			for each (var font:Font in fonts)
			{
				if (font.fontName.search(/lucida.*grande/i) > -1)
				{
					fontName = font.fontName;
					break;
				}
			}
			if (! fontName)
			{
				fontName = "DB FongNam X";
			}
			_captionFormat = new TextFormat(fontName,50,0xFFFFFF,true);
			_captionFormat.align = TextFormatAlign.CENTER;
			_captionField.defaultTextFormat = _captionFormat;
		}

		override public function set width(num:Number):void
		{
			_width = num;
			_background.width = _width;
			scrollRect = new Rectangle(0,0,_width,_height);
			this.transform.perspectiveProjection.projectionCenter = new Point(_width/2, _height-190);
			if (stage)
			{
				stage.invalidate();
			}
		}
		override public function get width():Number
		{
			return _width;
		}
		override public function set height(num:Number):void
		{
			_height = num;
			_background.height = _height;
			scrollRect = new Rectangle(0,0,_width,_height);
			this.transform.perspectiveProjection.projectionCenter = new Point(_width/2, _height-190);
			if (stage)
			{
				stage.invalidate();
			}
		}
		override public function get height():Number
		{
			return _height;
		}

		public function set backgroundColor(val:uint):void
		{
			_backgroundColor = val;
			drawBackground();
			for each (var cover:Cover in _covers)
			{
				cover.backgroundColor = val;
			}
		}
		public function get backgroundColor():uint
		{
			return _backgroundColor;
		}

		public function set centerMargin(num:Number):void
		{
			if (isNaN(num))
			{
				num = DEFAULT_CENTER_MARGIN;
			}
			_centerMargin = Math.max(0,num);
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get centerMargin():Number
		{
			return _centerMargin;
		}

		public function set horizontalSpacing(num:Number):void
		{
			if (isNaN(num))
			{
				num = DEFAULT_HORIZONTAL_SPACING;
			}
			_horizontalSpacing = Math.max(0,num);
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get horizontalSpacing():Number
		{
			return _horizontalSpacing;
		}

		public function set backRowDepth(num:Number):void
		{
			if (isNaN(num))
			{
				num = DEFAULT_BACK_ROW_DEPTH;
			}
			_backRowDepth = Math.max(0,num);
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get backRowDepth():Number
		{
			return _backRowDepth;
		}

		public function set backRowAngle(num:Number):void
		{
			if (isNaN(num))
			{
				num = DEFAULT_BACK_ROW_ANGLE;
			}
			_backRowAngle = Math.min(90,Math.abs(num));
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get backRowAngle():Number
		{
			return _backRowAngle;
		}

		public function set verticalOffset(num:Number):void
		{
			if (isNaN(num))
			{
				num = DEFAULT_VERTICAL_OFFSET;
			}
			_verticalOffset = num;
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get verticalOffset():Number
		{
			return _verticalOffset;
		}

		public function set fontName(name:String):void
		{
			_captionFormat.font = name;
			_captionField.defaultTextFormat = _captionFormat;
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get fontName():String
		{
			return _captionFormat.font;
		}

		public function set fontSize(size:Number):void
		{
			_captionFormat.size = size;
			_captionField.defaultTextFormat = _captionFormat;
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get fontSize():Number
		{
			return _captionFormat.size as Number;
		}

		public function set fontColor(color:uint):void
		{
			_captionFormat.color = color;
			_captionField.defaultTextFormat = _captionFormat;
			if (stage)
			{
				stage.invalidate();
			}
		}
		public function get fontColor():uint
		{
			return _captionFormat.color as uint;
		}

		public function get captionField():TextField
		{
			return _captionField;
		}

		public function set selectedIndex(index:uint):void
		{
			if (_covers.length == 0)
			{
				_selectedIndex = index;
				return;
			}
			_selectedIndex = Math.max(0,Math.min(index,_covers.length - 1));

			_startTime = getTimer();
			determineLayout(_selectedIndex);
			removeEventListener(Event.ENTER_FRAME, animate);
			addEventListener(Event.ENTER_FRAME, animate);
		}
		public function get selectedIndex():uint
		{
			return _selectedIndex;
		}


		private function drawBackground():void
		{
			_background.graphics.clear();
			_background.graphics.beginFill(_backgroundColor, 0.2);
			_background.graphics.drawRect(0, 0, _width, _height);
		}

		private function layout():void
		{
			var len:uint = _covers.length;
			var cover:Cover;
			var distanceFromCenter:uint;
			for (var i:uint = 0; i < len; i++)
			{
				cover = _covers[i];
				if (i == _selectedIndex)
				{
					cover.rotationY = 0;
					cover.x = _background.width / 2;
					cover.z = 0;
					_coversContainer.setChildIndex(cover, _coversContainer.numChildren-1);
					_captionField.text = cover.caption;
				}
				else if (i < _selectedIndex)
				{
					distanceFromCenter = _selectedIndex - i;
					cover.rotationY =  -  _backRowAngle;
					cover.x = ((_background.width / 2) - _centerMargin) - (distanceFromCenter * _horizontalSpacing);
					cover.z = _backRowDepth;
					_coversContainer.setChildIndex(cover, _coversContainer.numChildren - (distanceFromCenter + 1));
					cover.dropOff = distanceFromCenter / 10;
				}
				else if (i > _selectedIndex)
				{
					distanceFromCenter = i - _selectedIndex;
					cover.rotationY = _backRowAngle;
					cover.x = ((_background.width / 2) + _centerMargin) + (distanceFromCenter * _horizontalSpacing);
					cover.z = _backRowDepth;
					_coversContainer.setChildIndex(cover, _coversContainer.numChildren - (distanceFromCenter + 1));
					cover.dropOff = distanceFromCenter / 10;
				}
				cover.y = _background.height - 585 + _verticalOffset;
			}
			_captionField.x = (_background.width - _captionField.width) / 2;
			_captionField.y = _background.height - 80 + _verticalOffset;
		}

		private function determineLayout(destinationIndex:uint):void
		{
			_coversLength = _covers.length;
			for (var i:uint = 0; i < _coversLength; i++)
			{
				_iterationCover = _covers[i];
				if (i == destinationIndex)
				{
					_iterationCover.endRotationY = 0;
					_iterationCover.endX = _background.width / 2;
					_iterationCover.endZ = 0;
					_captionField.text = _iterationCover.caption;
				}
				else if (i < destinationIndex)
				{
					_unitsFromCenter = destinationIndex - i;
					_iterationCover.endRotationY =  -  _backRowAngle;
					_iterationCover.endX = ((_background.width / 2) - _centerMargin) - (_unitsFromCenter * _horizontalSpacing);
					_iterationCover.endZ = _backRowDepth;
				}
				else if (i > destinationIndex)
				{
					_unitsFromCenter = i - destinationIndex;
					_iterationCover.endRotationY = _backRowAngle;
					_iterationCover.endX = ((_background.width / 2) + _centerMargin) + (_unitsFromCenter * _horizontalSpacing);
					_iterationCover.endZ = _backRowDepth;
				}
			}
		}

		private function onXMLLoad(e:Event):void
		{
			_xml = new XML(_urlLoader.data);
			

			var imageList:XMLList = _xml.floorplan_path;
			var iLen:uint = imageList.length();
			var imageNode:XML;
			var cover:Cover;
			for (var i:uint = 0; i < iLen; i++)
			{
				imageNode = imageList[i];
				var title:String = imageNode. @ title;
				cover = new Cover(title,imageNode,_backgroundColor);
				cover.addEventListener(MouseEvent.CLICK, onCoverClick);
				cover.addEventListener(MouseEvent.MOUSE_DOWN, onCoverDown);
				//cover.addEventListener(MouseEvent.MOUSE_MOVE, onCoverMove);
				_coversContainer.addChild(cover);
				_covers.push(cover);
			}
			layout();
			_loadCounter = 0;
			loadNextCover();
		}

		private function onXMLLoadError(e:IOErrorEvent):void
		{
			trace("There was an error loading the XML document: " + e.text);
		}

		public function load(url:String):void
		{
			clearContents();
			_urlLoader.load(new URLRequest(url));
			
		}

		private function clearContents():void
		{
			for each (var cover:Cover in _covers)
			{
				cover.removeEventListener(Event.COMPLETE, onCoverLoad);
				cover.removeEventListener(ProgressEvent.PROGRESS, onCoverProgress);
				cover.addEventListener(MouseEvent.CLICK, onCoverClick);
				_coversContainer.removeChild(cover);
			}
			_covers = new Vector.<Cover>();
			this.removeEventListener(Event.ENTER_FRAME, animate);
		}

		private function loadNextCover():void
		{
			if(_loadCounter != 0)
			{
				var cover:Cover = _covers[_loadCounter];
				var src:String = _xml.image[_loadCounter]. @ src;
				cover.load(src);
				cover.addEventListener(Event.COMPLETE, onCoverLoad);
				cover.addEventListener(ProgressEvent.PROGRESS, onCoverProgress);
			}
			
		}

		private function onCoverLoad(e:Event):void
		{
			e.target.removeEventListener(Event.COMPLETE, onCoverLoad);
			_loadCounter++;
			if (_loadCounter < _covers.length)
			{
				loadNextCover();
			}
			else
			{
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}

		private function onCoverProgress(e:ProgressEvent):void
		{
			if (_bytesPerImage == 0)
			{
				_bytesPerImage = e.bytesTotal;
				_bytesTotal = _bytesPerImage * _covers.length;
			}
			var adjustedBytesLoaded:uint = e.bytesLoaded * (_bytesPerImage / e.bytesTotal);
			var cumulativeBytesLoaded:uint = (_loadCounter * _bytesPerImage) + adjustedBytesLoaded;
			dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, cumulativeBytesLoaded, _bytesTotal));
		}

		private function onRender(e:Event):void
		{
			layout();
			_captionField.setTextFormat(_captionFormat);
		}

		private function onCoverClick(e:MouseEvent):void
		{
			//this.selectedIndex = _covers.indexOf(e.currentTarget);


		}
		private function onCoverDown(e:MouseEvent):void
		{
			trace('onCoverDown', e.target.x);
			_indesdes = e.currentTarget;
			onCoverUpXX();
		}
		private function onCoverUpXX():void
		{
			new delayedFunctionCall(callIndex, 200);
			
			

		}
		
		private function callIndex():void
		{
			this.selectedIndex = _covers.indexOf(_indesdes);
		}

		private function onCoverDoubleClick(e:MouseEvent):void
		{
			dispatchEvent(new MouseEvent(MouseEvent.DOUBLE_CLICK));
		}

		private function animate(e:Event):void
		{
			_elapsed = getTimer() - _startTime;
			if (_elapsed > _tweenDuration)
			{
				removeEventListener(Event.ENTER_FRAME, animate);
				return;
			}

			for (_iteration = 0; _iteration < _coversLength; _iteration++)
			{
				_iterationCover = _covers[_iteration];
				_iterationCover.updateTween(_elapsed, _tweenDuration);
			}

			_sortedCovers = _covers.concat();
			_sortedCovers = _sortedCovers.sort(depthSort);
			for (_iteration = 0; _iteration < _coversLength; _iteration++)
			{
				_iterationCover = _sortedCovers[_iteration];
				_coversContainer.setChildIndex(_iterationCover, _coversContainer.numChildren-(_iteration+1));
			}

		}

		private function depthSort(a:Cover, b:Cover):Number
		{
			var _distanceA:Number = Math.abs(a.x - _background.width / 2);
			var _distanceB:Number = Math.abs(b.x - _background.width / 2);
			if (_distanceA < _distanceB)
			{
				return -1;
			}
			if (_distanceA > _distanceB)
			{
				return 1;
			}

			return 0;
		}

	}

}