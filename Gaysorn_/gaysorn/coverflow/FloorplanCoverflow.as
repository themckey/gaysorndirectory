﻿package coverflow
{
	import items.floorplan.FloorplanMC;
	import flash.display.Sprite;
	import flash.events.Event;

	public class FloorplanCoverflow extends Sprite
	{
		var centerX:Number = 960;
		var centerY:Number = 300;
		var radius:Number = 150;
		var angleStep:Number = .01;
		
		var twoPI:Number = 2 * Math.PI;
		var currentAngle:Number = 0;
		
		var floorplanMC:FloorplanMC;
		var floorplanArr:Array = new Array();
		
		public function FloorplanCoverflow()
		{
			
			 if( !this.stage ) 
				 this.addEventListener( Event.ADDED_TO_STAGE, init ); 
			 else 
				 init(); 
			
		}
		private function init(e:Event = null):void 
		{ 
		   this.removeEventListener( Event.ADDED_TO_STAGE, init ); 
		   stage.addEventListener(Event.ENTER_FRAME, advanceCircle);
		   
		   for(var i:int = 0; i < 5; i++)
		   {
				floorplanMC = new FloorplanMC();
				floorplanMC.x = 1480 * i;
				addChild(floorplanMC);
				floorplanArr.push(floorplanMC);
		   }
		 } 
		function advanceCircle(e:Event):void
		{
			currentAngle -= angleStep;
			var angleDifference:Number = Math.PI * (360 / 5) / 180;
			
			trace (currentAngle);
			for(var n:int = 0; n < floorplanArr.length; n++)
			{
				var startingAngle:Number = angleDifference * n;
				
				floorplanArr[n].x = centerX - Math.cos(startingAngle * currentAngle) * radius;
				floorplanArr[n].y = centerY - Math.sin(startingAngle * currentAngle) * radius;
				
				//floorplanArr[n].alpha = ( 1 * (1 + Math.sin(Math.PI + currentAngle)) ) - 0.5;
				
				if (currentAngle < -1) {
					currentAngle = 0;
					
				}
			}
			
		}
		
		
		
	}

}