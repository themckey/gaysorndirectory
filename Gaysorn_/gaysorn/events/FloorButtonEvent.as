﻿package events {
	import flash.events.Event;
	
	public class FloorButtonEvent extends Event{

		public static const		FLOORBUTTONCHOSE		:String		=	"floorchose";
		
		
		public var obj:Object;
		public var strname:String;
		
		public function FloorButtonEvent(type:String, strname = "", obj:Object = null) {
			// constructor code
			
			this.obj = obj;
			this.strname = strname;
			super( type );
		}
		
		public override function clone():Event 
		{
			var newEvt : FloorButtonEvent = new FloorButtonEvent ( type, strname, obj);
			newEvt.obj = obj;
			newEvt.strname = strname;
			return newEvt;
		}

	}
	
}
