﻿package events {
	import flash.events.Event;
	
	public class FacilityEvent extends Event{

		public static const		CHOOSE_FACILITY		:String		=	"chosefacility";
		
		
		public var obj:Object;
		public var strname:String;
		
		public function FacilityEvent(type:String, strname = "", obj:Object = null) {
			// constructor code
			
			this.obj = obj;
			this.strname = strname;
			super( type );
		}
		
		public override function clone():Event 
		{
			var newEvt : FacilityEvent = new FacilityEvent ( type, strname, obj);
			newEvt.obj = obj;
			newEvt.strname = strname;
			return newEvt;
		}

	}
	
}
