﻿package events {
	import flash.events.Event;
	
	public class ShopEvent extends Event{

		public static const		SHOPCHOOSE		:String		=	"shopchoose";
		
		
		public var obj:Object;
		public var strname:String;
		
		public function ShopEvent(type:String, strname = "", obj:Object = null) {
			// constructor code
			
			this.obj = obj;
			this.strname = strname;
			super( type );
		}
		
		public override function clone():Event 
		{
			var newEvt : ShopEvent = new ShopEvent ( type, strname, obj);
			newEvt.obj = obj;
			newEvt.strname = strname;
			return newEvt;
		}

	}
	
}
