﻿package events {
	import flash.events.Event;
	
	public class LanguageEvent extends Event{

		public static const		LANGUAGECHANGED		:String		=	"languagechanged";
		
		
		public var obj:Object;
		public var strname:String;
		
		public function LanguageEvent(type:String, obj = null, strname = "") {
			// constructor code
			
			this.obj = obj;
			this.strname = strname;
			super( type );
		}
		
		public override function clone():Event 
		{
			var newEvt : LanguageEvent = new LanguageEvent ( type, obj, strname);
			newEvt.obj = obj;
			newEvt.strname = strname;
			return newEvt;
		}

	}
	
}
