﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UIImageView;
	import com.greensock.TweenMax;
	import com.forviz.ui.UICarousel;
	
	
	public class ShopBubble extends MovieClip {
		var uiImage:UIImageView;
		var picArr:Array = new Array();
		var str:String;
		var fadeImgArr:Array = new Array();
		
		var uiCarousel:UICarousel;
		
		public function ShopBubble() {
			// constructor code
			
			uiCarousel = new UICarousel(new Rectangle(-148.50,17,300,150));
			uiCarousel.animation = "fade";
			_pane.addChild(uiCarousel);
			
		}
		
		public function set model(theModel:ShopModel):void
		{
			
			_pane._names.text = theModel.nameEn;
			_pane._des.text = theModel.desEn;
			picArr = theModel.pathMediaArr;
			_loadImage();
			//uiImage.loadImage(theModel.mediaPath);
			
		}
		function _loadImage() : void {
			
			uiCarousel.unload();
 
	   		for(var i:int = 0; i < picArr.length; i++)
			{
				/*
				uiImage = new UIImageView(new Rectangle(-148.50,17,300,150));
				_pane.addChild(uiImage)
				uiImage.loadImage(picArr[i].toString());
				fadeImgArr.push(uiImage);
				*/
				
				uiCarousel.addPage(picArr[i].toString());
			}
			
			
			
			
		}
		
		public function show():void
		{
			this.visible = true;
			uiCarousel.auto = 4;
			uiCarousel.playSlide();
		}
		
		public function hide():void
		{
			this.visible = false;
			uiCarousel.stopSlide();
		}
		
		
		
		
	}
	
}
