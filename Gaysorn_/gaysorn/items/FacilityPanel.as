﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIControlEvent;
	import events.FacilityEvent;
	
	public class FacilityPanel extends MovieClip {
		
		public var facilityItem:FacilityBTN;
		var tapFacilityEvent:FacilityEvent = new FacilityEvent('chosefacility');
		var btnArr:Array = new Array();
		
		public function FacilityPanel() {
			// constructor code
			
			for(var i:int = 0; i < this.numChildren; i++)
			{
				if(this.getChildAt(i) is MovieClip)
				{
					facilityItem = new FacilityBTN();
					facilityItem = this.getChildAt(i);
					btnArr.push(facilityItem);
					
				}
				
			}
			for(var n:int = 0; n < btnArr.length; n++)
			{
				(btnArr[n] as FacilityBTN).addEventListener('tap', onTap);
			}
			
		}
		
		public function onTap(e:UIControlEvent):void
		{
			tapFacilityEvent.obj = e.from;
			dispatchEvent(tapFacilityEvent);
		}
	}
	
}
