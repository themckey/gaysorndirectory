﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	
	public class SkipButton extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		public function SkipButton() {
			// constructor code
			super(theFrame);
			
		}
	}
	
}
