﻿package items {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	
	
	public class PrevBTN extends UIButton {
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		
		public function PrevBTN() {
			// constructor code
			
			super(theFrame);
		}
	}
	
}
