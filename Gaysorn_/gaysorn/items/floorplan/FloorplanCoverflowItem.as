﻿package items.floorplan {
	
	import flash.display.MovieClip;
	
	public class FloorplanCoverflowItem extends MovieClip {

		public var currentAngle:Number;
		public var startingAngle:Number;
		
		public var targetX:Number;
		public var targetY:Number;
		public var targetScaleX:Number;
		public var targetScaleY:Number;
		
		private var _xpos3D:Number;
		private var _ypos3D:Number;
		private var _zpos3D:Number;
		
		public function FloorplanCoverflowItem() {
			// constructor code
		}
		
		public function set xpos3D(value:Number):void
		{
			this._xpos3D = value;
		}

		public function set ypos3D(value:Number):void
		{
			this._ypos3D = value;
		}

		public function set zpos3D(value:Number):void
		{
			this._zpos3D = value;
		}
		
		public function get xpos3D():Number
		{
			return this._xpos3D;
		}

		public function get ypos3D():Number
		{
			return this._ypos3D;
		}
		
		public function get zpos3D():Number
		{
			return this._zpos3D;
		}


		

	}
	
}
