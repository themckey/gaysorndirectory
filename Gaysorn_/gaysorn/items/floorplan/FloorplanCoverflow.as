﻿package items.floorplan
{
	import flash.display.MovieClip;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.net.URLLoader;

	import flash.events.MouseEvent;
	import flash.display.Loader;
	import flash.filters.DropShadowFilter;
	import flash.filters.*;
	import flash.display.Sprite;
	import itemMC.Floormcitem;
	import itemMC.Youareheremc;
	import flash.utils.Dictionary;
	import flash.events.TouchEvent;
	import flash.display3D.IndexBuffer3D;

	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	import coverflow.FloorplanCoverflow;
	import manager.GaysornDataManager;
	import flash.events.MouseEvent;

	import manager.GaysornDataManager;
	import models.ShopModel;
	import items.ShopBubble;
	import items.YouAreHereMc;
	import item.LliftAct;
	import items.ToiletAct;
	import items.EscalatorAct;
	import items.FoodAct;



	public class FloorplanCoverflow extends MovieClip
	{

		var IMAGE_WIDTH:uint = 2000;
		var IMAGE_HEIGHT:uint = 800;

		var imgurl:URLRequest = new URLRequest();
		var loadedimgs:uint = 0;
		var countload:int = 0;
		var images_num = 0;
		var imageHolders:Array = new Array();

		//Set the focal length
		var focalLength:Number = 4000;//500

		//Set the vanishing point

		var vanishingPointX:Number = 960;
		var vanishingPointY:Number = 1080 / 2.2;

		//The 3D floor for the images
		var floor:Number = 40;//40


		var angleSpeed:Number = 0;


		var radius:Number = 100;// related with scale
		var radiusX:Number = 400;// 120 pin add more

		var xmlLoader:URLLoader = new URLLoader();
		var xmlData:XML = new XML();

		var xmlLoaderShop:URLLoader = new URLLoader();
		var xmlDataShop:XML = new XML();

		var hitSp:Sprite = new Sprite();
		var nowi:int = 0;
		var hitMC:MovieClip = new MovieClip();
		var nowcurrentAngle:int = 0;
		var nowangle0:int = 0;
		var nowangle1:int = 0;
		var nowangle2:int = 0;
		var nowangle3:int = 0;
		var nowangle4:int = 0;

		public var nowitem:int = 0;
		public var beforemoveitem:int = 0;

		var oriangle:int = 0;
		var foundright:Boolean = false;

		var dictShopwithflag:Dictionary = new Dictionary();

		var facilityDict:Dictionary = new Dictionary();
		var facilityArray:Array = new Array();

		var touchDownX:Number;

		public var _currentFloorIndex:int;
		var _currentAngle:Number = 0;
		var _startingCurrentAngle:Number;

		var configXML:XML = new XML();
		var youarehereFloor:int;

		var youarehereMC:YouAreHereMc = new YouAreHereMc();
		var youarehereMCX:Number;
		var youarehereMCY:Number;


		var shopMcArr:Array = new Array();
		var shopMC:MovieClip;
		var shopModelArray:Array = new Array();


		var shopBubble:ShopBubble = new ShopBubble();
		public var sendUnit:String;

		
		
		var shopList:XMLList = new XMLList();
		
		var _shopDictByInstancename:Dictionary;


		public function FloorplanCoverflow()
		{
			// constructor code


			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);


			xmlLoader.load(new URLRequest("content/floorplan.xml"));
			xmlLoader.addEventListener(Event.COMPLETE, LoadXML);

			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);

		}
		private function onInitData(e:Event = null):void
		{
			
			youarehereNode = GaysornDataManager.getInstance().configsXML.start_node;
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor;
			youarehereMCX = GaysornDataManager.getInstance().configsXML.start_nodeX;
			youarehereMCY = GaysornDataManager.getInstance().configsXML.start_nodeY;
			
			_shopDictByInstancename = GaysornDataManager.getInstance().shopDictByInstanceName;
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			callDefaultFloorplan();

		}
		private function callDefaultFloorplan():void
		{
			for (var i:int = 0; i < imageHolders.length; i++)
			{
				if (youarehereFloor == 2)
				{
					youarehereMC.x = youarehereMCX;
					youarehereMC.y = youarehereMCY;
					youarehereMC.scaleX = youarehereMC.scaleY = 0.8;
					(imageHolders[1] as MovieClip).addChild(youarehereMC);
					youarehereMC.cacheAsBitmap = true;
				}
				if (youarehereFloor == 4)
				{

					youarehereMC.x = youarehereMCX;
					youarehereMC.y = youarehereMCY;
					youarehereMC.scaleX = youarehereMC.scaleY = 0.8;
					(imageHolders[3] as MovieClip).addChild(youarehereMC);
					youarehereMC.cacheAsBitmap = true;
				}
			}

		}

		private function onMouseDownHandler(evt:MouseEvent):void
		{
			touchDownX = evt.stageX;
			_onTouchDownHandler(evt);
		}



		private function _onTouchDownHandler(evt:Event = null):void
		{
			this.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			//this.hideAllShop();
			_startingCurrentAngle = this._currentAngle;
			updateTarget(evt);

		}

		private function onMouseMoveHandler(evt:MouseEvent):void
		{
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);

			updateTarget(evt);
		}
		private function onMouseUpHandler(evt:MouseEvent):void
		{
			if ((evt.stageX - touchDownX) < -15)
			{
				goNext();
			}
			else if ((evt.stageX - touchDownX) > 15)
			{
				goPrev();
			}

			this.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			this.removeEventListener(MouseEvent.MIDDLE_MOUSE_UP, onMouseUpHandler);
		}
		private function updateTarget(evt:Event):void
		{
			diff = evt.stageX - touchDownX;

			var _angle:Number  = _startingCurrentAngle + Math.atan(diff / radius);

			rotateCarouselToAngle(_angle);

		}


		public function goNext():void
		{
			goToFloor(_currentFloorIndex + 1);
			hideAllShop();
		}

		public function goPrev():void
		{
			goToFloor(_currentFloorIndex - 1);
			hideAllShop();
		}

		public function goToFloor(targetFloorIndex:int):void
		{

			if ( targetFloorIndex < 0 )
			{
				targetFloorIndex = 4;
			}
			if ( targetFloorIndex > 4 )
			{
				targetFloorIndex = 0;
			}


			var _targetAngle:Number  = (Math.PI/180 + 20) + (-targetFloorIndex+2) * (Math.PI/180) * (360 / 5);

			rotateCarouselToAngle(_targetAngle, true);
			this._currentFloorIndex = targetFloorIndex;

			dispatchEvent(new Event('setFirstFloorDefault'));

		}

		public function LoadXML(e:Event):void
		{
			xmlData = new XML(e.target.data);
			Parseimage(xmlData);
		}
		function Parseimage(imageinput:XML):void
		{
			var imageurl:XMLList = imageinput.floorplan_path;

			images_num = imageurl.length();
			for (var i:int = 0; i < images_num; i++)
			{
				var urlElement:XML = imageurl[i];


				var imageHolder:MovieClip = new MovieClip();
				var imageinside:FloorplanMC = new FloorplanMC();
				var imageLoader = new Loader();
				imageinside.addChild(imageLoader);
				imageinside.mouseChildren = true;
				imageLoader.x =  -  (IMAGE_WIDTH / 2);
				imageLoader.y =  -  (IMAGE_HEIGHT / 2);
				imageHolders.push(imageHolder);
				imageLoader.name = i.toString();
				imageLoader.load(new URLRequest(imageurl[i]));
				imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
				imageinside.name = i.toString();
				imageHolder.name = i.toString();
				imageHolder.addChild(imageinside);

			}
		}

		private function imageLoaded(e:Event):void
		{

			loadedimgs++;
			var pathMap:MovieClip = (e.target.content as MovieClip).getChildByName('pathMap');
			pathMap.visible = false;

			var nodeMap:MovieClip = (e.target.content as MovieClip).getChildByName('nodeMap');
			nodeMap.visible = false;
			
			
			var logoMC:MovieClip = (e.target.content as MovieClip).getChildByName('logo');
			if(logoMC)
			{
				logoMC.mouseEnabled = false;
				logoMC.mouseChildren = false;
			}


			var shops_mc:MovieClip = (e.target.content as MovieClip).getChildByName('shops_mc');

			if (shops_mc)
			{
				for (var i:int = 0; i<shops_mc.numChildren; i++)
				{
					if (shops_mc.getChildAt(i) is MovieClip)
					{
						shopMC = new MovieClip();
						shopMC = shops_mc.getChildAt(i);
						shopMcArr.push(shopMC);
						shopMC.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownShop);
						//shopMC.addEventListener(MouseEvent.MOUSE_UP, onMouseUPShop);
						
						
						
						
						if (_shopDictByInstancename[shopMC.name]) {
							//trace("Add Link from Model " + _shopDictByInstancename[shopMC.name].nameEn + " to " + shopMC.name);
							(_shopDictByInstancename[shopMC.name] as ShopModel).shopMC = shopMC;
						}

					}
				}
			}

			for (var n:int = 0; n<shopMcArr.length; n++)
			{
				(shopMcArr[n] as MovieClip).alpha = 0;
			}

			
			var facility:MovieClip = (e.target.content as MovieClip).getChildByName('facility');
			var fireExit:MovieClip;
			var fireArr:Array = new Array();
			
			if (facility)
			{
				for (var i:int = 0; i<facility.numChildren; i++)
				{
					if (facility.getChildAt(i) is MovieClip)
					{
						
						fireExit = new MovieClip();
						fireExit = facility.getChildAt(i);
						fireArr.push(fireExit);
						facilityArr.push(fireExit);
					}
				}
				
			}
			for (var n:int = 0; n<fireArr.length; n++)
			{
				if((fireArr[n] as MovieClip).name == '_4') (fireArr[n] as MovieClip).alpha = 0;
			}
			
			countload +=  1;

			if (loadedimgs == images_num)
			{
				initializeCarousel();
			}
			
			addChild(shopBubble);


		}
		
		var facilityArr:Array = new  Array();
		var liftact:LliftAct;
		var toiletAct:ToiletAct;
		var escaAct:EscalatorAct;
		var foodact:FoodAct;
		var actArr:Array = new Array();
		
		public function showFacility(str:String):void
		{
			
			for (var n:int = 0; n<facilityArr.length; n++)
			{
				if(str == '_2')
				{
					liftact = new LliftAct();
					facilityArr[n].addChild(liftact);
					liftact.alpha = 0;
					actArr.push(liftact);
					if((facilityArr[n] as MovieClip).name == '_2') TweenMax.to(liftact,0.6, {alpha:1, onComplete:bringFacilityNormalState});
				}
				if(str == '_1')
				{
					toiletAct = new ToiletAct();
					facilityArr[n].addChild(toiletAct);
					toiletAct.alpha = 0;
					actArr.push(toiletAct);
					if((facilityArr[n] as MovieClip).name == '_1') TweenMax.to(toiletAct,0.6, {alpha:1, onComplete:bringFacilityNormalState});
				}
				if(str == '_3')
				{
					escaAct = new EscalatorAct();
					facilityArr[n].addChild(escaAct);
					escaAct.alpha = 0;
					actArr.push(escaAct);
					if((facilityArr[n] as MovieClip).name == '_3') TweenMax.to(escaAct,0.6, {alpha:1, onComplete:bringFacilityNormalState});
				}
				if(str == '_5')
				{
					foodact = new FoodAct();
					facilityArr[n].addChild(foodact);
					foodact.alpha = 0;
					actArr.push(foodact);
					if((facilityArr[n] as MovieClip).name == '_5') TweenMax.to(foodact,0.6, {alpha:1, onComplete:bringFacilityNormalState});
				}
				
			}
			
			
		}
		private function bringFacilityNormalState():void
		{
			//trace('bringFacilityNormalState');
			for (var n:int = 0; n<actArr.length; n++)
			{
				TweenMax.to(actArr[n],1.5, {alpha:0, delay:2.5});
			}
		}
		
		var _currentMC:Sprite = null;
		var _mouseDownTarget:Sprite = null;
		
		private function onMouseDownShop(e:MouseEvent):void
		{
			for (var n:int = 0; n<shopMcArr.length; n++)
			{
				(shopMcArr[n] as MovieClip).alpha = 0;
			}
			
			this.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUPShop);
			shopBubble.hide();
			_mouseDownTarget = e.target;
			_mouseDownTarget.alpha = 0.5;
		}
		
		private function onShowBubble (mc:MovieClip, _model:ShopModel):void
		{
			shopBubble.model = _model;
			//shopBubble.visible = true;
			shopBubble.show();
			shopBubble.x = mc.x;
			shopBubble.y = mc.y;
			shopBubble._pane._moreInfoBTN.addEventListener(MouseEvent.CLICK, onClickMoreInfo);
		
		
			if (mc.x < 480)
			{
				shopBubble.x = mc.x + 100;
				shopBubble._pane.x = 131;
			}
			if (mc.x > 480)
			{
				if (mc.width > 380)
				{
					shopBubble.x = mc.x - 150;
					shopBubble._pane.x = -199;
				}
				else if (mc.width < 380)
				{
					shopBubble.x = mc.x - 90;
					shopBubble._pane.x = -199;
				}
				else if (mc.width < 150)
				{
					shopBubble.x = mc.x;
					shopBubble._pane.x = -199;
				}
			}
			if(mc.y < 300)
			{
				shopBubble.y = mc.y + 60;
				shopBubble._pane.y = -80;
			}
			if (mc.y > 300)
			{
				shopBubble.y = mc.y + 80;
				shopBubble._pane.y = -350;
			}
			
		}
		
		private function isThereAShopInModel(instanceName:String):ShopModel
		{
			for (var i:int = 0; i < shopModelArray.length ; i++) {
				if ( (shopModelArray[i] as ShopModel).unitKey == instanceName ) {
					//YES
					return shopModelArray[i];
				} 
			}
			
			return null;
		}
		
		public function unHilightMCAndCloseBubble():void
		{
			this._unHilightMCAndCloseBubble();
		}
		
		private function _unHilightMCAndCloseBubble():void
		{
			/*
			for (var i:int = 0; i < shopMcArr.length; i++)
			{
				shopMcArr[i].alpha = 0;
			}*/
			if (_mouseDownTarget) _mouseDownTarget.alpha = 0;
			
			
			if (this._mouseDownTarget)
			{
				shopBubble.hide();
				
			}
			if (this._currentMC) {
				this._currentMC.alpha = 0;
				this._currentMC = null;
			}
			
			shopBubble.hide();
		}
		
		private function onMouseUPShop(e:MouseEvent):void
		{
			
			
			var strstr:String = e.target.name.substr(0,8);
			
			var shopModel:ShopModel = isThereAShopInModel(strstr);
			trace("shopModel", shopModel);
			sendUnit = e.target.name;
			if (shopModel != null) {
				//show Bubble
				if (_mouseDownTarget == _currentMC) {
					//If already show, hide it.
					_unHilightMCAndCloseBubble();
				} else {
					//If not show, then show it.
					
					if (e.target == _mouseDownTarget) 
					{
						onShowBubble(e.target, shopModel);
						this._currentMC = _mouseDownTarget;
					
					}
					
				}
			}
			else
			{
				trace("Here?");
				_unHilightMCAndCloseBubble();
				
				
			}
			
			 _mouseDownTarget = null;
			 this.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUPShop);

			

		}
		public function set showFacilitys(str:String):void
		{
			e.target.alpha = 1;

		}


		function initializeCarousel():void
		{

			var angleDifference:Number = Math.PI * (360 / images_num) / 180;

			for (var i:uint = 0; i < imageHolders.length; i++)
			{

				var imageHolder:MovieClip = (MovieClip)(imageHolders[i]);
				var startingAngle:Number = (angleDifference * i) + 40;


				imageHolder.xpos3D = radiusX * Math.cos(startingAngle);
				imageHolder.zpos3D = radius * Math.sin(startingAngle);
				imageHolder.ypos3D = floor;


				imageHolder.currentAngle = startingAngle;
				imageHolder.startingAngle = startingAngle;
				if (i == 0)
				{
					nowangle0 = imageHolder.currentAngle;
				}
				else if (i == 1)
				{
					nowangle1 = imageHolder.currentAngle;
				}
				else if (i == 2)
				{
					nowangle2 = imageHolder.currentAngle;
				}
				else if (i == 3)
				{
					nowangle3 = imageHolder.currentAngle;
				}
				else if (i == 4)
				{
					nowangle4 = imageHolder.currentAngle;
					_startingCurrentAngle = nowangle4;
				}


				var scaleRatio = 0.75;

				imageHolder.scaleX = imageHolder.scaleY = scaleRatio;

				imageHolder.alpha = 1;//0.8

				imageHolder.x = vanishingPointX + imageHolder.xpos3D * scaleRatio;
				imageHolder.y = vanishingPointY + imageHolder.ypos3D * scaleRatio;


				addChild(imageHolder);

			}

			dispatchEvent(new Event("completeload"));

			goToFloor(youarehereFloor);

		}


		private function moveSingleToAngle(imageHolder:MovieClip):void
		{
			imageHolder.xpos3D =radiusX*Math.cos((imageHolder.currentAngle));
			imageHolder.zpos3D = radius * Math.sin(imageHolder.currentAngle);
			imageHolder.ypos3D = 0.2 * radius * Math.sin(imageHolder.currentAngle);
			imageHolder.alpha = ( 2 * (1 + Math.sin(Math.PI + imageHolder.currentAngle)) ) - 2.4;
		}



		private function storeAngleToIndex(_i:int, imageHolder:MovieClip):void
		{
			if (_i == 0)
			{
				nowangle0 = imageHolder.currentAngle;
			}
			else if (_i == 1)
			{
				nowangle1 = imageHolder.currentAngle;
			}
			else if (_i == 2)
			{
				nowangle2 = imageHolder.currentAngle;
			}
			else if (_i == 3)
			{
				nowangle3 = imageHolder.currentAngle;
			}
			else if (_i == 4)
			{
				nowangle4 = imageHolder.currentAngle;
			}
		}

		function rotateCarouselToAngle(_angle:Number, _animated:Boolean = false):void
		{
			_currentAngle = _angle;
			//

			for (var i:uint = 0; i < imageHolders.length; i++)
			{

				var imageHolder:MovieClip = (MovieClip)(imageHolders[i]);
				imageHolder.currentAngle = _angle + imageHolder.startingAngle;
				imageHolder.mouseChildren = true;
				imageHolder.name = 'ddddddd';

				storeAngleToIndex(i, imageHolder);
				moveSingleToAngle(imageHolder);

				var scaleRatio = 1;

				if (! _animated)
				{

					imageHolder.x = vanishingPointX + imageHolder.xpos3D * scaleRatio;
					imageHolder.y = vanishingPointY + imageHolder.ypos3D * scaleRatio;
					imageHolder.scaleX = imageHolder.scaleY = (0.3 * Math.sin(Math.PI + imageHolder.currentAngle)) + 0.7;


				}
				else
				{

					var _scale:Number = (0.3 * Math.sin(Math.PI + imageHolder.currentAngle)) + 0.7;
					var targetX:Number = vanishingPointX + imageHolder.xpos3D * scaleRatio;
					var targetY:Number = vanishingPointY + imageHolder.ypos3D * scaleRatio;

					TweenMax.to(imageHolder, 0.4, {scaleX : _scale, scaleY : _scale, x : targetX, y : targetY, ease:Expo.easeOut});
				}

			}

			sortZ();

		}


		function sortZ():void
		{

			imageHolders.sortOn("zpos3D", Array.NUMERIC | Array.DESCENDING);

			for (var i:uint = 0; i < imageHolders.length; i++)
			{
				setChildIndex(imageHolders[i], i);


				if (i == 4)
				{
					nowitem = imageHolders[i].name;
					if (beforemoveitem != imageHolders[i].name)
					{
						dispatchEvent(new Event("sendnowitem"));
					}
					beforemoveitem = nowitem;

				}

			}
		}

		function imageClicked(str:String):void
		{
			//hideAllShop();

		}

		public function onClickMoreInfo(e:MouseEvent):void
		{
			dispatchEvent(new Event('ClickMoreInfo'));
			//trace('onClickMoreInfo', sendUnit);
		}

		public function hideAllShop():void
		{
			
			this._unHilightMCAndCloseBubble();
		
		}
		
		var _tModel:ShopModel;
		public function getCategoryButtonTapped(_categoryID:int):void
		{
			trace('getCategoryButtonTapped', _categoryID, this.shopModelArray.length);
			
			for (var i:int = 0; i < this.shopModelArray.length; i++) {
				
				_tModel = this.shopModelArray[i];
				
				
				if (_tModel.shopMC) {
					trace("shopMC", _tModel.shopMC);
					var c:Boolean = _tModel.isCategory(_categoryID);
					
					if (c == true) {
						_tModel.shopMC.alpha = 0.5;
					} else {
						_tModel.shopMC.alpha = 0;
					}
					
				}
				
			}
			
		}
		
		




	}

}