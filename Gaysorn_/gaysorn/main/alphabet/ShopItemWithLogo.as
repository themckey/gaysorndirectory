﻿package main.alphabet {
	
	import flash.display.MovieClip;
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UIImageView;
	
	
	public class ShopItemWithLogo extends UIButton{
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		public var uiImage:UIImageView = new UIImageView(new Rectangle(0,0,216,216));
		
		public function ShopItemWithLogo() {
			// constructor code
			
			
			super(theFrame);
			addChild(uiImage);
		}
		
		
		public function set model(theModel:ShopModel):void
		{
			
			_shopname.text = theModel.nameEn;
			uiImage.loadImage(theModel.logoPath);
			
		}
		
		
		
	}
	
}
