﻿package main.wifi
{

	import flash.display.MovieClip;
	import com.forviz.ui.UIScrollView;
	import flash.geom.Rectangle;
	import com.forviz.ui.CGSize;
	import flash.geom.Point;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class DropdownCountry extends MovieClip
	{
		var countryList:Array = new Array("Thailand","China","Japan","Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda","Argentina","Armenia","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Central African Republic","Chad","Chile","Colombia","Comoros","Congo (Brazzaville)","Congo","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor (Timor Timur)","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland","France","Gabon","Gambia, The","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica","Japan","Jordan",
		  "Kazakhstan","Kenya","Kiribati","Korea, North","Korea, South","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Northern Ireland","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Qatar","Romania","Russia","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Scotland","Senegal","Serbia and Montenegro","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","Spain","Sri Lanka","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzani"  ,
		  "Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom (UK)","United States (USA)","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Wales","Yemen","Zambia","Zimbabwe");

		var countryOptionBTN:CountryItem;
		
		var itemSCR:UIScrollView;
		
		var gridContainer:Sprite = new Sprite();
		
		
		public function DropdownCountry()
		{
			// constructor code
			
			itemSCR = new UIScrollView(new Rectangle(0, 0, 480, 30));
			_content.addChild(itemSCR);
			itemSCR.addChild(gridContainer);
			
			addButton();
			this.addEventListener(MouseEvent.CLICK, hideAfterClick);
			
			
		}
		
		private function addButton():void
		{
			for(var i:int = 0; i < countryList.length; i++)
			{
				countryOptionBTN = new CountryItem();
				countryOptionBTN.x = 5;
				countryOptionBTN.y = 5 + (i * 60);
				
				countryOptionBTN._names.text = countryList[i].toString();
				countryOptionBTN.name = countryList[i].toString()
				gridContainer.addChild(countryOptionBTN);
			}
			
			itemSCR.contentSize = new CGSize(480, 11600);
			
			
			
		}
		
		private function hideAfterClick(e:MouseEvent):void
		{
			for(var i:int = 0; i < countryList.length; i++)
			{
				if(e.target.name == countryList[i].toString())
				{
					itemSCR.moveTo(new Point(0,0));
					this.visible = false;
				}
			}
			if(e.target.name == 'countryDropDown')
			{
				itemSCR.moveTo(new Point(0,0));
			}
		}




		
		
	}

}