﻿package main {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import main.flooroption.MainFlootBTNExtends;
	import com.pvm.ui.UIScrollView;
	import flash.geom.Rectangle;
	import com.pvm.ui.CGSize;
	import manager.GaysornDataManager;
	import flash.events.Event;
	import flash.geom.Point;
	import main.flooroption.MainFloorBTNExtends;
	import com.forviz.ui.UIControlEvent;
	import models.ShopModel;
	import flash.display.Sprite;
	import main.directory.CategoryMask;
	import flash.utils.Dictionary;
	import main.directory.ShopScrollBar;
	import com.pvm.ui.UIScrollViewEvent;
	
	import com.greensock.TweenMax;
	import events.ShopEvent;
	import events.FloorButtonEvent;
	import flash.sampler.Sample;
	
	
	
	public class CategoryPanel extends MovieClip {
		
		
		var floorNameArr:Array = new Array('lobby', 'ground', 'first', 'second', 'third');
		var scrollNameArr:Array = new Array('L', 'G', '1', '2', '3');
		var floorButtonArr:Array = new Array();
		var floorButton:MainFloorBTNExtends;
		
		var uiScr:UIScrollView; 
		
		var shopHolderArr:Array = new Array();
		
		var shopItem:ShopItem;
		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();
		
		var shopDict:Dictionary = new Dictionary();
		var shopid:String;
		
		var countFound:Number;
		var shopCatStr:String;
		
		var activeFloor:String;
		
		var shopScrollBar:ShopScrollBar;
		var shopScrArr:Array = new Array();
		var scrollHolder:Sprite = new Sprite();
		var tapShopEvent:ShopEvent = new ShopEvent('shopchoose');
		var tapFloorEvent:FloorButtonEvent = new FloorButtonEvent('floorchose');
		var youarehereFloor:String;
		
		//var shopEvent:ShopEvent = new ShopEvent();
		
		
		
		
		
		
		
		
		public function CategoryPanel() {
			// constructor code
			
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
			
		}
		
		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor;
			onDefault();
			
			
		}
		
		private function onDefault():void
		{
			setDefaultFloorButton(youarehereFloor);
			addShopToPanel();
			
			this.hideBTN.parent.setChildIndex(hideBTN,this.numChildren - 1);
			
			
			
		}
		private function setDefaultFloorButton(youhereFloor:String):void
		{
			if(youhereFloor == '1') youhereFloor = 'lobby';
			if(youhereFloor == '2') youhereFloor = 'ground';
			if(youhereFloor == '3') youhereFloor = 'first';
			if(youhereFloor == '4') youhereFloor = 'second';
			if(youhereFloor == '5') youhereFloor = 'third';
			
			for(var i:int = 0; i < floorNameArr.length; i++)
			{
				floorButton = this.getChildByName(floorNameArr[i]) as MainFloorBTNExtends;
				floorButtonArr.push(floorButton);
				if(floorNameArr[i] == youhereFloor)
				{
					floorButton.setActive();
				}
				else
				{
					floorButton.setNormal();
				}
			}
			
			assignFloorButton();
		}
		private function addShopToPanel():void
		{
			var theFrameL:Rectangle = new Rectangle(40,120,367,279)
			addShopByFloor('L', theFrameL);
			
			var theFrameG:Rectangle = new Rectangle(408,120,367,279)
			addShopByFloor('G', theFrameG);
			
			var theFrame1:Rectangle = new Rectangle(777,120,366.5,279)
			addShopByFloor('1', theFrame1);
			
			var theFrame2:Rectangle = new Rectangle(1145,120,366.5,279)
			addShopByFloor('2', theFrame2);
			
			var theFrame3:Rectangle = new Rectangle(1513.2,120,366.5,279)
			addShopByFloor('3', theFrame3);
			
			
			
		}
		public function assignFloorButton():void
		{
			for(var i:int = 0; i<floorButtonArr.length; i++)
			{
				(floorButtonArr[i] as MainFloorBTNExtends).addEventListener('tap', onTapFloorBTNHandler);
			}
			
		}
		
		private function onTapFloorBTNHandler (e:UIControlEvent):void
		{
			setFloorBtnActiveByName(e.target.name);
			hideAllShopActive();
			
			tapFloorEvent.obj = e.from;
			dispatchEvent(tapFloorEvent);
			
		}
		
		public function setFloorBtnActiveByName(floorItemName:String):void
		{
			trace('floorItemName',  floorItemName);
			if(floorItemName == 'L') floorItemName = 'lobby';
			if(floorItemName == 'G') floorItemName = 'ground';
			if(floorItemName == '1') floorItemName = 'first';
			if(floorItemName == '2') floorItemName = 'second';
			if(floorItemName == '3') floorItemName = 'third';
			

			for(var i:int = 0; i < floorButtonArr.length; i++)
			{
				(floorButtonArr[i] as MainFloorBTNExtends).setNormal();
				if(floorButtonArr[i].name == floorItemName)
				{
					(floorButtonArr[i] as MainFloorBTNExtends).setActive();
					activeFloor = floorItemName;
				}
				
			}
			
			showScrollShopCat(floorItemName);
			
			
			
		}
		public function get floorItemName():String
		{
			return activeFloor;
		}
		
		private function showScrollShopCat(catShow:String):void
		{
			if(catShow == 'lobby') catShow = 'L';
			if(catShow == 'ground') catShow = 'G';
			if(catShow == 'first') catShow = '1';
			if(catShow == 'second') catShow = '2';
			if(catShow == 'third') catShow = '3';
			
			for(var i:int = 0; i<shopScrArr.length; i++)
			{
				TweenMax.to(shopScrArr[i], 0.3, {y:40});
				
				if(shopScrArr[i].name == catShow)
				{
					shopScrArr[i].visible = true;
				}
				else
				{
					shopScrArr[i].visible = false;
				}
			}
			
			
		}
		
		
		
		private function addShopByFloor(floorName:String, frame:Rectangle):void
		{
			var count:int = 0;
			var scrollHeight:Number;
			var nameText:String;
			
			uiScr = new UIScrollView(frame);
			uiScr.name = floorName;
			addChild(uiScr);
			
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			for (var i:int = 0; i < shopModelArray.length; i++) {
				floorText = (shopModelArray[i] as ShopModel).floor;
				shopid = (shopModelArray[i] as ShopModel).id;
				if(floorText == floorName)
				{ 
					
					shopItem = new ShopItem();
					shopItem.y = (count * 50);
					uiScr.addChild(shopItem);
					
					shopItem.setNormal();
					shopItem.name = shopid;
					shopItem.model = shopModelArray[i];
					shopItem.addEventListener('tap', onTap);
					shopHolderArr.push(shopItem);
					shopDict[shopid] = shopItem;
					count++;
					
				}
			}
			
			for(var n:int = 0; n<shopHolderArr.length; n++)
			{
				(shopHolderArr[n] as ShopItem).addEventListener('tap', onTapShopItemHamdler);
			}
			
			scrollHeight = (count * 50) + 100;
			uiScr.contentSize = new CGSize(366, scrollHeight);
			
			
			addScrollBar(floorName);
			uiScr.addEventListener('scrollViewDidScroll', onMoveScrollButton);
			uiScr.addEventListener('scrollViewWillBeginDragging', onBeginDragingScroll);
			
			
			
		}
		public function onTap(e:UIControlEvent):void
		{
			//trace ('from cat panel',e.from.name);
			tapShopEvent.obj = e.from;
			//trace ('from cat panel',tapShopEvent.obj);
			dispatchEvent(tapShopEvent);
			
		}
		
		private function addScrollBar(cat:String):void
		{
			shopScrollBar = new ShopScrollBar();
			shopScrollBar.x = (uiScr.width + uiScr.x) - 15;
			shopScrollBar.y = 40;
			addChild(shopScrollBar);
			
			shopScrollBar.name = cat;
			shopScrArr.push(shopScrollBar);
			
			for(var i:int = 0; i<shopScrArr.length; i++)
			{
				if(shopScrArr[i].name == 'G')
				{
					shopScrArr[i].visible = true;
				}
				else
				{
					shopScrArr[i].visible = false;
				}
			}
			
		}
		private function onMoveScrollButton(e:UIScrollViewEvent):void
		{
			var scr:UIScrollView = (e.from as UIScrollView);
			showScrollShopCat(scr.name);
			setFloorBtnActiveByName(scr.name);
			setCategoryScrollBar(scr.contentPoint.y);
			
			
		}
		private function onBeginDragingScroll(e:UIScrollViewEvent):void
		{
			hideAllShopActive();
		}
		
		private function setCategoryScrollBar(_yScr:Number = 0):void
		{
		
			var contentStreak:Number = Math.floor(_yScr/100);
			var scrollStreak:Number = (85) * (-contentStreak);
			
			if(contentStreak < 0)
			{
				if(scrollStreak > 120)
				{
					scrollStreak = 120;
				}
				for(var i:int = 0; i<shopScrArr.length; i++)
				{
					TweenMax.to(shopScrArr[i], 0.3, {y:scrollStreak});
				}
				
				
			}
			else
			{
				if(scrollStreak <= 40)
				{
					scrollStreak = 40;
				}
				for(var i:int = 0; i<shopScrArr.length; i++)
				{
					TweenMax.to(shopScrArr[i], 0.3, {y:scrollStreak});
				}
			}
			
			
			
			
			
		}
		
		
		private function onTapShopItemHamdler (e:UIControlEvent):void
		{
			setShopActiveByName(e.from.name);
			dispatchEvent(new Event('floorActive'));
			dispatchEvent(new Event('shopActiveDone'));
			
			
		}
		public function hideAllShopActive ():void
		{
			for(var i:int = 0; i < shopHolderArr.length; i++)
			{
				(shopHolderArr[i] as ShopItem).setNormal();
			}
		}
		public function setShopActiveByName (shops:String = ''):void
		{
			trace ('shops', shops);
			for(var i:int = 0; i < shopHolderArr.length; i++)
			{
				(shopHolderArr[i] as ShopItem).setNormal();
				
				if(shopHolderArr[i].name == shops)
				{
					(shopHolderArr[i] as ShopItem).setActive();
					
				}
				if(shops == 'dummy')
				{
					(shopHolderArr[i] as ShopItem).setNormal();
				}
				
			}
			//translateShopFloor(shops);
			
			
		}
		private function translateShopFloor(shopSTR:String):void
		{
			for (var i:int = 0; i < shopModelArray.length; i++) {
				var idShop:String = (shopModelArray[i] as ShopModel).id;
				if(shopSTR == idShop)
				{
					var floorSTR:String = (shopModelArray[i] as ShopModel).floor;
					
					if(floorSTR == 'L') floorSTR = 'lobby';
					if(floorSTR == 'G') floorSTR = 'ground';
					if(floorSTR == '1') floorSTR = 'first';
					if(floorSTR == '2') floorSTR = 'second';
					if(floorSTR == '3') floorSTR = 'third';
				}
			}
			setFloorBtnActiveByName(floorSTR);
		}
		 
		public function reArrangeShopByCat(category:String):void
		{
			for(var i:int = 0; i < scrollNameArr.length; i++)
			{

				uiScr = this.getChildByName(scrollNameArr[i].toString());
				uiScr.moveTo(new Point(0,0));
			}
			
			setShopActiveByName('');
			
			countFound = 0;

			for (var i:int = 0; i < shopModelArray.length; i++)
			{
				shopCatStr = (shopModelArray[i] as ShopModel).catId
				shopID = (shopModelArray[i] as ShopModel).id;
				if (shopCatStr.indexOf(category.toLowerCase()) != -1)
				{
					shopDict[shopID].x = 160 + (countFound * 140);
					shopDict[shopID].y = 0;
					shopDict[shopID].visible = true;
					shopItemHolderWidth = 320 + (countFound * 140);
					countFound++;
				}

			}

			
			
		}
		private function clearShopBeforeRearrange():void
		{
			for each (var item:ShopItem in shopDict)
			{
				item.visible = false;
			}
		}
		
		public function viewWillAppear():void
		{
			setDefaultFloorButton(youarehereFloor);
		
			
			
		}
		
		
		
		
		
	}
	
}
