﻿package main.flooroption {
	import com.forviz.ui.UIButton;
	import flash.geom.Rectangle;
	import com.forviz.ui.UIControlEvent;
	import flash.display.MovieClip;
	
	import com.greensock.TweenMax;
	
	public class MainFloorBTNExtends extends UIButton{
		
		var theFrame:Rectangle = new Rectangle(this.x, this.y, this.width, this.height);
		var lightMC:MovieClip;
		var activeMC:MovieClip;
		
		
		

		public function MainFloorBTNExtends() {
			// constructor code
			super(theFrame);
			
			
			lightMC = this.getChildByName('_light') as MovieClip;
			activeMC = this.getChildByName('_ac') as MovieClip;
			
			
			
			
			
		}
		
		
		public function setNormal():void
		{
			lightMC.gotoAndStop(0);
			TweenMax.to(activeMC, 0.1, {y:-300});
			(this.getChildByName('_acText') as MovieClip).visible = false;

		}
		public function setActive():void
		{
			TweenMax.to(activeMC, 0.1, {y:-18.55, onComplete:showLighting});
			(this.getChildByName('_acText') as MovieClip).visible = true;
		}
		
		public function showLighting():void
		{
			lightMC.gotoAndPlay(5);
		}
	}
	
}
