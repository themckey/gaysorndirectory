﻿package main {
	
	import flash.display.MovieClip;
	import main.language.MainLanguageExtends;
	import flash.events.MouseEvent;
	import events.LanguageEvent;
	import flash.events.Event;
	
	
	public class LanguaueBar extends MovieClip {
		
		var languageArray:Array = new Array('en', 'cn', 'jp');
		
		
		public function LanguaueBar() {
			// constructor code
			
			
			viewWillAppear();
			
			this.addEventListener(MouseEvent.CLICK, onChooseLanguage);
			

		}
		
		public function viewWillAppear():void
		{
			this.en.setStateActive();
		}
		
		public function onChooseLanguage(e:MouseEvent):void
		{
			setButtonState(e.target.name)
			
		}
		
		private function setButtonState(btnName:String = ''):void
		{
			for(var i:int = 0; i < languageArray.length; i++)
			{
				(this.getChildByName(languageArray[i]) as MainLanguageExtends).setStateNormal();
				
				if(btnName == languageArray[i].toString())
				{
					
					(this.getChildByName(btnName) as MainLanguageExtends).setStateActive();
				}
			}
		}
		
		
	}
	
}
