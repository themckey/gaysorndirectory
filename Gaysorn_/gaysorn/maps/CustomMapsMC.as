﻿package maps {
	import com.pvm.maps.MapMC;
	import items.ShowShopnameMC;
	import items.SkipButton;
	import com.forviz.ui.UIControlEvent;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import models.ShopModel;
	import com.pvm.maps.Node;
	import com.greensock.TweenMax;
	import items.floorplan.PinMc;
	import flash.display.Sprite;
	import items.floorplan.EscalatorMC;
	import items.YouAreHereMc;
	import items.WalkerMan;
	
	public class CustomMapsMC extends MapMC{
		
		var shopName:ShowShopnameMC = new ShowShopnameMC();
		var skipButton:SkipButton = new SkipButton();
		var shopXMLList:XMLList = new XMLList();
		
		var thewalker:WalkerMan = new WalkerMan();
		
		public var pinmc:PinMc = new PinMc();
		var escaMC:EscalatorMC = new EscalatorMC();
		
		var sp:Sprite = new Sprite();
		var youRhere:YouAreHereMc = new YouAreHereMc();
		
		
		var currentFloorKey:String;
		var targetFloorKey:String;
		
		public function CustomMapsMC(w:Number = 0, h:Number = 0) {
			// constructor code
			
			super(w, h);
			this.name = 'CustomMapMc';
			
			this.walker = thewalker;
			
			this.facing = 'f';
			
			this.addFloor("0", "content/1.swf", "content/1.xml");
			this.addFloor("1", "content/2.swf", "content/2.xml");
			this.addFloor("2", "content/3.swf", "content/3.xml");
			this.addFloor("3", "content/4.swf", "content/4.xml");
			this.addFloor("4", "content/5.swf", "content/5.xml");
			
			
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
		}

		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereNode = GaysornDataManager.getInstance().configsXML.start_node;
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor;
			
			if(youarehereFloor == 2)
			{
				this.addPinOnNode(youRhere, '1', '24');
				
				//addChild(youRhere);
			}
			if(youarehereFloor == 4)
			{
				this.addPinOnNode(youRhere, '3', '19');
			}
			

		}
		
		override public function onLoadAllContentComplete():void
		{
			this.addFloorLink("0-19", "1-27"); 
			this.addFloorLink("1-27", "0-19"); 
			this.addFloorLink("1-27", "2-10");
			this.addFloorLink("2-10", "1-27"); 
			this.addFloorLink("2-10", "3-14"); 
			this.addFloorLink("3-14", "2-10"); 
			this.addFloorLink("3-14", "4-19"); 
			this.addFloorLink("4-19", "3-14"); 
			
			//this.addChild(escaMC);
			//escaMC.visible = false;
			
			
		}
		override public function tweenChangeFloor(currentFloorNode:Node, targetFloorNode:Node):TweenMax
		{
			
			currentFloorKey = currentFloorNode.floorKey;
			targetFloorKey = targetFloorNode.floorKey;
			var keys:Array = [];
    		for (var key:* in linkLineDict);
			
			linkLine = linkLineDict[currentFloorNode.key+"-"+targetFloorNode.key];
			
			var ss:int = int(targetFloorNode.key.split("-")[0]);
			
			
			
			if (linkLine) {
				linkLine.animateLine(); 
			}
			
			
			
			var herestay:Number = Number(currentFloorKey);
			var otherstay:Number = Number(targetFloorKey);
			var theFar:Number = otherstay - herestay;
			var newX:Number = 450;
			
			
			
			return TweenMax.to(allFloor, 4, {y: (ss) * 2000,
								  onStart:showEscalatorMC,
								  onStartParams :[herestay, otherstay],
								  onComplete:hideEscalatorMC
								  });
			
	
		}
		public function showEscalatorMC (herestay:String, otherstay:String):void
		{
			/*if(herestay == 1)
			{
				this.addPinOnNode(escaMC, '1', '27');
				
			}
			if(herestay == 2)
			{
				this.addPinOnNode(escaMC, '2', '10');
				
			}
			if(herestay == 3)
			{
				this.addPinOnNode(escaMC, '3', '14');
				
			}*/
			//TweenMax.to(escaMC, 0.2, {scaleX:0.857, scaleY:0.857, visible:true});
		}
		public function hideEscalatorMC ():void
		{
			//TweenMax.to(escaMC, 0.2, {visible:false});
		}
		
		override public function changeColorMC(mc:Sprite, hilightColor:String):void { 
			TweenMax.to(mc, 0.4, {tint:uint(hilightColor)});
		}
		override public function onAddBubble(mc:Sprite, str:String):void
		{
			//pinmc.visible = true;
			//this.addPin(pinmc,str,mc.x, mc.y);
		}
		override public function beforeRouteStart(endNode:Node = null):void 
		{ 
			this.unHilightAll();
			dispatchEvent(new Event('beforeRouting'));
		}
		override public function afterRouteFinish(endNode:Node = null):void 
		{ 
			dispatchEvent(new Event('endRouting'));
		}
		
		private function onTapSkipButtonHandler(e:UIControlEvent):void
		{
			dispatchEvent(new Event('backToMainFloor'));
		}
		
	
	}
	
}
