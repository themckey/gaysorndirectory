﻿package gaysorn.maps {
	
	import flash.display.Sprite;
	import flash.text.TextField;
	
	import com.greensock.TweenMax;
	
	public class Dot extends Sprite {

		public function Dot() {
			// constructor code
			this.graphics.lineStyle(3,0x4F3933,1);
			this.graphics.beginFill(0xfae0ae, 1);
			this.graphics.drawCircle(0, 0, 7);
			this.graphics.endFill();
			this.alpha = 0;
			
			
			TweenMax.to(this, 0.4, {alpha:1});
		}

	}
	
}
