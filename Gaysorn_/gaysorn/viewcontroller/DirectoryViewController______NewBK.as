﻿package viewcontroller
{
	import com.forviz.ViewController;
	import flash.display.MovieClip;
	import items.floorplan.FloorplanCoverflow;
	import items.NextBTN;
	import items.PrevBTN;
	import com.forviz.ui.UIControlEvent;
	
	import com.greensock.TweenMax;
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.easing.Expo;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import manager.GaysornDataManager;
	import models.ShopModel;
	import maps.CustomMapsMC;
	import items.ShowShopnameMC;
	import items.SkipButton;
	import com.forviz.ViewControllerManager;
	import flash.geom.Rectangle;
	import com.jmx2.delayedFunctionCall;
	import items.MaskDetail;
	import items.MaskMap;
	import flash.display.Loader;
	import flash.net.URLRequest;
	

	public class DirectoryViewController extends ViewController
	{
		public var floorplanCoverflow:FloorplanCoverflow = new FloorplanCoverflow();
		var nextBTN:NextBTN = new NextBTN();
		var prevBTN:PrevBTN = new PrevBTN();
		
		var nextIndex:int;
		var prevIndex:int;
		var skipButton:SkipButton = new SkipButton();
		
		var floorKey:int;
		
		
		public var customMapMC:CustomMapsMC = new CustomMapsMC();
		var showShopName:ShowShopnameMC = new ShowShopnameMC();
		var vm:ViewControllerManager = new ViewControllerManager(new Rectangle(0,0,1920,1000));
		var shopDetailVC:ShopDeatailViewController = new ShopDeatailViewController();
		
		
		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();
		
		var visitfloor:String;
		var visitnode:String;
		var youarehereFloor:int = 0;
		var youarehereNode:String;
		
		var _mask:MaskMap = new MaskMap();
		var changeFloorMC:MovieClip = new MovieClip();
		var goupMC:Loader = new Loader();
		
		
		public function DirectoryViewController()
		{

			addChild(floorplanCoverflow);
			floorplanCoverflow.addEventListener('setFirstFloorDefault', checkIndex);
			floorplanCoverflow.addEventListener('ClickMoreInfo', showFloorplanMC);
			
			nextBTN.x = 1770;
			nextBTN.y = 550;
			addChild(nextBTN);
			nextBTN.addEventListener('tap', onTapNextBTN);
			
			prevBTN.x = 30;
			prevBTN.y = 550;
			addChild(prevBTN);
			
			prevBTN.addEventListener('tap', onTapPrevBTN);
			
			customMapMC.x = 595;
			customMapMC.y = 50;
			addChild(customMapMC);
			hideFloorplanMC();
			
			
			_mask.x = 150;
			_mask.y = 200;
			addChild(_mask);
			customMapMC.mask = _mask;
			
			addChild(showShopName);
			
			skipButton.x = 1450;
			skipButton.y = 850;
			addChild(skipButton);
			
			addChild(changeFloorMC);
			changeFloorMC.addChild(goupMC);
			changeFloorMC.gotoAndStop(0);
			goupMC.load(new URLRequest('content/GoUp.swf'));
			changeFloorMC.visible = false;
			
			
			customMapMC.addEventListener('endRouting', onRoutingEnd);
			skipButton.addEventListener('tap', onTapSkipHandler);
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
			
			addChild(shopDetailVC);
			backbtn = shopDetailVC.getChildByName('_back');
			shopDetailVC.addChild(backbtn);
			backbtn.addEventListener('tap', onTapBackBTNHandler);
			
		}
		
		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereNode = GaysornDataManager.getInstance().configsXML.start_node.text();
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor.text();
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			if(youarehereFloor == 2)
			{
				customMapMC.showFloorByKey('1', '');
			}
			if(youarehereFloor == 4)
			{
				customMapMC.showFloorByKey('3', '');
			}
		}
		private function onTapSkipHandler(e:UIControlEvent):void
		{
			//TweenMax.to(customMapMC,0.75,{scaleX:0.75,scaleY:0.75,x:930,y:70});
			showShopName.visible = false;
			new delayedFunctionCall(doneShowRoute, 500);
		}
		private function onClick(e:MouseEvent):void
		{
			trace(e.target.name);
		}
		public function onTapPrevBTN (e:UIControlEvent):void
		{
			floorplanCoverflow.goPrev();
			checkIndex(floorplanCoverflow._currentFloorIndex);
		}
		public function onTapNextBTN (e:UIControlEvent):void
		{
			floorplanCoverflow.goNext();
			checkIndex(floorplanCoverflow._currentFloorIndex);
		}
		
		private function checkIndex(currentIndex:int):void
		{
			setButtonName(floorplanCoverflow._currentFloorIndex);
		}
		
		public function scaleFloorplan(str:String):void
		{
			var mainnavArr:Array = new Array('all_btn', 'fashion_btn', 'jewelry_btn', 'liftstyle_btn', 'cuisine_btn');
			for(var i:int = 0; i < mainnavArr.length; i++)
			{
				if(str == mainnavArr[i].toString())
				{
					TweenMax.to(this.floorplanCoverflow, 1.5,{x:140 ,y:0, scaleX:0.857, scaleY:0.857, ease:Expo.easeOut});
					TweenMax.to(this.nextBTN, 1.5,{y:450, ease:Expo.easeOut});
					TweenMax.to(this.prevBTN, 1.5,{y:450, ease:Expo.easeOut});
				}
				
				if(str == 'hideBTN' || str == '__back'){
					TweenMax.to(this.floorplanCoverflow, 1.5,{x:0 ,y:0, scaleX:1, scaleY:1, ease:Expo.easeOut});
					TweenMax.to(this.nextBTN, 1.5,{y:550, ease:Expo.easeOut});
					TweenMax.to(this.prevBTN, 1.5,{y:550, ease:Expo.easeOut});
				}
			}
			
		}
		public function showFloorplanMC(e:Event = null):void
		{
			changeFloorMC.visible = true;
			changeFloorMC.gotoAndPlay(0);
			TweenMax.to(customMapMC, 1.5,{alpha:0,x:595,y:90, ease:Expo.easeOut, delay:1});
			this._logo.visible = false;
			skipButton.visible = true;
			showShopName.visible = true;
			
			
			
			setShowShopName(this.floorplanCoverflow.sendUnit);
			hideFloorplanCoverFlow();
		}
		
		public function hideFloorplanMC():void
		{
			_floorDes.visible = true;
			skipButton.visible = false;
			this._logo.visible = true;
			showShopName.visible = false;
			TweenMax.to(customMapMC,0.7,{autoAlpha:0,scaleX:1,scaleY:1,x:595,y:50, ease:Expo.easeOut});
			shopDetailVC.visible = false;
		}
		private function showRouting():void
		{
			customMapMC.pinmc.visible = false;
			if(youarehereFloor == 2)
			{
				customMapMC.showFloorByKey('1', '');
			}
			if(youarehereFloor == 4)
			{
				customMapMC.showFloorByKey('3', '');
			}
			var toFloor:String;
			if(visitfloor == 'L') toFloor = '0';
			if(visitfloor == 'G') toFloor = '1';
			if(visitfloor == '1') toFloor = '2';
			if(visitfloor == '2') toFloor = '3';
			if(visitfloor == '3') toFloor = '4';
			
			trace('showRouting', youarehereFloor, '    ', youarehereNode, '      ', toFloor, '       ', visitnode);
			if(youarehereFloor == 2)
			{
				if(youarehereNode != '-' && toFloor != '-' && visitnode != '-')
				{
					customMapMC.showRoute('1',youarehereNode,toFloor,visitnode);
				}
			}
			if(youarehereFloor == 4)
			{
				if(youarehereNode != '-' && toFloor != '-' && visitnode != '-')
				{
					customMapMC.showRoute('3',youarehereNode,toFloor,visitnode);
				}
			}
			
			skipButton.visible = true;
			dispatchEvent(new Event('showRouting'));
			
		}
		public function onRoutingEnd(e:Event):void
		{
			showShopBubble();
			new delayedFunctionCall(doneShowRoute, 2000);
		}
		private function showShopBubble():void
		{

			if(visitfloor == 'L') customMapMC.addBuble(0, this.floorplanCoverflow.sendUnit);
			if(visitfloor == 'G') customMapMC.addBuble(1, this.floorplanCoverflow.sendUnit);
			if(visitfloor == '1') customMapMC.addBuble(2, this.floorplanCoverflow.sendUnit);
			if(visitfloor == '2') customMapMC.addBuble(3, this.floorplanCoverflow.sendUnit);
			if(visitfloor == '3') customMapMC.addBuble(4, this.floorplanCoverflow.sendUnit);
			
			if(visitfloor == 'L') customMapMC.hilightMC(0,this.floorplanCoverflow.sendUnit);
			if(visitfloor == 'G') customMapMC.hilightMC(1,this.floorplanCoverflow.sendUnit);
			if(visitfloor == '1') customMapMC.hilightMC(2,this.floorplanCoverflow.sendUnit);
			if(visitfloor == '2') customMapMC.hilightMC(3,this.floorplanCoverflow.sendUnit);
			if(visitfloor == '3') customMapMC.hilightMC(4,this.floorplanCoverflow.sendUnit);
			customMapMC.pinmc.visible = true;
			
		}
		
		public function hideFloorplanCoverFlow():void
		{
			TweenMax.to(this.floorplanCoverflow, 1.5,{autoAlpha:0, ease:Expo.easeOut});
			nextBTN.visible = false;
			prevBTN.visible = false;
			_floorDes.visible = false;
		}
		private function showFloorplanCoverFlow():void
		{
			TweenMax.to(this.floorplanCoverflow, 1.5,{autoAlpha:1, x:140 ,y:0, scaleX:0.857, scaleY:0.857, ease:Expo.easeOut});
			nextBTN.visible = true;
			prevBTN.visible = true;
			_floorDes.visible = true;
			this.shopDetailVC.visible = false;
			
		}
		public function setShowShopName(ShopName:String):void
		{
			
			for(var i:int = 0; i<shopXMLList.length(); i++)
			{
				if(ShopName == shopXMLList[i].unit_key.text())
				{
					showShopName._shopname.text = 'Show Route to '+ shopXMLList[i].name_en.text();
					visitfloor = shopXMLList[i].floor.text();
					visitnode = shopXMLList[i].node.text();
					
				}
				else if(ShopName == shopXMLList[i].attribute('id'))
				{
					showShopName._shopname.text = 'Show Route to '+ shopXMLList[i].name_en.text();
					visitfloor = shopXMLList[i].floor.text();
					visitnode = shopXMLList[i].node.text();
					hideFloorplanCoverFlow();
					this.floorplanCoverflow.sendUnit = shopXMLList[i].unit_key.text()
					showFloorplanMC();
					
				}
				else
				{
					
				}
			}
			
			showRouting();
			
		}
		
		private function doneShowRoute():void
		{
			TweenMax.to(customMapMC,0.75,{scaleX:0.75,scaleY:0.75,x:930,y:70});
			showShopName.visible = false;
			TweenMax.to(shopDetailVC,0.1,{autoAlpha:1});	
			skipButton.visible = false;
			
			_logo.visible = false;
			
			
			for(var i:int = 0; i<shopModelArray.length; i++)
			{
				if((shopModelArray[i] as ShopModel).unitKey == this.floorplanCoverflow.sendUnit)
				{
					shopDetailVC.model = shopModelArray[i];
				}
			}
			
			
		}
		private function onTapBackBTNHandler (e:UIControlEvent):void
		{
			hideDetail();
			customMapMC.clearPath();
			scaleFloorplan('__back');
			floorplanCoverflow.hideAllShop();
			
		}
		public function hideDetail():void
		{
			customMapMC.unHilightAll();
			hideFloorplanMC();
			showFloorplanCoverFlow();
				
			
		}
		
		
		private function setButtonName(_index:int):void
		{
			
			if(_index == 1){
				floorKey = 0;
				_floorDes._floorname.text = 'Lobby Floor';
				nextBTN._flNamae.text = 'Groud Floor';
				prevBTN._flNamae.text = '3rd Floor';
			}
			if(_index == 2){
				floorKey = 1;
				_floorDes._floorname.text = 'Ground Floor';
				nextBTN._flNamae.text = '1st Floor';
				prevBTN._flNamae.text = 'Lobby Floor';
			}
			if(_index == 3){
				floorKey = 2;
				_floorDes._floorname.text = '1st Floor';
				nextBTN._flNamae.text = '2nd Floor';
				prevBTN._flNamae.text = 'Ground Floor';
			}
			if(_index == 4){
				floorKey = 3;
				_floorDes._floorname.text = '2nd Floor';
				nextBTN._flNamae.text = '3rd Floor';
				prevBTN._flNamae.text = '1st Floor';
			}
			if(_index == 0){
				floorKey = 4;
				_floorDes._floorname.text = '3rd Floor';
				nextBTN._flNamae.text = 'Lobby Floor';
				prevBTN._flNamae.text = '2nd Floor';
			}
			dispatchEvent(new Event('setFloorActive'));
		}
		public function setDefault():void
		{
			hideDetail();
			showFloorplanCoverFlow();
			hideFloorplanMC();
		}
		
		override public function viewWillAppear():void
		{
			
		}
	
		
	}

}