﻿package viewcontroller {
	
	import flash.display.MovieClip;
	import com.forviz.ViewController;
	import com.forviz.ui.UIImageView;
	import flash.geom.Rectangle;
	import models.ShopModel;
	import com.forviz.ui.UICarousel;
	
	
	public class ShopDeatailViewController extends ViewController {
		
		var logoIMG:UIImageView = new UIImageView(new Rectangle (198.95, 51.05, 216, 216));
		var uiCarousel:UICarousel;
		var picArr:Array = new Array();
		
		public function ShopDeatailViewController() {
			// constructor code
			
			addChild(logoIMG);
			
			uiCarousel = new UICarousel(new Rectangle (107, 403.95, 400, 200));
			uiCarousel.animation = "fade";
			addChild(uiCarousel);
			
			
		}
		
		public function set model(theModel:ShopModel):void
		{
			//_shopname.text = theModel.nameEn;
			if(theModel.floor == 'G')
			{
				_unitarea.text = ': Groud Floor';
			}
			else if(theModel.floor == 'L')
			{
				_unitarea.text = ': Lobby Floor';
			}
			else if(theModel.floor == '1')
			{
				_unitarea.text = ': 1st Floor';
			}
			else if(theModel.floor == '2')
			{
				_unitarea.text = ': 2nd Floor';
			}
			else if(theModel.floor == '3')
			{
				_unitarea.text = ': 3rd Floor';
			}
			
			_tel.text = ': ' + theModel.telNo;
			_detail.text = theModel.desEn;
			
			logoIMG.loadImage(theModel.logoPath);
			picArr = theModel.pathMediaArr;
			_loadImage();
			//uiImage.loadImage(theModel.mediaPath);
			
		}
		function _loadImage() : void {
			
			uiCarousel.unload();
 
	   		for(var i:int = 0; i < picArr.length; i++)
			{
				
				uiCarousel.addPage(picArr[i].toString());
			}
			
			show();
			
			
		}
		public function show():void
		{
			this.visible = true;
			uiCarousel.auto = 4;
			uiCarousel.playSlide();
		}
		
		public function hide():void
		{
			this.visible = false;
			uiCarousel.stopSlide();
		}
		
	}
	
}
