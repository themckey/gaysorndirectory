﻿package viewcontroller
{
	import com.forviz.ViewController;
	import com.forviz.ui.UIControlEvent;
	import main.MainNav;
	import com.forviz.ui.UIImageView;
	import flash.geom.Rectangle;
	import flash.display.MovieClip;
	import com.forviz.ui.UICarousel;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import com.forviz.ui.UICarouselEvent;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import com.greensock.TweenMax;
	import maps.CustomMapsMC;
	import com.pvm.ui.UIButtonSprite;
	import flash.events.MouseEvent;
	import items.ShopBubble;
	import models.ShopModel;

	import manager.GaysornDataManager;
	import com.forviz.data.models.ShopModel;
	import items.YouAreHereMc;
	import items.NextBTN;
	import items.PrevBTN;
	import items.SkipButton;
	import items.ShowShopnameMC;
	import com.forviz.ViewControllerManager;
	import com.forviz.ui.UIControl;
	import com.jmx2.delayedFunctionCall;
	import items.MaskDetail;

	public class DirectoryViewController extends ViewController
	{
		var floorplanPath:String = 'content/';
		var floorplanIndex:Array = new Array('1','2','3','4','5');
		var youarehereFloor:int = 0;
		var floorIndex:String;

		var floorHolder:MovieClip;
		var floorplanLoader:Loader;
		public var uiCarousel:UICarousel = new UICarousel(new Rectangle(0,0,1480,585));


		var currentFloorplan:MovieClip;
		var prevFloorplan:MovieClip;
		var nextFloorplan:MovieClip;

		var firstIndex:int = 0;
		var lastIndex:int;
		var currentIndex:int;
		var prevIndex:int;
		var nextIndex:int;

		var floorplanArr:Array = new Array();

		var shops_mc:MovieClip;

		var orgX:Number;

		var customMapMC:CustomMapsMC = new CustomMapsMC();
		var shopMcArr:Array = new Array();
		var shopMC:MovieClip;

		var shopBubble:ShopBubble = new ShopBubble();
		var bubbleArr:Array = new Array();

		var touchdownContentX:Number;
		var toIndex:int;

		var toScale:Number;
		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();
		var shopNum:String;
		
		var youarehereNode:String;
		
		var youarehereMC:YouAreHereMc = new YouAreHereMc();
		var youarehereMCX:Number;
		var youarehereMCY:Number;
		
		var nextBTN:NextBTN = new NextBTN();
		var prevBTN:PrevBTN = new PrevBTN();
		var skipButton:SkipButton = new SkipButton();
		var shopName:ShowShopnameMC = new ShowShopnameMC();
		
		var vm:ViewControllerManager = new ViewControllerManager(new Rectangle(0,0,1920,1000));
		var shopDetailVC:ShopDeatailViewController = new ShopDeatailViewController();
		var sendUnit:String;				
		
		var backbtn:UIControl = new UIControl();
		
		var visitfloor:String;
		var theNode:String;
		
		var visitFloor:String;
		
		
		var _mask:MaskDetail = new MaskDetail();

		public function DirectoryViewController()
		{

			addChild(uiCarousel);
			
			nextBTN.x = 1770;
			nextBTN.y = 410;
			addChild(nextBTN);
			
			prevBTN.x = 20;
			prevBTN.y = 410;
			addChild(prevBTN);
			
			uiCarousel.btnNext = nextBTN;
			uiCarousel.btnPrev = prevBTN;
			
			skipButton.x = 1450;
			skipButton.y = 850;
			addChild(skipButton);
			
			shopName.x = -120;
			shopName.y = 100;
			addChild(shopName);
			
			customMapMC.x = 822;
			customMapMC.y = 110;
			addChild(customMapMC);
			customMapMC.scaleX = customMapMC.scaleY = 1.1;
			customMapMC.showFloorByKey('0', '');
			hideFloorplanMc();

			
			
			
			addChild(vm);
			vm.addViewController(shopDetailVC, 'shopdetail');
			backbtn = shopDetailVC.getChildByName('_back');
			shopDetailVC.addChild(backbtn);
			backbtn.addEventListener('tap', onTapBackBTNHandler);
			
			
			
			skipButton.addEventListener('tap', onTapSkipButtonHandler);

			uiCarousel.addEventListener('carouselWillMove', carouselWillMove);
			uiCarousel.addEventListener("carouselDraggingBegin", carouselDraggingBegin);

			//uiCarousel.addEventListener('carouselDidMove', carouselDidMove);

			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
		}

		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereNode = GaysornDataManager.getInstance().configsXML.start_node;
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor;
			youarehereMCX = GaysornDataManager.getInstance().configsXML.start_nodeX;
			youarehereMCY = GaysornDataManager.getInstance().configsXML.start_nodeY;
			callDefaultFloorplan();

		}

		private function addFloorplanCarousel():void
		{
			for (var i:int = 0; i<floorplanIndex.length; i++)
			{
				floorIndex = floorplanIndex[i].toString();

				floorHolder = new MovieClip();
				floorplanLoader = new Loader();
				floorHolder.addChild(floorplanLoader);
				floorplanLoader.load(new URLRequest(floorplanPath + floorplanIndex[i] + '.swf'));

				uiCarousel.addPage(floorHolder);
				floorHolder.name = floorIndex;
				floorplanArr.push(floorHolder);

				floorplanLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onFloorplanLoaded);

			}



		}

		private function onFloorplanLoaded(e:Event):void
		{
			var shops_mc:MovieClip = (e.target.content as MovieClip).getChildByName('shops_mc');

			if (shops_mc)
			{
				for (var i:int = 0; i<shops_mc.numChildren; i++)
				{
					if (shops_mc.getChildAt(i) is MovieClip)
					{
						shopMC = new MovieClip();
						shopMC = shops_mc.getChildAt(i);
						shopMcArr.push(shopMC);

					}

				}
			}

			var path_mc:MovieClip = (e.target.content as MovieClip).getChildByName('pathMap');
			if (path_mc)
			{
				path_mc.visible = false;
			}

			var node_mc:MovieClip = (e.target.content as MovieClip).getChildByName('nodeMap');
			if (node_mc)
			{
				node_mc.visible = false;
				
			}

			for (var i:int = 0; i < shopMcArr.length; i++)
			{
				(shopMcArr[i] as MovieClip).alpha = 0;
				(shopMcArr[i] as MovieClip).addEventListener(MouseEvent.CLICK,onTapShopMc);

			}


		}
		private function onTapShopMc(e:MouseEvent):void
		{
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			for (var n:int = 0; n < shopMcArr.length; n++)
			{
				var shopMcInArray:MovieClip = (shopMcArr[n] as MovieClip);
				if (e.target.name == shopMcInArray.name)
				{
					for(var i:int = 0; i<shopModelArray.length; i++)
					{
						if((shopModelArray[i] as ShopModel).unitKey == shopMcInArray.name)
						{
							visitfloor = (shopModelArray[i] as ShopModel).floor;
							
							theNode = (shopModelArray[i] as ShopModel).node;
							
							shopMcInArray.alpha = 1;
							shopMcInArray.parent.setChildIndex(shopMcInArray, shopMcInArray.parent.numChildren-1);
							shopMcInArray.addChild(shopBubble);
							shopBubble.name = 'shopBubble';
							shopBubble.model = shopModelArray[i];
							setupShopName(e.target.name);
							sendUnit = e.target.name;
							
							
							
							if (shopMcInArray.x <= 250)
							{
								shopBubble.x = 50;
							}
							if (shopMcInArray.y <= 250)
							{
								shopBubble._pane.y = -70;
							}
							if (shopMcInArray.y >= 250)
							{
								shopBubble._pane.y = -341.55;
							}


						}
					}

					
				}
			}


		}
		
		public function setupShopTapped(str:String = ''):void
		{
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			var strstr:String = str.substr(0,8)
			
			for (var n:int = 0; n < shopMcArr.length; n++)
			{
				var shopMcInArray:MovieClip = (shopMcArr[n] as MovieClip);
				if (strstr == shopMcInArray.name)
				{
					trace ('Yes');
					for(var i:int = 0; i<shopModelArray.length; i++)
					{
						if((shopModelArray[i] as ShopModel).unitKey == shopMcInArray.name)
						{
							trace ('Yes2');
							visitfloor = (shopModelArray[i] as ShopModel).floor;
							
							theNode = (shopModelArray[i] as ShopModel).node;
							
							shopMcInArray.alpha = 1;
							shopMcInArray.parent.setChildIndex(shopMcInArray, shopMcInArray.parent.numChildren-1);
							shopMcInArray.addChild(shopBubble);
							shopBubble.name = 'shopBubble';
							shopBubble.model = shopModelArray[i];
							setupShopName(strstr);
							sendUnit = strstr;
							
							
							
							if (shopMcInArray.x <= 250)
							{
								shopBubble.x = 50;
							}
							if (shopMcInArray.y <= 250)
							{
								shopBubble._pane.y = -70;
							}
							if (shopMcInArray.y >= 250)
							{
								shopBubble._pane.y = -341.55;
							}


						}
					}

					
				}
				else
				{
					//trace ('Yes');
					for(var i:int = 0; i<shopModelArray.length; i++)
					{
						
						if((shopModelArray[i] as ShopModel).id == str)
						{
							//trace ('Yes2');
							strstr = (shopModelArray[i] as ShopModel).unitKey;
							visitfloor = (shopModelArray[i] as ShopModel).floor;
							
							theNode = (shopModelArray[i] as ShopModel).node;
							
							shopMcInArray.alpha = 1;
							shopMcInArray.parent.setChildIndex(shopMcInArray, shopMcInArray.parent.numChildren-1);
							shopMcInArray.addChild(shopBubble);
							shopBubble.name = 'shopBubble';
							shopBubble.model = shopModelArray[i];
							setupShopName(strstr);
							sendUnit = strstr;
							
							
							
							if (shopMcInArray.x <= 250)
							{
								shopBubble.x = 50;
							}
							if (shopMcInArray.y <= 250)
							{
								shopBubble._pane.y = -70;
							}
							if (shopMcInArray.y >= 250)
							{
								shopBubble._pane.y = -341.55;
							}


						}
					}

				}
			}
			
			//moveFloorplan(int(visitfloor));
			
			//showFloorplanMc();

		}
		private function setCarouselFirstLook(e:UICarouselEvent):void
		{
			var floorplanShowing:int = e.toIndex;

		}
		private function callDefaultFloorplan():void
		{
			uiCarousel.x = 280;
			uiCarousel.y = 90;
			uiCarousel.go(youarehereFloor);
			
			
			

			for (var i:int = 0; i < floorplanArr.length; i++)
			{
				if (youarehereFloor == 1)
				{
					prevBTN._flNamae.text = 'Lobby Floor';
					nextBTN._flNamae.text = '1st Floor';
					
					(floorplanArr[2] as MovieClip).scaleX = (floorplanArr[2] as MovieClip).scaleY = 0.8;
					(floorplanArr[2] as MovieClip).alpha = 0.3;
					(floorplanArr[2] as MovieClip).x = 1980;
					(floorplanArr[2] as MovieClip).y = 90;

					(floorplanArr[0] as MovieClip).scaleX = (floorplanArr[0] as MovieClip).scaleY = 0.8;
					(floorplanArr[0] as MovieClip).alpha = 0.3;
					(floorplanArr[0]as MovieClip).x = 1130;
					(floorplanArr[0] as MovieClip).y = 90;

					(floorplanArr[youarehereFloor] as MovieClip).x = 1336.5;
					(floorplanArr[youarehereFloor] as MovieClip).scaleX = (floorplanArr[youarehereFloor] as MovieClip).scaleY = 1.1;
					(floorplanArr[youarehereFloor] as MovieClip).alpha = 1;

					(floorplanArr[youarehereFloor] as MovieClip).parent.setChildIndex((floorplanArr[youarehereFloor] as MovieClip),(floorplanArr[youarehereFloor] as MovieClip).parent.numChildren - 1);
					
					
					youarehereMC.x = youarehereMCX;
					youarehereMC.y = youarehereMCY;
					youarehereMC.scaleX = youarehereMC.scaleY = 0.7;
					(floorplanArr[youarehereFloor] as MovieClip).addChild(youarehereMC);
				}
				
				if (youarehereFloor == 3)
				{
					prevBTN._flNamae.text = '1st Floor';
					nextBTN._flNamae.text = '3rd Floor';
					
					(floorplanArr[4] as MovieClip).scaleX = (floorplanArr[4] as MovieClip).scaleY = 0.8;
					(floorplanArr[4] as MovieClip).alpha = 0.3;
					(floorplanArr[4] as MovieClip).x = 4940;
					(floorplanArr[4] as MovieClip).y = 90;

					(floorplanArr[2] as MovieClip).scaleX = (floorplanArr[2] as MovieClip).scaleY = 0.8;
					(floorplanArr[2] as MovieClip).alpha = 0.3;
					(floorplanArr[2]as MovieClip).x = 4100;
					(floorplanArr[2] as MovieClip).y = 90;

					(floorplanArr[youarehereFloor] as MovieClip).x = 4296;
					(floorplanArr[youarehereFloor] as MovieClip).scaleX = (floorplanArr[youarehereFloor] as MovieClip).scaleY = 1.1;
					(floorplanArr[youarehereFloor] as MovieClip).alpha = 1;

					(floorplanArr[youarehereFloor] as MovieClip).parent.setChildIndex((floorplanArr[youarehereFloor] as MovieClip),(floorplanArr[youarehereFloor] as MovieClip).parent.numChildren - 1);
					
					youarehereMC.x = youarehereMCX;
					youarehereMC.y = youarehereMCY;
					youarehereMC.scaleX = youarehereMC.scaleY = 0.7;
					(floorplanArr[youarehereFloor] as MovieClip).addChild(youarehereMC);
				}
				
				

			}
			toScale = 0.9;

		}


		private function addCustomMapMC():void
		{
			
			
			

		}
		private function onTapSkipButtonHandler(e:UIControlEvent):void
		{
			doneShowRoute();
		
		}
		
		private function doneShowRoute():void
		{
			customMapMC.alpha = 0;
			TweenMax.to(customMapMC,0.7,{scaleX:0.8,scaleY:0.8,x:1150,y:130});
			customMapMC.showFloorByKey(currentIndex.toString(), '');
			new delayedFunctionCall(showMap, 2000); 
			shopName.visible = false;
			vm.openViewController(shopDetailVC);
			skipButton.visible = false;
			vm.visible = true;
			
			
			for(var i:int = 0; i<shopModelArray.length; i++)
			{
				if((shopModelArray[i] as ShopModel).unitKey == sendUnit)
				{
					shopDetailVC.model = shopModelArray[i];
				}
			}
			
			
		}
		private function showMap():void
		{
			TweenMax.to(customMapMC,1.5,{alpha:1});
		}
		
		private function onTapBackBTNHandler (e:UIControlEvent):void
		{
			hideDetail();
		}
		public function hideDetail():void
		{
			customMapMC.alpha = 0;
			TweenMax.to(customMapMC,0.7,{scaleX:0.8,scaleY:0.8,x:820,y:110});
			hideFloorplanMc();
			TweenMax.to(this.uiCarousel,1,{alpha:1});
			vm.visible = false;
			
		}
		public function showFloorplanMc():void
		{
			TweenMax.to(customMapMC,0.7,{scaleX:1.1,scaleY:1.1,x:820,y:110});
			TweenMax.to(customMapMC,1,{autoAlpha:1});
			_floorDes.visible = false;
			_logo.visible = false;
			nextBTN.visible = false;
			prevBTN.visible = false;
			skipButton.visible = true;
			shopName.visible = true;
			
			trace (visitfloor);
			
			if(visitfloor == 'L') visitFloor = '0';
			if(visitfloor == 'G') visitFloor = '1';
			if(visitfloor == '1') visitFloor = '2';
			if(visitfloor == '2') visitFloor = '3';
			if(visitfloor == '3') visitFloor = '4';
			
			customMapMC.addEventListener('beforeRouting', onRouting);
			customMapMC.showRoute(youarehereFloor,youarehereNode,visitFloor, theNode);
			
			customMapMC.addEventListener('endRouting', onRoutingEnd);
			customMapMC.pinmc.visible = false;
			customMapMC.unHilightAll();
		}
		
		public function showFloorplanMcSearch():void
		{
			//doneShowRoute();
			TweenMax.to(customMapMC,0.7,{scaleX:1.1,scaleY:1.1,x:820,y:110});
			TweenMax.to(customMapMC,1,{autoAlpha:1});
			_floorDes.visible = false;
			_logo.visible = false;
			nextBTN.visible = false;
			prevBTN.visible = false;
			skipButton.visible = false;
			shopName.visible = true;
			
			trace (visitfloor);
			
			if(visitfloor == 'L') visitFloor = '0';
			if(visitfloor == 'G') visitFloor = '1';
			if(visitfloor == '1') visitFloor = '2';
			if(visitfloor == '2') visitFloor = '3';
			if(visitfloor == '3') visitFloor = '4';
			
			customMapMC.addEventListener('beforeRouting', onRouting);
			customMapMC.showRoute(youarehereFloor,youarehereNode,visitFloor, theNode);
			//customMapMC.showFloorByKey(visitFloor, '');
			customMapMC.addEventListener('endRouting', onRoutingEnd);
			customMapMC.pinmc.visible = false;
			customMapMC.unHilightAll();
			
			trace (vm.visible);
			
		}
		public function onRoutingEnd(e:Event):void
		{
			var flstr:String = visitFloor + '_' + sendUnit;
			customMapMC.addBuble(visitFloor,sendUnit);
			customMapMC.hilightMC(visitFloor,sendUnit);
			new delayedFunctionCall(doneShowRoute, 2000);
		}
		public function onRouting(e:Event):void
		{
			shopName.visible = false;
		}
		
		public function hideFloorplanMc():void
		{
			TweenMax.to(customMapMC,1,{autoAlpha:0});
			_floorDes.visible = true;
			_logo.visible = true;
			nextBTN.visible = true;
			prevBTN.visible = true;
			skipButton.visible = false;
			shopName.visible = false;
			
		}
		
		public function setupShopName(unitName:String):void
		{
			
			for(var i:int = 0; i<shopXMLList.length(); i++)
			{
				if(unitName == shopXMLList[i].unit_key.text())
				{
					shopName._shopname.text = shopXMLList[i].name_en.text();
				}
			}
		}

		public function moveFloorplan(floorIndex:int):void
		{
			uiCarousel.go(floorIndex);

		}
		private function carouselDidMove(e:UICarouselEvent):void
		{
			dispatchEvent(new Event('floorplanMoved'));

		}
		private function carouselWillMove(e:UICarouselEvent):void
		{


			currentIndex = e.toIndex;
			dispatchEvent(new Event('floorplanMoving'));
			TweenMax.to(_floorDes._floorname,0.2,{alpha:0});
			for (var i:int = 0; i < shopMcArr.length; i++)
			{
				(shopMcArr[i] as MovieClip).alpha = 0;
			}

			for (var i:int = 0; i<floorplanArr.length; i++)
			{
				if ((floorplanArr[i] as MovieClip).scaleX == 1.1)
				{
					uiCarousel.x = 280;
					uiCarousel.y = 90;
					toScale = 0.9;

					if (e.toIndex == 0)
					{
						prevBTN._flNamae.text = '';
						nextBTN._flNamae.text = 'Ground Floor';
					
						currentFloorplan = (floorplanArr[0] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:1.1,scaleY:1.1, alpha:1, x:-150,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);


						nextFloorplan = (floorplanArr[1] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:500,y:90});

						TweenMax.to(_floorDes._floorname,0.8,{alpha:1});
						_floorDes._floorname.text = 'Lobby floor';

					}
					if (e.toIndex == 1)
					{
						prevBTN._flNamae.text = 'Lobby Floor';
						nextBTN._flNamae.text = '1st Floor';
						
						currentFloorplan = (floorplanArr[1] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:1.1,scaleY:1.1, alpha:1, x:1336.5,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[0] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:1130,y:90});

						nextFloorplan = (floorplanArr[2] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:1980,y:90});

						TweenMax.to(_floorDes._floorname,0.8,{alpha:1});
						_floorDes._floorname.text = 'Ground floor';


					}
					if (e.toIndex == 2)
					{
						prevBTN._flNamae.text = 'Ground Floor';
						nextBTN._flNamae.text = '2nd Floor';
						
						currentFloorplan = (floorplanArr[2] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:1.1,scaleY:1.1, alpha:1, x:2814,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[1] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:2620,y:90});

						nextFloorplan = (floorplanArr[3] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:3460,y:90});

						TweenMax.to(_floorDes._floorname,0.8,{alpha:1});
						_floorDes._floorname.text = '1st floor';

					}
					if (e.toIndex == 3)
					{
						prevBTN._flNamae.text = '1st Floor';
						nextBTN._flNamae.text = '3rd Floor';
						
						currentFloorplan = (floorplanArr[3] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:1.1,scaleY:1.1, alpha:1, x:4296,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[2] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:4100,y:90});

						nextFloorplan = (floorplanArr[4] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:4940,y:90});

						TweenMax.to(_floorDes._floorname,0.8,{alpha:1});
						_floorDes._floorname.text = '2nd floor';

					}
					if (e.toIndex == 4)
					{
						prevBTN._flNamae.text = '2nd Floor';
						nextBTN._flNamae.text = '';
						
						currentFloorplan = (floorplanArr[4] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:1.1,scaleY:1.1, alpha:1, x:5777,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[3] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.8,scaleY:0.8, alpha:0.3, x:5550,y:90});

						TweenMax.to(_floorDes._floorname,0.8,{alpha:1});
						_floorDes._floorname.text = '3rd floor';


					}
				}
				if ((floorplanArr[i] as MovieClip).scaleX == 0.9)
				{
					uiCarousel.x = 280;
					uiCarousel.y = 90;
					toScale = 0.6;
					if (e.toIndex == 0)
					{
						prevBTN._flNamae.text = '';
						nextBTN._flNamae.text = 'Ground Floor';
						
						currentFloorplan = (floorplanArr[0] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:0.9,scaleY:0.9, alpha:1, x:3,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);


						nextFloorplan = (floorplanArr[1] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:600,y:120});

					}
					if (e.toIndex == 1)
					{
						prevBTN._flNamae.text = 'Lobby Floor';
						nextBTN._flNamae.text = '1st Floor';
						
						currentFloorplan = (floorplanArr[1] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:0.9,scaleY:0.9, alpha:1, x:1483.5,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[0] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:1300,y:120});

						nextFloorplan = (floorplanArr[2] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:2100,y:120});

					}
					if (e.toIndex == 2)
					{
						prevBTN._flNamae.text = 'Ground Floor';
						nextBTN._flNamae.text = '2nd Floor';
						
						currentFloorplan = (floorplanArr[2] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:0.9,scaleY:0.9, alpha:1, x:2961,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[1] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:2800,y:120});

						nextFloorplan = (floorplanArr[3] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:3600,y:120});

					}
					if (e.toIndex == 3)
					{
						prevBTN._flNamae.text = '1st Floor';
						nextBTN._flNamae.text = '3rd Floor';
						
						currentFloorplan = (floorplanArr[3] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:0.9,scaleY:0.9, alpha:1, x:4443.5,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[2] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:4250,y:120});

						nextFloorplan = (floorplanArr[4] as MovieClip);
						TweenMax.to(nextFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:5070,y:120});
					}
					if (e.toIndex == 4)
					{
						prevBTN._flNamae.text = '2nd Floor';
						nextBTN._flNamae.text = '';
						
						currentFloorplan = (floorplanArr[4] as MovieClip);
						TweenMax.to(currentFloorplan,0.5,{scaleX:0.9,scaleY:0.9, alpha:1, x:5924,y:0});
						currentFloorplan.parent.setChildIndex(currentFloorplan, currentFloorplan.parent.numChildren-1);

						prevFloorplan = (floorplanArr[3] as MovieClip);
						TweenMax.to(prevFloorplan,0.5,{scaleX:0.6,scaleY:0.6, alpha:0.3, x:5770,y:120});

					}
				}
			}

			customMapMC.showFloorByKey(youarehereFloor, '');

			

		}

		private function carouselDraggingBegin(e:UICarouselEvent):void
		{
			this.touchdownContentX = uiCarousel.content.x;

			uiCarousel.addEventListener('carouselIsDragging', carouselIsDragging);
		}

		public function carouselIsDragging(e:UICarouselEvent = null):void
		{
			var newScale:Number = toScale;
			var cx:Number = uiCarousel.content.x;
			var diff:Number = cx - this.touchdownContentX;
			//trace("diff", diff);
			if (getPrevFloor() != null)
			{
				this.getPrevFloor().scaleX = this.getPrevFloor().scaleY = ( 1 / 2960 ) * diff + newScale;
				//trace("prevFloor", getPrevFloor().scaleX);
			}

			if (getNextFloor() != null)
			{
				this.getNextFloor().scaleX = this.getNextFloor().scaleY = ( -1 / 2960 ) * diff + newScale;
				//trace("nextFloor", getNextFloor().scaleX);
			}

		}
		public function getCurrentFloor():MovieClip
		{
			return floorplanArr[this.currentFloorIndex];
		}
		public function getPrevFloor():MovieClip
		{
			if (this.currentFloorIndex - 1 < 0)
			{
				return null;
			}
			else
			{
				return floorplanArr[this.currentFloorIndex - 1];
			}
		}

		public function getNextFloor():MovieClip
		{
			if (this.currentFloorIndex + 1 > floorplanArr.length - 1)
			{
				return null;
			}
			else
			{
				return floorplanArr[this.currentFloorIndex + 1];
			}
		}

		public function get currentFloorIndex():int
		{
			return currentIndex;
		}

		public function get toFloorIndex():int
		{
			return toIndex;
		}

		public function scaleZoomInCarouselFloorplan():void
		{
			for (var i:int = 0; i < floorplanArr.length; i++)
			{
				if ((floorplanArr[i] as MovieClip).scaleX == 1.1)
				{
					(floorplanArr[i] as MovieClip).scaleX = (floorplanArr[i] as MovieClip).scaleY = 0.9;
					uiCarousel.go(currentIndex);
				}

			}
		}
		public function scaleZoomOutCarouselFloorplan():void
		{
			for (var i:int = 0; i < floorplanArr.length; i++)
			{
				if ((floorplanArr[i] as MovieClip).scaleX == 0.9)
				{
					(floorplanArr[i] as MovieClip).scaleX = (floorplanArr[i] as MovieClip).scaleY = 1.1;
					uiCarousel.go(currentIndex);
				}

			}
		}

		override public function viewWillAppear():void
		{
			addFloorplanCarousel();
			//addCustomMapMC();
			
			_floorDes.visible = true;
			
			//carouselIsDragging();
		}


	}

}