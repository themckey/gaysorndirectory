﻿package viewcontroller
{
	import com.forviz.ViewController;
	import com.forviz.ui.UITextField;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import flash.text.TextFormat;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import com.forviz.ui.UIControlEvent;
	import models.ShopModel;
	import flash.display.Sprite;
	import main.alphabet.ShopItemWithLogo;
	import main.search.ShopWithLogoInSearch;

	import com.greensock.TweenMax;
	import main.BlurContentTourist;
	import main.search.FacilitiesSearchHeader;
	import main.search.BrandsSearchHeader;
	import models.FacilityModel;
	import com.pvm.ui.UIScrollView;
	import com.pvm.ui.CGSize;
	import flash.geom.Point;
	import events.ShopEvent;


	public class SearchViewcontroller extends ViewController
	{

		public var search_TF:UITextField;
		var textFormat:Dictionary = new Dictionary();
		var searchFieldOptions:Object;
		var shopModelArr:Array = new Array();
		var shopName:String;
		var shopNameArr:Array = new Array();
		var shopDict:Dictionary = new Dictionary();
		
		

		var facilityModel:Array = new Array();
		var facilityDict:Dictionary = new Dictionary();
		var facilityID:String;

		var countFound:int = 0;
		var countfacilityFound:int = 0;

		var shopItemHolder:Sprite = new Sprite();
		var brandHeader:BrandsSearchHeader = new BrandsSearchHeader();
		var shopItemHolderWidth:Number;

		var facilityItemHolder:Sprite = new Sprite();
		var facilityItemHolderWidth:Number;

		var facilitiesHeader:FacilitiesSearchHeader = new FacilitiesSearchHeader();
		var shopSecondaryItemHolder:Sprite = new Sprite();

		var shopItemWithLogo:ShopWithLogoInSearch;
		var shopID:String;
		var myTimeForTween:Number = Math.random() * 0.5;
		var tweenItemArr:Array = new Array  ;
		var allWidth:Number;

		var blurSideOfContent:BlurContentTourist = new BlurContentTourist();

		var uiScroll:UIScrollView = new UIScrollView(new Rectangle(0,0,1920,1080));
		var tapShopSearchEvent:ShopEvent = new ShopEvent('shopchoose');

		public function SearchViewcontroller()
		{
			// constructor code

			searchFieldOptions = {
			'placeholder' : {
			'text' : {
			'en' : 'Search Brands, Facilities and etc.'
			
			},
			'textFormat' : new TextFormat('AccentGraphic', 24, 0xb2b2b2, false, null, null, null, null, 'center')
			},
			'input' : {
			'textFormat' : new TextFormat('AccentGraphic', 24, 0x464646, false, null, null, null, null, 'left', 50)
			}
			
			};

			search_TF = new UITextField(new Rectangle(719.2,110,482,60),searchFieldOptions);
			addChild(search_TF);
			search_TF.addEventListener('valueChanged', onValueChanged);

			addChild(uiScroll);

			shopItemHolder.y = 400;
			uiScroll.addChild(shopItemHolder);
			shopItemHolder.addChild(brandHeader);
			
			
			facilityItemHolder.y = 400;
			uiScroll.addChild(facilityItemHolder);
			facilityItemHolder.addChild(facilitiesHeader);


			GaysornDataManager.getInstance().addEventListener('dataInitted', initData);

			blurSideOfContent.x = 1770;
			addChild(blurSideOfContent);




		}
		private function initData(e:Event = null):void
		{
			shopModelArr = GaysornDataManager.getInstance().shopArr;
			var shopId:String;
			for (var i:int = 0; i<shopModelArr.length; i++)
			{
				shopId = (shopModelArr[i] as ShopModel).id;
				shopItemWithLogo = new ShopWithLogoInSearch();
				shopItemWithLogo.x = 120 + (i * 160);
				shopItemWithLogo.y = 20;
				shopItemHolder.addChild(shopItemWithLogo);
				shopItemWithLogo.model = shopModelArr[i];
				shopItemWithLogo.name = (shopModelArr[i] as ShopModel).id;
				shopItemWithLogo.addEventListener('tap', onTap);
				shopDict[shopId] = shopItemWithLogo;
			}

			facilityModel = GaysornDataManager.getInstance().facilityArr;
			var facilityId:String;
			for (var i:int = 0; i<facilityModel.length; i++)
			{
				facilityId = (facilityModel[i] as FacilityModel).id;
				shopItemWithLogo = new ShopWithLogoInSearch();
				shopItemWithLogo.x = 120 + (i * 160);
				shopItemWithLogo.y = 20;
				facilityItemHolder.addChild(shopItemWithLogo);
				shopItemWithLogo.modelfacility = facilityModel[i];
				facilityDict[facilityId] = shopItemWithLogo;
			}

		}
		public function onTap(e:UIControlEvent):void
		{
			tapShopSearchEvent.obj = e.from;
			dispatchEvent(tapShopSearchEvent);
			
		}
		public function onValueChanged(e:UIControlEvent):void
		{
			if (search_TF.value != '')
			{

				showSearchResult(search_TF.value);
			}
			else
			{
				clearAllSearchResult();
			}
		}
		
		private function clearAllSearchResult():void
		{
			this.typetosearch.text = 'Please type what are you looking for...';

			shopItemHolder.visible = false;
			shopSecondaryItemHolder.visible = false;
			
			for each (var item:ShopWithLogoInSearch in facilityDict)
			{
				item.visible = false;
			}
			facilityItemHolder.visible = false;
			for each (var item:ShopWithLogoInSearch in shopDict)
			{
				item.visible = false;
			}
			
			

		}

		private function showSearchResult(searchStr:String):void
		{
			clearAllSearchResult();

			var shopNameStr:String;
			countFound = 0;
			
			
			for (var i:int = 0; i < shopModelArr.length; i++)
			{
				shopNameStr = (shopModelArr[i] as ShopModel).nameEn.toString().toLowerCase();
				shopID = (shopModelArr[i] as ShopModel).id;
				
				if (shopNameStr.indexOf(searchStr.toLowerCase()) == 0)
				{
					shopDict[shopID].x = 160 + (countFound * 140);
					shopDict[shopID].y = 0;
					shopDict[shopID].visible = true;
					shopItemHolderWidth = 320 + (countFound * 140);
					countFound++;
				}
				
				
			}

			var facilityStr:String;
			countfacilityFound = 0;

			for (var i:int = 0; i<facilityModel.length; i++)
			{
				facilityStr = (facilityModel[i] as FacilityModel).nameEn.toString().toLowerCase();
				facilityID = (facilityModel[i] as FacilityModel).id;

				if (facilityStr.indexOf(searchStr.toLowerCase()) == 0)
				{
					facilityDict[facilityID].x = 160 + (countfacilityFound * 140);
					facilityDict[facilityID].y = 0;
					facilityDict[facilityID].visible = true;
					facilityItemHolderWidth = 320 + (countfacilityFound * 140);
					countfacilityFound++;
				}
			}


			if(searchStr.length > 1)
			{
				setDisplayAndScrollPrimary(searchStr);
			}
			
			
		}

		private function setDisplayAndScrollPrimary(str:String):void
		{
			uiScroll.moveTo(new Point(0,0));
			
			if(countfacilityFound == 0)
			{
				if(countFound != 0)
				{
					shopItemHolder.x = 50;
					shopItemHolder.visible = true;
					this.typetosearch.text = "Found "+(countFound)+" "+ (((countFound) > 1) ? "results" : "result") +" of " + "“"+str+"”";
					uiScroll.contentSize = new CGSize(shopItemHolderWidth + 300,1080);
				}
				else
				{
					this.typetosearch.text = 'Result not found?';
				}
			}
			else
			{
				facilityItemHolder.x = 50;
				facilityItemHolder.visible = true;
				if(countFound != 0)
				{
					shopItemHolder.x = facilityItemHolderWidth + 160;
					shopItemHolder.visible = true;
					this.typetosearch.text = "Found "+(countFound + countfacilityFound)+" "+ (((countFound + countfacilityFound) > 1) ? "results" : "result") +" of " + "“"+str+"”";
					uiScroll.contentSize = new CGSize(Number(facilityItemHolderWidth + shopItemHolderWidth) + 300,1080);
					
				}
				else
				{
					uiScroll.contentSize = new CGSize(facilityItemHolderWidth + 300,1080);
				}
			}
			
		}
		
		override public function viewWillAppear():void
		{
			clearAllSearchResult();
		}

	}
}