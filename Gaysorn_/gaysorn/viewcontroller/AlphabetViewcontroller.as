﻿package viewcontroller
{
	import com.forviz.ViewController;
	import com.forviz.ui.UICarousel;
	import manager.GaysornDataManager;
	import flash.events.Event;
	import main.alphabet.ShopItemWithLogo;
	import models.ShopModel;
	import flash.display.Sprite;
	import main.alphabet.IndexAlphabetView;
	import com.pvm.ui.UIScrollView;
	import flash.geom.Rectangle;
	import com.pvm.ui.CGSize;
	import main.BlurContentTourist;
	import flash.display.MovieClip;
	import main.alphabet.AlphabetButton;
	import com.forviz.ui.UIControlEvent;
	import flash.geom.Point;
	import main.ShopItem;
	import events.ShopEvent;

	public class AlphabetViewcontroller extends ViewController
	{

		private var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();

		public var itemHolderScr:UIScrollView = new UIScrollView(new Rectangle(0,0,1920,854));
		public var alphabetArr:Array = new Array();
		private var shopName:String;

		public var itemHolder:Sprite = new Sprite();
		var itemHolderArr:Array = new Array();

		private var alphabetIndex:IndexAlphabetView;
		private var alphabetIndexLastY:Number;
		private var alphabetIndexLastX:Number;


		var gap:Number = 35;
		var count:Number = 0;
		var lastX:Number = 0;
		var lastY:Number = 0;
		var allItemWidth:Number = 0;
		var numObjects:int = 0;
		var numCols:int = 0;
		var column:int = 0;
		var row:int = 3;

		var shopItemLogo:ShopItemWithLogo;

		var blurContent:BlurContentTourist = new BlurContentTourist();

		private var _buttonAlphabetholder:MovieClip;
		
		var countAll:int = 0;
		var countHeadPlusItem:int = 0;
		var tapShopBrandEvent:ShopEvent = new ShopEvent('shopchoose');

		public function AlphabetViewcontroller()
		{
			_buttonAlphabetholder = this.getChildByName('_buttonHolder');

			addChild(itemHolderScr);
			blurContent.x = 1770;
			blurContent.height = 854;
			addChild(blurContent);
			GaysornDataManager.getInstance().addEventListener('dataInitted', onDataInnited);
		}
		private function onDataInnited(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			loadShopLogo();
			

			for (var i:int = 0; i<_buttonAlphabetholder.numChildren; i++)
			{
				if (_buttonAlphabetholder.getChildAt(i) is MovieClip)
				{
					_buttonAlphabetholder.getChildAt(i).addEventListener('tap', onTapButtonAlphabet);
				}

			}
		}

		private function onTapButtonAlphabet(e:UIControlEvent):void
		{
			moveScrollToIndex(e.from.name);
		}

		private function loadShopLogo():void
		{
			for (var i:int = 0; i<shopModelArray.length; i++)
			{
				shopName = (shopModelArray[i] as ShopModel).nameEn.toUpperCase().substr(0,1);
				alphabetArr.push(shopName);

			}
			alphabetArr.sort();

			var i:int = 0;
			while (i < alphabetArr.length)
			{
				while (i < alphabetArr.length+1 && alphabetArr[i] == alphabetArr[i+1])
				{
					alphabetArr.splice(i, 1);

				}
				i++;

			}
			
			for (var n:int = 0; n<alphabetArr.length; n++)
			{
				addItemHolder(alphabetArr[n].toString().toUpperCase());

			}
			
			arrangeItemToGrid();
			
			
		}
		private function setScroll():void
		{
			itemHolderScr.contentSize = new CGSize(lastX + 250, 854);
		}

		private function addItemHolder(_firstChar:String):void
		{
					
			itemHolderScr.addChild(itemHolder);
			
			alphabetIndex = new IndexAlphabetView();
			alphabetIndex.x = 40;
			alphabetIndex.y = 23 + lastY;
			itemHolder.addChild(alphabetIndex);
			alphabetIndex._Label.text = _firstChar;
			alphabetIndex.name = _firstChar;
			
			alphabetIndexLastY = (alphabetIndex.y + 249.1) + gap;
			alphabetIndexLastX = (alphabetIndex.x + alphabetIndex.width) + gap;
			
			
			var shopFirstCharInModel:String;
			itemHolderArr.push(alphabetIndex);

			for (var i:int; i < shopModelArray.length; i++)
			{
				shopFirstCharInModel = (shopModelArray[i] as ShopModel).nameEn.toUpperCase().substr(0,1);
				shopName = (shopModelArray[i] as ShopModel).nameEn;

				if (_firstChar == shopFirstCharInModel)
				{

					shopItemLogo = new ShopItemWithLogo();
					shopItemLogo.x = 40;
					shopItemLogo.y = alphabetIndexLastY;
					itemHolder.addChild(shopItemLogo);
					shopItemLogo.model = shopModelArray[i];
					lastY = alphabetIndexLastY + (shopItemLogo.height);
					shopItemLogo.name = (shopModelArray[i] as ShopModel).id;
					shopItemLogo.addEventListener('tap', onTap);
					
					itemHolderArr.push(shopItemLogo);
					
				}

			}
			
		}
		public function onTap(e:UIControlEvent):void
		{
			
			tapShopBrandEvent.obj = e.from;
			dispatchEvent(tapShopBrandEvent);
			
		}
		
		private function arrangeItemToGrid():void
		{
			
			for(var i:int = 0; i < itemHolderArr.length; i++)
			{
				itemHolderArr[i].y = 23 + ((249.1 + gap) * (i%3));
				itemHolderArr[i].x = 40 +  Math.floor(i/3) * 256;
			
			}
			
			lastX = itemHolder.width + 1500;
			lastY = itemHolder.height;
			setScroll();
			
		}

		private function moveScrollToIndex(buttonName:String):void
		{
			var moveX:Number = 0;
			for (var i:int; i < itemHolderArr.length; i++)
			{
				shopFirstCharInModel = (itemHolderArr[i].name);
				
				
				if (buttonName == 'All')
				{
					itemHolderScr.moveTo(new Point(0,0));
				}
				else if (buttonName == shopFirstCharInModel)
				{
					 moveX = -40 + (itemHolderArr[i].x);
					itemHolderScr.moveTo(new Point(-moveX,0));
				}
			}
			
		}
		
		override public function viewWillAppear():void
		{
			itemHolderScr.moveTo(new Point(0,0));
		}



	}

}