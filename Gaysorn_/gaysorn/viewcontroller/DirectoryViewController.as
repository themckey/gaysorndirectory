﻿package viewcontroller
{
	import com.forviz.ViewController;
	import items.floorplan.FloorplanCoverflow;
	import flash.events.Event;
	import manager.GaysornDataManager;
	import maps.CustomMapsMC;
	import items.NextBTN;
	import items.PrevBTN;
	import com.forviz.ui.UIControlEvent;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import com.greensock.TweenMax;
	import items.ShowShopnameMC;
	import flash.events.MouseEvent;
	import com.greensock.easing.Expo;
	import items.SkipButton;
	import com.forviz.ui.UIControl;
	import com.jmx2.delayedFunctionCall;
	import models.ShopModel;
	import items.BackBTN;
	import events.FacilityEvent;

	public class DirectoryViewController extends ViewController
	{
		public var floorOptionCoverFlow:FloorplanCoverflow = new FloorplanCoverflow();
		
		var mapMC:CustomMapsMC = new CustomMapsMC();
		var animationFloorplanMC:MovieClip = new MovieClip();
		var animationLoader:Loader = new Loader();
		var extSwf:MovieClip;
		
		var nextBTN:NextBTN = new NextBTN();
		var prevBTN:PrevBTN = new PrevBTN();
		var skipButton:SkipButton = new SkipButton();
		
		var showShopName:ShowShopnameMC = new ShowShopnameMC();
		
		
		var shopXMLList:XMLList = new XMLList();
		var shopModelArray:Array = new Array();

		var visitfloor:String;
		var visitnode:String;
		var youarehereFloor:int = 0;
		var youarehereNode:String;
		
		var visitfloorNumBer:String;
		
		var shopDetail:ShopDeatailViewController = new ShopDeatailViewController();
		
		var liftbtn:MovieClip = new MovieClip();
		var backbtn:BackBTN = new BackBTN();
		
		public function DirectoryViewController()
		{
			animationFloorplanMC.y = 0;
			addChild(animationFloorplanMC);
			animationFloorplanMC.addChild(animationLoader);
			animationLoader.load(new URLRequest('content/GoUp.swf'));
			animationLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, animationLoaded);
			
			this.addEventListener(MouseEvent.CLICK, onClickMainDirectView);
			
			GaysornDataManager.getInstance().addEventListener('dataInitted', onInitData);
		}
		private function onInitData(e:Event = null):void
		{
			shopXMLList = GaysornDataManager.getInstance().shopXMLList;
			youarehereNode = GaysornDataManager.getInstance().configsXML.start_node.text();
			youarehereFloor = GaysornDataManager.getInstance().configsXML.kiosk_floor.text();
			shopModelArray = GaysornDataManager.getInstance().shopArr;
			
			this.floorProperty.addEventListener('chosefacility', facilityChoose);
			
			
			


		}
		public function setDefaultDirectoryViewController():void
		{
			addChild(floorOptionCoverFlow);
			floorOptionCoverFlow.addEventListener('ClickMoreInfo', getShopInfo);
			floorOptionCoverFlow.addEventListener('setFirstFloorDefault', checkIndex);
			this.name = 'floorOptionCoverFlow';
			
			
			nextBTN.x = 1770;
			nextBTN.y = 550;
			addChild(nextBTN);
			nextBTN.addEventListener('tap', onTapNextBTN);
			
			prevBTN.x = 30;
			prevBTN.y = 550;
			addChild(prevBTN);
			prevBTN.addEventListener('tap', onTapPrevBTN);
			
			mapMC.x = 585;
			mapMC.y = 150;
			addChild(mapMC);
			mapMC.visible = false;
			//mapMC.addEventListener('endRouting', onRoutingEnd);
			mapMC.addEventListener('beforeRouting', beforeRouting);
			
			
			addChild(shopDetail);
			TweenMax.to(shopDetail,0.5,{autoAlpha:0});	
			TweenMax.to(shopDetail.floorselection,0.1,{x:1950,autoAlpha:0});
			
			backbtn = shopDetail.getChildByName('_back');
			shopDetail.addChild(backbtn);
			backbtn.addEventListener('tap', onTapBackBTNHandler);
			
			
			
		}
		
		
		public function onClickMainDirectView (e:Event):void
		{
			if(e.target.name == 'floorOptionCoverFlow')floorOptionCoverFlow.hideAllShop();
			if(e.target.name.substr(0,4) == 'inst')floorOptionCoverFlow.hideAllShop();
			
		}
		public function scaleFloorOptionCoverFlow(_scale:String):void
		{
			if(_scale == 'in')
			{
				showfloorOptionCoverFlowProperty();
				TweenMax.to(prevBTN, 1, {y:550, ease:Expo.easeOut});
				TweenMax.to(nextBTN, 1, {y:550, ease:Expo.easeOut});
				TweenMax.to(floorOptionCoverFlow, 1, {autoAlpha:1, x:0, y:0, scaleX:1,scaleY:1, ease:Expo.easeOut});
			}
			if(_scale == 'out')
			{
				floorProperty.visible = false;
				gaysornLogo.visible = false;
				TweenMax.to(prevBTN, 1, {y:400, ease:Expo.easeOut});
				TweenMax.to(nextBTN, 1, {y:400, ease:Expo.easeOut});
				TweenMax.to(floorOptionCoverFlow, 1, {autoAlpha:1, x:140 ,y:0, scaleX:0.857, scaleY:0.857, ease:Expo.easeOut});
				
			}
			
		}
		public function animationLoaded (e:Event):void
		{
			extSwf = animationLoader.content as MovieClip;
			addChild(extSwf);
			extSwf.y = -20;
			extSwf.gotoAndStop(0);
			TweenMax.to(extSwf, 0.5, {autoAlpha:0});
			addChild(showShopName);
			showShopName.visible = false;
			
			skipButton.x = 1560;
			skipButton.y = 850;
			addChild(skipButton);
			skipButton.addEventListener('tap', onTapSkipHandler);
			skipButton.visible = false;
			
			//addChild(shopDetail);
			
			
			
		}
		
		public function clearStage():void
		{
			
			setDefaultDirectoryViewController();
			floorOptionCoverFlow.hideAllShop();
			trace ('clearStage');
			if (extSwf) extSwf.visible = false;
			//extSwf.gotoAndStop(0);
			//TweenMax.to(extSwf, 0.5, {autoAlpha:0});
			showShopName.visible = false;
			
			mapMC.clearPath()
			mapMC.visible = false;
			shopDetail.visible = false;
			shopDetail.floorselection.visible = false;
			this.skipButton.visible = false;
			//TweenMax.to(shopDetail,0.5,{autoAlpha:0});	
			//TweenMax.to(shopDetail.floorselection,0.1,{x:1950,autoAlpha:0});
			
			showfloorOptionCoverFlowProperty();
			
			
		}
		public var isDone:Boolean = true;
		
		private function onTapSkipHandler(e:UIControlEvent):void
		{
			isDone = true;
			doneShowRoute();
		}
		private function onTapBackBTNHandler (e:UIControlEvent):void
		{
			//viewWillAppear();
			clearStage();
			mapMC.clearPath();
			scaleFloorOptionCoverFlow('in');
			mapMC.unHilightAll();
			isDone = false;
			
		}
		
		public function doneShowRoute():void
		{
			trace ('dadad',isDone);
			if(isDone == false)
			{
				clearStage();
			}
			else
			{
				_onShowShopDetail();
				//mapMC.clearPath();
				if(visitfloorNumBer == '1')
				{
					mapMC.showFloorByKey('0', '');
					mapMC.showRoute('0','19','0',visitnode);
				}
				if(visitfloorNumBer == '3')
				{
					mapMC.showFloorByKey('2', '');
					mapMC.showRoute('2','10','2',visitnode);
				}
				if(visitfloorNumBer == '4')
				{
					mapMC.showFloorByKey('3', '');
					mapMC.showRoute('3','10','3',visitnode);
				}
				if(visitfloorNumBer == '5')
				{
					mapMC.showFloorByKey('4', '');
					mapMC.showRoute('4','10','4',visitnode);
				}
			}
			
			
			//mapMC.showFloorByKey(visitfloorNumBer, '');
			

		}
		private function _onShowShopDetail():void
		{
			// hide extSwf(animetion changeFloor)
			if(isDone == false)
			{
				clearStage();
			}
			else
			{
				showShopBubble();
				
				if (extSwf) TweenMax.to(extSwf, 0.2, {autoAlpha:0});
				TweenMax.to(mapMC,0.75,{scaleX:0.7,scaleY:0.7,x:930,y:85, autoAlpha:1});
				
				
				// open shopDetail
				TweenMax.to(shopDetail,0.5,{autoAlpha:1});	
				TweenMax.to(shopDetail.floorselection,0.4,{x:1677.35,autoAlpha:1, delay:0.9});	
				
				showShopName.visible = false;
				skipButton.visible = false;
				
				for(var i:int = 0; i<shopModelArray.length; i++)
				{
					if((shopModelArray[i] as ShopModel).unitKey == this.floorOptionCoverFlow.sendUnit)
					{
						shopDetail.model = shopModelArray[i];
					}
				}
			}
			
		}
		public function onTapPrevBTN (e:UIControlEvent):void
		{
			floorOptionCoverFlow.goPrev();
			checkIndex(floorOptionCoverFlow._currentFloorIndex);
		}
		public function onTapNextBTN (e:UIControlEvent):void
		{
			floorOptionCoverFlow.goNext();
			checkIndex(floorOptionCoverFlow._currentFloorIndex);
		}
		private function checkIndex(currentIndex:int):void
		{
			setButtonName(floorOptionCoverFlow._currentFloorIndex);
		}
		
		public function getShopInfo(e:Event = null):void
		{
			isDone = true;
			mapMC.visible = false;
			getShopInfoFromShopCategory(this.floorOptionCoverFlow.sendUnit);
		}
		public function getShopInfoFromShopCategory(shopName:String):void
		{
			trace('getShopInfo',shopName);
			
			for (var i:int = 0; i<shopXMLList.length(); i++)
			{
				if (this.floorOptionCoverFlow.sendUnit == shopXMLList[i].unit_key.text())
				{
					trace('== shopXMLList[i].unit_key');
					showShopName._shopname.text = 'Show Route to '+ shopXMLList[i].name_en.text();
					visitfloor = shopXMLList[i].floor.text();
					visitnode = shopXMLList[i].node.text();
				}
				else if(shopName == shopXMLList[i].attribute('id'))
				{
					trace('== shopXMLList[i].id');
					showShopName._shopname.text = 'Show Route to '+ shopXMLList[i].name_en.text();
					visitfloor = shopXMLList[i].floor.text();
					visitnode = shopXMLList[i].node.text();
					this.floorOptionCoverFlow.sendUnit = shopXMLList[i].unit_key.text()
				
				}
			}
			
			showAnimationChangeFloor(visitfloor);
			
		}
		private function showAnimationChangeFloor(_visitfloor:String):void
		{
			dispatchEvent(new Event('showRouting'));
			isDone = true;
			
			if(_visitfloor == 'L')visitfloorNumBer = '1';
			if(_visitfloor == 'G')visitfloorNumBer = '2';
			if(_visitfloor == '1')visitfloorNumBer = '3';
			if(_visitfloor == '2')visitfloorNumBer = '4';
			if(_visitfloor == '3')visitfloorNumBer = '5';
			
			if(visitfloorNumBer == youarehereFloor) showDirectionSameFloor();
			if(visitfloorNumBer != youarehereFloor) showDirectionDifferenceFloor();
			
			hidefloorOptionCoverFlowProperty();
			showShopName.visible = true;
			skipButton.visible = true;
			
		}
		public function showDirectionSameFloor():void
		{
			
			floorOptionCoverFlow.visible = false;
			if (extSwf) TweenMax.to(extSwf, 0.5, {autoAlpha:0});
			mapMC.visible = true;
			mapMC.showFloorByKey('1', '');
			mapMC.showRoute('1',youarehereNode,'1',visitnode);
			TweenMax.to(mapMC,0.7,{autoAlpha:1,scaleX:1,scaleY:1,x:595,y:50, ease:Expo.easeOut});
			new delayedFunctionCall(_onShowShopDetail, 5000);
			
			
		}
		
		public function beforeRouting(e:Event):void
		{
			
		}
		
		public function showDirectionDifferenceFloor():void
		{
			trace('showDirectionDifferenceFloor');
			floorOptionCoverFlow.visible = false;
			
			if (extSwf) {
				TweenMax.to(extSwf, 0.5, {autoAlpha:1});
				extSwf.gotoAndPlay(0);
				extSwf.addEventListener(
				Event.ENTER_FRAME,
				function(e:Event):void
				{
					if(visitfloorNumBer == '1')
					{
						if(extSwf.currentFrame == 0)
						{
							extSwf.stop();
							extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
							mapMC.visible = true;
							//mapMC.showRoute('0','16','0',visitnode);
							TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8, onComplete:doneShowRoute});
							
							
						}
					}
					if(visitfloorNumBer == '3')
					{
						if(extSwf.currentFrame == 246)
						{
							extSwf.stop();
							extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
							mapMC.visible = true;
							//mapMC.showRoute('2','16','2',visitnode);
							TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8, onComplete:doneShowRoute});
							
						}
					}
					if(visitfloorNumBer == '4')
					{
						if(extSwf.currentFrame == 556)
						{
							extSwf.stop();
							extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
							mapMC.visible = true;
							//mapMC.showRoute('2','16','2',visitnode);
							TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8, onComplete:doneShowRoute});
							
							
						}
					}
					
					if(visitfloorNumBer == '5')
					{
						if(extSwf.currentFrame == 746)
						{
							extSwf.stop();
							extSwf.removeEventListener(Event.ENTER_FRAME, arguments.callee);
							mapMC.visible = true;
							//mapMC.showRoute('2','16','2',visitnode);
							TweenMax.to(extSwf, 0.7, {autoAlpha:0, delay:0.8, onComplete:doneShowRoute});
							
						}
					}
					
					
				});
				
			}// If 
			
			
		
		}// Function 
		
		
		private function showShopBubble():void
		{
			if(visitfloorNumBer == '1') mapMC.addBuble(0, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '2') mapMC.addBuble(1, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '3') mapMC.addBuble(2, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '4') mapMC.addBuble(3, this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '5') mapMC.addBuble(4, this.floorOptionCoverFlow.sendUnit);
			
			if(visitfloorNumBer == '1') mapMC.hilightMC(0,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '2') mapMC.hilightMC(1,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '3') mapMC.hilightMC(2,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '4') mapMC.hilightMC(3,this.floorOptionCoverFlow.sendUnit);
			if(visitfloorNumBer == '5') mapMC.hilightMC(4,this.floorOptionCoverFlow.sendUnit);
			
			
		}
		
		
		private function hidefloorOptionCoverFlowProperty():void
		{
			prevBTN.visible = false;
			nextBTN.visible = false;
			floorProperty.visible = false;
			gaysornLogo.visible = false;
			
			
		}
		
		private function showfloorOptionCoverFlowProperty():void
		{
			prevBTN.visible = true;
			nextBTN.visible = true;
			floorProperty.visible = true;
			gaysornLogo.visible = true;
			
			
		}
		
		override public function viewWillAppear():void
		{
			floorOptionCoverFlow.unHilightMCAndCloseBubble();
			setDefaultDirectoryViewController();
			

		}
		
		private function setButtonName(_index:int):void
		{
			
			if(_index == 1){
				//floorKey = 0;
				floorProperty._floorname.text = 'Lobby Floor';
				nextBTN._flNamae.text = 'Groud Floor';
				prevBTN._flNamae.text = '3rd Floor';
			}
			if(_index == 2){
				//floorKey = 1;
				floorProperty._floorname.text = 'Ground Floor';
				nextBTN._flNamae.text = '1st Floor';
				prevBTN._flNamae.text = 'Lobby Floor';
			}
			if(_index == 3){
				//floorKey = 2;
				floorProperty._floorname.text = '1st Floor';
				nextBTN._flNamae.text = '2nd Floor';
				prevBTN._flNamae.text = 'Ground Floor';
			}
			if(_index == 4){
				//floorKey = 3;
				floorProperty._floorname.text = '2nd Floor';
				nextBTN._flNamae.text = '3rd Floor';
				prevBTN._flNamae.text = '1st Floor';
			}
			if(_index == 0){
				//floorKey = 4;
				floorProperty._floorname.text = '3rd Floor';
				nextBTN._flNamae.text = 'Lobby Floor';
				prevBTN._flNamae.text = '2nd Floor';
			}
			dispatchEvent(new Event('setFloorActive'));
		}
		
		public function facilityChoose(e:FacilityEvent):void
		{
			floorOptionCoverFlow.showFacility(e.obj.name);
		}


	}
}