﻿package viewcontroller {
	import com.forviz.ViewController;
	import com.forviz.ui.UICarousel;
	import flash.geom.Rectangle;
	import main.BlurContentTourist;
	
	public class TouristInfoViewcontroller extends ViewController{
		
		var uiSlide:UICarousel = new UICarousel(new Rectangle(20,50,1770,970));
		var blurContent:BlurContentTourist = new BlurContentTourist();
		public function TouristInfoViewcontroller() {
			// constructor code
			
			this.addChild(uiSlide);
			uiSlide.addPage('content/tourist01.png');
			uiSlide.addPage('content/tourist02.png');
			
			blurContent.x = 1770;
			blurContent.height = 1080-110;
			addChild(blurContent);
			
		}

	}
	
}
