﻿package com.pvm.multitouch {
	
	import flash.display.Shape;
	
	public class MidPoint extends DisplayPoint {

		public var orgX:Number;
		public var orgY:Number;
		
		public function MidPoint() {
			// constructor code
			var myShape:Shape = new Shape();
			
			myShape.graphics.beginFill(0xFFCC00, 0.6);
			myShape.graphics.drawCircle(-4,-4,8);
			myShape.graphics.endFill();
			addChild(myShape);
		}

	}
	
}
