﻿package com.pvm.multitouch {
	
	import flash.display.Sprite;
	
	import flash.ui.Multitouch;
    import flash.ui.MultitouchInputMode;
	import flash.events.TouchEvent;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class MultiTouchObject extends Sprite {

		Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
		
		
		public var _content:Sprite;
		
		public var content_orgX:Number;
		public var content_orgY:Number;
		
		private var no_of_touch:int = 0;
		
		
		private var easingHolder:Number = 4;
		private var easingPower:Number = 5;
		
		var SCROLL_UPBOUND:Number = 0;
		var SCROLL_LOWBOUND:Number = 900;
		var SCROLL_LEFTBOUND:Number = 0;
		var SCROLL_RIGHTBOUND:Number = 1920;
		

		private var touchPointArray:Array = new Array();
		
		private var firstTouch:TouchPoint;
		private var secondTouch:TouchPoint;
		private var refTouch:TouchPoint;
		
		private var targetX:Number = 0;
		private var targetY:Number = 0;
		private var targetScale:Number = 1;
		
		private var diffX:Number;
		private var diffY:Number;
		
		private var touching:Boolean = false;
		private var veloX:Number;
		private var veloY:Number;
		private var lastRefPointX:Number;
		private var lastRefPointY:Number;
		
		private var org_distance:Number;
		private var dis:Number;
		
		private var org_scale:Number;
		
		private var refTouchArray:Array = new Array();
		
		private var refPoint:MidPoint = new MidPoint();
		private var tmpPoint:TouchPoint;
		private var tmpPoint2:TouchPoint = new TouchPoint();
		
		private var calculate:Boolean = false;
	
		public function MultiTouchObject() {
			// constructor code
			
			this._content = this;
						
			
			
			//this.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
			//addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
			addEventListener(Event.ENTER_FRAME, src);
		}
		
		public function setContent(_content:Sprite):void
		{
			this._content = _content;
			content_orgX = _content.x;
			content_orgY = _content.y;
		}
		
		public function setPositionDefault():void
		{
			targetX = content_orgX;
			targetY = content_orgY;
			calculate = true;
			trace("setPositionDefault x,y = " + content_orgX + ", " + content_orgY);
		}
		
		
		
		public function onTouchBegin(evt:TouchEvent):void
		{
			touching = true;
			trace("Touch Begin : touch on " + evt.target + " : x,y = " + evt.stageX +", " + evt.stageY);
			
			
			//----------------------------------- TouchPoint --------------------------------------			
			if (touchPointArray[evt.touchPointID] == null)
			{
				var touchPoint = new TouchPoint();
				touchPoint.touchID = evt.touchPointID;
				stage.addChild(touchPoint);
				touchPointArray[evt.touchPointID] = touchPoint;
			}
			touchPointArray[evt.touchPointID].visible = true;
			touchPointArray[evt.touchPointID].x = evt.stageX;
			touchPointArray[evt.touchPointID].y = evt.stageY;
			//====================================================================================
			
			//----------------------------------- No of Touch ----------------------------------------
			if(no_of_touch < 0){
				no_of_touch == 0;
			}
			
			no_of_touch++;
			if(no_of_touch == 1){	
				firstTouch 	= touchPointArray[evt.touchPointID];
				//refTouch	= firstTouch;
			} 
			else if (no_of_touch == 2){	
				secondTouch = touchPointArray[evt.touchPointID];
				//refTouch	= refPoint;
			}
			
			//----------------------------------- Ref Touch ----------------------------------------
			
			refTouchArray.push(touchPointArray[evt.touchPointID]);
			
			updateRefPoint();
			
			refPoint.orgX 	= refPoint.x;
			refPoint.orgY 	= refPoint.y;
			//trace("refTouchArray.length = " + refTouchArray.length);
			//trace("refPoint x,y = " + refPoint.x + ", " + refPoint.y);
		
			diffX = _content.x - refPoint.x;
			diffY = _content.y - refPoint.y;
				
			//trace("diff x,y = " + diffX + ", " + diffY);
			trace("no of touch = " + no_of_touch);
			if (no_of_touch == 2)
			{
				org_distance = getDistance(refTouchArray[0], refTouchArray[1]);
				
				org_scale = _content.scaleX;
				
				trace("TouchBegin dis = " + dis + " : org_distance = " + org_distance);
				
			}
			
			
			calculate = true;
			//====================================================================================

			
		}
		
		public function onTouchMove(evt:TouchEvent):void
		{
			//-------------------------------- TouchPoint------------------------------------------			
			if (touchPointArray[evt.touchPointID] != null)
			{
				touchPointArray[evt.touchPointID].targetX = evt.stageX;
				touchPointArray[evt.touchPointID].targetY = evt.stageY;	
				
				touchPointArray[evt.touchPointID].x = evt.stageX;
				touchPointArray[evt.touchPointID].y = evt.stageY;	
				
			}
			//====================================================================================
			
			
			
		}


		public function onTouchEnd(evt:TouchEvent):void
		{
			trace("TouchEnd");
			//-------------------------------- TouchPoint------------------------------------------			
			if (touchPointArray[evt.touchPointID] != null)
			{
				touchPointArray[evt.touchPointID].visible = false;
				//trace("IndexOf = " + refTouchArray.indexOf(touchPointArray[evt.touchPointID]));
				refTouchArray.splice(refTouchArray.indexOf(touchPointArray[evt.touchPointID]),1);
				
				//touchPointArray[evt.touchPointID] = null;
				
			}
			//====================================================================================
			
			no_of_touch--;
			trace("TouchEnd : no_of_touch = " + no_of_touch);
			
			
			//----------------------------------- Ref Touch ----------------------------------------
			
			updateRefPoint();
			
			
			refPoint.orgX 	= refPoint.x;
			refPoint.orgY 	= refPoint.y;
			
			//----------------------------------------
			veloX = refPoint.x - refPoint.lastX;
			veloY = refPoint.y - refPoint.lastY;
			
			
			//trace("refPoint x,y = " + refPoint.x + ", " + refPoint.y);
			
			diffX = _content.x - refPoint.x;
			diffY = _content.y - refPoint.y;
				
				
			//====================================================================================
			
			
			//trace("touchEnd : " + refTouchArray[0] + " : " + refTouchArray[1]);
			//trace("touchEnd : " + refTouchArray);
			
			if (no_of_touch <= 0)
			{
				touching = false;
			}
			
			
		}
	
	
		private function src(evt:Event):void
		{
			
			
			
			//-------------------------------- TouchPoint------------------------------------------			
			for (var i:int = 0;i<touchPointArray.length;i++)
			{
				if (touchPointArray[i] != null)
				{
					touchPointArray[i].x += (touchPointArray[i].targetX - touchPointArray[i].x) / 2;
					touchPointArray[i].y += (touchPointArray[i].targetY - touchPointArray[i].y) / 2;
				}
			}
			//====================================================================================
			
			
			
			//-------------------------------- refPoint------------------------------------------			
			updateRefPoint();
			//====================================================================================
			
			
			
			//----------------------------------- Photo ------------------------------------------			
			if (calculate)
			{
				
					if (touching)
					{
						targetX = refPoint.x + diffX;
						targetY = refPoint.y + diffY;
						
						//trace("refPoint.x = " + refPoint.x + " : diffX = " + diffX);
						
					} else {
						
						if (targetX < SCROLL_LEFTBOUND) {
							targetX = SCROLL_LEFTBOUND;
						} else if (targetX > SCROLL_RIGHTBOUND) {
							targetX = SCROLL_RIGHTBOUND;
						}
						
						
						if (targetY < SCROLL_UPBOUND) {
							targetY = SCROLL_UPBOUND;
						} else if (targetY > SCROLL_LOWBOUND) {
							targetY = SCROLL_LOWBOUND;
						}
						
						
					}
				
					trace("calculate = " + calculate + " : touching = " + touching + " : targetX = " + targetX);
				
					_content.x += (targetX - _content.x) / easingHolder;
					_content.y += (targetY - _content.y) / easingHolder;
					
					
					var diff2TargetX = _content.x - targetX;
					var diff2TargetY = _content.y - targetY;
				
					//---------
					
					if (!touching)
					{
						
						if ((diff2TargetY <= 10) && (diff2TargetY >= -10) && (diff2TargetX <= 10) && (diff2TargetX >= -10))
						{
								
							calculate = false;
							//removeEventListener(Event.ENTER_FRAME, scr);
							
						}
					
					}
				
				
				if ((org_distance != -1) && (dis != -1) && (no_of_touch == 2) )
				{
					//trace("Calculate : org_distance = " + org_distance + " : dis = " + dis);
					targetScale = (org_scale/org_distance) * dis;

					//trace("targetScale = " + targetScale);
					if (Math.abs(targetScale - _content.scaleX) < 0.4)
					{
						_content.scaleX += (targetScale - _content.scaleX) / 2;
						_content.scaleY += (targetScale - _content.scaleY) / 2;
						
						//touchArea.scaleX = touchArea.scaleY = 1/this.scaleX;
					}
				}
			}
			
			//====================================================================================
			
		}
		
		
		private function updateRefPoint():void
		{
			//trace("updateRefPoint refTouchArray[0] = " + refTouchArray[0]);
			//trace("updateRefPoint refTouchArray[1] = " + refTouchArray[1]);
			tmpPoint 	= calRefPosition(refTouchArray[0], refTouchArray[1]);
			dis			= getDistance(refTouchArray[0], refTouchArray[1]);
			
			//trace("updateRefPoint dis = " + dis);
			if (tmpPoint != null)
			{
				refPoint.lastX = refPoint.x;
				refPoint.lastY = refPoint.y;
				
				refPoint.x 	= tmpPoint.x;
				refPoint.y 	= tmpPoint.y;
			}
		}
		
		private function calRefPosition(n1:TouchPoint, n2:TouchPoint):TouchPoint
		{
			
			if ((n1 == null) &&(n2 == null)) 
			{
				return null;
			} else if (n2 == null) { 
				n2 = n1;
				//trace("return n1 : " + n1.x + ", " + n1.y);
				return n1; 
			} else {
			
				var dx:Number = 0.5 * (n1.x + n2.x);
				var dy:Number = 0.5 * (n1.y + n2.y);
				tmpPoint2.x = dx;
				tmpPoint2.y = dy;
				
				return tmpPoint2;
			}
		}
		
		private function getDistance(n1:TouchPoint, n2:TouchPoint):Number
		{
			if ((n1 == null) &&(n2 == null)) 
			{
				return -1;
			} else if (n2 == null) { 
				n2 = n1;
				//trace("return n1 : " + n1.x + ", " + n1.y);
				return -1; 
			} else {
				
				var dis:Number;
				var dx:Number = n1.x - n2.x;
				var dy:Number = n1.y - n2.y;
				//trace("n1.x = " + n1.x + " : n2.x = " + n2.x + " : dx = " + dx);
				//trace("n1.y = " + n1.y + " : n2.y = " + n2.y + " : dy = " + dy);
				
				dis = Math.sqrt(dx*dx+dy*dy);
				return dis;
			}
		}
		
	}
	
}
