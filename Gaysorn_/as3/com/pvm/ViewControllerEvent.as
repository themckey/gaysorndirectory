﻿package com.pvm {
	
	import flash.events.Event;
	
	public class ViewControllerEvent extends Event {
		
		public static const OPEN		: String	 = "open";
		public static const CLOSE		: String	 = "close";
		
		public var viewController:ViewController;
		
		public var fromVC:ViewController;
		public var toVC:ViewController;
		
		public function ViewControllerEvent(type:String, viewController:ViewController = null, fromVC:ViewController = null, toVC:ViewController = null) {
			// constructor code
			this.viewController = viewController;
			this.fromVC = fromVC;
			this.toVC = toVC;
			super(type);
		}

		public override function clone():Event 
		{
			var newEvt : ViewControllerEvent = new ViewControllerEvent ( type, viewController, fromVC, toVC);
			newEvt.viewController = viewController;
			newEvt.fromVC = fromVC;
			newEvt.toVC = toVC;
			return newEvt;
		}

	}
	
}
