﻿package com.pvm.input.keyboard {
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import flash.utils.ByteArray;
 
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.display.DisplayObject;
	
	import com.pvm.ui.UIControl;
	import com.pvm.ui.UIControlEvent;
	import com.pvm.ui.UITextField;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.net.URLRequest;
	

	
	public class TouchScreenKeyboard extends Sprite {

		private var _theme:String = "dark";
		private var _mode:String = "default";
		
		private var _inputLang:String;
		private var _defaultLang:String = 'en';
		
		public static var DEFAULT:String	= "default";
		public static var NUMBER:String		= "number";
		public static var EMAIL:String		= "email";
		
		private var _position:String = "bottom";
		private var _showPoint:Point;
		
		
		private var _rowHeight:int = 55;
		
		
		private var _submitKey:KeyItem;
		private var _submitLabel:String = 'Go';
		
		private var _boardStrokeColor:Number = 0x000000;
		private var _boardStrokeAlpha:Number = 0;
		private var _boardFillColor:Number = 0x000000;
		private var _boardFillAlpha:Number = 0;
		
		private var _keyFillColor:Number = 0x000000;
		private var _keyFillAlpha:Number = 1;
		
		private var _keyTextColor:Number = 0x000000;
		private var _keyTextAlpha:Number = 1.0;
		
		private var _primaryKeyColor:uint = 0xb9422f;
		private var _primaryTextColor:uint = 0xffffff;
		
		private var _margin_horizontal:Number = 9;
		private var _margin_vercital:Number = 9;
		
		private var _positionMode:String = "auto";
		
		private var _board:Sprite = new Sprite();
		
		private var _boardBG:Shape = new Shape();
		private var _largestX:Number = 0;
		private var _largestY:Number = 0;
		
		private var _boardWidth:Number;
		private var _boardHeight:Number;
		
		private var _elements:Array = new Array();
		private var _focusingElement:UIControl;
		
		public var keyDict:Dictionary = new Dictionary();
		
		private var _textFormatDict:Dictionary = new Dictionary();
		
		private var _hidden:Boolean = true;
		
		/* Event */
		private var touchScreenKeyboardPressedEvent:TouchScreenKeyboardEvent = new TouchScreenKeyboardEvent('onTouchScreenKeyPressed');
		private var keyboardWillAppearEvent:TouchScreenKeyboardEvent = new TouchScreenKeyboardEvent('keyboardWillAppear');
		private var keyboardWillDisappearEvent:TouchScreenKeyboardEvent = new TouchScreenKeyboardEvent('keyboardWillDisappear');
		


		private var _touchedKeyItem:KeyItem;
		
		var soundClick:Sound;
		
		public function TouchScreenKeyboard() {
			init();
			
			soundClick = new Sound();
			//soundClick.load(new URLRequest('src/bell1.mp3'));
		}
		
		
		public function setDefaultTextFormat():void
		{
			var _en:TextFormat = new TextFormat('DB FongNam X', 35, 0x333333,false,null,null,null,null,'left', 17);
			_en.leading = -20;
			
			var _th:TextFormat = new TextFormat('DB FongNam X', 35, 0x333333,false,null,null,null,null,'left', 17);
			_th.leading = -20;
			
			var _label:TextFormat = new TextFormat('DB FongNam X', 19, 0x333333,true);
			_label.align = 'right';
			
			_textFormatDict['en'] = _en;
			_textFormatDict['th'] = _th;
			_textFormatDict['label'] = _label;
			
		}
		
		
		public function init():void
		{
			touchScreenKeyboardPressedEvent.from = this;
			keyboardWillAppearEvent.from = this;
			keyboardWillDisappearEvent.from = this;
			
			this.addChild(_board);
			
			
			/* Default Layout */
			keyDict['default'] = defineKeysDefault();
			keyDict['number'] = defineKeysNumber();
			keyDict['email'] = defineKeysEmail();
			keyDict['phone'] = defineKeysPhone();
			
			
			setDefaultTextFormat();
			
			createKeys('default');
			
		}
		
		
		public function defineKeysDefault():Array
		{
			/* Default Layout */
			
			var dr0:Array = new Array(); //Default row 0
			var dr1:Array = new Array(); //Default row 1
			var dr2:Array = new Array(); //Default row 2
			var dr3:Array = new Array(); //Default row 3
			var dr4:Array = new Array(); //Default row 4
			
			
			//dr0.push({en:['`', '~']});
			dr0.push({en:['1', '!']});
			dr0.push({en:['2', '@'], th:['/', '๑']});
			dr0.push({en:['3', '#'], th:['_', '๒']});
			dr0.push({en:['4', '$'], th:['ภ', '๓']});
			dr0.push({en:['5', '%'], th:['ถ', '๔']});
			dr0.push({en:['6', '^'], th:['ุ', 'ู']});
			dr0.push({en:['7', '&'], th:['ึ', '฿']});
			dr0.push({en:['8', '*'], th:['ค', '๕']});
			dr0.push({en:['9', '('], th:['ต', '๖']});
			dr0.push({en:['0', ')'], th:['จ', '๗']});
			//dr0.push({en:['-', '_'], th:['ข', '๘']});
			//dr0.push({en:['=', '+'], th:['ช', '๙']});
			dr0.push({key:'backspace', width:70});
			
			dr1.push({en:['q', 'Q'], th:['ๆ', '๐'], offsetX:-40});
			dr1.push({en:['w', 'W'], th:['ไ', '']});
			dr1.push({en:['e', 'E'], th:['ำ', 'ฎ']});
			dr1.push({en:['r', 'R'], th:['พ', 'ฑ']});
			dr1.push({en:['t', 'T'], th:['ะ', 'ธ']});
			dr1.push({en:['y', 'Y'], th:['ั', 'ํ']});
			dr1.push({en:['u', 'U'], th:['ี', '๊']});
			dr1.push({en:['i', 'I'], th:['ร', 'ณ']});
			dr1.push({en:['o', 'O'], th:['น', 'ฯ']});
			dr1.push({en:['p', 'P'], th:['ย', 'ญ']});
			dr1.push({en:['[', '{'], th:['บ', 'ฐ']});
			dr1.push({en:[']', '}'], th:['ล', ',']});
			
			dr2.push({en:['a', 'A'], th:['ฟ', 'ฤ']});
			dr2.push({en:['s', 'S'], th:['ห', 'ฆ']});
			dr2.push({en:['d', 'D'], th:['ก', 'ฏ']});
			dr2.push({en:['f', 'F'], th:['ด', 'โ']});
			dr2.push({en:['g', 'G'], th:['เ', 'ฌ']});
			dr2.push({en:['h', 'H'], th:['้', '็']});
			dr2.push({en:['j', 'J'], th:['่', '๋']});
			dr2.push({en:['k', 'K'], th:['า', 'ษ']});
			dr2.push({en:['l', 'L'], th:['ส', 'ศ']});
			dr2.push({en:[';', ':'], th:['ว', 'ซ']});
			dr2.push({en:['\'', '"'], th:['ง', '.']});
			
			
			
			dr3.push({key:'shift', offsetX:-40});
			dr3.push({en:['z', 'Z'], th:['ผ', '(']});
			dr3.push({en:['x', 'X'], th:['ป', ')']});
			dr3.push({en:['c', 'C'], th:['แ', 'ฉ']});
			dr3.push({en:['v', 'V'], th:['อ', 'ฮ']});
			dr3.push({en:['b', 'B'], th:['ิ', 'ฺ']});
			dr3.push({en:['n', 'N'], th:['ื', '์']});
			dr3.push({en:['m', 'M'], th:['ท', '?']});
			dr3.push({en:[',', '<'], th:['ม', 'ฒ']});
			dr3.push({en:['.', '>'], th:['ใ', 'ฬ']});
			dr3.push({en:['/', '?'], th:['ฝ', 'ฦ']});
			dr3.push({en:['@', '@'], th:['@', '@']});
		
			
			dr4.push({key:'switch_lang', width:80, offsetX:42});
			dr4.push({key:'space', width:300, offsetX:-10});
			
			dr4.push({key:'submit', width:120, offsetX:315});
			
			return [dr0, dr1, dr2, dr3, dr4];
		}
		
		public function defineKeysEmail():Array
		{
			var er0:Array = new Array(); //Default row 0
			var er1:Array = new Array(); //Default row 1
			var er2:Array = new Array(); //Default row 2
			var er3:Array = new Array(); //Default row 3
			var er4:Array = new Array(); //Default row 4
			var er5:Array = new Array();
			
			er0.push({en:'@gmail.com', width:120});
			er0.push({en:'@hotmail.com', width:150});
			er0.push({en:'@yahoo.com', width:120});
			
			er1.push({en:['1', '']});
			er1.push({en:['2', ''], th:['/', '๑']});
			er1.push({en:['3', ''], th:['_', '๒']});
			er1.push({en:['4', ''], th:['ภ', '๓']});
			er1.push({en:['5', ''], th:['ถ', '๔']});
			er1.push({en:['6', ''], th:['ุ', 'ู']});
			er1.push({en:['7', ''], th:['ึ', '฿']});
			er1.push({en:['8', ''], th:['ค', '๕']});
			er1.push({en:['9', ''], th:['ต', '๖']});
			er1.push({en:['0', ''], th:['จ', '๗']});
			er1.push({en:['-', '_'], th:['ข', '๘']});
			er1.push({key:'backspace', width:70});
			
			er2.push({en:['q', 'Q'], th:['ๆ', '๐'], offsetX:20});
			er2.push({en:['w', 'W'], th:['ไ', '']});
			er2.push({en:['e', 'E'], th:['ำ', 'ฎ']});
			er2.push({en:['r', 'R'], th:['พ', 'ฑ']});
			er2.push({en:['t', 'T'], th:['ะ', 'ธ']});
			er2.push({en:['y', 'Y'], th:['ั', 'ํ']});
			er2.push({en:['u', 'U'], th:['ี', '๊']});
			er2.push({en:['i', 'I'], th:['ร', 'ณ']});
			er2.push({en:['o', 'O'], th:['น', 'ฯ']});
			er2.push({en:['p', 'P'], th:['ย', 'ญ']});
			
			er3.push({en:['a', 'A'], th:['ฟ', 'ฤ'], offsetX:40});
			er3.push({en:['s', 'S'], th:['ห', 'ฆ']});
			er3.push({en:['d', 'D'], th:['ก', 'ฏ']});
			er3.push({en:['f', 'F'], th:['ด', 'โ']});
			er3.push({en:['g', 'G'], th:['เ', 'ฌ']});
			er3.push({en:['h', 'H'], th:['้', '็']});
			er3.push({en:['j', 'J'], th:['่', '๋']});
			er3.push({en:['k', 'K'], th:['า', 'ษ']});
			er3.push({en:['l', 'L'], th:['ส', 'ศ']});
			er3.push({key:'enter', width:80});
			
			
			er4.push({key:'shift'});
			er4.push({en:['z', 'Z'], th:['ผ', '(']});
			er4.push({en:['x', 'X'], th:['ป', ')']});
			er4.push({en:['c', 'C'], th:['แ', 'ฉ']});
			er4.push({en:['v', 'V'], th:['อ', 'ฮ']});
			er4.push({en:['b', 'B'], th:['ิ', 'ฺ']});
			er4.push({en:['n', 'N'], th:['ื', '์']});
			er4.push({en:['m', 'M'], th:['ท', '?']});
			er4.push({en:['.', '>'], th:['ใ', 'ฬ']});
			er4.push({en:['@', '@'], th:['@', '@']});
			er4.push({en:'.com'});
			er4.push({key:'shift', width:70});
			
			
			er5.push({key:'space', width:300});
			
			er5.push({key:'submit', width:120, offsetX:200});
		
			
			
			return [er0, er1, er2, er3, er4, er5];
		}
		
		public function defineKeysNumber():Array 
		{
			/* Number */
			var nr0:Array = new Array(); //Number row 0
			var nr1:Array = new Array(); //Number row 1
			var nr2:Array = new Array(); //Number row 2
			var nr3:Array = new Array(); //Number row 3

			nr0.push({en:7});
			nr0.push({en:8});
			nr0.push({en:9});
			nr1.push({en:4});
			nr1.push({en:5});
			nr1.push({en:6});
			nr2.push({en:1});
			nr2.push({en:2});
			nr2.push({en:3});
			nr2.push({key:'submit', 'height':105});
			nr3.push({en:0, 'width' : 105});
			nr3.push({en:'.'});
			
			
			return [nr0, nr1, nr2, nr3];
		}
		
		public function defineKeysPhone():Array 
		{
			/* Number */
			var nr0:Array = new Array(); //Number row 0
			var nr1:Array = new Array(); //Number row 1
			var nr2:Array = new Array(); //Number row 2
			var nr3:Array = new Array(); //Number row 3
			var nr4:Array = new Array(); //Number row 4

			nr0.push({en:['7', '7'], width:73.5, height:73.5});
			nr0.push({en:['8', '8'], width:73.5, height:73.5});
			nr0.push({en:['9', '9'], width:73.5, height:73.5});
			nr0.push({en:['+', '+'], height:154, width:73.5});
			
			nr1.push({en:['4', '4'], width:73.5, height:73.5});
			nr1.push({en:['5', '5'], width:73.5, height:73.5});
			nr1.push({en:['6', '6'], width:73.5, height:73.5});
			nr2.push({en:['1', '1'], width:73.5, height:73.5});
			nr2.push({en:['2', '2'], width:73.5, height:73.5});
			nr2.push({en:['3', '3'], width:73.5, height:73.5});
			nr2.push({key:'submit', 'width':73.5, 'height':154, 'label' : 'Done', primary : true});
			
			nr3.push({en:['0', '0'], 'width' : 151.5, 'height' :73.5});
			nr3.push({key:'backspace','label' : '<', 'width' :73.5, 'height' :73.5});
			
			
			return [nr0, nr1, nr2, nr3];
		}
		
		
		public function set showPoint(thePoint:Point):void
		{
			if (!_showPoint) {
				_showPoint = thePoint;
			}
		}


		function clone(source:Object):*
		{
			var myBA:ByteArray = new ByteArray();
			myBA.writeObject(source);
			myBA.position = 0;
			return(myBA.readObject());
		}
		
		public function createKey(obj:Object, options:Object = null):KeyItem
		{
			// {textFormatDict : this._textFormatDict}
		
			return new KeyItem(obj, options);
		}


		private function createKeys(layout:String = 'default'):void
		{
			//clear all keys 
			while(_board.numChildren) {
				_board.removeChildAt(0);
			}
			
			
			var largestX:Number = 0;
			var largestY:Number = 0;
			var keyItem:KeyItem;
			//Create KeyItem

			var useThisY:Number = 0;
			var currentRow:int = 0;
			
			trace("=========================================================");
			trace("kayout", layout);
			trace("=========================================================");
			for (var row:int=0; row< keyDict[layout].length; row++)
			{
				var lastX:Number = 0;
				
				
				for (var i:int=0; i< keyDict[layout][row].length; i++)
				{
					keyItem = createKey(keyDict[layout][row][i], {textFormatDict : this._textFormatDict});

					if (row != currentRow) {
						useThisY = useThisY + this._margin_vercital + keyItem.keyHeight;
						currentRow = row;
					}

					
					keyItem.x = keyItem.offsetX + lastX;
					keyItem.y = useThisY; //row * (_rowHeight);
					
					lastX += keyItem.keyWidth + keyItem.offsetX + _margin_horizontal;
					_board.addChild(keyItem);
					
					keyItem.addEventListener("click", onClickKeyItem);
					
					
					if (keyItem.keyID == "submit") {
						this._submitKey = keyItem;
					}
						if (keyItem.keyID == "space") {
						keyItem.gotoAndStop(2);
					}
				}
				
				largestY += (keyItem.keyHeight + _margin_vercital);
				if (lastX > largestX) largestX = lastX;
			}
			
			_largestX = largestX;
			_largestY = largestY;
			
			_board.x = -0.5 * largestX;
			_board.y = -0.5 * largestY;
			
		//	this.drawBackground();
			
			
			this._inputLang = this._defaultLang;
			configureInputLang();
		}
		
		/*
		 * Getter / Setter
		 ***************************************************************/
		 
		 public function get board():Sprite
		 {
			 return this._board; 
		 }
		 
		 public function set mode (theMode:String):void {
			 if (this._mode != theMode) {
				 this._mode = theMode;
				 this.createKeys(this._mode);
			 }
		 }
		 
		 public function get mode():String {
			 return this._mode;
		 }
	
		 
		 public function set submitLabel(theLabel:String):void
		 {
			 this._submitKey.label = theLabel;
		 }
		 
		public function set backgroundColor(theColor:uint):void
		{
			 _boardFillColor = theColor;
			// drawBackground();
		}
		 
		public function set backgroundAlpha(theAlpha:Number):void
		{
			 _boardFillAlpha = theAlpha;
			 drawBackground();
		}
		
		public function set keyColor(theColor:uint):void
		{
			 _keyFillColor = theColor;
			 
			 var tKey:KeyItem;
			 for (var i:int=0;i<_board.numChildren;i++) {
				 if (_board.getChildAt(i) is KeyItem) {
				 	tKey = _board.getChildAt(i) as KeyItem;
				 	tKey.backgroundColor = _keyFillColor;
				 }
			 }
			// drawBackground();
		}
		
		public function set primaryKeyColor(theColor:uint):void
		{
			 var tKey:KeyItem;
			 if (this._submitKey)  {
				 this._submitKey.backgroundColor = theColor;
			 }
			// drawBackground();
		}
		
		public function get primaryKeyColor():uint
		{
			return this._primaryKeyColor; 
		}
		
		public function set primaryTextColor(theColor:uint):void
		{
			 var tKey:KeyItem;
			 if (this._submitKey)  {
				 this._submitKey.textColor = theColor;
			 }
			// drawBackground();
		}
		
		public function get primaryTextColor():uint
		{
			return this._primaryTextColor;
		}
		
		public function set keyAlpha(theAlpha:Number):void
		{
			 _keyFillAlpha = theAlpha;
			 
			 var tKey:KeyItem;
			 for (var i:int=0;i<_board.numChildren;i++) {
				 if (_board.getChildAt(i) is KeyItem) {
				 	tKey = _board.getChildAt(i) as KeyItem;
				 	tKey.backgroundAlpha = _keyFillAlpha;
				 }
			 }
			 //drawBackground();
		}
		
		public function set textColor(theColor:uint):void
		{
			 _keyTextColor = theColor;
			 
			 var tKey:KeyItem;
			 for (var i:int=0;i<_board.numChildren;i++) {
				 if (_board.getChildAt(i) is KeyItem) {
				 	tKey = _board.getChildAt(i) as KeyItem;
				 	tKey.textColor = _keyTextColor;
				 }
			 }
			// drawBackground();
		}
		
		
		public function set position(value:String):void
		{
			this._position = value;
			
		}
		
		
		public function get boardWidth():Number
		{
			return this._boardWidth;
		}
		
		public function get boardHeight():Number
		{
			return this._boardHeight;
		}
		
		
		
		/*
		 * Event
		 ***************************************************************/
		
		public function onClickKeyItem(evt:Event):void
		{
			
			_touchedKeyItem = evt.target as KeyItem;
			
			//soundClick.play();
			
			switch (_touchedKeyItem.keyID) {
				
				case 'shift' :
					this.shift();
					break;
				case 'switch_lang' : 
					this._inputLang = (this._inputLang == 'en') ? 'th' : 'en';
					configureInputLang();
					break;
					
				case 'return' : 
				case 'enter' :
					touchScreenKeyboardPressedEvent.keyValue = "\n";
					dispatchEvent(touchScreenKeyboardPressedEvent);
			
					break;
				case 'backspace' : 
				case 'space' : 
				default: 
					
					touchScreenKeyboardPressedEvent.keyID =  _touchedKeyItem.keyID;
					touchScreenKeyboardPressedEvent.keyValue =  _touchedKeyItem.keyValue;
					dispatchEvent(touchScreenKeyboardPressedEvent);
			
					break;
			}
			
			
		}
		
		public function addElement(element:UIControl):void
		{
			_elements.push(element);
			element.addEventListener("focus", onFocusElementHandler);
		}
		
		private function onFocusElementHandler(evt:UIControlEvent):void
		{
			//FocusOut previous one
			if (this._focusingElement != null && this._focusingElement != evt.target) this._focusingElement.focus = false;
			
			//Select new
			this._focusingElement = evt.target as UIControl;
			if (this._focusingElement) {
				showMe(this._focusingElement.frame);
			}
		}
		
		public function get focusingElement():UIControl
		{
			return this._focusingElement;
		}
		
		
		
		
		var showX:Number = 0;
		var showY:Number = 0;
		
		public function showMe(fromRect:Rectangle = null):void
		{
			_board.x = -0.5 * _largestX;
			_board.y = -0.5 * _largestY;
			//Move Keyboard to top level
			

			
			this.parent.addChild(this);
			//this.parent.swapChildren(this, this.parent.getChildAt(this.parent.numChildren-1));
			
			
			switch (_position) {
				case 'under' :
					if (fromRect) {
						showX = fromRect.x + .5 * fromRect.width + 83.5;
						showY = fromRect.y + fromRect.height + .5 * _board.height;
						
						trace('showX', showX);
						trace('showY', showY);
					} else {
						showX = 572.5;
						showY = 633;
					}
					break;
				
				case 'screenCenter' : 
					showX =  0.5 * this.stage.stageWidth;
					showY = this.stage.stageHeight - _board.height;
					break;
				
				case 'frameCenter' : 
					showX = 0.5 * fromRect.x + .5 * fromRect.width;
					showY = 0.5 * fromRect.y + .5 * fromRect.height;
					
					break;
					
				case 'screenBottom' : 
					showX =  0.5 * this.stage.stageWidth;
					showY = this.stage.stageHeight - _board.height;
					break;
					
			}
			
			dispatchEvent(keyboardWillAppearEvent);
			
			animateShowMe(showX, showY);
			
			
			_hidden = false;
		}
		
		public function animateShowMe(posX:Number, posY:Number):void
		{
			if (_showPoint) {
				posX = _showPoint.x;
				posY = _showPoint.y;
			}
			
			TweenMax.to(this, 0.6, {x:posX, y:posY, alpha:1, ease:Expo.easeInOut, overwrite:true});
		}
		
		public function hideMe():void
		{
			var targetX:int = 0.5 * 1920; //this.stage.stageWidth pin keep
			var targetY:int = 1080 + 0.5 * this.boardHeight  + 50; //this.stage.stageHeight pin keep
			
			dispatchEvent(keyboardWillDisappearEvent);
			
			if (this._focusingElement && this._focusingElement is UITextField) {
				this._focusingElement.focus = false;
			}
			
			animateHideMe(targetX, targetY);
			
			_hidden = true;
			
		}
		
		public function animateHideMe(posX:Number, posY:Number):void
		{
			TweenMax.to(this, 0.5, {x:posX, y:posY, alpha:0, overwrite:true});
		}
		
		public function get hidden():Boolean {
			return this._hidden;
		}
		
		
		
		
		private function drawBackground():void
		{
			
			_boardBG.graphics.clear();
			_boardBG.graphics.beginFill(_boardFillColor, _boardFillAlpha);
      		_boardBG.graphics.lineStyle(1, _boardStrokeColor, _boardStrokeAlpha, true);
      		_boardBG.graphics.drawRoundRect(-10,-10,_largestX+20,_largestY+20,6);
			_boardBG.graphics.endFill();
			_board.addChildAt(_boardBG, 0);
			
			_boardWidth = _largestX;
			_boardHeight = _largestY;
		}
		
		private function shift():void
		{
			var tKey:KeyItem;
			 for (var i:int=0;i<_board.numChildren;i++) {
				 if (_board.getChildAt(i) is KeyItem) {
					 tKey = _board.getChildAt(i) as KeyItem;
				 	 tKey.shift = !tKey.shift;
					 
				 }
				
			 }
		}
		
		private function configureInputLang():void
		{
			var tKey:KeyItem;

			 for (var i:int=0;i<_board.numChildren;i++) {
				 if (_board.getChildAt(i) is KeyItem) {
					 tKey = _board.getChildAt(i) as KeyItem;
				 	tKey.inputLang = this._inputLang;
				 }
			 }
		}

	}
	
}
