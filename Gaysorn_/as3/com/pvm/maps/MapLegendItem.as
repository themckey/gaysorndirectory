﻿package com.pvm.maps {
	
	import com.pvm.ui.UIButton;
	import flash.geom.Rectangle;
	import com.pvm.ui.UIControlEvent;
	
	public class MapLegendItem extends UIButton {

		private var _markers:Array;
		
		private var _map:LocationMap;
		public var _type:String;
		
		public function MapLegendItem(frame:Rectangle, options:Object = null) {
			// constructor code
			
			super(frame, options);
			
			//this.addEventListener("tap", onTapMapLegendItem);
		}
		
		public function get markers():Array {
			return this._markers;
		}
		
		public function set map(theMap:LocationMap):void
		{
			this._map = theMap;
		}
		public function addMarker(marker:Marker):void
		{
			if (!_markers) _markers = new Array();
			_markers.push(marker);
		}
		
		public function addMarkers(markerArr:Array):void
		{
			if (!_markers) _markers = new Array();
			
			for (var i:int= 0; i<markerArr.length; i++) {
				_markers.push(markerArr[i]);
			}
			
		}
		
		override public function onStateChanged():void
		{
			
			if (_markers) {
				for (var i:int=0;i<_markers.length;i++) {
					_markers[i].state = this.state;
					
				}
			}
			
			if (_map && _map.autoAdjustPositionToShowAllMarkers) _map.setMapToShowAllMarkers(50);
		}
		/*
		private function onTapMapLegendItem(evt:UIControlEvent):void
		{
			var targetState:String = (this._state == "normal") ? "highlight" : "normal";
			this.state = targetState;
			
		}
		
		override public function set state(theState:String):void
		{
			if (this._state != theState) {
				this._state = theState;
				configureState();
				
				if (_markers) {
					for (var i:int=0;i<_markers.length;i++) {
						_markers[i].state = theState;
					}
				}
			}
		}*/
		
		

	}
	
}
