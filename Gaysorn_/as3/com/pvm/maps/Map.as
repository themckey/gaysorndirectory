﻿package com.pvm.maps {
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.display.LineScaleMode;
	import flash.display.CapsStyle;
	import flash.display.JointStyle;

	import com.greensock.*;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	
	import flash.filters.DisplacementMapFilter;
	import flash.system.LoaderContext;
	
	import flash.system.SecurityDomain;
	import flash.system.ApplicationDomain;
	import flash.net.URLLoader;
	import flash.utils.Dictionary;
	
	import com.pvm.maps.utils.PathStyle;
	import flash.display.DisplayObject;
	
	import flash.external.ExternalInterface;
	import items.WalkerMan;
	//import masterplan.Walkermen;
	import items.floorplan.Walkermen;
	
	import manager.EventManagers;
	import events.TouchScreenEvent;
	import flash.text.TextField;
	import com.forviz.ui.UITextField;
	import com.pvm.directory.manager.TouchScreenEvent;
	
	public class Map extends Sprite {
		
		private var mapMC:MapMC;
		private var _viewport:Viewport;
		
		private var _facing:String = "b";
		public var floorkey:String;
		
		
		public var pathLine:Shape = new Shape();
		
		public var walker:WalkerMan = new WalkerMan();

		public var logoMC:MovieClip;
		public var facilityMC:MovieClip;//pin edit



		//----------- CONSTANT Parameter -------------
		public var WIDTH:int;
		public var HEIGHT:int;
		public var orgX:int;
		public var orgY:int;
		private var smallestDistance:Number = 10000;
		
		//--------------- Loader ----------------------
		private var mapURLReq:URLRequest;
		public var mapLdr:Loader = new Loader();
		
		
		//------------- DisplayObject -------------------
		private var _pathStyle:PathStyle = new PathStyle(1, 0xb42300, 1); //3, 0xff0000, 1
		
		
		public var trapezoid:Shape = new Shape();
		
		public var floorplan:Sprite;
		public var nodeMap:Sprite;
		
		private var _shops_mc:Sprite;
		public var design:Sprite;
		public var unitno:Sprite;
		
		public var startNode:Node;
		public var endNode:Node;
		
		
		//---------------- Data Array -------------------
		public var nodeArray:Array = new Array();
		public var nodeDict:Dictionary = new Dictionary();

		//---------------- number -----------------------
		private var nNODE:int = 0;

		//---------------- Temp Variables for ShortestRoute -------------------
		private var smallestDistanceNode:Node;
		private var smallestNode:Node;
		private var currentNode:Node;
		private var aNode:Node;
		public var checkList:Array = new Array();
		
		public var id:String;
		public var index:int;
		
		//public var mapLoadedEvent:MapEvent = new MapEvent("mapLoaded");
		
		public var itemDataArray:Array = new Array();
		
		
		public var startLineMC:Sprite = new Sprite();
		public var endLineMC:Sprite = new Sprite();
		
		public var s:Array = new Array();
		var dx:Number;
		var dy:Number;
		var time:Number;
		public var time_factor:Number = 200;
		//public var tl:TimelineMax = new TimelineMax({paused:true, onUpdate:drawLine});
		
		public var show_unit_no:Boolean = false;
		var context:LoaderContext = new LoaderContext();
		
		private var _showNode:Boolean = false;
		
		private var _selectedItem:DisplayObject;
		
		public function Map(mapMC:MapMC, floorkey:String, url:String, nodeMapXMLPath:String = "", show_unit_no:Boolean = false) {
			
			this.mapMC = mapMC;
			
			context.checkPolicyFile = true;
			context.securityDomain = SecurityDomain.currentDomain;
			context.applicationDomain = ApplicationDomain.currentDomain;

			this.floorkey = floorkey;
			this.show_unit_no = show_unit_no;
			
			itemInitted(6);		//TEMP
			
			//setStartLineMC();
			//setEndLineMC();
			
			
			loadMap(url);
			//EventManagers.getInstance().addEventListener('tap', onTap);
			
			if (nodeMapXMLPath != "") 
			{
				//Load XML
				var xmlLoader:URLLoader = new URLLoader();
				xmlLoader.load(new URLRequest(nodeMapXMLPath));
				xmlLoader.addEventListener(Event.COMPLETE, onLoadNodeXMLComplete);
				
			}
			
		}
		
		
		
		//************************************************************
		//			       Get / Set Function 
		//************************************************************
		
		public function set viewport(theViewport:Viewport):void
		{
			if (this._viewport != theViewport)
			{
				
				this._viewport = theViewport;
				TweenMax.to(this, 1, {
							 x 			: theViewport.x,
							 y 			: theViewport.y,
							 rotationX	: theViewport.rotationX,
							 rotationY	: theViewport.rotationY,
							 rotationZ	: theViewport.rotationZ,
							 alpha		: theViewport.alpha,
							 scaleX 	: theViewport.zoom,
							 scaleY 	: theViewport.zoom,
							 alpha		: theViewport.alpha,
							 overwrite 	: 1
							 });
			}
		}
		
		public function set facing(theFacing:String):void
		{
			this._facing = theFacing;
			configureFacing();
		}
		public function showFacility(_id:String = ''):void
		{
			
			if (facilityMC) {
				
				//(facilityMC.getChildByName('_2') as MovieClip).visible = true;
				//(facilityMC.getChildByName('_2') as MovieClip).visible = true;  
				//(facilityMC.getChildByName('_2') as MovieClip).visible = true;
					//	(facilityMC.getChildByName('_5') as MovieClip).visible = true;
						//(facilityMC.getChildByName('_4') as MovieClip).visible = true;
						//(facilityMC.getChildByName('_10') as MovieClip).visible = true;
				if(_id != '')
				{
					for (var i:int=0;i<facilityMC.numChildren;i++)
					{

						
						if('_'+_id == facilityMC.getChildAt(i).name)
						{
							if((facilityMC.getChildAt(i) as MovieClip).visible == false)
							{
								if (facilityMC.getChildAt(i) is MovieClip) {
								
									(facilityMC.getChildAt(i) as MovieClip).visible = true;
							
								}
							}
							else
							{
								(facilityMC.getChildAt(i) as MovieClip).visible = false;
							}
						
	
						}
						
					}
				}
					
			}
		}
		public function onTap(e:TouchScreenEvent):void
		{
			var count:int = 0;
			var arr:Array = new Array('1','2','3','4','5','6','7','8','9','10','11');
			for (var i:int=0;i<facilityMC.numChildren;i++)
			{
				for(var n:int = 0; n<arr.length; n++)
				{
					if(e.from.name == arr[i])
					{
					
						showFacility(e.from.name);
						
						
					}
				}
				
			}
			
		}
		public function configureFacing():void
		{
			
			if (this._facing == "f") {
				this.walker.rotationX = 0;
				
				if (logoMC) {
					for (var i:int=0;i<logoMC.numChildren;i++)
					{
						
						if (logoMC.getChildAt(i) is MovieClip) {
							(logoMC.getChildAt(i) as MovieClip).rotationX = 0;
							(logoMC.getChildAt(i) as MovieClip).rotationY = 0;
							
						}
					}
				}
				if (facilityMC) {
					
					
					for (var i:int=0;i<facilityMC.numChildren;i++)
					{
						
						if (facilityMC.getChildAt(i) is MovieClip) {
							if(facilityMC.getChildAt(i).name  == '_4')
							{
								(facilityMC.getChildAt(i) as MovieClip).visible = false;
							}else{
								(facilityMC.getChildAt(i) as MovieClip).visible = true;
							
							}
							//trace (facilityMC.getChildAt(i).name , 'facilityMC.getChildAt(i).name');
							
							
							
							//(logoMC.getChildAt(i) as MovieClip).mouseEnabled = false;
						}
					}
				}
			}else if (this._facing == "b") {
				
				this.walker.rotationX = 180;
				
				if (logoMC) {
					for (var i:int=0;i<logoMC.numChildren;i++)
					{
						
						if (logoMC.getChildAt(i) is MovieClip) {
							(logoMC.getChildAt(i) as MovieClip).rotationX = 0;
							(logoMC.getChildAt(i) as MovieClip).rotationY = 0;
							
						}
					}
				}
				if (facilityMC) {
					
					
					for (var i:int=0;i<facilityMC.numChildren;i++)
					{
						
						if (facilityMC.getChildAt(i) is MovieClip) {
							(facilityMC.getChildAt(i) as MovieClip).rotationX = 0;
							(facilityMC.getChildAt(i) as MovieClip).rotationY = 0;
							
							if(facilityMC.getChildAt(i).name  == '_4')
							{
								(facilityMC.getChildAt(i) as MovieClip).visible = false;
							}else
							{
								(facilityMC.getChildAt(i) as MovieClip).visible = true;
								
							}
							
						}
						
						
					}
				}
				
			}
		}
		
		
		public function set showNode(theValue:Boolean):void
		{
			this._showNode = theValue;
			this.nodeMap.visible = theValue;
		}
		
		public function get showNode():Boolean
		{
			return this._showNode;
		}
		
		
		public function get shops_mc():Sprite
		{
			return _shops_mc;
		}
		
		public function getShopByName(theName:String):DisplayObject
		{
			//trace("getshopbyname >>>> "+theName);
			return _shops_mc.getChildByName(theName);
		}
		
		
		public function loadMap(url:String = "") {
			
			while (numChildren)
			{
				removeChildAt(0);
			}
			
			addChild(mapLdr);
			
			pathLine.graphics.lineStyle(_pathStyle.line_thickness, 
										 _pathStyle.line_color,
										 _pathStyle.line_alpha, 
										 _pathStyle.line_pixelHinting, 
										 _pathStyle.line_scaleMode, 
										 _pathStyle.line_caps, 
										 _pathStyle.line_joints, 
										 _pathStyle.line_miterLimit);
			
			addChild(pathLine);
			
			/*walker = new Sprite();
			
			walker.graphics.beginFill(0xff0000, 1);
			walker.graphics.drawEllipse(-4, -4, 10, 5);
			walker.graphics.endFill();
			*/
			// edit more walker.visible = true;
			addChild(walker);
			
			
			//addChild(trapezoid);
			addChild(startLineMC);
			addChild(endLineMC);
			startLineMC.visible = false;
			endLineMC.visible = false;
			
			//-------------- Load Swf ---------------------------
				
				//var mapURL:String = "floorplan/floorplan_" + floor + ".swf";
				var mapURL:String = url;
				//trace("url = " + url);
				mapURLReq = new URLRequest(mapURL);
				//var mapLdr:Loader = new Loader();
				mapLdr.load(mapURLReq);
				mapLdr.contentLoaderInfo.addEventListener(Event.INIT, onContentInit);
			
			//-------------- End of Load Swf --------------------------
			
		}
		
		
		private function onLoadNodeXMLComplete(evt:Event):void
		{
			var nodeMapXML:XML = new XML(evt.target.data);
			
			nodeMap = new Sprite();
			nodeMap.x = 0;
			nodeMap.y = 0;
			addChild(nodeMap);
			
			nNODE = nodeMapXML.node.length();
				
			for (var i=0;i<nNODE;i++)
			{
				nodeMap.addChild(createNode(nodeMapXML.node[i]));
			}
			nodeMap.visible = false;
			
			
			//Notify MapMC
			mapMC.loadNodeCompleteHandler(this.floorkey);
		}
		
		private function createNode(obj:Object):Node
		{
			var node:Node = new Node(obj);
			
			if (mapMC.enableHoverEffect) {
				node.addEventListener("mouseOver", onMouseOverNodeHandler);
				node.addEventListener("mouseOut", onMouseOutNodeHandler);
				node.addEventListener("click", onClickNodeHandler);
			}
			
			nodeArray.push(node);
			nodeDict[node.id] = node;
			return node;
		}
		
		public function roundDecimal(num:Number, precision:int):Number{

			var decimal:Number = Math.pow(10, precision);
			return Math.round(decimal* num) / decimal;
		}
		
		
		public function onClickNodeHandler(evt:MouseEvent):void
		{
			_selectedItem = DisplayObject(evt.target);
			//ExternalInterface.call("selectNode", _selectedItem.name, roundDecimal(_selectedItem.x, 2), roundDecimal(_selectedItem.y,2));
		}
			
		
		private function onMouseOverNodeHandler(e:Event):void
		{
			TweenMax.to(e.target, 0.3, {scaleX:2.5, scaleY:2.5, tint:0xffff00});
			nodeMap.swapChildren(e.target as Sprite, nodeMap.getChildAt(nodeMap.numChildren - 1));
		}
		
		private function onMouseOutNodeHandler(e:Event):void
		{
			TweenMax.to(e.target, 0.3, {scaleX:1, scaleY:1, removeTint:true});			
		}
		
		
		public function setStartLineMC(__x:Number = -8,__y:Number = -4, __w:Number = 16, __h:Number = 8):void
		{
			//trace("SetStartLineMC w,h = " + __w + ", " + __h);
			/*
			startLineMC.graphics.clear();
			startLineMC.graphics.beginFill(0x666666, 1);
			startLineMC.graphics.drawEllipse(__x,__y,__w,__h);
			startLineMC.graphics.endFill();
			startLineMC.visible = false;
			*/
			
		}
		
		public function setEndLineMC(__x:Number = -8,__y:Number = -4, __w:Number = 16, __h:Number = 8):void
		{
			/*
			endLineMC.graphics.clear();
			endLineMC.graphics.beginFill(0x666666, 1);
			endLineMC.graphics.drawEllipse(__x,__y,__w,__h);
			endLineMC.graphics.endFill();
			endLineMC.visible = false;
			*/
			
		}
		
		public function hideWalker():void
		{
			if (walker) walker.visible = false;
		}
		
		public function showWalker():void
		{
			if (walker) walker.visible = true;
		}
		
		/*
		public function set walker(theWalkerMC:MovieClip):void
		{
			this._walker = theWalkerMC;
		}
		
		public function get walker():Sprite
		{
			return this._walker;
		}
		*/

		//-----------------------------------  EVENT  ----------------------------------------------
		private function onContentInit(evt:Event):void
		{
		
			floorplan = Sprite(evt.target.content);			
			
			
			if (floorplan.getChildByName("nodeMap") != null) { floorplan.getChildByName("nodeMap").visible = false; }
			if (floorplan.getChildByName("pathMap") != null) { floorplan.getChildByName("pathMap").visible = false; }
			
			
			if (floorplan.getChildByName("logo") != null) {
				logoMC = MovieClip(floorplan.getChildByName("logo")); //pin edit sprite == movieclip
				
				logoMC.mouseChildren = false; //pin edit
				logoMC.mouseEnabled = false; //pin edit
				
				configureFacing();
			}
			if (floorplan.getChildByName("facility") != null) {
				facilityMC = MovieClip(floorplan.getChildByName("facility")); //pin edit sprite == movieclip
				
				facilityMC.mouseChildren = false; //pin edit
				facilityMC.mouseEnabled = false; //pin edit
				
				configureFacing();
			}
			
			if (floorplan.getChildByName("shops_mc") != null) {
				_shops_mc = Sprite(floorplan.getChildByName("shops_mc"));
				
				//_shops_mc.alpha = 0;
				if (_shops_mc != null)
				{
					
					for each (var shopMC in _shops_mc)
					{
						shopMC.alpha = 0;
						if (mapMC.enableHoverEffect) {
						
							shopMC.addEventListener("mouseOver", onMouseOverHandler);
							shopMC.addEventListener("mouseOut", onMouseOutHandler);
							
							
							
						}

						//shopMC.addEventListener("click", onClickHandler);
						//shopMC.addEventListener("mouseDown", onMouseDownHandler);
						//shopMC.addEventListener("mouseUp", onMouseUpHandler);
						//shopMC.alpha = 0;

					}
					
						
				}
			}
			
			if (floorplan.getChildByName("design") != null) {
				design = Sprite(floorplan.getChildByName("design"));
			}
			
			
			if (floorplan.getChildByName("unitno") != null)
			{
				unitno = Sprite(floorplan.getChildByName("unitno"));
				
			}
			
			//Notify MapMC
			mapMC.loadSwfCompleteHandler(this.floorkey);
			
		}
		public function changeColorMC():void
		{
			if (floorplan.getChildByName("shops_mc") != null) {
				_shops_mc = Sprite(floorplan.getChildByName("shops_mc"));
				
				
				if (_shops_mc != null)
				{
					
					for each (var shopMC in _shops_mc)
					{
						if (mapMC.enableHoverEffect) {
						
							shopMC.addEventListener("mouseOver", onMouseOverHandler);
							shopMC.addEventListener("mouseOut", onMouseOutHandler);
							
							
							
						}

						shopMC.addEventListener("click", onClickHandler);
						shopMC.addEventListener("mouseDown", onMouseDownHandler);
						shopMC.addEventListener("mouseUp", onMouseUpHandler);
						//shopMC.alpha = 0;

					}
					
						
				}
			}
			
		}
		
		public function onClickHandler(evt:MouseEvent):void
		{
			_selectedItem = DisplayObject(evt.target);
			mapMC.onClickHandler(_selectedItem);
			//ExternalInterface.call("selectUnit", _selectedItem.name, roundDecimal(_selectedItem.x, 2), roundDecimal(_selectedItem.y, 2) );
		}
		
		public function onMouseDownHandler(evt:MouseEvent):void
		{
			_selectedItem = DisplayObject(evt.target);
			mapMC.onMouseDownHandler(_selectedItem);
			//ExternalInterface.call("selectUnit", _selectedItem.name, roundDecimal(_selectedItem.x, 2), roundDecimal(_selectedItem.y, 2) );
		}
		
		public function onMouseUpHandler(evt:MouseEvent):void
		{
			if (evt.target) {
				_selectedItem = DisplayObject(evt.target);
				mapMC.onMouseUpHandler(_selectedItem);
				_selectedItem.alpha = 1;
				//ExternalInterface.call("selectUnit", _selectedItem.name, roundDecimal(_selectedItem.x, 2), roundDecimal(_selectedItem.y, 2) );
			}
		}
		
		public function onMouseOverHandler(evt:MouseEvent):void
		{
			TweenMax.to(evt.target, 0.3, {tint:0xb42300, z:-5}); //0xff0000
			if (_shops_mc)
				_shops_mc.swapChildren(evt.target as Sprite, _shops_mc.getChildAt(_shops_mc.numChildren-1));
		}
		
		public function onMouseOutHandler(evt:MouseEvent):void
		{
			TweenMax.to(evt.target, 0.3, {removeTint:true, z:0});
		}
		
		public function itemInitted(len:int):void
		{
			for (var i:int = 0;i<len;i++)
			{
				itemDataArray[i] = new Array();
			}
		}
		
		public function showUnitNo():void
		{
			if (unitno != null)
			{
				unitno.visible = true;
			}
		}
		
		
		public function set pathStyle(theStyle:PathStyle):void
		{
			
			this._pathStyle = theStyle;
			configurePathStyle();
			
			
		}
		
		private function configurePathStyle():void
		{
			
			pathLine.graphics.lineStyle(_pathStyle.line_thickness, 
										 _pathStyle.line_color,
										 _pathStyle.line_alpha, 
										 _pathStyle.line_pixelHinting, 
										 _pathStyle.line_scaleMode, 
										 _pathStyle.line_caps, 
										 _pathStyle.line_joints, 
										 _pathStyle.line_miterLimit);

		}
		
		
		//----------------------------------------------------------------------------------------
		//									PRIVATE METHOD
		//----------------------------------------------------------------------------------------
		
		
		public function clearLine():void
		{
			startLineMC.visible = false;
			endLineMC.visible = false;
			pathLine.graphics.clear();
			if (walker) walker.visible = false;
		
			configurePathStyle();
			
		}
		
		public function getNode(nodeID:String):Node
		{
			if (nodeDict != null) return nodeDict[nodeID];
			else return null;
		}
		
		
		
	}
	
	
	
}
