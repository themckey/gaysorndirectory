﻿//Node.as

package com.pvm.maps {
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.events.Event;
	
	public dynamic class Node extends Sprite
	{
		
		public var id:String;
		public var key:String;
		public var floor:Number;
		public var floorKey:String;
		public var distance:Number;
		public var previous:Node;
		public var neighbor:Array;
		public var item:Array;
		
		public var status:String;
		
		
		public function Node(obj:Object, floorKey:String = "") 
		{
			var circle:Sprite = new Sprite();
			circle.x = 0;
			circle.y = 0;
			circle.graphics.beginFill(0x66CC66, 1);
			circle.graphics.drawCircle(-3,-3,6);
			circle.graphics.endFill();
			addChild(circle);
			
			this.x = obj.x;
			this.y = obj.y;
			this.id = obj.id;
			this.floorKey = floorKey;
			this.key = floorKey + "-" + obj.id;
			
			//trace("id >>>> "+id.toString());
			//trace("obj.neighbor.split() >>> "+obj.neighbor.split(",").toString());
			
			if (obj.neighbor.toString() != "") {
				this.neighbor = obj.neighbor.split(",");
			}else{
				this.neighbor = [];
			}
			
			for (var i:int = 0;i<this.neighbor.length;i++)
			{
				this.neighbor[i] = floorKey + "-" + this.neighbor[i];
			}
			
			if ((obj.elements("item").length() != 0) && (obj.item != null))
			{
				this.item = obj.item.split(",");
				/*
				for (var it=0;it<this.item.length;it++)
				{
					
				}
				*/
			}
			
			this.mouseChildren = false;
			
			
			
		}
		
		
		public function addNeighbor(nodeKey:String)
		{
			this.neighbor.push(nodeKey);
		}
	
	
		public function setStart():void 
		{
			status = "Start";
		}
		
		public function setEnd():void
		{
			status = "End";
		}
		
		public function getInstance():Node 
		{
			return this;
		}
	}
	
}