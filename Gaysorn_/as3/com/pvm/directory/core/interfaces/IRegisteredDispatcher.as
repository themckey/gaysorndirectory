﻿package com.pvm.directory.core.interfaces
{
	import flash.events.IEventDispatcher;
	
	/**
	* Required methods for a class to register with the EventManager
	* @author Patrick Cousins    pj@pj-co.com
	*/
	public interface IRegisteredDispatcher extends IEventDispatcher
	{
		function getEvents ( ) : Array;
	}
	
}