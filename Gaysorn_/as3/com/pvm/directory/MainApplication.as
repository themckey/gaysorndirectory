﻿package com.pvm.directory {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import manager.GEDataManager;
	
	import flash.system.fscommand;
	
	
	public class MainApplication extends MovieClip {
		
		private var click_on_top_right:Boolean = false;
		private var click_on_top_left:Boolean = false;
		private var checkCloseTimer:Timer = new Timer(1000, 5);
		private var restartTimer:Timer = new Timer(1000, 5);
		public var idleTimer:Timer = new Timer(1000, 10);
		
		

		public function MainApplication() {
			// constructor code
			stage.addEventListener(MouseEvent.MOUSE_DOWN, clickOnStage); 
			checkCloseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			restartTimer.addEventListener(TimerEvent.TIMER_COMPLETE, restartApp);
			idleTimer.addEventListener(TimerEvent.TIMER_COMPLETE, showHome);
			fscommand("fullscreen", "true"); // for fullscreen exe
			
			
		}
		
		private function clickOnStage(evt:MouseEvent):void
		{
			trace("click on stage");
			trace("stage : clickonstage "+stage);
			//trace("stage.stageWidth = " +  stage.stageWidth);
			
			if ((evt.stageX >= 1820) &&(evt.stageY <= 100))
			{
				trace("click on top right");
				click_on_top_right = true;
				
			} else if ((evt.stageX <= 100) &&(evt.stageY <= 100))
			{
				trace("click on top left");
				click_on_top_left = true;
			}
			
			onCloseApplication();
			

		}
		
		private function onCloseApplication():void
		{
			
			if (click_on_top_right && click_on_top_left)
			{
				click_on_top_left = false;
				click_on_top_right = false;
				exitApp();
			}
			
			checkCloseTimer.reset();
			checkCloseTimer.start();
		}
		
		private function onTimerComplete(evt:TimerEvent):void
		{
			//trace("onTimerComplete");
			click_on_top_left = false;
			click_on_top_right = false;
		}
		
		public function restartApp(evt:Event):void
		{
			trace("");
			trace("-------");
			trace("RESTART");
			trace("-------");
			trace("");
			
			//restartitselfEvent.obj = "restartit";
			//dispatchEvent(restartitselfEvent);
			
			
			fscommand("exec","start_app.bat")
			fscommand("quit", "");
		}
		private function showHome(evt:Event):void 
		{
			//ContentCenter.getInstance().stopMedia();
			
			//MainInterfaces.getInstance().showHome();
			//ContentCenter.getInstance().openAppByString("home");
			//ContentCenter.getInstance().app_home.resetMap();
			//ContentCenter.getInstance().app_home.gotoPage(0);
			//openAppEvent.obj = "home";
			//dispatchEvent(openAppEvent);
			//dispatchEvent(showmainmenuEvent);
			//keyboard_is_show = false;
			//dispatchEvent(hideKeyboardEvent);
			idleTimer.reset();
			restartTimer.start();
			
			//dispatchEvent(showIntroEvent);
		}
		
		
		
		
		public function exitApp():void
		{
			trace("exit app");
			fscommand("quit");
		}
		//*********************************  

	}
	
}
