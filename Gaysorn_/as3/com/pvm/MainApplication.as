﻿package com.pvm {
	
	import flash.display.MovieClip;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import flash.display.StageScaleMode;
	import flash.display.StageDisplayState;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import flash.system.fscommand;
	import flash.filesystem.File;
	
	
	
	public class MainApplication extends MovieClip  {

		public const FULLSCREEN_ONACTIVATE:int = 0;
		public const FULLSCREEN_ALWAYS:int = 1;
		
		private var lastClick:int;
		
		private var click_on_box1:Boolean = false;
		private var click_on_box2:Boolean = false;
		
		private var click_on_top_right:Boolean = false;
		private var click_on_top_left:Boolean = false;
		
		
		private var checkCloseTimer:Timer = new Timer(1000, 5);
		
		var myApp:File;
		var myAppProcessStartupInfo:NativeProcessStartupInfo;
		var myAppProcess:NativeProcess;
		
		public function MainApplication() {
			// constructor code
			
			//trace("NativeApplication.nativeApplication = " + NativeApplication.nativeApplication);
			//createCloseBox();
			var appDescriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appDescriptor.namespace();
			var appCopyright:String = appDescriptor.ns::copyright;
			var appVersion:String = appDescriptor.ns::version;
			var myName:String = appDescriptor.ns::filename;
			
			myApp = File.applicationDirectory.resolvePath(myName+".exe");
			myAppProcessStartupInfo = new NativeProcessStartupInfo();
			myAppProcess = new NativeProcess();
			myAppProcessStartupInfo.executable = myApp;
			
			
			addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN, clickOnStage);
			checkCloseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			
			//addEventListener(Event.DEACTIVATE, onDeactivate); 
			//addEventListener(Event.ACTIVATE, onActivate); 
			
		}
		
		private function onAddToStage(evt:Event):void
		{
			/*
			var window:NativeWindow = stage.nativeWindow;
			window.alwaysInFront = true;
			trace("Native Window = " + window);
			
			removeEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			*/
		}
		
		public function setFullScreen(option:int = FULLSCREEN_ONACTIVATE):void
		{
			if (option == FULLSCREEN_ONACTIVATE) {
				//Always
				addEventListener(Event.DEACTIVATE, onDeactivate); 
				addEventListener(Event.ACTIVATE, onActivate); 
			
			} else if (option == FULLSCREEN_ALWAYS) {
				//Always
				addEventListener(Event.ENTER_FRAME, _onEnterFrame);
		
			}
		}
		
		public function onActivate(evt:Event):void
		{
			//trace("on Activate");
			goFullScreen();
			
		}

		public function onDeactivate(evt:Event):void
		{
			//trace("On Deactivate");			
			//goFullScreen();
			
		}
		
		public function _onEnterFrame(evt:Event):void
		{
			goFullScreen();
		}
		
		private function goFullScreen():void
		{
			if (StageDisplayState.FULL_SCREEN != stage.displayState)
			{
				try {
					stage.displayState = StageDisplayState.FULL_SCREEN;
				} catch (e:SecurityError) {
					trace("error occur");
				}
			}
		}
		
		private function clickOnStage(evt:MouseEvent):void
		{
			//trace("click on stage");
			//trace("stage.stageWidth = " +  stage.stageWidth);
			
			if ((evt.stageX >= 980) &&(evt.stageY <= 100))
			{
				trace("click on top right");
				click_on_top_right = true;
				
			} else if ((evt.stageX <= 100) &&(evt.stageY <= 100))
			{
				trace("click on top left");
				click_on_top_left = true;
			}
			
			onCloseApplication();

		}
		
		private function onCloseApplication():void
		{
			
			if (click_on_top_right && click_on_top_left)
			{
				click_on_top_left = false;
				click_on_top_right = false;
				exitApp();
			}
			
			checkCloseTimer.reset();
			checkCloseTimer.start();
		}
		
		private function onTimerComplete(evt:TimerEvent):void
		{
			//trace("onTimerComplete");
			click_on_top_left = false;
			click_on_top_right = false;
		}
		
		public function exitApp():void
		{
			trace("exit app");
			NativeApplication.nativeApplication.exit();
			//fscommand("quit", "");
		}
		
		public function restartApp(evt:Event = null):void
		{
			trace("restart app");
			//fscommand("quit", "");
			NativeApplication.nativeApplication.exit();
			myAppProcess.start(myAppProcessStartupInfo);

		}
		
		

	}
	
}
