﻿package com.pvm.ui {
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	
	import com.pvm.TouchScreenApplication;
	import com.pvm.input.keyboard.TouchScreenKeyboard;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class UIDropdown extends UIButton {

		private var popover:UIPopover;
		private var contentScrollView:UIScrollView;
		private var _searchBox:UITextField;
		private var contentSize:CGSize = new CGSize();
		
		private var _backdropArea:UIControl;
		
		private var _listItemArray:Array;
		
		private var _keyboard:TouchScreenKeyboard;
		
		private var _allowSearch:Boolean = true;
		private var _listPadding:int = 20;
		private var _listHeight:Number = 612;
		private var _inputTextFormat:TextFormat;
		private var _itemTextFormat:TextFormat;
		
		private var _placeHolderTextFormat:*;
		private var _placeHolderTextObj:Object;
		
		private var _defaultLabelText:String;
		
		private var _searchHeight:Number = 44;
		
		private var _listItemHeight:Number = 62;
		var listItemBG:Shape = new Shape();
		
			
		var listOptions:Object;
		
		public var _lang:String = 'en';
			
		
		public function UIDropdown(frame:Rectangle = null, options:Object = null) {
			
			super(frame, options);
			
			_placeHolderTextFormat = new Object();
			
			if (options) {
				
				if (options.hasOwnProperty('labelText')) {
					var obj = options.labelText;
					for (var lang:String in obj) {
						if (obj.hasOwnProperty(lang)) {
							this.setLabelTextForLang(lang, obj[lang]);
						}
					}
					
					
					if (options.hasOwnProperty('placeHolderText')) {
						
						this._placeHolderTextObj = options.placeHolderText;
						
						
					}
				}
				
				
				
				if (options.hasOwnProperty('allowSearch'))  { this.allowSearch = options['allowSearch'] }
				if (options.hasOwnProperty('inputTextFormat')) { this._inputTextFormat = options['inputTextFormat'] }
				if (options.hasOwnProperty('itemTextFormat')) { this._itemTextFormat = options['itemTextFormat'] } 
				if (options.hasOwnProperty('placeHolderTextFormat')) { this.placeHolderTextFormat = options['placeHolderTextFormat'] }
			}
			
			if (!_itemTextFormat) _itemTextFormat = new TextFormat('AvantGarde Bk BT', 22, 0x000000, null, null, null, null, null, 'left');
			
			popover = new UIPopover(new CGSize(frame.width, _listHeight));
			
			var caret:Shape = new Shape();
			caret.graphics.beginFill(0x231F20, 1);
			caret.graphics.lineStyle(1, 0x231F20, 1);
			caret.graphics.moveTo(0, 0);
			caret.graphics.lineTo(6, 7);
			caret.graphics.lineTo(12, 0);
			caret.graphics.lineTo(0, 0);
			caret.graphics.endFill();
			caret.x = frame.width - 85;
			caret.y = frame.height * 0.5 - 4;
			this.addChild(caret);
			
			
			contentScrollView = new UIScrollView();
			configureAllowSearch();
			
			contentScrollView.lockScrollHorizontal = true;
			contentScrollView.x = _listPadding;
			contentScrollView.y = _listPadding;
			
			popover.addChild(contentScrollView);
			
			
			var popover_caret:Shape = new Shape();
			popover_caret.graphics.beginFill(0x231F20, 1);
			popover_caret.graphics.lineStyle(1, 0x231F20, 1);
			popover_caret.graphics.moveTo(0, 0);
			popover_caret.graphics.lineTo(6, 7);
			popover_caret.graphics.lineTo(12, 0);
			popover_caret.graphics.lineTo(0, 0);
			popover_caret.graphics.endFill();
			popover_caret.x = .5 * frame.width - 8;
			popover_caret.y = _listHeight - 34;
			popover.addChild(popover_caret);
			
			
			this.addEventListener("tap", onTapHandler);
			
			
			
		}
		
		override protected function onConfiguredFrame(theFrame:Rectangle):void
		{
			
			_backdropArea = new UIControl(new Rectangle(0, 0, this.stage.stageWidth, this.stage.stageHeight));
			_backdropArea.graphics.beginFill(0xffffff, 0);
			_backdropArea.graphics.drawRoundRect(0, 0, this.stage.stageWidth, this.stage.stageHeight, 20, 20);
			_backdropArea.graphics.endFill();
			
			_backdropArea.visible = false;
			this.parent.addChild(_backdropArea);
			this.parent.addChild(this);
			
		}
		
		public function set defaultLabelText(value:String):void
		{
			this._defaultLabelText = value;
			this.labelText = value;
		}
		
		
		public function set allowSearch(value:Boolean):void
		{
			this._allowSearch = value;
			
			configureAllowSearch();
		}
		
		public function set keyboard(theKeyboard:TouchScreenKeyboard):void
		{
			this._keyboard = theKeyboard;
			if (!_searchBox) _searchBox.keyboard = _keyboard;
		}
		
		public function get searchBox():UITextField
		{
			return this._searchBox;
		}
		
		public function get uiPopover():UIPopover
		{
			return this.popover;
		}
		
		public function set placeHolderTextFormat(theTextFormat:*) {
			
			if (theTextFormat is TextFormat) {
				
				this._placeHolderTextFormat = theTextFormat;
				
			} else if (theTextFormat is Object) {
				
				this._placeHolderTextFormat = theTextFormat;
				/*
				for (var lang:String in theTextFormat) {
					if (theTextFormat.hasOwnProperty(lang)) {
						_placeHolderTextFormat[lang] = theTextFormat[lang];
					}
				}*/
				
			}
		}
		
		private function configureAllowSearch():void
		{
			//Create Frame
			if (_allowSearch) {
				
				var _searchBoxOptions = {'placeholder' : 'Search',
										'border_radius' : .5 * _searchHeight};
				
				if (_placeHolderTextFormat) _searchBoxOptions['placeholderTextFormat'] = _placeHolderTextFormat;
				
				
				if (_inputTextFormat) _searchBoxOptions['textFormat'] = _inputTextFormat;
				
				_searchBox = new UITextField(new Rectangle(_listPadding, _listPadding, frame.width - 2*_listPadding, _searchHeight), _searchBoxOptions);
				
				
				for (var lang:String in _placeHolderTextFormat) {
					var str:String = (_placeHolderTextObj[lang]) ? _placeHolderTextObj[lang] : 'Search';
					
					_searchBox.setPlaceholderTextForLang(lang, str, _placeHolderTextFormat[lang]);
				}
				
				if (!_keyboard) _keyboard = TouchScreenApplication.getInstance().touchScreenKeyboard;
				_searchBox.keyboard = _keyboard;
				
				_searchBox.addEventListener("valueChanged", onValueChanged);
				popover.addChild(_searchBox);
				
				//contentScrollView.frame = new Rectangle(_listPadding, _searchHeight + 2*_listPadding, frame.width - 2*_listPadding , _listHeight - 3*_listPadding - _searchHeight);
				contentScrollView.frame = new Rectangle(_listPadding, _searchHeight + 2*_listPadding, frame.width - 2*_listPadding , _listHeight - 3*_listPadding - _searchHeight );
			} else {
				contentScrollView.frame = new Rectangle(_listPadding, _listPadding, frame.width - 2*_listPadding , _listHeight - 2*_listPadding);
			
			}
		}
		
		
		
		public function set listArray(arr:Array):void
		{
			if (!listOptions) {
				
				listItemBG.graphics.beginFill(0xffffff, 0.9);
				listItemBG.graphics.drawRect(0, 0, contentScrollView.frame.width, _listItemHeight);
				listItemBG.graphics.endFill();
				
				listOptions = {
					'backgroundNormal' : listItemBG,
					'labelTextFormat' : _itemTextFormat
				};
			
			}
			
			_listItemArray = new Array();
			
			for (var i:int=0;i< arr.length; i++ ) {
				var listItem:UIButton = new UIButton(new Rectangle(0, _listItemHeight * i, contentScrollView.frame.width, _listItemHeight), listOptions);
				listItem.labelText = arr[i];
				listItem.formValue = arr[i].toLowerCase();
				listItem.addEventListener("tap", onTapListItem);
				contentScrollView.addChild(listItem);
				
				_listItemArray.push(listItem);
			}
			
			contentScrollView.contentSize = new CGSize(contentScrollView.frame.width, _listItemHeight * arr.length);
		}
		
		
		private function onTapHandler(evt:UIControlEvent):void
		{
			
			if (popover.hidden) {
				openPopover();
			} else {
				closePopover();
			}
		}
		
		public function clear():void
		{
			this.labelText = _defaultLabelText;
			this.formValue = '';
			_searchBox.value = '';
			closePopover();
			
			if (!_keyboard.hidden) _keyboard.hideMe();
		}
		
		public function openPopover():void
		{
			_backdropArea.visible = true;
			_backdropArea.addEventListener("tap", closePopover); 
			popover.presentPopoverFromRect(this.frame, this.parent);
		}
		
		private function closePopover(evt:UIControlEvent = null):void
		{
			if (!_keyboard.hidden) return;
			
			_backdropArea.visible = false;
			popover.dismissPopover();
			if (_backdropArea) {
				_backdropArea.removeEventListener("tap", closePopover); 
			}
			
			
		}
		
		private function onTapListItem(evt:UIControlEvent):void
		{
			this.formValue = ( evt.from as UIButton ).labelText
			this.labelText = ( evt.from as UIButton ).labelText;
			
			_searchBox.value = '';
			closePopover();
			
			popover.dismissPopover(); // Force popover to quit
			
			if (!_keyboard.hidden) _keyboard.hideMe();
		}
		
		var _value:String;
		
		private function onValueChanged(evt:Event):void
		{
			//Filter
			var counter:int = 0;
			
			if (searchBox.formValue != '') {
				
				_value = trim(searchBox.formValue.toLowerCase());
				
				for (var i:int=0;i<_listItemArray.length;i++) {
					if (_listItemArray[i].formValue.indexOf(_value) != -1) {
						_listItemArray[i].visible = true;
						_listItemArray[i].y = counter * _listItemHeight;
						counter++;
					} else {
						_listItemArray[i].visible = false;
					}
				}
				
			} else {
				for (var i:int=0;i<_listItemArray.length;i++) {
					_listItemArray[i].visible = true;
					_listItemArray[i].y = i * _listItemHeight;
				}
				counter = _listItemArray.length;
			}
			
			contentScrollView.contentSize = new CGSize(contentScrollView.frame.width, _listItemHeight * counter);
			contentScrollView.moveTo(new Point(0, 0));
		}
		
		function trim( s:String ):String
{		{
  			return s.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
}		}
		
		public function setPlaceholderTextForLang(lang:String, thePlaceHolder:String, textFormat:TextFormat = null):void
		{
			this._searchBox.setPlaceholderTextForLang(lang, thePlaceHolder, textFormat);
		}
		
		override public function onTranslated():void
		{
			trace("UIDROPDOWN .translate");
			if (this._searchBox) this._searchBox.lang = this.lang;
			
				
		}
		
		public function set setLang(startLang:String):void
		{
			this.lang = startLang;
			
		}
		public function set setnewtext(strnew:String):void{
			trace("strnew >>>> ",strnew);
			this.setnewword = strnew;
		}

	}
	
}
