﻿package com.pvm.ui {
	import flash.text.TextField;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	
	public class UICheckbox extends UIControl {

		/* Value */
		private var _checked:Boolean = false;
		
		private var _label:TextField;
		private var _checkbox:UIButton;
		
		private static var _stateNormal:String = '';
		private static var _stateChecked:String = '';
		private static var _labelTextFormat:TextFormat;
		
		private var _labelTextDict:Dictionary;
		private static var _labelTextFormatDict:Dictionary;
		
		private var valueChangedEvent:UIControlEvent = new UIControlEvent("valueChanged", this);
		
		public function UICheckbox(frame:Rectangle, options:Object = null) {
			// constructor code
			
			super(frame);
			
			_label = new TextField();
			
			
			if (options) {
				if (options.hasOwnProperty('labelText')) _label.text = options.labelText;
				
				if (options.hasOwnProperty('labelTextFormat')) _labelTextFormat = options.labelTextFormat;
				
				if (options.hasOwnProperty('stateNormal')) _stateNormal = options.stateNormal;
				if (options.hasOwnProperty('stateChecked')) _stateChecked = options.stateChecked;
			}
		
			
			if (_stateNormal == '') throw new ReferenceError('Please call UICheckbox.checkboxNormal = "#PATH#" to identify image for unchecked state');
			if (_stateChecked == '') throw new ReferenceError('Please call UICheckbox.checkboxActive = "#PATH#" to identify image for checked state');
			
			_checkbox = new UIButton(new Rectangle(0, .5 *(frame.height - 50), 60, 60), {
									 'backgroundNormal' : _stateNormal,
									 'backgroundActive' : _stateChecked
									 });
			addChild(_checkbox);
			
			_label.x = 62;
			_label.width = frame.width - 30;
			_label.multiline = false;
			_label.embedFonts = true;
			_label.selectable = false;
			_label.setTextFormat(_labelTextFormat);
			addChild(_label);
			
			configurePosition();
			
			this.unmask = true;
			
			this.addEventListener("tap", onTapHandler);
			
			
		}
		
		public function get checkbox():UIButton
		{
			return this._checkbox;
		}
		
		
		public function get labelText():TextField
		{
			return this._label;
		}
		
		public static function set stateNormal(src:String):void
		{
			_stateNormal = src;
		}
		
		public static function set stateChecked(src:String):void
		{
			_stateChecked = src;
		}
		
		public static function set labelTextFormat(tf:TextFormat):void
		{
			_labelTextFormat = tf;
		}
		
		
		public static function setLabelTextFormatForLang(lang:String, _textFormat:TextFormat):void
		{
			if (!_labelTextFormatDict) _labelTextFormatDict = new Dictionary();
			
			_labelTextFormatDict[lang] = _textFormat;
		}
		
		


		/*public function setLabelTextFormatForLang(lang:String, _textFormat:TextFormat):void
		{
			if (!_labelTextFormatDict) _labelTextFormatDict = new Dictionary();
			
			_labelTextFormatDict[lang] = _textFormat;
		}*/

		public function setLabelTextForLang(lang:String, _text:String):void
		{
			if (!_labelTextDict) _labelTextDict = new Dictionary();
			_labelTextDict[lang] = _text;
			
		}
		
		override public function get formValue():String
		{
			return _checked;
		}
		
		public function set checked(value:Boolean):void
		{
			this._checked = value;
			
			if (_checked) {
				_checkbox.state = "active";
			} else {
				_checkbox.state = "normal";
			}
			
			dispatchEvent(valueChangedEvent);
		}
		
		public function get checked():Boolean {
			return this._checked;
		}
		
		public function configurePosition():void
		{
			_label.y = 0.5 * (this.frame.height - _label.textHeight) - 2;
			
			if (lang == 'th') {
				_label.y = _label.y - .05 * _label.textHeight;
			} else {
			
			}
		}
		
		private function onTapHandler(evt:UIControlEvent):void
		{
			if (!_checked) {
				_checkbox.state = "active";
			} else {
				_checkbox.state = "normal";
			}
			
			_checked = !_checked;
			
			dispatchEvent(valueChangedEvent);
		}
		
		override public function translate():void
		{
			if (_labelTextDict && _labelTextDict[lang]) {
				_label.text = _labelTextDict[lang];
				if ( _labelTextFormatDict[lang] ) _label.setTextFormat(_labelTextFormatDict[lang]);
				configurePosition();
			}
			
			if(this.lang == 'ru' || this.lang == 'th')
			{
				_label.embedFonts = false;
			}
		}
		

	}
	
}
