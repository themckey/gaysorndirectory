﻿/* 
 * UIButton version 1.0.1
 * By Pitipat Srichairat
 * Last updated Christmas Eve, 2012
 *******************************/
package com.pvm.ui
{
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.filters.DropShadowFilter;
	import flash.display.GradientType;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.text.TextFormat;

	import flash.utils.getQualifiedClassName;

	import com.pvm.ui.BitmapHolder;


	public class UIButton extends UIControl
	{

		//private var _frame:Rectangle = new Rectangle(0, 0, 80, 80);
		private var _frame:Rectangle;

		private var _theme:String = "light";
		private var _type:String = "default";
		private var _layout:String = "text";

		private var _state:String = "normal";

		private var _titleTF:TextField;
		private var _labelTextFormat:TextFormat;
		private var _labelTextFormatTouchDown:TextFormat;
		private var _labelTextFormatActive:TextFormat;
		

		private var _padding:Number = 10;

		private var _icon:BitmapHolder;

		private var _backgroundMC:Sprite;

		private var _bg:Sprite;
		private var _active_bg:Sprite;

		public var index:int;

		var matrix:Matrix = new Matrix();


		var dropShadow:DropShadowFilter = new DropShadowFilter(0,0,0x000000,0.3,8,8,1,1,true);


		public function UIButton(theFrame:Rectangle = null, options:Object = null)
		{
			// constructor code
			this.mouseChildren = false;

			/*
			if (frame) {
			this._frame = frame;
			this.x = frame.x;
			this.y = frame.y;
			configureView();
			}*/
			if (theFrame)
			{
				this.frame = theFrame;
				_frame = this.frame;
				//this.x = _frame.x;
				//this.y = _frame.y;
			}

			if (options)
			{
				if (options.hasOwnProperty('backgroundNormal'))
				{
					this.background = options.backgroundNormal;
				}
				if (options.hasOwnProperty('backgroundActive'))
				{
					this.backgroundActive = options.backgroundActive;

				}
			}

			if (this._type == "default" && this.numChildren == 0)
			{
				configureView();



			}
		}

		public function set type(theType:String):void
		{
			this._type = theType;

			configureView();
		}


		public function set theme(theTheme:String):void
		{
			this._theme = theTheme;

			configureView();
		}

		public function set background(theBG:*):void
		{
			/*
			this._backgroundMC = theBG;
			this._bg = theBG as Sprite;
			addChild(_bg);
			*/
			_bg = setBackground(theBG);
			addChild(_bg);

			this._type = 'custom';
		}



		public function set backgroundActive(theBG:*):void
		{
			/*
			this._active_bg = theBG as Sprite;
			_active_bg.visible = false;
			addChild(_active_bg);
			*/
			_active_bg = setBackground(theBG);
			addChild(_active_bg);

		}


		private function setBackground(obj:*):Sprite
		{
			var theBG:Sprite;

			if (obj.hasOwnProperty("stage"))
			{
				//Given Sprite, MovieClip, Bitmap, or other DisplayObject
				theBG = obj;
				return theBG;

			}
			else if (getQualifiedClassName(obj) == "String")
			{
				//Given URL to image
				var bh:BitmapHolder = new BitmapHolder();
				bh.setSize(frame.width, frame.height);
				bh.loadImage(obj);
				return bh;

			}
			else
			{
				throw new ReferenceError("Error: background must be either , DisplayObject or String");
				return null;
			}


		}

		private function configureView():void
		{
			if (_bg)
			{
				_bg.graphics.clear();
			}

			switch (this._type)
			{
				case 'default' :
					if (! _bg)
					{
						createDefaultButtonGraphics();
					}
					break;

			}


		}

		private function createDefaultButtonGraphics():void
		{
			switch (this._theme)
			{
				case 'light' :

					/* TextFormat */
					_labelTextFormat = new TextFormat( );
					_labelTextFormat.bold = false;
					_labelTextFormat.color = 0x555555;
					_labelTextFormat.align = "center";
					_labelTextFormat.font = "Arial";
					_labelTextFormat.size = 18;

					matrix.createGradientBox(this._frame.width, this._frame.height, Math.PI/2, 25, 0);

					if (! _bg)
					{
						_bg = new Sprite();
					}

					_bg.graphics.clear();
					_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xFFFFFF, 0xE4E4E4], [1, 1], [50, 205], matrix);
					_bg.graphics.lineStyle(1, 0xcccccc, 1, true);
					_bg.graphics.drawRoundRect(0,0,this._frame.width, this._frame.height, 16);
					_bg.graphics.endFill();
					addChild(_bg);

					_bg.filters = [dropShadow];


					if (! _active_bg)
					{
						_active_bg = new Sprite();
					}

					_active_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xC9C9C9, 0xDEDEDE], [1, 1], [50, 205], matrix);
					_active_bg.graphics.lineStyle(1, 0xcccccc, 1, true);
					_active_bg.graphics.drawRoundRect(0,0,this._frame.width, this._frame.height, 16);
					_active_bg.graphics.endFill();
					addChild(_active_bg);
					_active_bg.visible = false;

					_active_bg.filters = [dropShadow];



					break;

				case 'dark' :

					/* TextFormat */
					_labelTextFormat = new TextFormat( );
					_labelTextFormat.bold = false;
					_labelTextFormat.color = 0xffffff;
					_labelTextFormat.align = "center";
					_labelTextFormat.font = "Arial";
					_labelTextFormat.size = 18;

					matrix.createGradientBox(this._frame.width, this._frame.height, Math.PI/2, 25, 0);

					if (! _bg)
					{
						_bg = new Sprite();
					}

					_bg.graphics.clear();
					_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xFFFFFF, 0xE4E4E4], [0, 0.1], [50, 205], matrix);
					_bg.graphics.lineStyle(1, 0xffffff, 1, true);
					_bg.graphics.drawRoundRect(0,0,this._frame.width, this._frame.height, 16);
					_bg.graphics.endFill();
					addChild(_bg);

					_bg.filters = [dropShadow];


					if (! _active_bg)
					{
						_active_bg = new Sprite();
					}

					_active_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xC9C9C9, 0xDEDEDE], [0, 0.1], [50, 205], matrix);
					_active_bg.graphics.lineStyle(1, 0xffffff, 1, true);
					_active_bg.graphics.drawRoundRect(0,0,this._frame.width, this._frame.height, 16);
					_active_bg.graphics.endFill();
					addChild(_active_bg);
					_active_bg.visible = false;

					_active_bg.filters = [dropShadow];


					

					break;
			}
		}

		public function set layout(theLayout:String):void
		{
			this._layout = theLayout;
			configurePosition();
		}
		
		public function set title(theTitle:String):void
		{
			_titleTF.text = theTitle;
			_titleTF.setTextFormat(_labelTextFormat);

			configurePosition();
		}

		public function set icon(theIcon:*):void
		{
			//if thePrev is String
			if (getQualifiedClassName(theIcon) == "String")
			{
				//Given URL to image
				_icon = new BitmapHolder();
				_icon.setup(0.5 * this._frame.width, 0.5 * this._frame.height);
				_icon.loadImage(theIcon);
				addChild(_icon);


				configurePosition();
				return;
			}

			//if thePrev is DisplayObject
			if (theIcon.hasOwnProperty('stage'))
			{
				//Given Sprite, MovieClip, Bitmap, or other DisplayObject

				this.addChild(theIcon);
				configurePosition();
				return;
			}

			throw new ReferenceError("Error: UIButton btnPrev must be UIButton, DisplayObject or String");

		}

		private function configurePosition():void
		{
			switch (this._layout)
			{
				case 'icon_text' :
					if (_icon)
					{
						_icon.visible = true;
						_icon.x = 0.25 * this._frame.width;
						_icon.y = 0.25 * this._frame.height;
					}

					if (_titleTF)
					{
						_labelTextFormat.size = 14;
						_titleTF.setTextFormat(_labelTextFormat);

						_titleTF.visible = true;
						_titleTF.y = this._frame.height - _titleTF.textHeight;

					}
					break;

				case 'text' :
					if (_icon)
					{
						_icon.visible = false;
					}
					if (_titleTF)
					{
						_titleTF.visible = true;
						_titleTF.y = 0.5 * (this._frame.height - _titleTF.textHeight) - 3;

					}
					break;

				case 'icon' :
					if (_icon)
					{
						_icon.visible = true;
						_icon.x = 0.25 * this._frame.width;
						_icon.y = 0.25 * this._frame.height;
					}

					if (_titleTF)
					{
						_titleTF.visible = false;
					}

					break;
			}
		}


		override public function set state(theState:String):void
		{
			if (this._state != theState)
			{
				this._state = theState;
				configureState();
			}
		}
		

		public function configureState():void
		{
			switch (this._state)
			{
				case 'normal' :
					if (_bg)
					{
						_bg.visible = true;
					}
					if (_active_bg)
					{
						_active_bg.visible = false;
					}
					
					if (this._titleTF) this._titleTF.setTextFormat(_labelTextFormat);

					this.alpha = 1;
					this.mouseEnabled = true;

					break;

				case 'disabled' :
					this.alpha = 0.2;
					this.mouseEnabled = false;
					break;

				case 'touchDown' :
					this.alpha = 0.5;
					//if (this._titleTF) this._titleTF.setTextFormat(_labelTextFormatTouchDown);
					break;

				case 'highlight' :
				case 'active' :
					if (_bg)
					{
						_bg.visible = false;
					}
					if (_active_bg)
					{
						_active_bg.visible = true;
					}
					if (this._titleTF) this._titleTF.setTextFormat(_labelTextFormatActive);
					
					break;
			}


		}
		
		public function set labelText (theTitle:String):void
		{
			if (! _titleTF){
				_titleTF = new TextField();
				addChild(_titleTF);
			}
			
			_titleTF.x = _padding;
			_titleTF.width = this._frame.width - 2 * _padding;
			_titleTF.text = theTitle;
			
			_titleTF.setTextFormat(_labelTextFormat);
			
			configurePosition();
		}
		
		public function get labelText ():String
		{
			return this._titleTF.text;
		}
		
		public function set labelTextFormat (theTF:TextFormat):void
		{
			//Force align Cetner
			if (theTF.align != "center") theTF.align = "center";
			_labelTextFormat = theTF;
			
		}
		
		public function get labelTextFormat ():TextFormat
		{
			return _labelTextFormat;
		}
		
		
		
		public function set labelTextFormatTouchDown (theTF:TextFormat):void
		{
			//Force align Cetner
			if (theTF.align != "center") theTF.align = "center";
			_labelTextFormatTouchDown = theTF;
			
		}
		
		public function get labelTextFormatActive ():TextFormat
		{
			return _labelTextFormatActive;
		}
		
		public function set labelTextFormatActive (theTF:TextFormat):void
		{
			//Force align Cetner
			if (theTF.align != "center") theTF.align = "center";
			_labelTextFormatActive = theTF;
			
		}
		
		public function get labelTextFormatTouchDown ():TextFormat
		{
			return _labelTextFormatTouchDown;
		}
		
		public function reformatLabelText():void
		{
			this._titleTF.setTextFormat(_labelTextFormat);
		}

		/*
		public function addBTNLabel(theColor:uint, theFontsize:Number, theTitle:String = "", theFont:String = "")
		{
			_labelTextFormat = new TextFormat( );
			_labelTextFormat.bold = false;
			_labelTextFormat.color = theColor;
			_labelTextFormat.align = "center";
			_labelTextFormat.font = theFont;
			_labelTextFormat.size = theFontsize;


			_titleTF = new TextField();
			_titleTF.text = theTitle;
			addChild(_titleTF);
		}*/
	}

}