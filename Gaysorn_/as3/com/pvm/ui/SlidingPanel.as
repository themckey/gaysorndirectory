﻿/*
SlidingPanel.as
-------------------------------------------------------------------------
version 		: 1.0
Created by		: Pitipat Srichairat

*/

package com.pvm.ui {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.events.Event;

	
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.display.Sprite;
	
		
	public dynamic class SlidingPanel extends Sprite 
	{
		
		public var circular:Boolean = false;
		public var auto:Boolean = false;
		
		
		public var mask_mc:Sprite = new Sprite();
		public var touchArea:Sprite = new Sprite();
		
		var title:String = "Page";
		public var title_en:String;
		public var title_th:String;
		
		public var _content:Sprite;
		
		public var scrX:Boolean = false;
		public var scrY:Boolean = true;
		
		public var enableSnapping:Boolean = false;
		public var snappingSpan:int = 250;
		
		public var snapToGrid:Boolean = false;
		public var gridSpan:int = 30;
		
		public var scrEnable:Boolean = true;
		var no:Number = 0;
		
		public var pageH:Number;
		public var pageW:Number;
		
		public var contentH:Number;
		public var contentW:Number;
		
		public var stageH:Number;
		public var stageW:Number;
		
		//Scrolling Variable
		public var scrolling	:Boolean = true;
		public var dir:String = "";
		
		
		protected var orgY		:Number = 0;
		var pressY		:Number = 0;
		var releaseY	:Number = 0;
		public var targetY		:Number = 0;
		var diffY		:Number = 0;
		var lastY		:Number = 0;
		var lastY2		:Number = 0;
		var lastY3		:Number = 0;
		var veloY		:Number = 0;
		var diff2TargetY :Number = 0;
		
		protected var orgX		:Number = 0;
		var pressX		:Number = 0;
		var releaseX	:Number = 0;
		public var targetX		:Number = 0;
		var diffX		:Number = 0;
		var lastX		:Number = 0;
		var lastX2		:Number = 0;
		var lastX3		:Number = 0;
		var veloX		:Number = 0;
		var diff2TargetX :Number = 0;
		
		var drag		:Boolean = false;
		public var calculate	:Boolean = false;
		
		public var SCROLL_UPBOUND:Number = 0;
		public var SCROLL_LOWBOUND:Number = 0;
		public var SCROLL_LEFTBOUND:Number = 0;
		public var SCROLL_RIGHTBOUND:Number = 0;
		
		public var TRESHOLD	:Number = 1;
		
		var MOVESPAN	:Number = 640;
		
		//Scroll Variable
		public var easingPower = 3;
		public var easingHolder = 5;
		
		public var slot_machine:Boolean = false;
		public var slot_area:Array = [100, 150];
		
		
		public function SlidingPanel()
		{
			this.scrEnable = scrEnable;
			
			
		}

//============================PRIVATE FUNCTIONS=================================================
		
		
		
//============================EVENTS============================================================	
		
		
		
//============================PUBLIC FUNCTIONS==================================================
		public function setOrientation(dir:String):void
		{
			if (dir == "x") {
				this.scrX = true;
				this.scrY = false;
			} else if (dir == "y") {
				this.scrY = true;
				this.scrX = false;
			} else if (dir == "xy") {
				this.scrX = true;
				this.scrY = true;
			}
			
			this.dir = dir;
		}
		
		public function enableClick(flag:Boolean):void
		{
			this.mouseEnabled = flag;
		}
		
		
		
		
		public function resetScrollParameter():void
		{
			//Reset value
			
			orgX		 = 0;
			pressX		 = 0;
			releaseX	 = 0;
			targetX		 = 0;
			diffX		 = 0;
			lastX		 = 0;
			lastX2		 = 0;
			lastX3		 = 0;
			veloX		 = 0;
			drag		 = false;
			calculate	 = false;
			
						
			orgY		 = 0;
			pressY		 = 0;
			releaseY	 = 0;
			targetY		 = 0;
			diffY		 = 0;
			lastY		 = 0;
			lastY2		 = 0;
			lastY3		 = 0;
			veloY		 = 0;
			drag		 = false;
			calculate	 = false;
			
			if (scrX)
			{
				TweenLite.to(_content, 0.5, {x:0});
			}
			
			if (scrY)
			{
				TweenLite.to(_content, 0.5, {y:0});
			}
			
		}
		
		
		
		public function setScroll(dir:String, board:Sprite = null, stageH:Number = 0, stageW:Number = 0, pageH:Number = 0, pageW:Number = 0, allow_mask:Boolean = true):void
		{
			setOrientation(dir);
			
			this.stageH = stageH;
			this.stageW = stageW;
			
			if (board != null)
			{ this._content = board; } else { this._content = this; }
			
			_content.graphics.beginFill(0x00ff00, 0); //set color old alpha = 0.5
			_content.graphics.drawRect(0,0, pageW, pageH);
			_content.graphics.endFill();
			
			trace("SlidingPanel.SetScroll");
			/*
			touchArea.x = _content.x;
			touchArea.y = _content.y;
			touchArea.graphics.beginFill(0x336699, 0);
			touchArea.graphics.drawRect(0,0,pageW,pageH);
			touchArea.graphics.endFill();
			_content.addChild(touchArea);
			_content.swapChildren(touchArea, _content.getChildAt(0));
			*/
			
			if (allow_mask)
			{
				mask_mc.x = _content.x;
				mask_mc.y = _content.y;
				mask_mc.graphics.beginFill(0xffff00, 1); // set color
				mask_mc.graphics.drawRect(0,0,stageW,stageH);
				mask_mc.graphics.endFill();
				addChild(mask_mc);
				_content.mask = mask_mc;
			}
			
			//trace("stageH, stageW = " + stageH + ", " + stageW);
			//trace("boardH, boardW = " + board.height + ", " + board.width);
			
			
			if (pageH != 0)
			{
				contentH = pageH;
			} else {
				contentH = board.height;
			}
			
			if (pageW != 0)
			{
				contentW = pageW;
			} else {
				contentW = board.width;
			}
			
			trace("dir = " + dir);
			trace("contentW = " + contentW);
			trace("stageW = " + stageW);
			
			
			if (dir == "x")
			{
				if ((contentW < stageW) || (scrEnable == false))
				{
					scrolling = false;
					scrX = false;
					
					
					/*if (contentW < stageW)
					{
						//Set Center
						_content.x = 0.5 * (stageW - contentW);
						
						//trace("contentX = " + _content.x);
					}*/
					
				} else {
					scrolling = true;
					scrX = true;
					resetScrollParameter();
				}
			} else if (dir == "y")
			{
				if ((contentH < stageH) || (scrEnable == false))
				{
					scrolling = false;
					scrY = false;
					
					//trace ("content H ================================================ "+contentH);
					//trace ("stage H ================================================== "+stageH);
					
					if (contentH < stageH)
					{
						//Set Center
						_content.y = 0.5 * (stageH - contentH);
						
						//trace("contentX = " + _content.x);
					}
					
				} else {
					scrolling = true;
					scrY = true;
					resetScrollParameter();
				}
			}
			
			if (!circular)
			{
				if (scrY)
				{
					SCROLL_UPBOUND = 0;
					SCROLL_LOWBOUND = stageH - contentH; //
					
				}
				
				if (scrX) {
					SCROLL_LEFTBOUND = 0;
					SCROLL_RIGHTBOUND = stageW - contentW;
				}
			}
			
			
			trace("SetScroll : LEFTBOUND, RIGHTBOUND = " + SCROLL_LEFTBOUND + ", " + SCROLL_RIGHTBOUND);
			trace("UPBOUND, LOWBOUND = " + SCROLL_UPBOUND + ", " + SCROLL_LOWBOUND);
			trace("scrolling = " + scrolling);
			
			
			if (scrolling)
			{
				
				//Maybe I should move this addListener to when page is loaded instead, save resources.
				_content.addEventListener(MouseEvent.MOUSE_DOWN, _onMouseDown);
				
				
			} else {
				_content.removeEventListener(MouseEvent.MOUSE_DOWN, _onMouseDown);
				
			}
			
			if (auto)
			{
				stage.addEventListener(Event.ENTER_FRAME, scr);
			}
			
			
		}
		
		
		public function setContentWidth(cWidth:Number):void
		{
			contentW = cWidth;
			
			if ((contentW < stageW) || (scrEnable == false))
			{
				scrolling = false;
				scrX = false;
			} else {
				scrolling = true;
				scrX = true;
			}
			
			if (scrX) { // change scroll x
				SCROLL_LEFTBOUND = 0;
				SCROLL_RIGHTBOUND = stageW - contentW; //1920 - contentW
			}
			
			
			
		}
		
		public function setContentHeight(cHeight:Number):void
		{
			contentH = cHeight;
			
			if ((contentH < 1080) || (scrEnable == false))
			{
				scrolling = false;
				scrY = false;
			} else {
				scrolling = true;
				scrY = true;
			}
			
			if (scrY) // set scroll y
			{
				SCROLL_UPBOUND = 0;
				SCROLL_LOWBOUND = 1080 - contentH; //stageH - contentH
				trace ("contentH >>>>>>>>>>>>>>>>>>>>>>>>>>> "+contentH.toString());
				
			}
			_content.y = 0;
		}
		
		public function moveSpan(_dire:int):void
		{
			calculate = false;
			//stage.removeEventListener(Event.ENTER_FRAME, scr);
			
			var targetY:Number = this.y - _dire * MOVESPAN; 
			var targetX:Number = this.x - _dire * MOVESPAN;
			
			if (targetY > SCROLL_UPBOUND) {
				targetY = SCROLL_UPBOUND;
				//trace ("targetY upbound >>>>>>>>>>>>>>>>>>>>>>>  "+targetY.toString());
			} else if (targetY < SCROLL_LOWBOUND) {
				targetY = SCROLL_LOWBOUND;
				//trace ("targetY lowbound >>>>>>>>>>>>>>>>>>>>>>>  "+targetY.toString());
			} 
			orgX = targetX;
			orgY = targetY;
			
			TweenLite.to(_content, 0.2, {y:targetY, ease:Expo.easeOut});
			TweenLite.to(_content, 0.2, {x:targetX, ease:Expo.easeOut});
			
		}
		
		private function _onMouseDown(evt:MouseEvent):void
		{
			
			auto = false;
			
			drag = true;
			pressX = stage.mouseX;
			pressY = stage.mouseY;
			diffX = _content.x - stage.mouseX;
			diffY = _content.y - stage.mouseY;
			calculate = true;
			
			lastX = pressX;
			lastX2 = pressX;
			lastX3 = pressX;
			
			lastY = pressY;
			lastY2 = pressY;
			lastY3 = pressY;
			
			trace("Press stage.mouseX = " + stage.mouseX);
			//trace(SCROLL_UPBOUND + " : " + SCROLL_LOWBOUND);
			
			
			stage.addEventListener(MouseEvent.MOUSE_UP, _onMouseUp);
			
			stage.addEventListener(Event.ENTER_FRAME, scr);
			
			
		}
		
		private function _onMouseOut(evt:MouseEvent):void
		{
			_onMouseUp(null);
		}
		
		private function _onMouseUp(evt:MouseEvent):void
		{
			drag = false;
			releaseX = stage.mouseX;
			releaseY = stage.mouseY;
			
			//trace("Release stage.mouseX = " + stage.mouseX);
			//trace("lastX3 = " + lastX3);
			//trace("veloX = " + veloX);
			
			//Get speed of last stroke
			veloX = releaseX - lastX3;
			veloY = releaseY - lastY3;
			
			//Calculate targetY for inertia movement
			
			if (!slot_machine)
			{
				if(enableSnapping)
				{
					if (pressX - releaseX < -100)
					{
						targetX = orgX + snappingSpan;
						
					} else if (pressX - releaseX > 100)
					{
						targetX = orgX - snappingSpan;
					}
					
					
				} else {
					targetX = _content.x + veloX * easingPower;
					targetY = _content.y + veloY * easingPower;
				}
				
				if (!circular)
				{
					if (scrX)
					{
						if (targetX > SCROLL_LEFTBOUND) {
							targetX = SCROLL_LEFTBOUND;
						} else if (targetX < SCROLL_RIGHTBOUND) {
							targetX = SCROLL_RIGHTBOUND;
						}
					}
					
					if (scrY)
					{
						if (targetY > SCROLL_UPBOUND) {
							targetY = SCROLL_UPBOUND;
						} else if (targetY < SCROLL_LOWBOUND) {
							targetY = SCROLL_LOWBOUND;
						}
					}
				}
				
				orgX = targetX;
				orgY = targetY;
				
				if (scrX)
				{
					onAnimationBegin(targetX);
				}
				if (scrY)
				{
					onAnimationBegin(targetY);
				}
			
			} else {
				
				
			}
			
			onMouseRelease(evt);
			
			//trace("targetY:onMouseUp = " + targetY);
			
			stage.removeEventListener(MouseEvent.MOUSE_UP, _onMouseUp);
		}
		
		public function addScrEvent():void
		{
			 stage.addEventListener(Event.ENTER_FRAME, scr);
		}
		
		public function moveToX(posX:Number):void
		{
			targetX = posX;
			calculate = true;
			stage.addEventListener(Event.ENTER_FRAME, scr);
			
			
		}
		
		public function moveToY(posY:Number):void
		{
			targetY = posY;
			//trace("moveToY = " + posY);
			calculate = true;
			stage.addEventListener(Event.ENTER_FRAME, scr);
			
			
		}
		
		private function scr(evt:Event):void
		{
			
			if (calculate)
			{
				
				//trace("targetY:Before = " + targetY);
				
				if (drag)
				{
					if (scrY)
					{
						targetY = stage.mouseY + diffY;
					
						//Get last Y for Super Easing
						lastY3 = lastY2;
						lastY2 = lastY;
						lastY = stage.mouseY;
					}
					
					if (scrX)
					{
						targetX = stage.mouseX + diffX;
					
						//Get last Y for Super Easing
						lastX3 = lastX2;
						lastX2 = lastX;
						lastX = stage.mouseX;
					}
				} else {
					//No drag
					if (!circular)
					{
						
					
						if (targetX > SCROLL_LEFTBOUND) {
							targetX = SCROLL_LEFTBOUND;
						} else if (targetX < SCROLL_RIGHTBOUND) {
							targetX = SCROLL_RIGHTBOUND;
						} else {
							
							if (snapToGrid)
							{
								targetX = Math.floor(targetX / gridSpan) + 0.5*gridSpan;
								
							}
							
						}
						
						
						if (targetY > SCROLL_UPBOUND) {
							targetY = SCROLL_UPBOUND;
						} else if (targetY < SCROLL_LOWBOUND) {
							targetY = SCROLL_LOWBOUND;
						} else {
							
							if (snapToGrid)
							{
								targetY = Math.floor(targetY/ gridSpan) * gridSpan;
								
							}
							
						}
					
					}
					
					
				}
				
				
				//trace("auto = " + auto + " : targetX = " + targetX);
				
				if (scrX)
				{
					_content.x += (targetX - _content.x) / easingHolder;
					onMove(_content.x);
					
				}
				if (scrY)
				{
					_content.y += (targetY - _content.y) / easingHolder;
					onMove(_content.y);
				}
				
				diff2TargetX = _content.x - targetX;
				diff2TargetY = _content.y - targetY;
				
				//---------
				
				if (drag == false)
				{
					
					if (scrY)
					{
						if ((diff2TargetY <= TRESHOLD) && (diff2TargetY >= -TRESHOLD))
						{
							
							calculate = false;
							onAnimationComplete(_content.y);
							stage.removeEventListener(Event.ENTER_FRAME, scr);
						}
					}

					if (scrX)
					{
						if ((diff2TargetX <= TRESHOLD) && (diff2TargetX >= -TRESHOLD))
						{
							
							calculate = false;
							onAnimationComplete(_content.x);
							stage.removeEventListener(Event.ENTER_FRAME, scr);
						}
					}
				
				}
				
				
				
			}

			
		}
		
		public function onMouseRelease(evt:MouseEvent):void
		{
			
		}
		
		public function onMove(_contentX:Number):void
		{
			
					
		}
		
		public function onAnimationBegin(pos:Number):void
		{
			//trace("onAnimationBegin targetX = " + pos);
		}
		
		public function onAnimationComplete(pos:Number):void
		{
			//trace("onComplete pos = " + pos);
			
		}
		
		
//============================SETTERS \ GETTERS==================================================
	}
}