﻿/* 
 * UIButton version 1.0.1
 * By Pitipat Srichairat
 * Last updated Christmas Eve, 2012
 *******************************/
package com.pvm.ui
{
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.filters.DropShadowFilter;
	import flash.display.GradientType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	
	import flash.events.TimerEvent;
	
	import flash.utils.Timer;
	import flash.utils.getQualifiedClassName;
	
	import com.greensock.TweenMax;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.GlowFilterPlugin;
	
	import com.pvm.ui.BitmapHolder;
	import com.pvm.ViewController;
	import flash.utils.Dictionary;
	import flash.display.Shape;


	public class UIButtonSprite extends UIControlSprite
	{

		private var _theme:String = "light";
		private var _type:String = "default";
		private var _layout:String = "text";
		
		private var _paddingX:Number = 0;

		
		protected var _labelText:Dictionary = new Dictionary();
		private var _labelTextFormat:Dictionary = new Dictionary();
		
		protected var _titleTF:TextField;
		private var _labelTextFormatNormal:TextFormat;
		private var _labelTextFormatTouchDown:TextFormat;
		private var _labelTextFormatActive:TextFormat;
		
		protected var _titleTFOrignialY:int = 0;

		private var _options:Object = {};
		private var _padding:Number = 10;

		private var _icon:BitmapHolder;

		private var _backgroundMC:Sprite;

		private var _bg:Sprite;
		private var _active_bg:Sprite;
		
		private var _targetViewController:ViewController;

		public var index:int;
		public var key:String;
		public var obj:Object;

		var matrix:Matrix = new Matrix();

		private var onStateChangedEvent:UIControlEvent = new UIControlEvent("stateChanged");

		var dropShadow:DropShadowFilter = new DropShadowFilter(0,0,0x000000,0.3,8,8,1,1,true);
		
		TweenPlugin.activate([GlowFilterPlugin]);


		public function UIButtonSprite(theFrame:Rectangle = null, options:Object = null)
		{
			// constructor code
			this.mouseChildren = true;

			super();
			//if (theFrame) this.frame = theFrame;


			_labelTextFormatNormal = new TextFormat( );
			_labelTextFormatNormal.bold = false;
			_labelTextFormatNormal.color = 0x555555;
			_labelTextFormatNormal.align = "center";
			_labelTextFormatNormal.font = "Arial";
			_labelTextFormatNormal.size = 18;
					
			if (options)
			{
				_options = options;
				
				if (options.hasOwnProperty('backgroundNormal')) { 
					this.backgroundNormal = options.backgroundNormal; 
				} else { 
					var clearBackground:Sprite = new Sprite();
					clearBackground.graphics.beginFill(0xffffff, 0);
					clearBackground.graphics.drawRect(0, 0, frame.width, frame.height);
					clearBackground.graphics.endFill();
					this.backgroundNormal = clearBackground;
			
				}
				if (options.hasOwnProperty('backgroundActive')) {
					this.backgroundActive = options.backgroundActive;
				}
				
				if (options.hasOwnProperty('padding')) this.padding = options.padding;
				if (options.hasOwnProperty('labelTextFormat')) this.labelTextFormat = options.labelTextFormat;
				if (options.hasOwnProperty('labelTextFormatActive')) this.labelTextFormatActive = options.labelTextFormatActive;
				
				if (options.hasOwnProperty('labelText')) this.labelText = options.labelText;
				
				if (options.hasOwnProperty('targetViewController')) this.targetViewController = options.targetViewController;

			}

			if (this._type == "default" && this.numChildren == 0)
			{
				configureView();



			}
		}

		public function set type(theType:String):void
		{
			this._type = theType;

			configureView();
		}


		public function set theme(theTheme:String):void
		{
			this._theme = theTheme;

			configureView();
		}
		
		public function set padding(value:Number):void
		{
			this._padding = value;
		}
		
		public function set paddingX(value:Number):void
		{
			this._paddingX = value;
		}

		public function set background(theBG:*):void
		{
			this.backgroundNormal = theBG;
		}
		
		public function set backgroundNormal(theBG:*):void
		{
			_bg = _setBackground(theBG);
			if(_bg && !this.contains(_bg)) {
				addChildAt(_bg, 0);
			}
			this._type = 'custom';
		}
		
		public function get background():Sprite
		{
			return this._bg;
		}
		
		public function get backgroundNormal():*
		{
			return this._bg;
		}

		public function get backgroundActive():Sprite
		{
			return this._active_bg;
		}

		public function set backgroundActive(theBG:*):void
		{
			_active_bg = _setBackground(theBG);
			if(!this.contains(_active_bg)) addChildAt(_active_bg, 1);
			
			_active_bg.visible = false;

		}
		
		public function setBackground(obj:*, forState:String, options:Object = null):void
		{
			if (options && options.hasOwnProperty('replace') && options.replace) _options.replace = options.replace;
			
			switch (forState) {
				case 'normal' : _bg = _setBackground(obj); break;
				case 'active' : _active_bg = _setBackground(obj); break;
			}
		}

		private function _setBackground(obj:*):Sprite
		{
			var theBG:Sprite;
			
			
			if (getQualifiedClassName(obj) == "com.pvm.ui::UIImageView") {
				theBG = obj;
				if (!this.frame) this.frame = (obj as UIImageView).frame;
				return theBG;
				
			} else
			if (obj.hasOwnProperty("stage"))
			{
				if (obj.parent == this) {
					theBG = obj;
				} else 	if (_options && _options.hasOwnProperty('replace') && _options.replace) {
					theBG = obj;	
					obj.x = 0;
					obj.y = 0;
				} else {
					theBG = new Sprite();
					theBG.graphics.copyFrom(obj.graphics);
					
				}

				
				return theBG;

			}
			else if (getQualifiedClassName(obj) == "String")
			{
				//Given URL to image
				var imv:UIImageView;
				
				if (frame) imv = new UIImageView(new Rectangle(0, 0, this.frame.width, this.frame.height));
				else imv = new UIImageView();
				
				imv.loadImage(obj);
				imv.addEventListener("doneLoaded", function(){
					if (!frame) {
						this.frame = new Rectangle(0, 0, imv.frame.width, imv.frame.height);
					}
				}, false, 0, true);
				return imv;

			}
			else
			{
				throw new ReferenceError("Error: background must be either , DisplayObject or String");
				return null;
			}


		}
		
		public function set contentMode(theMode:String):void
		{
			if (_bg is UIImageView) (_bg as UIImageView).contentMode = theMode;
			if (_active_bg is UIImageView) (_active_bg as UIImageView).contentMode = theMode;
			
		}
		
		public function set targetViewController(vc:ViewController):void
		{
			if (this._targetViewController != vc) {
				
				this._targetViewController = vc;
				
				if (!this._targetViewController.viewControllerManager)
					throw new ReferenceError("Error: set targetViewController // viewController must belong to a ViewControllerManager");
				
				this._targetViewController.addBtnNav(this);
				
				//Set btnNav to active, if vc is initial state
				if (this._targetViewController.isInitial) this.state = "active";
				this._targetViewController.viewControllerManager.addNavButton(this);
				this.addEventListener("tap", openViewController, false, 0, true);
			}
		}
		
		private function openViewController(evt:UIControlEvent) {
			if (this._targetViewController)
				this._targetViewController.viewControllerManager.openViewController(this._targetViewController);
			
		}
		
		public function get targetViewController():ViewController
		{
			return this._targetViewController;
		}

		private function configureView():void
		{
			if (_bg)
			{
				_bg.graphics.clear();
			}

			


		}

		private function createDefaultButtonGraphics():void
		{
			if (!frame) { throw new ReferenceError("Error: UIButton has no frame specified"); return; }
			
			switch (this._theme)
			{
				case 'light' :

					/* TextFormat */
					_labelTextFormatNormal = new TextFormat( );
					_labelTextFormatNormal.bold = false;
					_labelTextFormatNormal.color = 0x555555;
					_labelTextFormatNormal.align = "center";
					_labelTextFormatNormal.font = "Arial";
					_labelTextFormatNormal.size = 18;

					matrix.createGradientBox(this.frame.width, this.frame.height, Math.PI/2, 25, 0);

					if (! _bg)
					{
						_bg = new Sprite();
					}

					_bg.graphics.clear();
					_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xFFFFFF, 0xE4E4E4], [1, 1], [50, 205], matrix);
					_bg.graphics.lineStyle(1, 0xcccccc, 1, true);
					_bg.graphics.drawRoundRect(0,0,this.frame.width, this.frame.height, 16);
					_bg.graphics.endFill();
					addChild(_bg);

					_bg.filters = [dropShadow];


					if (! _active_bg)
					{
						_active_bg = new Sprite();
					}

					_active_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xC9C9C9, 0xDEDEDE], [1, 1], [50, 205], matrix);
					_active_bg.graphics.lineStyle(1, 0xcccccc, 1, true);
					_active_bg.graphics.drawRoundRect(0,0,this.frame.width, this.frame.height, 16);
					_active_bg.graphics.endFill();
					addChild(_active_bg);
					_active_bg.visible = false;

					_active_bg.filters = [dropShadow];



					break;

				case 'dark' :

					/* TextFormat */
					_labelTextFormatNormal = new TextFormat( );
					_labelTextFormatNormal.bold = false;
					_labelTextFormatNormal.color = 0xffffff;
					_labelTextFormatNormal.align = "center";
					_labelTextFormatNormal.font = "Arial";
					_labelTextFormatNormal.size = 18;

					matrix.createGradientBox(this.frame.width, this.frame.height, Math.PI/2, 25, 0);

					if (! _bg)
					{
						_bg = new Sprite();
					}

					_bg.graphics.clear();
					_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xFFFFFF, 0xE4E4E4], [0, 0.1], [50, 205], matrix);
					_bg.graphics.lineStyle(1, 0xffffff, 1, true);
					_bg.graphics.drawRoundRect(0,0,this.frame.width, this.frame.height, 16);
					_bg.graphics.endFill();
					addChild(_bg);

					_bg.filters = [dropShadow];


					if (! _active_bg)
					{
						_active_bg = new Sprite();
					}

					_active_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xC9C9C9, 0xDEDEDE], [0, 0.1], [50, 205], matrix);
					_active_bg.graphics.lineStyle(1, 0xffffff, 1, true);
					_active_bg.graphics.drawRoundRect(0,0,this.frame.width, this.frame.height, 16);
					_active_bg.graphics.endFill();
					addChild(_active_bg);
					_active_bg.visible = false;

					_active_bg.filters = [dropShadow];

					break;
			}
		}

		public function set layout(theLayout:String):void
		{
			this._layout = theLayout;
			configurePosition();
		}
		
		public function set title(theTitle:String):void
		{
			if (!_titleTF) {
				_titleTF = new TextField();
				_titleTF.antiAliasType = AntiAliasType.ADVANCED;
				_titleTF.gridFitType = GridFitType.NONE;
				_titleTF.embedFonts = true;
				_titleTF.selectable = false;			
			}

			_titleTF.text = theTitle;
			_titleTF.setTextFormat(_labelTextFormatNormal);

			configurePosition();
		}

		public function set icon(theIcon:*):void
		{
			//if thePrev is String
			if (getQualifiedClassName(theIcon) == "String")
			{
				//Given URL to image
				_icon = new BitmapHolder();
				_icon.setup(0.5 * this.frame.width, 0.5 * this.frame.height);
				_icon.loadImage(theIcon);
				addChild(_icon);


				configurePosition();
				return;
			}

			//if thePrev is DisplayObject
			if (theIcon.hasOwnProperty('stage'))
			{
				//Given Sprite, MovieClip, Bitmap, or other DisplayObject

				this.addChild(theIcon);
				configurePosition();
				return;
			}

			throw new ReferenceError("Error: UIButton btnPrev must be UIButton, DisplayObject or String");

		}

		private function configurePosition():void
		{
			switch (this._layout)
			{
				case 'icon_text' :
					if (_icon)
					{
						_icon.visible = true;
						_icon.x = 0.25 * this.frame.width;
						_icon.y = 0.25 * this.frame.height;
					}

					if (_titleTF)
					{
						_labelTextFormatNormal.size = 14;
						_titleTF.setTextFormat(_labelTextFormatNormal);

						_titleTF.visible = true;
						_titleTF.y = this.frame.height - _titleTF.textHeight;

					}
					break;

				case 'text' :
					if (_icon)
					{
						_icon.visible = false;
					}
					if (_titleTF)
					{
						_titleTF.visible = true;
						_titleTF.y = 0.5 * (this.frame.height - _titleTF.textHeight) - 3;
						
					}
					break;

				case 'icon' :
					if (_icon)
					{
						_icon.visible = true;
						_icon.x = 0.25 * this.frame.width;
						_icon.y = 0.25 * this.frame.height;
					}

					if (_titleTF)
					{
						_titleTF.visible = false;
					}

					break;
			}
			
			_titleTFOrignialY = _titleTF.y;
			
			/* BETA */
			if (lang == 'th') {
				_titleTF.y = _titleTFOrignialY - .05 * _titleTF.textHeight;
			} else {
				_titleTF.y = _titleTFOrignialY;
			}
			
		}

	
		
		

		override public function configureState():void
		{
			switch (this._state)
			{
				case 'normal' :
					if (_bg)
					{
						_bg.visible = true;
					}
					if (_active_bg)
					{
						_active_bg.visible = false;
					}
					
					if (this._titleTF) {
						this._titleTF.text = this._titleTF.text;
						this._titleTF.setTextFormat(_labelTextFormatNormal);
						
					}
					this.alpha = 1;
					this.mouseEnabled = true;

					break;

				case 'disabled' :
					this.alpha = 0.2;
					this.mouseEnabled = false;
					break;

				case 'touchDown' :
					this.alpha = 0.5;
					//if (this._titleTF) this._titleTF.setTextFormat(_labelTextFormatTouchDown);
					break;

				case 'highlight' :
				case 'active' :
					if (_bg && _active_bg)
					{
						_bg.visible = false;
					}
					if (_active_bg)
					{
						_active_bg.visible = true;
					}
					if (this._titleTF && _labelTextFormatActive) this._titleTF.setTextFormat(_labelTextFormatActive);
					break;
			}
			
			onStateChanged();
			
			onStateChangedEvent.from = this;
			dispatchEvent(onStateChangedEvent);


		}
		
		public function onStateChanged():void
		{
			
		}
		
		public function setLabelTextForLang(lang:String, theTitle:String, textFormat:TextFormat = null, textFormatActive:TextFormat = null):void
		{
			if (lang == "") lang = "default";
			_labelText[lang] = theTitle;
			
			
			if (textFormat) {
				if (textFormat.align == null) textFormat.align = "center";
				_labelTextFormat[lang] = textFormat;
				_labelTextFormat[lang+"-normal"] = textFormat;
			}
			
			if (textFormatActive) {
				if (textFormatActive.align == null) textFormatActive.align = "center";
				_labelTextFormat[lang+"-active"] = textFormatActive;
			}
			
			if (this.lang == lang) this.labelTextFormat = _labelTextFormat[lang];
			
			translate();
		}
		
		
		
		public function set labelText (theTitle:String):void
		{
			if (! _titleTF){
				_titleTF = new TextField();
				//_titleTF.antiAliasType = AntiAliasType.ADVANCED;
				_titleTF.embedFonts = true;
				_titleTF.selectable = false;
				addChild(_titleTF);
			}
			
			
			if (!_labelText) _labelText = new Dictionary();
			_labelText[this.lang] = theTitle;
			
			_titleTF.x = _padding;
			if (theTitle) _titleTF.text = theTitle;
			if (frame) _titleTF.width = this.frame.width - 2 * _padding; else _titleTF.width = _titleTF.textWidth;
			
			
			if (_labelTextFormat[this.lang + "-" + this.state]) 
				_titleTF.setTextFormat(_labelTextFormat[this.lang + "-" + this.state]);
			else if (_labelTextFormat[this.lang]) 
				_titleTF.setTextFormat(_labelTextFormat[this.lang]);
			
			if (frame) configurePosition();
		}
		
		public function get labelText ():String
		{
			return this._titleTF.text;
		}
		
		public function get labelTextField():TextField
		{
			return this._titleTF;
		}
		
		public function set labelTextFormat (theTF:TextFormat):void
		{
			//Force align Cetner
			if (theTF.align == null) theTF.align = "center";
			_labelTextFormatNormal = theTF;
			
			_labelTextFormat[this.lang] = theTF;
			_labelTextFormat[this.lang+"-normal"] = theTF;
			
		}
		
		public function get labelTextFormat ():TextFormat
		{
			return _labelTextFormatNormal;
		}
		
		
		
		public function set labelTextFormatTouchDown (theTF:TextFormat):void
		{
			//Force align Cetner
			if (theTF.align == null) theTF.align = "center";
			_labelTextFormatTouchDown = theTF;
			
		}
		
		public function get labelTextFormatActive ():TextFormat
		{
			return _labelTextFormatActive;
		}
		
		public function set labelTextFormatActive (theTF:TextFormat):void
		{
			//Force align Cetner
			if (theTF.align == null) theTF.align = "center";
			_labelTextFormatActive = theTF;
			
			_labelTextFormat[this.lang+"-active"] = theTF;
			
		}
		
		public function get labelTextFormatTouchDown ():TextFormat
		{
			return _labelTextFormatTouchDown;
		}
		
		public function reformatLabelText():void
		{
			this._titleTF.setTextFormat(_labelTextFormatNormal);
		}
		
		public function translate():void
		{
			this.labelText = _labelText[this.lang];
			if (this.lang) {
				if (_labelTextFormat[this.lang]) {
					
					
					_labelTextFormatNormal = _labelTextFormat[this.lang];
					_labelTextFormatActive = _labelTextFormat[this.lang+"-active"];
					
					if (this.state == "active") {
						if (_labelTextFormat[this.lang +"-active"]) 
							this._titleTF.setTextFormat(_labelTextFormat[this.lang+"-active"]);
						else 
							this._titleTF.setTextFormat(_labelTextFormat[this.lang]);
					} else { 
						this._titleTF.setTextFormat(_labelTextFormat[this.lang]);
					}
					
				}//else this._titleTF.setTextFormat(_labelTextFormatNormal);
				
				
				/* BETA */
				
				/*if (lang == 'th') {
					_titleTF.y = _titleTFOrignialY - .05 * _titleTF.textHeight;
				} else {
					_titleTF.y = _titleTFOrignialY;
				}*/
				
			}
			
			onTranslated();
		}
		
		public function onTranslated():void
		{
			
		}
		
		override public function addHintEffect(_trigger:Array = null):void
		{
			
			hintEffectTimer.addEventListener(TimerEvent.TIMER, doHintEffect);
				
			
			for (var i:int = 0; i< _trigger.length; i++ )
			{
				switch (_trigger[i]) {
					case 'appear' : 
						
						break;
					
					case 'random' :
						hintEffectTimer.reset();
						hintEffectTimer.start();
						break;
				}
			}
			
		}
		
		override public function doHintEffect(evt:TimerEvent = null):void
		{
			
			TweenMax.to(this.backgroundNormal, 1.5, {glowFilter:{color:0xFFFFFF, alpha:0.8, blurX:20, blurY:20}, onComplete:removeGlow});
			
		}
		
		function removeGlow() {
			
			TweenMax.to(this.backgroundNormal, 1.5, {glowFilter:{alpha:0, blurX:0, blurY:0, remove:true}});
			
			hintEffectTimer.delay = 4000 + Math.random() * 20000;
			hintEffectTimer.reset();
			hintEffectTimer.start();
			
		}
		
		
		
		
	}

}