﻿package com.pvm.ui {
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flashx.textLayout.formats.Float;
	
	public class CoverFlow extends Sprite {
		
		public var SHRINK_FACTOR:int = 2; //2
		public var EASING_POWER:int = 4;
		public var EASING_HOLDER:int = 5;
		public var ITEM_SPAN:int = 325; //200

		public var _pressX:Number;
		private var _releaseX:Number;
		private var _lastX:Number = 0;
		private var _velo:Number;
		public var _moveX:Number = 0;
		
		public var _scrollCenter:Number;
		
		public var _onTouchDown_deltaX:Number;
		public var _calculate:Boolean = true;
		public var _onTouchDown:Boolean = false;
		
		
		public var numItem:int = 0;
		public var itemArray:Array = new Array(); // origin private
		
		public var bounceLeft:Number;
		public var bounceRight:Number;
		
		public var board:Sprite;  // origin private
		private var touchArea:Sprite;
		private var htArea:Sprite;
		
		public var endScaleArea:Array = new Array();
		public var moveScaleArea:Array = new Array();
		
		public var calculatedetail:int;
		
		
		public function CoverFlow(_w:Number = 0, _h:Number = 0) {
			// constructor code
			
			board = new Sprite();
			addChild(board);
			
			touchArea = new Sprite();
			touchArea.graphics.beginFill(0xff0000, 0);  // alpha 0.5
			touchArea.graphics.drawRect(0, 0, _w, _h);
			touchArea.graphics.endFill();
			addChildAt(touchArea, 0);
			
			_scrollCenter = (_w / 2);
			//trace("_scrollCenter = " + _scrollCenter);
			
			htArea = new Sprite();
			htArea.graphics.beginFill(0xff0000, 0);
			htArea.graphics.drawRect(_scrollCenter, -_h, 10, 3*_h); // _scrollCenter, -_h, 10, 3*_h
			htArea.graphics.endFill();
			addChild(htArea);
			
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, onTouchDownHandler, false, 0, true);
			this.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}
		
		public function onOut(evt:Event):void
		{
			trace("on out");
		}
		
		public function addItem(mc:Sprite):void
		{
			board.addChild(mc);
			itemArray.push(mc);
			
			mc.addEventListener("click", moveBoard);
			mc.x = numItem * (ITEM_SPAN);
			
			numItem = itemArray.length;
			
			
			//trace("numItem = " + numItem + " : " +mc.x);
			
			endScaleArea.push();
			moveScaleArea.push();
			
			bounceLeft = 0;
			bounceRight = (1 - numItem) * ITEM_SPAN;
			
			//trace("bounceLeft start >>>>> "+bounceLeft);
			//trace("bounceRight start >>>> "+bounceRight);
				
			
		}
		
		public function moveBoardToItem(_pos:Number):void
		{
			_moveX = itemArray[_pos].x;
			trace ("moveBoardToItem >>>>>>>>>>>>>>>  "+_pos.toString());
			_moveX = setTarget(_moveX);
			
			_calculate = true;
			
		}
		public function moveBoardToItemfilter(_pos:Number):void{
			//trace ("moveBoardToItemfilter >>>>>>>>>>>>>>>  "+_pos.toString());
			_moveX = itemArray[0].x; //_pos //first item
			_moveX = setTarget(_moveX);
			
			_calculate = true;
		}
		
		public function moveBoard(evt:MouseEvent):void
		{
			_moveX = _scrollCenter - evt.target.x;
			_moveX = setTarget(_moveX);
			
			//trace("_moveX in moveBoard(evt:MouseEvent) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   "+_moveX);
		}
		
		
		
		public function setTarget(_moveX:Number):Number
		{
			_moveX = (_scrollCenter - ITEM_SPAN) + roundToNearest(ITEM_SPAN, _moveX);//// + (_scrollCenter % ITEM_SPAN);
			
			//trace("_moveX in setTarget following roundToNearest >>>>>>>>>>>>>>>>>>>>>>>>>    "+_moveX);
			
			var bounceLeft:Number = _scrollCenter;
			var bounceRight:Number = _scrollCenter - (((numItem - 1) * ITEM_SPAN)); //_scrollCenter - ((numItem - 1) * ITEM_SPAN)
			
			//trace("bounceLeft in setTarget following bounceLeft >>>>>>>>>>>>>>>>>>>>>>>>>    "+bounceLeft);
			//trace("bounceRight in setTarget following bounceRight >>>>>>>>>>>>>>>>>>>>>>>    "+bounceRight);
			
			if (_moveX > bounceLeft) {
				_moveX = bounceLeft;
				
				//trace("bounceLeft movex >>>>>>>>> "+_moveX);
				
			} else if (_moveX < bounceRight) {
				_moveX = bounceRight;
				
				//trace("bounceRight movex >>>>>>>> "+_moveX);
				
			}
			
			return _moveX;
			
			//trace("return _moveX in setTarget following return >>>>>>>>>>>>>>>>>>>>>>>>>>>     "+_moveX);
		}
		
		public function roundToNearest(round:Number, value:Number):Number {  // for slide nearest
			return Math.round(value / round)* round;  //Math.round(value / round) * round
		}
		
		public function onAnimate(_x:Number):void
		{
			
		}
		
		
		
		public function onAnimationFinish(mc:DisplayObject):void
		{
			
		}
		
		public function showDetail(mc:DisplayObject):void{
			
		}
		
		public function keepDetailvariable(intVar:int):void{
			
		}
		
		
		public function onTouchDownHandler(e:MouseEvent):void  //private
		{
			_onTouchDown = true;
			_pressX = e.stageX;
			_onTouchDown_deltaX = board.x - _pressX;
			_calculate = true;
			
			board.stage.addEventListener(MouseEvent.MOUSE_UP, onTouchUpHandler, false, 0, true);
			
			
		}
		
		public function onTouchUpHandler(e:MouseEvent):void
		{
			board.stage.removeEventListener(MouseEvent.MOUSE_UP, onTouchUpHandler);
			_onTouchDown = false;
			_releaseX = e.stageX;
			/*
			_velo = _releaseX - _lastX;
			if (_velo < 5 && _velo > -5) _velo *= 5;
			_moveX = board.x + _velo * EASING_POWER;
			trace("_velo = " + _velo);
			*/
			_moveX = board.x + _releaseX - _pressX; 
			_moveX = setTarget(_moveX);
			
		}
		
		private function onEnterFrameHandler(e:Event):void
		{
			if (_calculate) {
				
					if (_onTouchDown) {
						_moveX = mouseX + _onTouchDown_deltaX;
						_lastX = mouseX;
					}
					
					
					//easing 
					board.x += (_moveX - board.x) / EASING_HOLDER;
					//board.x = _moveX
					
					//Calculate EndPos
					for (var i = 0; i<numItem; i++) {
						//var xValue:Number = Math.abs(board.x + (i-0.5*numItem) * ITEM_SPAN);
						var xValue:Number = Math.abs(_scrollCenter - (board.x +itemArray[i].x));  //_scrollCenter - (board.x + itemArray[i].x)
						
						//trace ("xValue in frame about board.x >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   "+board.x);
						endScaleArea[i] = (Math.exp(-0.001 *  xValue));  //Math.exp(-0.001 * SHRINK_FACTOR * xValue)
						
					}
					
					//Set Scale	& Set Y
					for (var i = 0; i<numItem; i++) {
						moveScaleArea[i] = (endScaleArea[i] - itemArray[i].scaleY)/4;
						itemArray[i].scaleX += moveScaleArea[i];
						itemArray[i].scaleY += moveScaleArea[i];
					}
					
					//Kill onEnterFrame when stop
					if ((Math.abs(board.x - _moveX) < 10) && (_onTouchDown == false)) {  //1
						
						calculatedetail = Math.round(board.x - _moveX);
						
						if (Math.abs(board.x - _moveX) < 1){
							_calculate = false;
						}
						
						//trace("Math.abs(board.x - _moveX)  >>>>>>>>>>>>>>>>>>>>>>>>>  "+(board.x - _moveX));
						
						//trace("animation finish numItem = " + numItem);
						
						//Animation Finish
						for (var k:int; k < numItem; k++) {
							if (itemArray[k].hitTestObject(htArea)) {
								
								showDetail(itemArray[k]);
								
								keepDetailvariable(calculatedetail);
								break;
							}
						}
					}
					/*if ((Math.abs(board.x - _moveX) < 1) && (_onTouchDown == false)) {  //1
						_calculate = false;
						
						trace("animation finish numItem = " + numItem);
						
						//Animation Finish
						for (var k:int; k < numItem; k++) {
							if (itemArray[k].hitTestObject(htArea)) {
								
								onAnimationFinish(itemArray[k]);
								break;
							}
						}
					}*/
					
					
					/*if ((Math.abs(board.x - _moveX) < 30) && (_onTouchDown == false)) {  //1

						for (var k:int; k < numItem; k++) {
							if (itemArray[k].hitTestObject(htArea)) {
								
								showDetail(itemArray[k]);
								break;
							}
						}
					}*/
					
					onAnimate(board.x);
				
			};
		}
		
		
		
	}
	
}
