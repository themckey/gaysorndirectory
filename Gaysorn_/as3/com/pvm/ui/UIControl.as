﻿/* 
 * UIControl version 1.0
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.pvm.ui {
	
	import flash.geom.Point;
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.utils.Timer;
	import flash.geom.Matrix;
	import flash.media.Sound;
	
	import flash.events.TimerEvent;

	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import com.greensock.TweenMax;
	import flash.media.SoundTransform;
	import flash.media.SoundChannel;
	
	public class UIControl extends UIView {
		
		private var _formValue:String;

		private var _onFocus:Boolean = false;
		
		protected var _state:String = "normal";
		private var _toggle:Boolean = false;
		private var _enableButtonMode:Boolean = true;
		
		private var pointDown:Point = new Point();
		private var pointUp:Point = new Point();
		private var pointMove:Point = new Point();
		
		private var touchDownTarget:UIControl;
		
		private var _dragging:Boolean = false;
		public static var moveTreshold:Number = 50;
		public static var selectionTreshold:Number = 20;
		
		private var focusEvent:UIControlEvent = new UIControlEvent("focus");
		private var focusOutEvent:UIControlEvent = new UIControlEvent("focusOut");
		
		private var touchDownEvent:UIControlEvent = new UIControlEvent("touchDown");
		private var touchUpInsideEvent:UIControlEvent = new UIControlEvent("touchUpInside");
		private var touchUpOutsideEvent:UIControlEvent = new UIControlEvent("touchUpOutside");
		private var tapEvent:UIControlEvent = new UIControlEvent("tap");
		
		public static var soundClick:Sound;
		var soundup:SoundTransform;
		
		public function UIControl(frame:Rectangle = null) {
			
			if (frame) super(frame);
			
			if (this._enableButtonMode) {
				this.addEventListener("mouseDown", onTouchDownHandler, false, 0, true);
				this.addEventListener("touchBegin", onTouchDownHandler, false, 0, true);
			}
			
			
			
		}
		
		public static function setSound(url:String, forEvent:String = 'click'):void
		{	
			
			if (forEvent == 'click') {
				soundClick = new Sound();
				soundClick.load(new URLRequest(url));
				
				
				
				
			}
		}
		
		private function onTouchDownHandler(evt:Event):void
		{
			if (evt.type == "mouseDown") {
				pointDown.x = MouseEvent(evt).stageX;
				pointDown.y = MouseEvent(evt).stageY;
			} else if (evt.type == "touchBegin") {
				pointDown.x = TouchEvent(evt).stageX;
				pointDown.y = TouchEvent(evt).stageY;
			} 
			
			this.state = 'touchDown';
			
			this.stage.addEventListener("mouseMove", onMouseMoveHandler, false, 0, true);
			this.stage.addEventListener("mouseUp", onMouseUpHandler, false, 0, true);
			
			
			touchDownEvent.from = this;
			dispatchEvent(touchDownEvent);
		}
		
		private function onMouseUpHandler(evt:MouseEvent):void
		{
			pointUp.x = evt.stageX;
			pointUp.y = evt.stageY;
			
			this.state = 'normal';
			
			this.stage.removeEventListener("mouseUp", onMouseUpHandler);
			this.stage.removeEventListener("mouseMove", onMouseMoveHandler);
			
			
			if ((Math.abs(pointDown.x - pointUp.x) <= selectionTreshold) &&
				(Math.abs(pointDown.y - pointUp.y) <= selectionTreshold) &&
				!_dragging) 
			{
				tapEvent.from = this;
				dispatchEvent(tapEvent);
				
				if (UIControl.soundClick) {
					UIControl.soundClick.play();
				}
			}
			
			if (evt.target == this) {
					touchUpInsideEvent.from = this;
					dispatchEvent(touchUpInsideEvent);
			} else {
				touchUpOutsideEvent.from = this;
				dispatchEvent(touchUpOutsideEvent);
			}
			
			_dragging = false;
			
		}
		
		private function onMouseMoveHandler(evt:MouseEvent):void
		{
			pointMove.x = evt.stageX;
			pointMove.y = evt.stageY;
			
			if ((Math.abs(evt.stageY - pointDown.y) >= moveTreshold) || ((Math.abs(evt.stageX - pointDown.x) >= moveTreshold) ))
			{
				_dragging = true;
				
			}
		}
		
		
		public function set enableButtonMode(theValue:Boolean):void
		{
			this._enableButtonMode = theValue;
			
			if (this._enableButtonMode) {
				this.addEventListener("mouseDown", onTouchDownHandler);
				this.addEventListener("touchBegin", onTouchDownHandler);
			} else {
				this.removeEventListener("mouseDown", onTouchDownHandler);
				this.removeEventListener("touchBegin", onTouchDownHandler);
			}
		}
		
		public function set state(theState:String):void
		{
			if (this._state != theState) {
				this._state = theState;
				configureState();
			}
		}
		
		public function get state():String
		{
			return this._state;
		}
		
		public function set toggle(theToggle:Boolean):void
		{
			this._toggle = theToggle;
		}
		
		public function get toggle():Boolean
		{
			return this._toggle;
		}
		
		public function configureState():void
		{
			switch (this._state) {
				case 'touchDown' : this.onStateTouchDown(); break;
				case 'highlighted' : this.onStatehighlight(); break;
				case 'disabled' : this.onStateDisabled(); break;
				case 'selected' : this.onStateSelected(); break;
				default : this.onStateNormal(); break;
				
			}
		}
		
		public function onStateTouchDown():void{ this.alpha = 0.5; }
		public function onStatehighlight():void{ }
		public function onStateDisabled():void{ }
		public function onStateSelected():void{ }
		public function onStateNormal():void{ this.alpha = 1; }
		
		
		public function get focus():Boolean {
			return this._onFocus;
		}
		
		public function set focus(theValue:Boolean):void
		{
			this._onFocus = theValue;
			if (this._onFocus) setFocusState();//this.dispatchEvent(focusEvent);
			else setFocusOutState();//this.dispatchEvent(focusOutEvent);
		}
		
		public function setFocusState():void
		{
		
		}
		
		public function setFocusOutState():void
		{
		
		}
		
		public function get formValue():String
		{
			return this._formValue;
		}
		
		public function set formValue(value:String):void
		{
			this._formValue = value;
		}
		
		
		
		
		protected var hintEffectTimer:Timer = new Timer(5000);
		protected var reflection:Shape = new Shape();
		protected var reflectionWidth;
		protected var startX:Number = 0;
		protected var endX:Number = 0;
		
		
		public function addHintEffect(_trigger:Array = null):void
		{
			if (frame) {
				/* Metalic sheet */
				
				var matrix:Matrix = new Matrix();
				var boxWidth:Number = frame.height * 3; 
				var boxHeight:Number = frame.height * 3; 
				var boxRotation:Number = Math.PI/4; // 90° 
				var tx:Number = 0; 
				var ty:Number = 0; 
				matrix.createGradientBox(boxWidth, boxHeight, boxRotation, tx, ty); 
	
				reflection.graphics.beginGradientFill('linear', [0xffffff, 0xffffff, 0xffffff], [0, 1, 0], [70, 128, 185], matrix);
				reflection.graphics.drawRect(0,0, boxWidth, boxHeight);
				reflection.graphics.endFill();
				reflection.blendMode = "overlay";
				reflection.y = -frame.height;
				reflection.x = this.transform.pixelBounds.left * -1;
				
				//this.unmask = true;
				startX = reflection.x -20;
				endX = frame.width + 20;
				addChild(reflection);
				
				
				
				//hintTween = new Tween(reflection, "x", None.easeNone, startX, endX, 2.5, true);
				
				hintEffectTimer.addEventListener(TimerEvent.TIMER, doHintEffect, false, 0, true);
				
				
			}
			
			for (var i:int = 0; i< _trigger.length; i++ )
			{
				switch (_trigger[i]) {
					case 'appear' : 
						
						break;
					
					case 'random' :
						hintEffectTimer.reset();
						hintEffectTimer.start();
						break;
				}
			}
			
		}
		
		public function doHintEffect(evt:TimerEvent = null):void
		{
			
			reflection.x = startX;
			TweenMax.to(reflection, 2.5, {x:endX});			
			
			hintEffectTimer.delay = 4000 + Math.random() * 20000;
			hintEffectTimer.reset();
			hintEffectTimer.start();
			
		}
		

	}
	
}
