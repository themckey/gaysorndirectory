﻿/* 
 * UIControl version 1.0
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.pvm.ui {
	
	import flash.geom.Point;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	import flash.geom.Rectangle;
	
	public class UIControl extends UIView {

		private var _isFocusing:Boolean = false;
		
		private var _state:String = "normal";
		private var _toggle:Boolean = false;
		
		private var pointDown:Point = new Point();
		private var pointUp:Point = new Point();
		private var pointMove:Point = new Point();
		
		private var touchDownTarget:UIControl;
		
		private var _dragging:Boolean = false;
		public static var moveTreshold:Number = 50;
		public static var selectionTreshold:Number = 20;
		
		
		private var focusEvent:UIControlEvent = new UIControlEvent("focus");
		private var focusOutEvent:UIControlEvent = new UIControlEvent("focusOut");
		
		private var touchDownEvent:UIControlEvent = new UIControlEvent("touchDown");
		private var touchUpInsideEvent:UIControlEvent = new UIControlEvent("touchUpInside");
		private var touchUpOutsideEvent:UIControlEvent = new UIControlEvent("touchUpOutside");
		private var tapEvent:UIControlEvent = new UIControlEvent("tap");
			
		
		public function UIControl() {
			
			this.addEventListener("mouseDown", onMouseDownHandler);
		}
		
		private function onMouseDownHandler(evt:MouseEvent):void
		{
			pointDown.x = evt.stageX;
			pointDown.y = evt.stageY;
			
			this.state = 'touchDown';
			
			this.stage.addEventListener("mouseMove", onMouseMoveHandler, false, 0, true);
			this.stage.addEventListener("mouseUp", onMouseUpHandler, false, 0, true);
			
			
			touchDownEvent.from = this;
			dispatchEvent(touchDownEvent);
		}
		
		private function onMouseUpHandler(evt:MouseEvent):void
		{
			pointUp.x = evt.stageX;
			pointUp.y = evt.stageY;
			
			this.state = 'normal';
			
			this.stage.removeEventListener("mouseUp", onMouseUpHandler);
			this.stage.removeEventListener("mouseMove", onMouseMoveHandler);
			
			
			if ((Math.abs(pointDown.x - pointUp.x) <= selectionTreshold) &&
				(Math.abs(pointDown.y - pointUp.y) <= selectionTreshold) &&
				!_dragging) 
			{
				tapEvent.from = this;
				dispatchEvent(tapEvent);
				trace ("TAP : "+ tapEvent.from + "Parent : " + tapEvent.from.parent);
			}
			
			if (evt.target == this) {
					touchUpInsideEvent.from = this;
					dispatchEvent(touchUpInsideEvent);
			} else {
				touchUpOutsideEvent.from = this;
				dispatchEvent(touchUpOutsideEvent);
			}
			
		}
		
		private function onMouseMoveHandler(evt:MouseEvent):void
		{
			pointMove.x = evt.stageX;
			pointMove.y = evt.stageY;
			
			if ((Math.abs(evt.stageY - pointDown.y) >= moveTreshold) || ((Math.abs(evt.stageX - pointDown.x) >= moveTreshold) ))
			{
				_dragging = true;
				
			}
		}
		
		
		
		
		
		public function set state(theState:String):void
		{
			if (this._state != theState) {
				this._state = theState;
				configureState();
			}
		}
		
		public function get state():String
		{
			return this._state;
		}
		
		public function set toggle(theToggle:Boolean):void
		{
			this._toggle = theToggle;
		}
		
		public function get toggle():Boolean
		{
			return this._toggle;
		}
		
		private function configureState():void
		{
			switch (this._state) {
				case 'touchDown' : this.onStateTouchDown(); break;
				case 'highlighted' : this.onStatehighlight(); break;
				case 'disabled' : this.onStateDisabled(); break;
				case 'selected' : this.onStateSelected(); break;
				default : this.onStateNormal(); break;
				
			}
		}
		
		public function onStateTouchDown():void{ this.alpha = 0.5; }
		public function onStatehighlight():void{ }
		public function onStateDisabled():void{ }
		public function onStateSelected():void{ }
		public function onStateNormal():void{ this.alpha = 1; }
		
		
		public function get isFocus():Boolean 
		{
			return this._isFocusing;
		}
		
		public function focus():void
		{
			//trace("focus "+ this.name+ " _isFocusing = " + _isFocusing);
			if (!_isFocusing) {
				this.setFocusState();
				this._isFocusing = true;
				this.dispatchEvent(focusEvent);

			}
		}
		
		public function focusOut():void
		{
			//trace("focusOut "+ this.name+ " _isFocusing = " + _isFocusing);
			if (_isFocusing) {
				_isFocusing = false;
				this.setFocusOutState();
				this.dispatchEvent(focusOutEvent);
			}
		}
		
		public function setFocusState():void
		{
		
		}
		
		public function setFocusOutState():void
		{
		
		}

	}
	
}
