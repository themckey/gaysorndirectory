﻿/* 
 * UITextField version 1.0
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.pvm.ui {
	
	import flash.display.Sprite;
	import flash.display.Shape;
	
	import flash.display.GradientType;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldType;
	import flash.events.Event;
	
	import flash.filters.GlowFilter;
	import flash.filters.DropShadowFilter;
	import flash.filters.BitmapFilterQuality;
	
	import com.pvm.input.keyboard.TouchScreenKeyboard;
	import com.pvm.input.keyboard.TouchScreenKeyboardEvent;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	public class UITextField extends UIControl {
	
		private var _frame:Rectangle;
		
		private var _default_bg:Shape;
		
		private var _tfWidth:Number = 200;
		private var _tfHeight:Number = 40;
		
		private var _strokeColor:Number = 0x999999;
		private var _strokeAlpha:Number = 1.0;
		
		private var matrix:Matrix = new Matrix();
		private var boxRotation:Number = Math.PI/2; // 90° 
		private var tx:Number = 25; 
		private var ty:Number = 0; 
		
		private var _textFormatDict:Dictionary = new Dictionary();
		
		private var _placeholder:String = "PlaceHolder";
		private var _placeholderDict:Dictionary;
		
		private var _placeholderTextFormat:TextFormat;
		private var _placeholderTextFormatDict:Dictionary;
		
		private var _tooltipTextFormat:TextFormat;
		private var _tooltipTextFormatDict:Dictionary;
		
		private var _validate:Boolean = true;
		
		private var _validation:String = 'none';
		private var _require:Boolean = false;
		private var _error_message:String = '';
		
		private var _textPadding:Number = 10;
		private var _textVerticalAlign:String = 'middle';
		private var _textFormat:TextFormat;
		private var _errorFormat:TextFormat;
		
		private var _capitalization:Boolean = true;
		
		private var _lineHeight:Number;
		
		private var _multiline:Boolean = false;
		
		private var _placeHolder:TextField;
		private var _textField:TextField;
		
		private var _keyboard:TouchScreenKeyboard;
		private var _keyboardMode:String = "default";
		private var _keyboardPosition:String = "under";
		
		private var _textFieldOriginalY:Number = 0;
		private var _placeHolderOriginalY:Number = 0;

		
		private var tooltip:UITooltip;
		
		var glow:GlowFilter = new GlowFilter(); 
		var dropShadow:DropShadowFilter = new DropShadowFilter(0, 0, 0x000000, 0.5, 8, 8, 1, 1, true);
		var onFocusDropShadow:DropShadowFilter = new DropShadowFilter(2, -90, 0x000000, 1, 0, 0, 0.5, 1, true);
		
		private var valueChangedEvent:UIControlEvent = new UIControlEvent("valueChanged");
		private var focusEvent:UIControlEvent = new UIControlEvent("focus");
		private var focusOutEvent:UIControlEvent = new UIControlEvent("focusOut");
		
		
		private var submitEvent:UIControlEvent;

		public function UITextField(frame:Rectangle, options:Object = null) {
			// constructor code
			
			valueChangedEvent.from = this;
			focusEvent.from = this;
			focusOutEvent.from = this;
			
			this.x = frame.x;
			this.y = frame.y;
			
			
			this.frame = frame;
			
			this._tfWidth = frame.width;
			this._tfHeight = frame.height;
			
			submitEvent = new UIControlEvent("submit", this);
			
			var border_radius:Number = 8;
			
			
			/* Default */
			_placeHolder = new TextField();
			_placeHolder.width = this._tfWidth - 2 * _textPadding;
			_placeHolder.height = this._tfHeight;
			_placeHolder.embedFonts = true;
			_placeHolder.text = '';
			addChild(_placeHolder);
				
			_placeholderTextFormat = new TextFormat( );
			_placeholderTextFormat.bold = false; 
			_placeholderTextFormat.color = 0x999999; 
			_placeholderTextFormat.align = "left";
			_placeholderTextFormat.font = "Miso";    
			_placeholderTextFormat.size = 30;
		
			_textField = new TextField();
			_textField.embedFonts = true;
			_textField.width = this._tfWidth;
			_textField.height = this._tfHeight;
			_textField.type = TextFieldType.INPUT;
			addChild(_textField);
			_textField.selectable = false;
			
			if (options) {
				if (options.hasOwnProperty('border_radius')) border_radius = options['border_radius'];
				if (options.hasOwnProperty('validation')) this._validation = options['validation'];
				if (options.hasOwnProperty('textFormat')) {
					this.textFormat = options['textFormat'];
				}
				
				if (options.hasOwnProperty('placeholderTextFormat')) {
					this._placeholderTextFormat = options['placeholderTextFormat'];
				}

				if (options.hasOwnProperty('tooltipTextFormatDict')) {
					if (!this._tooltipTextFormatDict) this._tooltipTextFormatDict = new Dictionary();
					_tooltipTextFormat = options['tooltipTextFormatDict'][this.lang];
					this._tooltipTextFormatDict = options['tooltipTextFormatDict'];
				}
				
				
				if (options.hasOwnProperty('placeholder')) this.placeholder = options['placeholder'];
				if (options.hasOwnProperty('keyboardPosition')) this.keyboardPosition = options['keyboardPosition'];
			}
		
			
			
			matrix.createGradientBox(this._tfWidth, this._tfHeight, boxRotation, tx, ty); 

			_default_bg = new Shape();
			_default_bg.graphics.beginGradientFill(GradientType.LINEAR, [0xFFFFFF, 0xE4E4E4], [1, 1], [0, 255], matrix);
      		_default_bg.graphics.lineStyle(1, _strokeColor, _strokeAlpha, true);
      		_default_bg.graphics.drawRoundRect(0,0,this._tfWidth, this._tfHeight, border_radius);
			_default_bg.graphics.endFill();
			addChildAt(_default_bg, 0);
			
			_default_bg.filters = [dropShadow];
			
			
			
			
			
			
			this.mouseChildren = true;
			
			this.initDefaultTextFormat();
			
			
			this.configureTextFormat();
			
			
			//Grow Filter
			glow.color = 0x52a8ec; 
			glow.alpha = 0.8; 
			glow.blurX = 8; 
			glow.blurY = 8; 
			glow.quality = BitmapFilterQuality.MEDIUM; 
			 
			
			//Interaction 
			this.addEventListener("mouseDown", onTouchDownHandler);
			
		}
		
		
		
		private function initDefaultTextFormat():void
		{
			if (!this.textFormat) {
				_textFormat = new TextFormat( );
				_textFormat.bold = false; 
				_textFormat.color = 0x333333; 
				_textFormat.align = "center";
				_textFormat.font = "Arial";    
				_textFormat.size = 18;
			}
			
			
			_errorFormat = new TextFormat(_textFormat.font, _textFormat.size, 0xB94A48, _textFormat.bold);
			
			
		}
		
		public function clear():void
		{
			_textField.text = '';
			onValueUpdate();
			
			if (tooltip) tooltip.hide();
		}
		public function clearalltool():void{
			if (tooltip) tooltip.hide();
		}
		override public function get formValue():String
		{
			return _textField.text;
		}
		
		public function get textFormat():TextFormat
		{
			return this._textFormat;
		}
		
		
		public function set textFormat(theFormat:TextFormat):void{
			
			if (_textFormat != theFormat)
			{
				_textFormat = theFormat;
				configureTextFormat();
			}
			
		}
		
		public function set keyboard(theKeyboard:TouchScreenKeyboard):void
		{
			this._keyboard = theKeyboard;
			this._keyboard.addElement(this);
			this._keyboard.position = _keyboardPosition;
			this._keyboard.addEventListener("onTouchScreenKeyPressed", onKeyPressed);
			
		}
		
		
		public function get keyboard():TouchScreenKeyboard
		{
			return this._keyboard;
			
		}
		
		public function set keyboardPosition(value:String):void
		{
			this._keyboardPosition = value;
		}
		
		public function set keyboardMode(theKeyboardMode:String):void
		{
			this._keyboardMode = theKeyboardMode;
		}
		
		public function set multiline(theValue:Boolean):void
		{
			this._multiline = theValue;
			this._textField.multiline = theValue;
			
			if (!this._multiline) {
				this._lineHeight = this._frame.height;
			} else {
				this._lineHeight = Number(_textFormat.size) * 3.0;
			}
			
		}
		
		public function set wordWrap(theValue:Boolean):void
		{
			this._textField.wordWrap = theValue;
		}
		
		public function set placeholder(theText:String):void
		{
			
			this._placeHolder.text = theText;
			this._placeHolder.setTextFormat(this._placeholderTextFormat);
		}
		
		public function get placeHolder():TextField
		{
			return this._placeHolder;
		}
		
		public function setPlaceholderTextForLang(lang:String, thePlaceHolder:String, textFormat:TextFormat = null):void
		{
			if (lang == "") lang = "default";
			
			if (!_placeholderDict) _placeholderDict = new Dictionary();
			_placeholderDict[lang] = thePlaceHolder;
			
			
			if (textFormat) {
				if (!_textFormatDict) _textFormatDict = new Dictionary();
				_textFormatDict["placeholder-"+lang] = textFormat;
			}
			
			translate();
		}
		
		public function setTooltipTextFormatForLang(lang:String, textFormat:TextFormat):void
		{
			if (lang == "") lang = "default";
			
			if (textFormat) {
				if (!tooltip) tooltip = new UITooltip();
				tooltip.setTextFormatForLang(lang, textFormat);
			}
			
			//translate();
		}
		
		public function set capitalization(value:Boolean):void
		{
			this._capitalization = value;
		}
		
		public function set textVerticalAlign(value:String):void
		{
			this._textVerticalAlign = value;
			configureTextFormat();
		}
		
		
		public function set defualtBG(theBG:Number):void
		{
			this._default_bg.alpha = theBG;
		}
		
		
		
		public function set require(val:Boolean):void
		{
			this._require = val;
		}
		
		public function set validation(theString:String):void
		{
			this._validation = theString;
		}
		
		public function get validate():Boolean 
		{
			return validateField();
		}
		
		public function validateField():Boolean
		{
			this._error_message = "";
			
			//Check require
			if (this._require) {
				if (_textField.text.length == 0) {
					this._error_message = "This field is required.";
					this._validate = false;
					//(this._validate) ? _textField.setTextFormat(_textFormat) : _textField.setTextFormat(_errorFormat);
					
				} else {
					this._error_message = "";
					this._validate = true;
				}
			} else {
				if (_textField.text.length == 0) this._validate = true;
				this._error_message = "";
			}
			
			//Check Validator, check if still no error so far
			if ( this._error_message == "" ) {
			
				if (this._validation != "none") {
					switch (_validation) {
						case 'email' :
							this._validate = this.checkEmail(_textField.text);
							this._error_message = (!_validate)  ? "Please make sure your email address is correct" : '';
							break;
						
						case 'name' : 
							this._validate = this.checkName(_textField.text);
							break;
						
						case 'phoneTH' : 
							this._validate = this.checkPhoneTH(_textField.text);
							break;
					}
				} else {
					this._validate = true;
				}
			
			}
			
			
			
			if (!this._validate) {
				 _textField.setTextFormat(_errorFormat);
				 
				if (!tooltip) { 
					tooltip = new UITooltip();
					if (_tooltipTextFormatDict) tooltip.textFormatDict = _tooltipTextFormatDict; 
				}
				tooltip.element = this;
				tooltip.show(this._error_message);
				
			} else {
				_textField.setTextFormat(_textFormat);
				
				if (tooltip) tooltip.hide();
			}
			
			return this._validate;
		}
		
		
		private function checkName(txt:String):Boolean
		{
			var nameEx:RegExp = /^([a-zA-Zก-๙])([ \u00c0-\u01ffa-zA-Z']){2,40}+$/;
			return nameEx.test(txt);
		}
		
		private function checkEmail ( str:String ):Boolean
		{
			
			  var regExpPattern : RegExp = /^[0-9a-zA-Z][-._a-zA-Z0-9]*@([0-9a-zA-Z][-._0-9a-zA-Z]*\.)+[a-zA-Z]{2,4}$/;	
			 // trace("checkEmail " + str + " result = " + str.match(regExpPattern));
			  return (str.match(regExpPattern) == null) ? false : true;
		}
		
		private function checkPhoneTH (str:String):Boolean
		{
			var regExpPattern : RegExp = /^((\+66?)|(0))(\s*[-\/\.]?)?((((8)|(9))(\s*[-\/\.]?)?\d{1})|(2))(\s*[-\/\.]?)?(\d{3})(\s*[-\/\.]?)?(\d{4})$/;
			return ((str.match(regExpPattern) == null) && (str.length > 8)) ? false : true;

		}
		
		
		public function get error_message():String
		{
			return this._error_message;
		}
		
		private function configureTextFormat():void
		{
			
			if (_placeHolder) {
				if (_textFormatDict && _textFormatDict["placeholder-" + lang]) {
					_placeHolder.setTextFormat(_textFormatDict["placeholder-" + lang]);
				}
				_placeHolder.x = this._textPadding;
				switch (_textVerticalAlign) {
					
					case 'middle' : 
						_placeHolder.y = Math.round((_placeHolder.height - _placeHolder.textHeight) / 2); 
						_textField.y = Math.round((_textField.height - _textField.textHeight) / 2);
						break;
					case 'top' : 
						_placeHolder.y = this._textPadding - 4;
						_textField.y = this._textPadding;
						
					
						
					
				}
				
				_placeHolderOrignialY = _placeHolder.y;
				
			}
			_textField.setTextFormat(_textFormat);
			//_textField.y = Math.round((_textField.height - _textField.textHeight) / 2);
			
			_textFieldOriginalY = _textField.y;
			
			
			if (lang && lang == 'th') {
				_placeHolder.y = _placeHolderOriginalY - .05 * _placeHolder.textHeight;
				_textField.y = _textFieldOriginalY -.05 * _textField.textHeight;
			} else {
				//_placeHolder.y = _placeHolderOriginalY;
				//_textField.y = _textFieldOriginalY;
			}
			
			
			
		}


		private function onTouchDownHandler(e:Event):void
		{
			this.stage.addEventListener("mouseUp", onTouchUpHandler, false, 0, true);
		}
		
		public function onTouchUpHandler(e:Event = null):void
		{
			if (!this.focus)  {
				this.focus = true;
				dispatchEvent(focusEvent);
			} else {
				//this.focus = false;
				//dispatchEvent(focusOutEvent);
			}
			if (_keyboard) this._keyboard.mode = this._keyboardMode;
			this.stage.removeEventListener("mouseUp", onTouchUpHandler);
			
			if (this._keyboard && this._keyboard.hidden) this._keyboard.showMe(this._frame);
		}
		
		override public function setFocusState():void
		{
			this.filters = [glow, onFocusDropShadow];
			
			configureTextFormat();
			
			this.stage.focus = _textField;
			_textField.setSelection(_textField.text.length,_textField.text.length);
		}
		
		override public function setFocusOutState():void
		{
			this.filters = [];
		}
		
		
		private function onKeyPressed(touchScreenEvent:TouchScreenKeyboardEvent):void
		{
			if (this.focus) {
				
				switch (touchScreenEvent.keyID) {
					case "backspace": 
					case "delete" : 
						deleteText(); break;
					
					
					case "submit" :
						this._keyboard.hideMe();
						this.focus = false;
						
						dispatchEvent(submitEvent);
						break;
						
					default: appendText(touchScreenEvent.keyValue); break;
				}
				
			}
		}
		
		public function set value(theValue:String):void
		{
			_textField.text = theValue;
			configureTextFormat();
			onValueUpdate();
		}
		
		public function get value():String
		{
			return this._textField.text;
		}
		
		private function appendText(keyValue:String):void
		{
			
			var initialText:String = _textField.text;
			var ci:int = _textField.caretIndex;
			var newText:String = initialText.substring(0, ci) + keyValue + initialText.substring(ci, initialText.length);
			
			
			if (_capitalization) newText = upperCase(newText);
			
			_textField.text = newText;
			_textField.setSelection(ci+(keyValue.length), ci+(keyValue.length));
			//num++;
			this.stage.focus = _textField;
			
			
			
			onValueUpdate();
			
			//updateCursorPosition();
		}
		
		private function deleteText():void
		{
			var initialText:String = _textField.text;
			var ci:int = _textField.caretIndex;
			if (_textField.text != "") {
				var newText:String = initialText.substring(0, ci-1) + initialText.substring(ci, initialText.length);
				_textField.text = newText;
				_textField.setSelection(ci-1, ci-1);
			}
			this.stage.focus = _textField;
			
			onValueUpdate();
			
			
		}
		
		function upperCase(str:String) : String {
			var firstChar:String = str.substr(0, 1);
			var restOfString:String = str.substr(1, str.length);
			return firstChar.toUpperCase()+restOfString.toLowerCase();
		}
		
		private function onValueUpdate():void
		{
			if (_textField.text.length == 0) {
				_placeHolder.visible = true;
			} else {
				_placeHolder.visible = false;
			}
			
			validateField();
			
			configureTextFormat();
			
			dispatchEvent(valueChangedEvent);
		}
		
		override public function translate():void
		{
			
			if (this.lang) {
				//Placeholder
				this._placeHolder.text = this._placeholderDict[this.lang];
				if (_textFormatDict["placeholder-"+lang]) this._placeHolder.setTextFormat(_textFormatDict["placeholder-" + lang]);

				
			}
			
			onTranslated();
		}
		
		public function onTranslated():void
		{
			
		}
		

	}
	
}
