﻿package com.pvm.ui {
	
	import flash.events.Event;
	import com.greensock.TweenLite;
	
	public class AdvanceInteractiveItem extends InteractiveItem {

		public var isShow:Boolean = true;
		
		public function AdvanceInteractiveItem() {
			// constructor code
		}
		
		public function showMe(evt:Event):void
		{
			isShow = true;
			this.visible = true;
			TweenLite.to(this, 0.5, {alpha:1});
		}
		
		public function hideMe(evt:Event):void
		{
			
			isShow = false;
			TweenLite.to(this, 0.5, {alpha:0, onComplete:devisibleMe});
		}
		
		private function devisibleMe():void
		{
			this.visible = false;
		}
		

	}
	
}
