﻿package com.forviz.ui {
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.events.Event;
	
	import flash.geom.Matrix;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	import org.gestouch.gestures.TransformGesture;
	import org.gestouch.gestures.SwipeGesture;
	import org.gestouch.gestures.SwipeGestureDirection;
	
	import com.greensock.TweenMax;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.TransformMatrixPlugin;
	
	import com.greensock.easing.Expo;
	
	public class UIScrollView extends UIView {
		
		TweenPlugin.activate([TransformMatrixPlugin]);

		
		public static const ZERO_POINT : Point = new Point(0, 0);
		private var _frameRatio:Number;
		
		protected var _content:Sprite = new Sprite();
		private var _contentFrame:Rectangle;
		private var _contentSize:CGSize;
		private var _touchArea:Sprite;
		
		
		/* Options */
		private var _disabled:Boolean = false;
		private var _scrollEnabled:Boolean = false;
		private var _pagingEnabled:Boolean = false;
		private var _allowBouncing:Boolean = true;
		
		private var _directionalLockEnabled:Boolean = true;
		private var _lockScrollHorizontal:Boolean = false;
		private var _lockScrollVertical:Boolean = false;
		
		private var _zoomMaxBouncing:Boolean = true;
		private var _zoomMinBouncing:Boolean = true;
		
		
		/* Bound */
		private var _topBound:Number = 0;
		private var _bottomBound:Number = 0;
		private var _leftBound:Number = 0;
		private var _rightBound:Number = 0;
		
		private var _totalPageX:int = 0;
		private var _totalPageY:int = 0;
		private var _currentPageX:int = 0;
		private var _currentPageY:int = 0;
		private var _targetPageX:int = 0;
		private var _targetPageY:int = 0;
		
		private var _horizontalAlign:String = "center";
		private var _verticalAlign:String = "middle";
		
		
		private var _mouseDown:Boolean = false;
		private var _pointDown:Point = new Point();
		private var _pointMove:Point = new Point();
		private var _pointUp:Point = new Point();
		private var _pointTarget:Point = new Point();
		private var _pointOffset:Point = new Point();
		private var _pointHistory:Array = new Array();
		private var _forceMovePointTarget:Point = new Point();
		private var _forceMove:Boolean = false;
		
		private var _contentPoint:Point = new Point();
		
		
		private var _easingPower:Number = 3;
		private var _easingHolder:Number = 3;
		
		//While Moving
		protected var scrollViewDidScrollEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewDidScroll");
		protected var scrollViewIsDraggingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewIsDragging");
		
		protected var scrollViewWillBeginDraggingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewWillBeginDragging");
		protected var scrollViewWillEndDraggingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewWillEndDragging");
		
		protected var scrollViewWillBeginDeceleratingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewWillBeginDecelerating");
		
		//Zooming
		protected var scrollViewDidZoomEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewDidZoom");
		
		//Finish Moving
		protected var scrollViewDidEndDeceleratingEvent:UIScrollViewEvent = new UIScrollViewEvent("scrollViewDidEndDecelerating");
		
		protected var freeTransform:TransformGesture = new TransformGesture(this);
		protected var swipe:SwipeGesture;
		
		public var image:UIImageView;
		
		private var _enableZooming:Boolean = false;
		protected var _minimumScale:Number = -1;
		protected var _maximumScale:Number = -1;		
		protected var _currentScale:Number = 1;
		
		private var _keepCenter:Boolean = true;
		
		private var _treshold:Number = 100;

		private var orgMatrix:Matrix;
		
		var _mask:Sprite = new Sprite();
		
		public function UIScrollView(frame:Rectangle = null, options:Object = null) {
			// constructor code
			super(frame);
			if (frame) _frameRatio = frame.width / frame.height;
			//if (frame) this.frame = frame;
			
			_content = this.contentContainer;
			
			orgMatrix = _content.transform.matrix;
			
			for (var i:int = 0;i<4;i++) {
				_pointHistory[i] = new Point();
			}
			
			if (options) {
				if (options.hasOwnProperty('allowBouncing')) this._allowBouncing = options.allowBouncing;
			}
			
			

		}
		
		public function set disabled(value:Boolean):void
		{
			this._disabled = value;
		}
		public function set enableZooming(value:Boolean):void
		{
			this._enableZooming = value;
			configureListeners();
		}
		
		public function set zoomMaxBouncing(value:Boolean):void
		{
			this._zoomMaxBouncing = value;
		}
		
		public function configureListeners():void
		{
			if (this._scrollEnabled) {
				if (_enableZooming) {
					
					//Can't use paging and zoom at the same time.
					if (this._pagingEnabled) this.pagingEnabled = false;
					
					
					freeTransform.addEventListener(GestureEvent.GESTURE_BEGAN, onGestureBegan);
					freeTransform.addEventListener(GestureEvent.GESTURE_CHANGED, onFreeTransform);
					freeTransform.addEventListener(GestureEvent.GESTURE_ENDED, onGestureEnded);
					
					this.removeEventListener(MouseEvent.MOUSE_DOWN, _onMouseDownHandler);
					this.addEventListener(MouseEvent.MOUSE_DOWN, stopMouseDownPropagation);
					
				} else {
					freeTransform.removeEventListener(GestureEvent.GESTURE_BEGAN, onGestureBegan);
					freeTransform.removeEventListener(GestureEvent.GESTURE_CHANGED, onFreeTransform);
					freeTransform.removeEventListener(GestureEvent.GESTURE_ENDED, onGestureEnded);
					
					this.removeEventListener(MouseEvent.MOUSE_DOWN, stopMouseDownPropagation);
					this.addEventListener(MouseEvent.MOUSE_DOWN, _onMouseDownHandler);
				}
			} else {
				removeListeners();
			}
		}
		
		public function removeListeners():void
		{
			this.removeEventListener(MouseEvent.MOUSE_DOWN, _onMouseDownHandler);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, stopMouseDownPropagation);
			freeTransform.removeEventListener(GestureEvent.GESTURE_BEGAN, onGestureBegan);
			freeTransform.removeEventListener(GestureEvent.GESTURE_CHANGED, onFreeTransform);
			freeTransform.removeEventListener(GestureEvent.GESTURE_ENDED, onGestureEnded);
			
			this.removeEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
			this._mouseDown = false;
				
		}
		
		public function viewForZoomingInScrollView():DisplayObject
		{
			return this.contentContainer;
			//return this.viewContainer;
		}
		
		public function set minimumScale(value:Number):void
		{
			if (this._maximumScale != -1 && value > this._maximumScale) 
				throw new ReferenceError("Error: Minimum scale can't be larger than maximumScale");
			else 
				this._minimumScale = value;
		}
		
		public function get minimumScale():Number { return this._minimumScale; }
		
		public function set maximumScale(value:Number):void
		{
			if (this._minimumScale != -1 && value < this._minimumScale) 
				throw new ReferenceError("Error: Maximum scale can't be smaller than minimumScale");
			else 
				this._maximumScale = value;
			
		}
		
		public function get maximumScale():Number { return this._maximumScale; }

		
		public function setZoomScale(theScale:Number, animated:Boolean = false):void
		{
			/*
			var matrix:Matrix = _content.transform.matrix;
			matrix.scale(theScale, theScale);
			_content.transform.matrix = matrix;
			
			this._currentScale = theScale;*/
			var newMatrix:Matrix = orgMatrix.clone();
			newMatrix.scale(theScale, theScale);
				
			if (false && !animated) {
				
				//ปิดการใช้งานชั่วคราวจนกว่าจะเข้าใจว่า ทำไมไม่ Work วะ
				_content.transform.matrix = newMatrix;
				this._currentScale = theScale;
			
				fixBound(true);
			} else {
				
				//ทำไมอันนี้ Work
				TweenMax.to(_content, 0.4, {transformMatrix:{a:newMatrix.a, b:newMatrix.b, 
															 c:newMatrix.c, d:newMatrix.d, 
															 tx:newMatrix.tx, ty:newMatrix.ty}});
				this._currentScale = theScale;
			}
			
		}
		
		private function setScroll():void
		{
			
			_topBound = 0;
			_bottomBound = frame.height - contentFrame.height; 
			_leftBound = 0;
			_rightBound = frame.width - contentFrame.width;
			
			_totalPageX = Math.ceil(contentFrame.width / frame.width);
			_totalPageY = Math.ceil(contentFrame.height / frame.height);
			
			/*
			if (_directionalLockEnabled) {
				if (frame.width > contentFrame.width && frame.height > contentFrame.height) {
					_directionalLockEnabled = false;
				} else if (frame.width > contentFrame.width) {
					_lockScrollHorizontal = true;
				} else if (frame.height > contentFrame.height) {
					_lockScrollVertical = true;
				}
			} else {
				
			}
			*/
			
			
			if (frame.width < contentFrame.width || frame.height < contentFrame.height)
			{
				this._scrollEnabled = true;
				//Maybe I should move this addListener to when page is loaded instead, save resources.
				
				
				configureListeners();

			} else {
				this._scrollEnabled = false;
				removeListeners();
				
				this.moveTo(new Point(0, 0), false);
				
			}
			
		}
		
		public function set lockScrollHorizontal(value:Boolean):void
		{
			this._lockScrollHorizontal = value;
		}
		
		public function set lockScrollVertical(value:Boolean):void
		{
			this._lockScrollVertical = value;
		}
		
		public function get currentPageX():int {
			return this._currentPageX;
		}

		public function get currentPageY():int {
			return this._currentPageY;
		}
		
		public function get totalPageX():int {
			return this._totalPageX;
		}

		public function get totalPageY():int {
			return this._totalPageY;
		}
		
		protected function stopMouseDownPropagation(evt:Event):void
		{
			evt.stopPropagation();
		}

		protected function onGestureBegan(event:GestureEvent):void
		{
			if (_disabled) return;
			
			//trace("gesture Begin");
			const gesture:TransformGesture = event.target as TransformGesture;
			
			_pointDown = new Point(gesture.offsetX, gesture.offsetY);
			onFreeTransform(event);
			
			dispatchEvent(scrollViewWillBeginDraggingEvent);
			
			event.stopPropagation();
		}
		
		
		protected function onFreeTransform(event:GestureEvent):void
		{
			if (_disabled) return;
			
			const gesture:TransformGesture = event.target as TransformGesture;
			var matrix:Matrix = _content.transform.matrix;
			
			//trace(gesture.offsetX, gesture.offsetY, _content);
			// Panning
			matrix.translate(gesture.offsetX, gesture.offsetY);
			_content.transform.matrix = matrix;
			
			dispatchEvent(scrollViewIsDraggingEvent);
			
			
			if (!_zoomMaxBouncing && _content.scaleX > this._maximumScale) {
				event.stopPropagation();
				return;
			}
			
			if (!_zoomMinBouncing && _content.scaleX < this._minimumScale) {
				event.stopPropagation();
				return;
			}
			
			if ((gesture.scale != 1  && gesture.scale != 0 && 
				 gesture.scale != Number.NEGATIVE_INFINITY && 
				 gesture.scale != Number.POSITIVE_INFINITY) || gesture.rotation != 0)
			{
				// Scale and rotation.
				var transformPoint:Point = matrix.transformPoint(_content.globalToLocal(gesture.location));
				matrix.translate(-transformPoint.x, -transformPoint.y);
				//matrix.rotate(gesture.rotation);
				matrix.scale(gesture.scale, gesture.scale);
				matrix.translate(transformPoint.x, transformPoint.y);
				
				dispatchEvent(scrollViewDidZoomEvent);
				
				_content.transform.matrix = matrix;
				
			} else {
				//if (gesture.scale != 1)	trace("gesturescale = " + gesture.scale);
			}
			
			
			if (isNaN(_content.scaleX) || _content.scaleX == 0 || _content.scaleX == Number.NEGATIVE_INFINITY || _content.scaleX == Number.POSITIVE_INFINITY) {
				_content.width = this._contentSize.width * _currentScale;
				_content.height = this._contentSize.height * _currentScale;
			} else {
				_currentScale = _content.scaleX;
			}
			
			scrollViewDidScrollEvent.currentScale = this.currentScale;
			dispatchEvent(scrollViewDidScrollEvent);
			
			event.stopPropagation();
			
			
		}
		
		private function onGestureEnded(event:GestureEvent):void
		{
			if (_disabled) return;
			
			
			const gesture:TransformGesture = event.target as TransformGesture;
			_pointUp = new Point(gesture.offsetX, gesture.offsetY);
			
			fixBound();
			
			
		}
		
		public function fixBound(initState:Boolean = false):void
		{
			var contentBound:Rectangle = new Rectangle(_content.x, _content.y, _content.width, _content.height);
			var targetX:Number = _content.x;
			var targetY:Number = _content.y;
			var targetScale:Number = _content.scaleX;
			
			//trace(contentBound);
			//trace("targetX0 = ", targetX);
			
			
			if (_minimumScale != -1 && targetScale < this._minimumScale) {
				
				targetX = targetX - 0.5 * contentBound.width * ((_minimumScale/targetScale) - 1);
				targetY = targetY - 0.5 * contentBound.height * ((_minimumScale/targetScale) - 1);
				targetScale = this._minimumScale;
				
			}else if (_maximumScale != -1 && targetScale > this._maximumScale) {
				
				targetX = targetX - 0.5 * contentBound.width * ((_maximumScale/targetScale) - 1);
				targetY = targetY - 0.5 * contentBound.height * ((_maximumScale/targetScale) - 1);
				targetScale = this._maximumScale;
			}
			
			//trace("targetX1 = ", targetX);
			
			
			//If Paging 
			if (this._pagingEnabled) {
				/*
				if (frame.width < contentFrame.width) {
							
					if (_pointUp.x - _pointDown.x < -100) _targetPageX = _currentPageX+1; 
					else if (_pointUp.x - _pointDown.x > 100) _targetPageX = _currentPageX-1;
					else _targetPageX = _currentPageX;
				} 
				
				if (frame.height < contentFrame.height){
					if (_pointUp.y - _pointDown.y < -100) _targetPageY = _currentPageY+1;
					else if (_pointUp.y - _pointDown.y > 100) _targetPageY = _currentPageY-1;
					else _targetPageY = _currentPageY;
				}
				
				if (_targetPageX <= 0) _targetPageX = 0;
				if (_targetPageX >= _totalPageX) _targetPageX = _totalPageX - 1;
				
				if (_targetPageY <= 0) _targetPageY = 0;
				if (_targetPageY >= _totalPageY) _targetPageY = _totalPageY - 1;
				
				
				targetX = _targetPageX * -frame.width;
				targetY = _targetPageY * -frame.height;
				
				_currentPageX = _targetPageX;
				_currentPageY = _targetPageY;
				*/
			}
			
			
			var targetWidth:Number = _contentSize.width * targetScale;
			var targetHeight:Number = _contentSize.height * targetScale;
			//trace("targetWidth", targetWidth, " : frame.width ", frame.width);
			if (targetWidth > frame.width ) {
				//trace("If content Width is bigger than frame", targetWidth, frame.width);
				if (initState && _horizontalAlign == "center") { 
					targetX = 0.5 * (frame.width - targetWidth);
				} else {
					if (targetX > 0) targetX = 0;
					if (targetX + targetWidth < this.frame.right) targetX = frame.width - targetWidth;
				}
			} else {
				//If content Width is smaller than frame
				
				if (_keepCenter) {
					
					targetX = 0.5 * (frame.width - targetWidth);
				}else {
					//if (contentBound.left < frame.left) targetX = frame.left;
					//if (contentBound.right > this.frame.right) targetX = frame.left + frame.width - contentBound.width;
				}

			}
			
			//trace("targetX3 = ", targetX);
			
			if (targetHeight > frame.height ) {
				//If content Height is bigger than frame
				if (initState && _verticalAlign == "middle" ) {
					targetY = 0.5 * (frame.height - targetHeight);
				} else {
					if (targetY > 0) targetY = 0;
					if (targetY + targetHeight < this.frame.bottom) targetY = frame.height - targetHeight;
				}
			} else {
				//If content Width is smaller than frame
				
				if (_keepCenter) {
					targetY = 0.6 * (frame.height - targetHeight);
				}else {
					//if (contentBound.top < 0) targetY = frame.top;
					//if (contentBound.bottom > this.frame.bottom) targetY = frame.top + frame.height - contentBound.height;
				}

			}
			
			//trace("fixBound targetX", targetX, "  :: targetY ", targetY);
			
			TweenMax.to(_content, 0.8, {x:targetX, y:targetY, scaleX:targetScale, scaleY:targetScale, ease:Expo.easeOut, 
										onUpdateParams : [this, _content],
										onUpdate: function(uiScrollView:UIScrollView, _srcContent:Sprite){
											uiScrollView._currentScale = _srcContent.scaleX;
											dispatchEvent(scrollViewDidZoomEvent);
											dispatchEvent(scrollViewDidScrollEvent);
										}
								});
			contentBound = null;
		}
		
		public function reset():void
		{
			_content.x = 0;
			_content.y = 0;
			_content.scaleX = _content.scaleY = 1;
		}
		
		
		public function moveTo (point:Point, animated:Boolean = true):void
		{
			if (_contentSize) {
				if (-point.x <  _contentSize.width && -point.y < _contentSize.height) {
					if (animated) {
						_forceMovePointTarget = point;
						_forceMove = true;
						this.addEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
					} else {
						_content.x = point.x;
						_content.y = point.y;
					}
				}
			}
			this._currentPageX = -point.x / this.frame.width;
			this._currentPageY = -point.y / this.frame.height;
			
		}
		
		
		public function set contentFrame(theFrame:Rectangle):void
		{
			this._contentFrame = theFrame;
			this._contentSize = new CGSize(theFrame.width, theFrame.height);
			
			if (!_touchArea) {
				_touchArea = new Sprite();
				this.addChildAt(_touchArea, 0);
			}
			_touchArea.graphics.clear();
			_touchArea.graphics.beginFill(0xff0000, 0);
			_touchArea.graphics.drawRect(0, 0, _contentFrame.width, _contentFrame.height);
			_touchArea.graphics.endFill();
			
			setScroll();
		}
		
		public function set contentSize(theSize:CGSize):void
		{
			this._contentFrame = new Rectangle(0, 0, theSize.width, theSize.height);
			this._contentSize = theSize;
			
			if (!_touchArea) {
				_touchArea = new Sprite();
				this.addChildAt(_touchArea, 0);
			}
			_touchArea.graphics.clear();
			_touchArea.graphics.beginFill(0xff0000, 0);
			_touchArea.graphics.drawRect(0, 0, theSize.width, theSize.height);
			_touchArea.graphics.endFill();
			
			setScroll();
		}
		
		
		public function setViewFrame(rect:Rectangle, expand:Number = 0, _time:Number = 0.5):void
		{
			if (expand) rect.inflate(expand, expand);
			//trace("setViewFrame", rect);
			
			if (rect.width > _content.width || rect.height > _content.height) {
				rect = new Rectangle(0, 0, _content.width, _content.height);
			}
			
			var _rectRatio:Number = rect.width / rect.height;
			
			//trace("rectRatio", _rectRatio, " : frameRatio", _frameRatio);
			//Move Content
			var _scale:Number = (_rectRatio < _frameRatio) ? frame.height / rect.height : frame.width / rect.width;
			//trace("scale", _scale);
			var marginX:Number = 0.5 * (frame.width - rect.width * _scale);
			var marginY:Number = 0.5 * (frame.height - rect.height * _scale);
			var tx:Number = -rect.x * _scale + marginX;
			var ty:Number = -rect.y * _scale + marginY;
		
			
			this._currentScale = _scale; 
			
			if (_time > 0) {
				TweenMax.to(_content, _time, {x:tx, y:ty, scaleX:_scale, scaleY:_scale, onUpdate:onUpdateHandler, onComplete:onSetFrameComplete, ease:Expo.easeInOut});
			} else {
				_content.x = tx;
				_content.y = ty;
				_content.scaleX = _scale;
				_content.scaleY = _scale;
				onSetFrameComplete();
				
			}
		}
		
		private function onUpdateHandler():void
		{
			configureScale();
			
		}
		
		public function onSetFrameComplete():void{ }
		
		
		private function configureScale():void
		{
			this._currentScale = _content.scaleX;
			
			/*
			for (var i:int=0;i<this._markerArray.length;i++)
			{
				_markerArray[i].scale = 1/this._contentScale;
				
			}*/
		}
		
		public function get content():Sprite
		{
			return this._content;
		}
		
		public function get contentOffset():Point
		{
			return new Point(this._content.x, this._content.y);
		}
		
		public function get contentSize():CGSize
		{
			return this._contentSize;
		}
		
		public function get contentFrame():Rectangle
		{
			return this._contentFrame;
		}
		
		public function get currentScale():Number
		{
			return this._currentScale;
		}
		public function set pagingEnabled(theValue:Boolean):void
		{
			this._pagingEnabled = theValue;
		}
		
		public function set scrollEnabled (theValue:Boolean):void
		{
			this._scrollEnabled = theValue;
			configureListeners();

		}
		
		public function set allowBouncing(theValue:Boolean):void
		{
			this._allowBouncing = theValue;
		}
		
		
		
		
		
		
		private function _onMouseDownHandler(evt:MouseEvent):void
		{
			if (_disabled) return;
			
			if (this._scrollEnabled) {
				this._mouseDown = true;
				
				_pointDown.x = mouseX;//evt.stageX;
				_pointDown.y = mouseY;//evt.stageY;
				
				
				_pointTarget = _pointDown.clone();
				_pointUp = _pointDown.clone();
				
				
				_pointOffset.x = _content.x - mouseX;
				_pointOffset.y = _content.y - mouseY;
				
				_pointHistory[3] = _pointHistory[2] = _pointHistory[1] = _pointHistory[0] = _pointDown;
				
				
				
				
				this.addEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
				
				dispatchEvent(scrollViewWillBeginDraggingEvent);
			}
			
			this.stage.addEventListener(MouseEvent.MOUSE_UP, _onMouseUpHandler);
			
			evt.stopPropagation();
			
		}
		
		
		
		private function _onMouseUpHandler(evt:MouseEvent):void
		{
			if (_disabled) return;
			
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, _onMouseUpHandler);
				
			if (this._scrollEnabled) {
				dispatchEvent(scrollViewWillEndDraggingEvent);
				
				
				this._mouseDown = false;
				
				_pointUp.x = mouseX;//evt.stageX;
				_pointUp.y = mouseY;//evt.stageY;
				
				 
				if (!_pagingEnabled) {
					var veloX:Number = _pointUp.x - _pointHistory[3].x;
					var veloY:Number = _pointUp.y - _pointHistory[3].y;
					_pointTarget.x = _content.x + veloX * _easingPower;
					_pointTarget.y = _content.y + veloY * _easingPower;
					
					
				} else {
					
					if (frame.width < contentFrame.width) {
						
						if (_pointUp.x - _pointDown.x < -_treshold) {
							_targetPageX = _currentPageX + 1;
							
						}else if (_pointUp.x - _pointDown.x > _treshold) {
							_targetPageX = _currentPageX-1;
							
						}else {
							_targetPageX = _currentPageX;
						}
					} 
					
					if (frame.height < contentFrame.height){
						if (_pointUp.y - _pointDown.y < -_treshold) _targetPageY = _currentPageY+1;
						else if (_pointUp.y - _pointDown.y > _treshold) _targetPageY = _currentPageY-1;
						else _targetPageY = _currentPageY;
					}
					
					if (_targetPageX <= 0) _targetPageX = 0;
					if (_targetPageX >= _totalPageX) _targetPageX = _totalPageX - 1;
					
					if (_targetPageY <= 0) _targetPageY = 0;
					if (_targetPageY >= _totalPageY) _targetPageY = _totalPageY - 1;
					
					
					_pointTarget.x = _targetPageX * -frame.width;
					_pointTarget.y = _targetPageY * -frame.height;
					
					_currentPageX = _targetPageX;
					_currentPageY = _targetPageY;
					
				}
			
			
				dispatchEvent(scrollViewWillBeginDeceleratingEvent);
			}
			
			
			evt.stopPropagation();
			
			
		}
		
		private function _onEnterFrameHandler(evt:Event):void
		{
			
			if (this._mouseDown) {
				
				_pointTarget.x = mouseX + _pointOffset.x;
				_pointTarget.y = mouseY + _pointOffset.y;
				
				if (!_allowBouncing || this.frame.width >= this._contentSize.width) {
					if (_pointTarget.x > _leftBound) _pointTarget.x = _leftBound;
					if (_pointTarget.x < _rightBound) _pointTarget.x = _rightBound;
				}
				
				if (!_allowBouncing || this.frame.height >= this._contentSize.height) {
					if (_pointTarget.y > _topBound) _pointTarget.y = _topBound;
					if (_pointTarget.y < _bottomBound) _pointTarget.y = _bottomBound;
				}
				
				_pointHistory[3] = _pointHistory[2];
				_pointHistory[2] = _pointHistory[1];
				_pointHistory[1] = _pointHistory[0];
				_pointHistory[0] = new Point(mouseX, mouseY);
				
				dispatchEvent(scrollViewIsDraggingEvent);
			
			} else {
				
				if (!_pagingEnabled) {
					//Check Bound
					if (_pointTarget.x > _leftBound) _pointTarget.x = _leftBound;
					if (_pointTarget.x < _rightBound) _pointTarget.x = _rightBound;
					if (_pointTarget.y > _topBound) _pointTarget.y = _topBound;
					if (_pointTarget.y < _bottomBound) _pointTarget.y = _bottomBound;
					
				} else {
					
				}
				
			}
			
			if (_forceMove) {
				_pointTarget = _forceMovePointTarget;
			}
			
			//trace("_poinTarget " + _pointTarget);
			if (!_lockScrollHorizontal) _content.x += (_pointTarget.x - _content.x) / _easingHolder;
			if (!_lockScrollVertical) _content.y += (_pointTarget.y - _content.y) / _easingHolder;
			
			dispatchEvent(scrollViewDidScrollEvent);
			
			_contentPoint.x = _content.x;
			_contentPoint.y = _content.y;
			
			_currentPageX = Math.round(-_content.x / frame.width);
			_currentPageY = Math.round(-_content.y / frame.height);
			
			
			if (!this._mouseDown && Point.distance(_contentPoint, _pointTarget) < 1) {
				
				_forceMove = false;
				
				this.removeEventListener(Event.ENTER_FRAME, _onEnterFrameHandler);
				dispatchEvent(scrollViewDidEndDeceleratingEvent);
			}
			
		}
		
		public function get contentPoint():Point
		{
			return this._contentPoint;
		}
		
		
		private function set page(pageIndex:int):void
		{
			
		}
		
		
		var _testFrame:Rectangle;
		public function pointInViewport(point:Point, contract:Number = 0):Boolean
		{
			_testFrame = this.frame.clone();
			
			_testFrame.inflate(contract, contract);
			return _testFrame.containsPoint( this.localToLocal(_content, this, point) );
			
		}
		
		public function getAngle(fromPoint:Point, toPoint:Point):Number
		{
			//this.localToLocal(this.viewContainer, this, toPoint)
			return 0;
		}
		
		public function localToLocal(containerFrom:DisplayObject, containerTo:DisplayObject, origin:Point=null):Point
		{
			var point:Point = origin ? origin : new Point();
			point = containerFrom.localToGlobal(point);
			point = containerTo.globalToLocal(point);
			return point;
		}
		

	}
	
}
