﻿/* 
 * UIScrollViewEvent version 1.0
 * By Pitipat Srichairat
 * Last updated Dec 10, 2012
 *******************************/
package com.forviz.ui {
	
	import flash.events.Event;
	
	public class UICarouselEvent extends Event{


		public static const DRAGGING_BEGIN : String 		= "carouselDraggingBegin";
		public static const DRAGGING : String 				= "carouselIsDragging";
		public static const DRAGGING_FINISH : String 		= "carouselDraggingFinish";
		
		public static const WILL_MOVE:String				= "carouselWillMove";
		public static const MOVING:String 					= "carouselIsMoving";
		public static const MOVED:String 					= "carouselDidMove";
		
		public var from:UICarousel;
		public var fromIndex:int = 0;
		public var toIndex:int = 0;
		
		
		public function UICarouselEvent(type:String, from:UICarousel = null) {
			// constructor code
			this.from = from;
			super(type);
		}

		public override function clone():Event 
		{
			var newEvt : UICarouselEvent = new UICarouselEvent ( type, from);
			newEvt.fromIndex = fromIndex;
			newEvt.toIndex = toIndex;
			
			newEvt.from = from;
			return newEvt;
		}

	}
	
}



